#include "Log.hpp"
#include <fstream>
#include <filesystem>
#include <ctime>
#include <algorithm>
#include <iterator>
#include <tuple>

std::string Log::fileDir;
std::string Log::fileName;
std::ostream* Log::outputStream;
std::ofstream Log::logFile;

bool Log::enableVerbose;
bool Log::fileLogging;
char Log::timeDateBuffer[128];
int  Log::maxLogs;
std::mutex Log::loggingSync;

Log::Log()
{
	
}

void Log::init(std::ostream* output, int maxFiles, bool verbose, std::string path)
{
	outputStream = output;

	fileLogging = true;
	enableVerbose = verbose;

#ifdef __linux__
	getTime("%d.%m.%Y %H:%M:%S");
#endif

#ifdef _WIN32
	getTime("%d.%m.%Y %H_%M_%S"); // No colons
#endif

	fileDir = path;
	fileName = path + "QUASAR_LOG " + timeDateBuffer;

	if (!std::filesystem::is_directory(path)) {
		std::filesystem::create_directory(path);
	}

	logFile.open(fileName, std::ios::app);

	if (!logFile.is_open()) {
		*outputStream << "Failed to open logging file: " << fileName << '\n';
	}

	logFile.close();

	// Delete old logs once the file limit is reached
	auto dirIterator = std::filesystem::directory_iterator(fileDir);
	std::vector < std::tuple<std::filesystem::file_time_type, std::string> > fileEntries;
	int numFiles = 0;

	/*Retrieve a list of all files in the log directory*/
	for (const std::filesystem::directory_entry& entry : dirIterator) {
		/*Ensure that only log files are added to the list*/
		if( std::string(entry.path().string()).find("QUASAR_LOG ") != std::string::npos){
			fileEntries.push_back(std::make_tuple(
					entry.last_write_time(), entry.path().string()));
			numFiles++;
		}
	}

	// Sort in order of date
	std::sort(fileEntries.begin(), fileEntries.end());

	for (int i = 0; i < numFiles - maxFiles; ++i){
		std::filesystem::remove( std::get<1>(fileEntries.at(i)) );
	}
}

void Log::getTime(const char* format)
{
	std::tm* tm;
	time_t t;
	time(&t);
	tm = ::localtime(&t);

	std::strftime(timeDateBuffer, 64, format, tm);
}

Log::~Log()
{
}

