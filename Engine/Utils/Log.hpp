#ifndef QML_QUASAR_LOG_H
#define QML_QUASAR_LOG_H

#include <cstdlib>
#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <tuple>
#include <sstream>
#include <mutex>
#include <streambuf>
#include "Utils/WindowsExportDLL.hpp"

class DLLEXP Log
{
private:
	/*TODO: Move to timer class*/
	static void getTime(const char* format);

public:
	Log();
	~Log();

	static void init(std::ostream* output, int maxFiles = 5, bool verbose = 1, 
			std::string path = "logs/");

	template <typename ... S>
	static void out(const S& ... s)
	{
		if (enableVerbose){
			printInternal(s...);
		}
	}
	
	template <typename ... S>
	static void error(const S& ... s)
	{
		printInternal("[Error] ", s...);
	}

	template <typename ... S>
	static void fatal(const S& ... s)
	{
		printInternal("[Fatal Error] ", s..., "\nExiting...");
		exit(EXIT_FAILURE);
	}

private:
	template<typename S>
	static void combineParameters(std::stringstream& output, const S& param)
	{
		output << param;
	}

	template <typename ... S>
	static void printInternal(const S& ... s)
	{
		std::lock_guard<std::mutex> lck(loggingSync);
		getTime("%H:%M:%S ");
		std::stringstream ss;
		(combineParameters<S>(ss, s), ...);
		std::string combinedParams = ss.str();

		*outputStream << combinedParams << '\n';
		logFile.open(fileName, std::ios::app);
		logFile << timeDateBuffer << combinedParams << '\n';
		logFile.close();
	}

	
	static void delOld();

	/*Only logs errors and warnings if disabled*/
	static bool enableVerbose;
	/*Where the log file will generate*/
	static std::string fileDir;
	static std::string fileName;
	/*Enable logging to a file*/
	static bool fileLogging;
	static int maxLogs;
	static char timeDateBuffer[128];
	static std::mutex loggingSync;

	static std::ostream* outputStream;
	static std::ofstream logFile;

};

#endif //QML_QUASAR_LOG_H

