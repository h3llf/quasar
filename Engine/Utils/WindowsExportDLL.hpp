#ifndef QUASAR_WINDOWS_EXPORT_DLL_H
#define QUASAR_WINDOWS_EXPORT_DLL_H

#ifdef _MSC_VER
#ifdef _QUASAR
#define DLLEXP __declspec(dllexport)
#else
#define DLLEXP __declspec(dllimport)
#endif //_QUASAR
#else
#define DLLEXP
#endif //_MSC_VER

#endif //QUASAR_WINDOWS_EXPORT_DLL_H
