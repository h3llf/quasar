#ifndef QML_QUATERNION_H
#define QML_QUATERNION_H

#include "Utils/Math/VectorFunctions.hpp"
#include "Vector.hpp"
#include <cmath>

namespace qml
{
	typedef vectorType<float, 4> quat;

	// https://stackoverflow.com/questions/1171849/finding-quaternion-representing-the-rotation-from-one-vector-to-another
	// Returns a quaternion rotation which rotates vector u onto v
	template <typename T>
	static vectorType<T, 4> getRotationBetween(vectorType<T, 3> u, vectorType<T, 3> v)
	{
		float kct = dot(u, v);
		float k = sqrt(u.magnitude() * v.magnitude());

		if (kct / k == -1) {
			return vectorType<T, 4>(u.normalize(), 0.0);
		}

		return vectorType<T, 4>(cross(u, v), kct + k).normalize();
	}	

	// https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
	template <typename T>
	static vectorType<T, 4> eulerToQuat(vectorType<T, 3> euler)
	{
    		T cx = cos(euler.x * 0.5);
    		T sx = sin(euler.x * 0.5);
		T cy = cos(euler.y * 0.5);
    		T sy = sin(euler.y * 0.5);
    		T cz = cos(euler.z * 0.5);
    		T sz = sin(euler.z * 0.5);

		vectorType<T, 4> q;
		q.w = cx * cy * cz + sx * sy * sz;
		q.x = sx * cy * cz - cx * sy * sz;
		q.y = cx * sy * cz + sx * cy * sz;
		q.z = cx * cy * sz - sx * sy * cz; 
		return q;
	}

	template <typename T>
	static vectorType<T, 3> quatToEuler(vectorType<T, 4> quat)
	{
		vectorType<T, 3> euler;

		T sxcz = 2.0 * (quat.w * quat.x + quat.y * quat.z);
		T cxcz = 1.0 - 2.0 * (quat.x * quat.x + quat.y * quat.y);
		euler.x = std::atan2(sxcz, cxcz);

		T sz = 2.0 * (quat.w * quat.y - quat.z * quat.x);
		if (std::abs(sz) >= 1.0) {
			euler.y = std::copysign(M_PI / 2.0, sz);
		} else {
			euler.y = std::asin(sz);
		}

		T sycz = 2.0 * (quat.w * quat.z + quat.x * quat.y);
		T cycz = 1.0 - 2.0 * (quat.y * quat.y + quat.z * quat.z);
		euler.z = std::atan2(sycz, cycz);

		return euler;

	}

	template <typename T>
	static vectorType<T, 3> rotateVec(vectorType<T, 3>& vec, vectorType<T, 4>& quat)
	{
		return 2.0 * dot(quat.xyz, vec) * quat.xyz + 
			(quat.w * quat.w - dot(quat.xyz, quat.xyz)) * vec +
			2.0 * quat.w * cross(quat.xyz, vec);
	}

}

#endif //QML_QUATERNION_H
