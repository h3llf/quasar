#ifndef QML_TRIG_H
#define QML_TRIG_H

#include <cmath>
#include <math.h>
#include "Vector.hpp"

#ifndef M_PI
#define M_PI 3.141592654f
#endif

namespace qml
{
	template <typename T>
	inline T radians (T degrees) { return (degrees / 360.0f) * 2 * M_PI; }

	template <typename T>
	inline T degrees (T radians) { return radians / (2 * M_PI) * 360.0f; }

	template <typename T, int size>
	vectorType<T, size> degToRad(vectorType<T, size>& vec)
	{
		vectorType<T, size> buffer;

		for (int i = 0; i < size; ++i) {
			buffer[i] = radians(vec[i]);
		}

		return buffer;
	}

	template <typename T, int size>
	vectorType<T, size> radToDeg(vectorType<T, size>& vec)
	{
		vectorType<T, size> buffer;

		for (int i = 0; i < size; ++i) {
			buffer[i] = degrees(vec[i]);
		}

		return buffer;
	}

	static double cotangent(double radians)
	{
		return 1.0 / tan(radians);
	}
};

#endif //QML_TRIG_H
