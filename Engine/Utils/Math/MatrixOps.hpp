#ifndef QML_MATRIX_OPS_H
#define QML_MATRIX_OPS_H

#include "Vector.hpp"
#include "VectorOps.hpp"
#include "Matrix.hpp"
#include "Trig.hpp"

#include <iostream>
namespace qml
{
	//Multiply
	template <typename T, int cSize1, int rSize1, int cSize2, int rSize2>
	matrixType<T, cSize1, rSize2> operator*(
			matrixType<T, cSize1, rSize1> mat1, 
			matrixType<T, cSize2, rSize2> mat2)
	{
		matrixType<T, cSize1, rSize2> buffer = {};

		//Iterate rows
		for (int i = 0; i < cSize1; ++i) {
			//Iterate columns
			for (int j = 0; j < cSize2; ++j) {
				for (int k = 0; k < rSize2; ++k) {
					buffer[j][i] += mat1[k][i] * mat2[j][k];
				}	
			}
		}

		return buffer;
	}

//	static_assert(rSize1 == cSize2, "Invalid matrix multiplication");

	// Scalar multiplication
	template <typename T, int cSize, int rSize>
	matrixType<T, cSize, rSize> operator*=(matrixType<T, cSize, rSize>& mat, T val)
	{
		const int count = cSize * rSize;
		for (int i = 0; i < count; ++i) {
			mat.dataArray[i] *= val;
		}

		return mat;
	}

	template <typename T, int cSize, int rSize>
	matrixType<T, cSize, rSize> operator/=(matrixType<T, cSize, rSize>& mat, T val)
	{
		const int count = cSize * rSize;
		for (int i = 0; i < count; ++i) {
			mat.dataArray[i] /= val;
		}

		return mat;
	}

	template <typename T, int cSize, int rSize>
	matrixType<T, cSize, rSize> operator*(matrixType<T, cSize, rSize>& mat, T val)
	{
		matrixType<T, cSize, rSize> buffer;

		const int count = cSize * rSize;
		for (int i = 0; i < count; ++i) {
			buffer.dataArray[i] = mat.dataArray[i] * val;
		}

		return buffer;
	}

	template <typename T, int cSize, int rSize>
	matrixType<T, cSize, rSize> operator/(matrixType<T, cSize, rSize>& mat, T val)
	{
		matrixType<T, cSize, rSize> buffer;

		const int count = cSize * rSize;
		for (int i = 0; i < count; ++i) {
			buffer.dataArray[i] = mat.dataArray[i] / val;
		}

		return buffer;
	}

	//Matrix vector multiplications
	template <typename T, int cSize, int rSize>
	vectorType<T, cSize> operator*(matrixType<T, cSize, rSize> mat, vectorType<T, rSize> vec)
	{
		vectorType<T, cSize> buffer {};

		for (int c = 0; c < cSize; ++c) {
			for (int r = 0; r < rSize; ++r) {
				buffer[c] += mat[r][c] * vec[r];
			}
		}

		return buffer;
	}

	//Calculate the determinant of a 2x2 matrix
	template <typename T>
	static T det(matrixType<T, 2, 2> mat)
	{
		return mat[0][0] * mat[1][1] - mat[1][0] * mat[0][1];
	}

	//Calculates the determinant of an nxn matrix
	template <typename T, int size>
	static T det(matrixType<T, size, size>& mat)
	{
		T sum = 0;

		for (int i = 0; i < size; ++i) {
			int currentColumn = i;

			T coefficient = mat[currentColumn][0];

			matrixType<T, size - 1, size - 1> subMatrix;
			int subIndex = 0;

			for (int c = 0; c < size; ++c) {
				if (c == currentColumn) continue;
				for (int r = 1; r < size; ++r) {
					subMatrix.dataArray[subIndex] = mat[c][r];
					subIndex++;
				}
			}

			if (currentColumn % 2)
				sum -= coefficient * det(subMatrix);
			else
				sum += coefficient * det(subMatrix);
		}

		return sum; 
	}

	template <typename T, int size>
	static matrixType<T, size, size> adj(matrixType<T, size, size>& mat)
	{
		matrixType<T, size, size> buffer;
		matrixType<T, size - 1, size - 1> cofactor;

		for (int c = 0; c < size; ++c) {
			float sign = static_cast<float>((c%2) * 2 - 1);
			for (int r = 0; r < size; ++r) {
				sign = -sign;

				for (int i = 1; i < size; ++i) {
					for (int j = 1; j < size; ++j) {
						cofactor[i-1][j-1] = mat[(r+i)%size][(c+j)%size];
					}
				}

				// Transpose of cofactor matrix
				buffer[c][r] = sign * det(cofactor);
			}
		}

		return buffer;
	}

	// Not optimized
	template <typename T, int size>
	static matrixType<T, size, size> invert(matrixType<T, size, size> mat)
	{
		T determinant = det(mat);
		matrixType<T, size, size> adjmat = adj(mat);

		return adjmat / determinant;
	}
	
	template <typename T, int cSize, int rSize>
	static matrixType<T, rSize, cSize> transpose(matrixType<T, cSize, rSize> mat)
	{
		matrixType<T, rSize, cSize> buffer;

		for (int c = 0; c < cSize; ++c) {
			for (int r = 0; r < rSize; ++r) {
				buffer[c][r] = mat[r][c];
			}
		}

		return buffer;
	}


	// TODO: Implement a far more optimized version
//	static matrixType<float, 4, 4> invert(matrixType<float, 4, 4> mat);
}

#endif //QML_MATRIX_OPS_H
