#ifndef QML_MATRIX_H
#define QML_MATRIX_H

#include <initializer_list>
#include <sstream>
#include "Vector.hpp"

#include <iostream>

#include <Utils/Log.hpp>
#include <string>

namespace qml 
{
	template <typename T, int rowCount, int columnCount>
	struct matrixType
	{
		union
		{
			T dataArray[rowCount * columnCount];
			vectorType<T, rowCount> data[columnCount];
		};

		matrixType()
		{
			clear();
		}

		matrixType(T fill)
		{
			clear();

			for (int i = 0; i < std::min(rowCount, columnCount); ++i) {
				data[i][i] = fill;
			}
		}

		matrixType(std::initializer_list<T> elements) 
		{
			const T* elementData = elements.begin();

			for (int i = 0; i < rowCount * columnCount; ++i) {
				dataArray[i] = elementData[i];
			}
		};

		void clear()
		{
			for (int i = 0; i < rowCount * columnCount; ++i)
			{
				dataArray[i] = 0;
			}		
		}

		static matrixType<T, rowCount, columnCount> identity()
		{
			matrixType<T, rowCount, columnCount> buffer(static_cast<T>(1));
			return buffer;
		}

		//Matricies are stored using column major ordering.
		//To index individual elements use [column][row]
		vectorType<T, rowCount>& operator[](int col)
		{
			return data[col];
		}
		
		//Returns the matrix with vectors oriented vertically
		std::string str(int precision = 0) {
			std::stringstream ss;
			if (precision) 
				ss.precision(precision);

			ss << "mat" << rowCount << 'x' << columnCount;

			int strLens[rowCount * columnCount] = {0};
			int maxLens[columnCount] = {0};
			int strLen = ss.str().length();
			int oldLen = strLen;

			for (int r = 0; r < rowCount; ++r) {
				strLens[r * columnCount] += 2;
				ss << "\n|";
				for (int c = 0; c < columnCount; ++c) {
					ss << data[c][r] << '|';

					//Measure length
					int currentLen = ss.str().length();
					strLens[r * columnCount + c] = currentLen - oldLen;
					maxLens[c] = std::max(maxLens[c], currentLen - oldLen);
					oldLen = currentLen;
				}
			}

			//Align columns
			std::string output = ss.str();
			for (int i = 0; i < rowCount * columnCount; ++i) {
				int numLen = strLens[i];
				int targetLen = maxLens[i % columnCount];
				
				for (int c = 0; c < targetLen - numLen; ++c) {
					output.insert(strLen + numLen - 1, 1, ' ');
				}

				strLen += targetLen;
			}
			
			return output;
		}
	};

	//Abbreviated types
	typedef matrixType<float, 4, 4> mat4;
	typedef matrixType<float, 3, 3> mat3;
	typedef matrixType<float, 2, 2> mat2;
	typedef matrixType<double, 4, 4> dmat4;
	typedef matrixType<double, 3, 3> dmat3;
	typedef matrixType<double, 2, 2> dmat2;
	typedef matrixType<float, 3, 4> mat3x4;
}

#endif //QML_MATRIX_H
