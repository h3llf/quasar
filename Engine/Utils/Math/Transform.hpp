#ifndef QML_TRANSFORM_HPP
#define QML_TRANSFORM_HPP

#include "Utils/Math/MatrixOps.hpp"
#include "Utils/Math/Quaternion.hpp"
#include "Vector.hpp"
#include "Matrix.hpp"

namespace qml
{	
	template <typename T>
	static void translate(matrixType<T, 4, 4>& mat, vectorType<T, 3>& translation)
	{
		mat.data[3][0] += translation[0];
		mat.data[3][1] += translation[1];
		mat.data[3][2] += translation[2];
	}

	template <typename T>
	static void scale(matrixType<T, 4, 4>& mat, vectorType<T, 3>& scale)
	{
		mat.data[0][0] *= scale[0];
		mat.data[1][1] *= scale[1];
		mat.data[2][2] *= scale[2];
	}

	// Rotate the vector V by quaternon Q
	template <typename T>
	static vectorType<T, 3> rotate(vectorType<T, 3>& v, vectorType<T, 4>& q)
	{
		vectorType<T, 3> uv = cross(q.xyz, v);
		vectorType<T, 3> uuv = cross(q.xyz, uv);

		return v + ((uv * q.w) + uuv) * static_cast<T>(2.0);
	}

	// Based on glm matrix_transform.inl
	template <typename T>
	static matrixType<T, 4, 4> rotate(matrixType<T, 4, 4>& mat, T radians, vectorType<T, 3>& axis)
	{
		T c = static_cast<T>(cos(radians));
		T s = static_cast<T>(sin(radians));

		matrixType<T, 4, 4> result;
		axis.toNormalized();

		result[0][0] = c + (1-c) * axis.x * axis.x;
		result[0][1] = (1 - c) * axis.x * axis.y + s * axis.z;
		result[0][2] = (1 - c) * axis.x * axis.z - s * axis.y;
		result[0][3] = 0;

		result[1][0] = (1 - c) * axis.y * axis.x - s * axis.z;
		result[1][1] = c + (1 - c) * axis.y * axis.y;
		result[1][2] = (1 - c) * axis.y * axis.z + s * axis.x;
		result[1][3] = 0;

		result[2][0] = (1 - c) * axis.z * axis.x + s * axis.y;
		result[2][1] = (1 - c) * axis.z * axis.y - s * axis.x;
		result[2][2] = c + (1 - c) * axis.z * axis.z;
		result[2][3] = 0;

		result[3] = vectorType<T, 4>(0, 0, 0, 1);
		return mat * result;
	}

	template <typename T>
	static matrixType<T, 4, 4> getRotationMat(vectorType<T,4>& q)
	{
		matrixType<T, 4, 4> m;

		m[0][0] = 2.0 * (q.w * q.w + q.x * q.x) - 1.0;
		m[0][1] = 2.0 * (q.x * q.y + q.w * q.z);
		m[0][2] = 2.0 * (q.x * q.z - q.w * q.y);

		m[1][0] = 2.0 * (q.x * q.y - q.w * q.z);
		m[1][1] = 2.0 * (q.w * q.w + q.y * q.y) - 1.0;
		m[1][2] = 2.0 * (q.y * q.z + q.w * q.x);

		m[2][0] = 2.0 * (q.x * q.z + q.w * q.y);
		m[2][1] = 2.0 * (q.y * q.z - q.w * q.x);
		m[2][2] = 2.0 * (q.w * q.w + q.z * q.z) - 1.0;

		m[3][3] = 1.0;

		return m;
	}


	// http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
	// TODO: Test and make sure the sign flip doesn't cause issues
	template <typename T>
	static vectorType<T, 4> getQuatFromRotMat(matrixType<T, 3, 3> m)
	{
		vectorType<T, 4> quat;

		quat.w = sqrt(std::max(0.0, 1.0 + m[0][0] + m[1][1] + m[2][2])) / 2.0;
		quat.x = sqrt(std::max(0.0, 1.0 + m[0][0] - m[1][1] - m[2][2])) / 2.0;
		quat.y = sqrt(std::max(0.0, 1.0 - m[0][0] + m[1][1] - m[2][2])) / 2.0;
		quat.z = sqrt(std::max(0.0, 1.0 - m[0][0] - m[1][1] + m[2][2])) / 2.0;

/*		quat.x = copysign(quat.x, m[2][1] - m[1][2]);
		quat.y = copysign(quat.y, m[0][2] - m[2][0]);
		quat.z = copysign(quat.z, m[1][0] - m[0][1]);*/

		quat.x = copysign(quat.x, m[1][2] - m[2][1]);
		quat.y = copysign(quat.y, m[2][0] - m[0][2]);
		quat.z = copysign(quat.z, m[0][1] - m[1][0]);

		return quat.normalize();
	}

	template <typename T>
	static void getTransformation(matrixType<T, 4, 4>& mat, vectorType<T, 3>& translation, 
			vectorType<T, 3>& scale, vectorType<T, 4>& rotation)
	{
		mat = matrixType<T, 4, 4>::identity();
		qml::scale(mat, scale);

		mat = qml::getRotationMat(rotation) * mat;

		qml::translate(mat, translation);
	}

	// From three.js source https://github.com/mrdoob/three.js/blob/dev/src/math/Matrix4.js
	template <typename T>
	static void decompose(matrixType<T, 4, 4>& mat, vectorType<T, 3>& translation, 
			vectorType<T, 3>& scale, vectorType<T, 4>& rotation)
	{
		scale = vec3{
			mat[0].xyz.magnitude(),
			mat[1].xyz.magnitude(),
			mat[2].xyz.magnitude() };

		translation = mat[3].xyz;

/*		if (det(mat) < 0) {
			scale.x = -scale.x;
		}*/

		vec3 invS = {
			static_cast<T>(1.0) / scale.x, 
			static_cast<T>(1.0) / scale.y, 
			static_cast<T>(1.0) / scale.z};

		matrixType<T, 3, 3> rMat;
		rMat[0] = mat[0].xyz * invS.x;
		rMat[1] = mat[1].xyz * invS.y;
		rMat[2] = mat[2].xyz * invS.z;

		rotation = getQuatFromRotMat(rMat);
	}
}

#endif //QML_TRANSFORM_HPP
