#ifndef QML_VEC_H
#define QML_VEC_H

#include <sstream>
#include <string>
#include <cmath>
#include <cstdint>
#include <iostream>

namespace qml
{
	template <typename T, int size> struct vectorType;

	template <typename T, int size>
	struct baseVector
	{
		T& operator[](int i)
		{
			return ((T*)this)[i];
		}

		T magnitude()
		{
			T squares = 0;
	
			for (int i = 0; i < size; ++i) {
				squares += ((T*)this)[i] * ((T*)this)[i];
			}
	
			return sqrt(squares);
		}

		vectorType<T, size> normalize()
		{
			return *(vectorType<T, size>*)this / magnitude();	
		}

		void toNormalized()
		{
			*(vectorType<T, size>*)this = this->normalize();
		}

		//Formats the elements into a printable strng
		std::string str() 
		{ 
			std::stringstream ss;

			int i = 0;

			ss << "vec" << size << "(";
			while (i < size - 1) {
				ss << ((T*)this)[i];
				ss << ", ";
				++i;
			}
			ss << ((T*)this)[i] << ")";

			return ss.str();
		}
	};

	template <typename T, int size> 
	struct vectorType : public baseVector<T, size>
	{
		T data[size] = {};
	};

	template <typename T>
	struct vectorType<T, 2> : public baseVector<T, 2>
	{
		vectorType(T x, T y)
			: x(x), y(y) {}

		vectorType(T t)
			: x(t), y(t) {}

		vectorType() {}
		union
		{
			T data[2] = {};
			struct {T x, y;};
		};
	};

	template <typename T>
	struct vectorType<T, 3> : public baseVector<T, 3>
	{
		vectorType(T x, T y, T z)
			: x(x), y(y), z(z) {}

		vectorType(T t)
			: x(t), y(t), z(t) {}

		vectorType(vectorType<T, 2> xy, T z)
		{
			this->xy = xy;
			this->z = z;
		}

		vectorType() {}
		union
		{		
			T data[3] = {};
			struct {T x, y, z;};
			struct {T r, g, b;};
			struct {vectorType<T, 2> xy;};
		};
	};

	template <typename T>
	struct vectorType<T, 4> : public baseVector<T, 4>
	{
		vectorType(T x, T y, T z, T a)
			: x(x), y(y), z(z), w(a) {}

		vectorType(vectorType<T, 3> xyz, T w)
		{
			this->xyz = xyz;
			this->w = w;
		}

		vectorType(vectorType<T, 2> xy, vectorType<T, 2> zw)
			: xy(xy), zw(zw) {}

		vectorType(T t)
			: x(t), y(t), z(t), w(t) {}

		vectorType() {}
		union
		{
			T data[4] = {};
			struct {T x, y, z, w;};
			struct {T r, g, b, a;};
			struct {vectorType<T, 3> xyz;};
			struct {vectorType<T, 2> xy, zw;};
		};
	};

	//Abbreviated types
	typedef vectorType<float, 2> vec2;
	typedef vectorType<float, 3> vec3;
	typedef vectorType<float, 4> vec4;

	typedef vectorType<double, 2> dvec2;
	typedef vectorType<double, 3> dvec3;
	typedef vectorType<double, 4> dvec4;

	typedef vectorType<int, 2> ivec2;
	typedef vectorType<int, 3> ivec3;
	typedef vectorType<int, 4> ivec4;

	typedef vectorType<uint32_t, 2> uvec2;
	typedef vectorType<uint32_t, 3> uvec3;
	typedef vectorType<uint32_t, 4> uvec4;
}
#endif //QML_VEC_H
