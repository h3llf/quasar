#ifndef QML_PROJECTION_H
#define QML_PROJECTION_H

#include "Matrix.hpp"
#include "MatrixOps.hpp"

namespace qml
{
	//TODO: Investigate altering coordinate system handedness
	template <typename T>
	static matrixType<T, 4, 4> lookAt(vectorType<T, 3> position, vectorType<T, 3> target, 
			vectorType<T, 3> up)
	{
		matrixType<T, 4, 4> buffer(0);

		vectorType<T, 3> view = target - position;
		view.toNormalized();
		vectorType<T, 3> side = cross(view, up);
		side.toNormalized();
		vectorType<T, 3> newUp = cross(side, view);

		buffer[0] = vec4{side.x, newUp.x, -view.x, static_cast<T>(0.0f)};
		buffer[1] = vec4{side.y, newUp.y, -view.y, static_cast<T>(0.0f)};
		buffer[2] = vec4{side.z, newUp.z, -view.z, static_cast<T>(0.0f)};
		buffer[3] = vec4{-dot(side, position), -dot(newUp, position), dot(view, position),
			static_cast<T>(1)};

		return buffer;
	}

	static  matrixType<float, 4, 4> infPerspective(float fov, float aspectRatio, float nearPlane)
	{
		matrixType<float, 4, 4> buffer;

		float focalLength = 1.0f / tan(fov / 2);
		buffer[0][0] = focalLength / aspectRatio;
		buffer[1][1] = -focalLength;
		buffer[2][3] = -1;
		buffer[3][2] = nearPlane;

		return buffer;
	}


	static matrixType<float, 4, 4> perspective(float fov, float aspectRatio, 
			float nearPlane, float farPlane)
	{
		matrixType<float, 4, 4> buffer;

		float focalLength = 1.0f / tan(fov / 2);
		float dp = farPlane - nearPlane;

		buffer[0][0] = focalLength / aspectRatio;
		buffer[1][1] = -focalLength;
		buffer[2][2] = nearPlane / dp;
		buffer[2][3] = -1;
		buffer[3][2] = farPlane * (nearPlane / dp);

		return buffer;
	}

	static matrixType<float , 4, 4> ortho(float left, float right, float up, float down, 
			float near, float far)
	{
		matrixType<float, 4, 4> buffer;

		buffer[0][0] = 2.0f / (right - left);
		buffer[1][1] = 2.0f / (up - down);
		buffer[2][2] = 1.0f / (near - far);
		buffer[3][0] = -(right + left) / (right - left);
		buffer[3][1] = -(up + down) / (up - down);
		buffer[3][2] = near / (near - far);
		buffer[3][3] = 1.0f;

		return buffer;
	}

}

#endif //QML_PROJECTION_H
