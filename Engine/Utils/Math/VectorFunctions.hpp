#ifndef QML_VECTOR_FUNCTIONS_H
#define QML_VECTOR_FUNCTIONS_H

#include <algorithm>
#include "Utils/Log.hpp"
#include "Vector.hpp"

namespace qml
{
	template <typename T, int size>
	T dot(vectorType<T, size> v1, vectorType<T, size> v2)
	{
		float buffer = 0;

		for (int i = 0; i < size; ++i) {
			buffer += v1.data[i] * v2.data[i];
		}

		return buffer;
	}

	template <typename T, int size>
	vectorType<T, size> cross(vectorType<T, size> v1, vectorType<T, size> v2)
	{
		vectorType<T, size> buffer {};

		for (int i = 0; i < size; ++i) {
			//Uses other 2 components than i
			buffer.data[i] = v1.data[(i + 1) % size] * v2.data[(i + 2) % size] -
					v1.data[(i + 2) % size] * v2.data[(i + 1) % size];
		}

		return buffer;
	}

	template <typename T, int size>
	vectorType<T, size> abs(vectorType<T, size> v1)
	{
		vectorType<T, size> buffer;

		for (int i = 0; i < size; ++i) {
			buffer[i] = std::abs(v1[i]);
		}

		return buffer;
	}

	template <typename T, int size>
	vectorType<T, size> clamp(vectorType<T, size> v1, T min, T max)
	{
		vectorType<T, size> buffer;

		for (int i = 0; i < size; ++i) {
			buffer[i] = std::clamp(v1[i], min, max);
		}

		return buffer;
	}

	template <typename T, int size>
	vectorType<T, size> clamp(vectorType<T, size> v1, vectorType<T, size> min,
			vectorType<T, size> max)
	{
		vectorType<T, size> buffer;

		for (int i = 0; i < size; ++i) {
			buffer[i] = std::clamp(v1[i], min[i], max[i]);
		}

		return buffer;
	}

	template <typename T, int size>
	vectorType<T, size> lerp(vectorType<T, size>& v1, vectorType<T, size>& v2, T weight)
	{
		vectorType<T, size> buffer;

		for (int i = 0; i < size; ++i) {
			buffer[i] = std::lerp(v1[i], v2[i], weight);
		}

		return buffer;
	}

	template <typename T, int size>
	vectorType<T, size> min(vectorType<T, size>& v1, vectorType<T, size>& v2)
	{
		vectorType<T, size> buffer;
		
		for (int i = 0; i < size; ++i) {
			buffer[i] = std::min(v1[i], v2[i]);
		}

		return buffer;
	}

	template <typename T, int size>
	vectorType<T, size> max(vectorType<T, size>& v1, vectorType<T, size>& v2)
	{
		vectorType<T, size> buffer;
		
		for (int i = 0; i < size; ++i) {
			buffer[i] = std::max(v1[i], v2[i]);
		}

		return buffer;
	}
}

#endif //QML_VECTOR_FUNCTIONS_H
