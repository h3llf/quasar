#ifndef QML_MATH_H
#define QML_MATH_H

#ifndef M_PI_2f
#define M_PI_2f 1.570796327f
#endif

#include "MatrixOps.hpp"
#include "VectorFunctions.hpp"
#include "VectorOps.hpp"
#include "Transform.hpp"
#include "Vector.hpp"
#include "Matrix.hpp"
#include "Projection.hpp"
#include "Quaternion.hpp"

using qml::ivec2;
using qml::ivec3;
using qml::ivec4;
using qml::vec2;
using qml::vec3;
using qml::vec4;
using qml::dvec2;
using qml::dvec3;
using qml::dvec4;
using qml::mat2;
using qml::mat3;
using qml::mat4;
using qml::dmat2;
using qml::dmat3;
using qml::dmat4;
using qml::quat;

#endif //QML_MATH_H
