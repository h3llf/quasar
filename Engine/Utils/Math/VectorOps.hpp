#ifndef QML_VECTOR_OPS_H
#define QML_VECTOR_OPS_H

#include "Vector.hpp"

namespace qml
{
	// String stream operations (assignment & printing)
	template <typename T, int size>
	std::ostream& operator<<(std::ostream& output, vectorType<T, size>& vec)
	{
		output << vec.str();
		return output;
	}

	template <typename T, int size>
	std::istream& operator>>(std::istream& input, vectorType<T, size>& vec)
	{
		for (int i = 0; i < size; ++i) {
			input >> vec[i];
		}
		return input;
	}

	//Negative vector
	template <typename T, int size>
	vectorType<T, size> operator-(vectorType<T, size> v)
	{
		vectorType<T, size> buffer;

		for (int i = 0; i < size; ++i) {
			buffer[i] = -v[i];
		}

		return buffer;
	}

	//Addition
	template <typename T, int size>
	vectorType<T, size> operator+(vectorType<T, size> v1, vectorType<T, size> v2)
	{
		vectorType<T, size> buffer {};
		
		for (int i = 0; i < size; ++i) {
			buffer[i] = v1[i] + v2[i];
		} 

		return buffer;
	}

	template <typename T, int size>
	vectorType<T, size> operator+=(vectorType<T, size>& v1, vectorType<T, size> v2)
	{
		for (int i = 0; i < size; ++i) {
			v1[i] += v2[i];
		} 

		return v1;
	}

	//Subtraction
	template <typename T, int size>
	vectorType<T, size> operator-(vectorType<T, size> v1, vectorType<T, size> v2)
	{
		vectorType<T, size> buffer {};
		
		for (int i = 0; i < size; ++i) {
			buffer[i] = v1[i] - v2[i];
		} 

		return buffer;
	}

	template <typename T, int size>
	vectorType<T, size> operator-=(vectorType<T, size>& v1, vectorType<T, size> v2)
	{
		for (int i = 0; i < size; ++i) {
			v1[i] -= v2[i];
		} 

		return v1;
	}

	//Scalar Multiplication
	template <typename T, int size>
	vectorType<T, size> operator*(vectorType<T, size> v1, T s)
	{
		vectorType<T, size> buffer {};
		
		for (int i = 0; i < size; ++i) {
			buffer[i] = v1[i] * s;
		} 

		return buffer;
	}
	
	template <typename T, int size>
	vectorType<T, size> operator*(T s, vectorType<T, size> v1)
	{
		vectorType<T, size> buffer {};
		
		for (int i = 0; i < size; ++i) {
			buffer[i] = v1[i] * s;
		} 

		return buffer;
	}

	template <typename T, int size>
	vectorType<T, size> operator*=(vectorType<T, size>& v1, T s)
	{
		for (int i = 0; i < size; ++i) {
			v1[i] *= s;
		} 

		return v1;
	}

	//Multiplication
	template <typename T, int size>
	vectorType<T, size> operator*=(vectorType<T, size>& v1, vectorType<T, size> v2)
	{
		for (int i = 0; i < size; ++i) {
			v1[i] *= v2[i];
		} 

		return v1;
	}

	template <typename T, int size>
	vectorType<T, size> operator*(vectorType<T, size> v1, vectorType<T, size> v2)
	{
		vectorType<T, size> buffer {};
		
		for (int i = 0; i < size; ++i) {
			buffer[i] = v1[i] * v2[i];
		} 

		return buffer;
	}

	//Scalar Division
	template <typename T, int size>
	vectorType<T, size> operator/(vectorType<T, size> v1, T s)
	{
		vectorType<T, size> buffer {};
		
		for (int i = 0; i < size; ++i) {
			buffer[i] = v1[i] / s;
		} 

		return buffer;
	}
	
	template <typename T, int size>
	vectorType<T, size> operator/=(vectorType<T, size>& v1, T s)
	{
		for (int i = 0; i < size; ++i) {
			v1[i] /= s;
		} 

		return v1;
	}

	//Division
	template <typename T, int size>
	vectorType<T, size> operator/=(vectorType<T, size>& v1, vectorType<T, size> v2)
	{
		for (int i = 0; i < size; ++i) {
			v1[i] /= v2[i];
		} 

		return v1;
	}

	template <typename T, int size>
	vectorType<T, size> operator/(vectorType<T, size> v1, vectorType<T, size> v2)
	{
		vectorType<T, size> buffer {};
	
		for (int i = 0; i < size; ++i) {
			buffer[i] = v1[i] / v2[i];
		} 

		return buffer;
	}

	template <typename T, int size>
	bool operator==(vectorType<T, size> v1, vectorType<T, size> v2)
	{
		bool result = true;
		for (int i = 0; i < size; ++i) {
			result &= (v1[i] == v2[i]);
		}

		return result;
	}

	template <typename T, int size>
	bool operator>(vectorType<T, size> v1, vectorType<T, size> v2)
	{
		bool result = true;
		for (int i = 0; i < size; ++i) {
			result &= (v1[i] > v2[i]);
		}

		return result;
	}

	template <typename T, int size>
	bool operator<(vectorType<T, size> v1, vectorType<T, size> v2)
	{
		bool result = true;
		for (int i = 0; i < size; ++i) {
			result &= (v1[i] < v2[i]);
		}

		return result;
	}

	template <typename T, int size>
	bool operator>=(vectorType<T, size> v1, vectorType<T, size> v2)
	{
		bool result = true;
		for (int i = 0; i < size; ++i) {
			result &= (v1[i] >= v2[i]);
		}

		return result;
	}

	template <typename T, int size>
	bool operator<=(vectorType<T, size> v1, vectorType<T, size> v2)
	{
		bool result = true;
		for (int i = 0; i < size; ++i) {
			result &= (v1[i] <= v2[i]);
		}

		return result;
	}
}

#endif //QML_VECTOR_OPS_H
