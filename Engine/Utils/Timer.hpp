#ifndef QUASAR_TIMER_H
#define QUASAR_TIMER_H

#include "Core/Types.hpp"
#include <chrono>
#include <limits>
#include <string>

namespace Timer
{
	struct ScopedTimer 
	{
		void (*callBack)(uint64) = nullptr;
		std::chrono::high_resolution_clock::time_point t0;

		ScopedTimer(void (*callBack)(uint64)) :
			callBack(callBack),
			t0(std::chrono::high_resolution_clock::now()) {}

		~ScopedTimer()
		{
			std::chrono::high_resolution_clock::time_point t1 =
				std::chrono::high_resolution_clock::now();

			long duration = std::chrono::duration_cast<std::chrono::microseconds>
				(t1 - t0).count();

			callBack(duration);
		}
	};

	struct ScopedPrintTimer
	{
		std::chrono::high_resolution_clock::time_point t0;
		std::string prefix;

		ScopedPrintTimer(std::string prefix) :
			t0(std::chrono::high_resolution_clock::now()),
			prefix(prefix) {}

		~ScopedPrintTimer()
		{
			std::chrono::high_resolution_clock::time_point t1 =
				std::chrono::high_resolution_clock::now();

			long duration = std::chrono::duration_cast<std::chrono::microseconds>
				(t1 - t0).count();

			std::cout << prefix << duration << "μs" << std::endl;
		}
	};

	struct StopStartTimer
	{
		std::chrono::high_resolution_clock::time_point t0;

		void start()
		{
			t0 = std::chrono::high_resolution_clock::now();
		}

		long stop()
		{
			std::chrono::high_resolution_clock::time_point t1 =
				std::chrono::high_resolution_clock::now();

			return std::chrono::duration_cast<std::chrono::microseconds> (t1 - t0).count();
		}
	};

	class ExecuteEvery
	{
	public:
		ExecuteEvery(float interval, float offset = 0, float limit = std::numeric_limits<float>::infinity()) : 
			rate(interval), accumulator(offset), maxDelta(limit) {}

		bool shouldTick(float delta)
		{
			accumulator += delta;
			if (accumulator >= rate) {
				accumulator = 0.0f;

				if (delta < maxDelta) {
					return true;
				}
			}

			return false;
		}

		float rate;
		float accumulator;
		float maxDelta;
	};
};

#endif //QUASAR_TIMER_H
