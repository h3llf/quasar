#include "Core/Assets/AssetManager.hpp"
#include "Core/Assets/InstanceManager.hpp" // Light structures
#include "Core/Camera.hpp"
#include "Descriptors.hpp"
#include "Buffers.hpp"
#include "Drawable/LightManager.hpp"
#include "GFX/Drawable/Viewports.hpp"
#include "GFX/RendererData.hpp"
#include "Utils/Math/MatrixOps.hpp"
#include <chrono>
#include <string.h>
#include <vulkan/vulkan_core.h>

void DescriptorBuilder::bindBuffer(uint32 binding, VkDescriptorType type, VkDescriptorBufferInfo* info)
{
	VkWriteDescriptorSet write {};
	write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	write.dstBinding = binding;
	write.pNext = nullptr;
	write.descriptorCount = 1;
	write.descriptorType = type;
	write.pBufferInfo = info;

	writes.push_back(write);
}

void DescriptorBuilder::bindImage(uint32 binding, VkDescriptorType type, VkDescriptorImageInfo *imageInfo)
{
	VkWriteDescriptorSet write {};
	write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	write.dstBinding = binding;
	write.pNext = nullptr;
	write.descriptorCount = 1;
	write.descriptorType = type;
	write.pImageInfo = imageInfo;

	writes.push_back(write);
}

void DescriptorBuilder::createLayoutBinding(uint32 binding, VkDescriptorType type, 
		VkShaderStageFlags stageFlags)
{
	VkDescriptorSetLayoutBinding layoutBinding {};
	layoutBinding.binding = binding;
	layoutBinding.descriptorCount = 1;
	layoutBinding.descriptorType = type;
	layoutBinding.stageFlags = stageFlags;

	bindings.push_back(layoutBinding);
}

void DescriptorBuilder::createLayout(VkDescriptorSetLayout *pLayout)
{
	VkDescriptorSetLayoutCreateInfo createInfo {};
	createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	createInfo.bindingCount = bindings.size();
	createInfo.pBindings = bindings.data();

	VK_CHECK( vkCreateDescriptorSetLayout(GFX.device, &createInfo, nullptr, pLayout),
			"Failed to create descriptor set layout");
}

void DescriptorBuilder::build(VkDescriptorSet* pSet, VkDescriptorSetLayout layout, VkDescriptorPool pool)
{
	VkDescriptorSetAllocateInfo allocInfo {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = pool;
	allocInfo.descriptorSetCount = 1;
	allocInfo.pSetLayouts = &layout;

	VK_CHECK( vkAllocateDescriptorSets(GFX.device, &allocInfo, pSet),
			"Failed to allocate descriptor set");

	for (VkWriteDescriptorSet& write : writes) {
		write.dstSet = *pSet;
	}


	vkUpdateDescriptorSets(GFX.device, (uint32)writes.size(), writes.data(), 0, nullptr);
}

void Descriptors::createDescriptorSetLayouts()
{
	//Used for depth passes
	DescriptorBuilder cameraSet;
	cameraSet.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
			VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_COMPUTE_BIT);
	cameraSet.createLayout(&GFX.cameraOnlyLayout);

	//Light list
	DescriptorBuilder lightingSet;

	// Point light list TODO: Spot lights
	lightingSet.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 
			VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_COMPUTE_BIT);
	// Directional light
	lightingSet.createLayoutBinding(1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 
			VK_SHADER_STAGE_FRAGMENT_BIT);
	lightingSet.createLayout(&GFX.lightingLayout);

	// Texture array
	VkDescriptorSetLayoutBinding layoutBinding {
		.binding = 0,
		.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
		.descriptorCount = GFX.maxImageDescriptors,
		.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT
	};

	VkDescriptorBindingFlagsEXT bindFlag = VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT |
		VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT;
	VkDescriptorSetLayoutBindingFlagsCreateInfo flagCreateInfo{
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO,
		.pNext = nullptr,
		.bindingCount = 1,
		.pBindingFlags = &bindFlag
	};

	VkDescriptorSetLayoutCreateInfo layoutInfo {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
		.pNext = &flagCreateInfo,
		.flags = 0,
		.bindingCount = 1,
		.pBindings = &layoutBinding
	};

	VK_CHECK( vkCreateDescriptorSetLayout(GFX.device, &layoutInfo, nullptr, &GFX.textureArrayLayout),
			"Failed to create texture array descriptor layout");

}

void Descriptors::updateTextureDescriptor(uint32 index, VkImageView imageView)
{
	VkDescriptorImageInfo imgInfo {
		.sampler = GFX.textureSampler,
		.imageView = imageView,
		.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
	};

	VkWriteDescriptorSet setWrite {
		.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
		.pNext = nullptr,
		.dstSet = GFX.textureArraySet,
		.dstBinding = 0,
		.dstArrayElement = index,
		.descriptorCount = 1,
		.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
		.pImageInfo = &imgInfo,
		.pBufferInfo = nullptr
	};

	vkUpdateDescriptorSets(GFX.device, 1, &setWrite, 0, nullptr);
}

// TODO: MOVE to main shading pass
void Descriptors::initFrameDescriptor()
{
	GFX.cameraDescriptors.resize(GFX.maxFramesInFlight);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		VkDescriptorBufferInfo cameraInfo{};
		cameraInfo.buffer = GFX.cameraUniformBuffers.at(i);
		cameraInfo.offset = 0;
		cameraInfo.range = sizeof(Camera::CameraProperties);

		DescriptorBuilder camOnly;
		camOnly.bindBuffer(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, &cameraInfo);
		camOnly.build(&GFX.cameraDescriptors[i], GFX.cameraOnlyLayout, GFX.descriptorPool);
	}

}

void Descriptors::createPersistantDescPool()
{
	std::array<VkDescriptorPoolSize, 2> poolSizes {};
	poolSizes.at(0).type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes.at(0).descriptorCount = 1024;
	poolSizes.at(1).type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSizes.at(1).descriptorCount = 1024;

	VkDescriptorPoolCreateInfo poolInfo {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = poolSizes.size();
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = 1024;
	//Set flag to allow descriptor set to be freed

	VK_CHECK( vkCreateDescriptorPool(GFX.device, &poolInfo, nullptr, &GFX.persistantDescPool),
			"Failed to create descriptor pool");
}

void Descriptors::createDescriptorPool()
{
	std::array<VkDescriptorPoolSize, 3> poolSizes {};
	poolSizes.at(0).type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes.at(0).descriptorCount = 2048;
	poolSizes.at(1).type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSizes.at(1).descriptorCount = 512;
	poolSizes.at(2).type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	poolSizes.at(2).descriptorCount = GFX.maxFramesInFlight;

	VkDescriptorPoolCreateInfo poolInfo {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = poolSizes.size();
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = 2048;
	//Set flag to allow descriptor set to be freed

	VK_CHECK( vkCreateDescriptorPool(GFX.device, &poolInfo, nullptr, &GFX.descriptorPool),
			"Failed to create descriptor pool");
}

void Descriptors::createUniformBuffers()
{
	VkDeviceSize bufferSize = sizeof(Camera::CameraProperties);
	GFX.cameraUniformBuffers.resize(GFX.maxFramesInFlight);
	GFX.cameraUniformAllocations.resize(GFX.maxFramesInFlight);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		Buffers::createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
				VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				&GFX.cameraUniformBuffers.at(i), 
				&GFX.cameraUniformAllocations.at(i));
	}
}

void Descriptors::updateCamera(uint32 currentImage, ViewportHandle viewport)
{
	ViewportData& vp = viewports.viewportList[viewport];
	Camera::CameraProperties& cam = vp.camProps;

	Camera::genProjection(GFX.surfaceExtent.width, GFX.surfaceExtent.height, &cam);
	cam.invProj = qml::invert(cam.proj);

	cam.xScale = static_cast<float>(vp.width) / GFX.internalRes.width;
	cam.yScale = static_cast<float>(vp.height) / GFX.internalRes.height;

	void* data;
	vmaMapMemory(GFX.allocator, GFX.cameraUniformAllocations.at(currentImage), &data);
	memcpy(data, &cam, sizeof(Camera::CameraProperties));
	vmaUnmapMemory(GFX.allocator, GFX.cameraUniformAllocations.at(currentImage));
}

void Descriptors::destroyUniforms()
{
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vkDestroyBuffer(GFX.device, GFX.cameraUniformBuffers.at(i), nullptr);
		vmaFreeMemory(GFX.allocator, GFX.cameraUniformAllocations.at(i));
	}
}
