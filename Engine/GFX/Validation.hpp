#ifndef QUASAR_VALIDATION_H
#define QUASAR_VALIDATION_H

#include "RendererData.hpp"
#include <vulkan/vulkan_core.h>

class Validation
{
public:
	Validation();
	~Validation();

	static bool checkValidationLayers();

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
			VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
			VkDebugUtilsMessageTypeFlagsEXT messageType,
			const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
			void* pUserData);

	static void setupDebugMessenger();

	static void destroyDebugMessenger(VkInstance instance, 
			VkDebugUtilsMessengerEXT debugMessenger,
			const VkAllocationCallbacks* pAllocator);
	
	static void populateCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);

private:
	static VkResult createDebugUtilsMessenger(VkInstance instance, 
			const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, 
			const VkAllocationCallbacks* pAllocator,
			VkDebugUtilsMessengerEXT* pDebugMessenger);
	
};

#endif //QUASAR_VALIDATION_H

