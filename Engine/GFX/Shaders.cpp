#include "Shaders.hpp"

#include <fstream>
#include <vector>
#include <vulkan/vulkan_core.h>

VkShaderModule Shaders::createShaderModule(const char* filename)
{
	std::vector<char> code = readFile(filename);

	VkShaderModuleCreateInfo createInfo {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = code.size();
	/*Only accepts uint32_t pointer*/
	createInfo.pCode = reinterpret_cast<const uint32*>(code.data());

	VkShaderModule shaderModuele;

	VK_CHECK( vkCreateShaderModule(GFX.device, &createInfo, nullptr, &shaderModuele),
			"[Vulkan] Failed to create shader module" );

	return shaderModuele;
}

std::vector<char> Shaders::readFile (const char* filename)
{
	/*Starts reading at the end so the size can be determined*/
	std::ifstream file(filename, std::ios::ate | std::ios::binary);

	if (!file.is_open()) { 
		Log::fatal("Couldn't open shader file at ", filename);
		return std::vector<char>();
	}

	size_t size = (size_t) file.tellg();
	std::vector<char> buffer(size);

	file.seekg(0);
	file.read(buffer.data(), size);
	file.close();

	return buffer;

}
