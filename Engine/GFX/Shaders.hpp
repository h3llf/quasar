#ifndef QUASAR_SHADERS_H
#define QUASAR_SHADERS_H

#include "RendererData.hpp"

class Shaders
{
public:
	static VkShaderModule createShaderModule(const char* filename);

private:
	static std::vector<char> readFile (const char* filename);
	
};

#endif //QUASAR_SHADERS_H
