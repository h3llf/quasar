#ifndef QUASAR_BUFFERS_H
#define QUASAR_BUFFERS_H

#include "RendererData.hpp"

class Buffers
{
public:
	static void createBuffer(VkDeviceSize size,
			VkBufferUsageFlags usage,
			VkMemoryPropertyFlags properties,
			VkBuffer* buffer,
			VmaAllocation* allocation);

	static void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
	static void copyToImage(VkBuffer buffer, VkImage image, VkExtent3D extent);

	// Copy data without the use of staging
	static void copyToBuffer(VkBuffer dstBuffer, VmaAllocation dstAlloc, 
			size_t* size, void** srcData, uint32 count);
	static void copyToBuffer(VkBuffer dstBuffer, VmaAllocation dstAlloc, 
			size_t size, void* srcData);

	static void createStagingBuffer(size_t size, VkBuffer* buffer, VmaAllocation* alloc);
	static void copyToBufferStaging(VkBuffer dstBuffer, size_t* size, void** srcData, uint32 count);
	static void copyToBufferStaging(VkBuffer dstBuffer, size_t size, void* srcData);

	static void createVertexBuffer(VkBuffer* vertexBuffer,
			VmaAllocation* vertexAllocation,
			std::vector<quasar::Vertex>* vertices);
	static void createIndexBuffer(VkBuffer* indexBuffer,
			VmaAllocation* indexAllocation,
			std::vector<uint32>* indices);
	static void destroyBuffers();

	static VkVertexInputBindingDescription getBindDesc();
	static std::vector<VkVertexInputAttributeDescription> getAttribDesc(bool depthOnly);

	static uint32 findMemoryType(uint32 typeFilter, VkMemoryPropertyFlags properties);

};

#endif //QUASAR_BUFFERS_H
