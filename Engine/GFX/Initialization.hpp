#ifndef QUASAR_INITIALIZATION_H
#define QUASAR_INITIALIZATION_H

#include <vector>
#include "Core/Types.hpp"

class Initialization
{
public:

	static void initVulkan();
	static void cleanUp();
	static void initSwapchainDependencies();
	static void cleanupSwapchainDependencies();
	static void recreateSwapchain();

	static void initInstance();
	static void initVMA();

private:
	static std::vector<const char*> getRequiredExtensions();
	static void createSyncObjects();

};

#endif //QUASAR_INITIALIZATION_H

