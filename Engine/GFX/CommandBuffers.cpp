#include "CommandBuffers.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include "Core/Globals.hpp"
#include "Drawable/LightManager.hpp"
#include "Editor/Editor.hpp"
#include "GFX/Passes/FontRendering.hpp"
#include "GFX/RenderSettings.hpp"
#include "RendererData.hpp"
#include "GFX/Drawable/MeshManager.hpp"
#include "GFX/Passes/DebugLines.hpp"
#include "GFX/Passes/Outline.hpp"
#include "backends/imgui_impl_vulkan.h"
#include "imgui.h"
#include <cstdint>
#include <ctime>
#include <exception>
#include <utility>
#include <vulkan/vulkan_core.h>

using namespace quasar;

void CommandBuffers::createCommandPool()
{
	VkCommandPoolCreateInfo createInfo {};
	createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	createInfo.queueFamilyIndex = GFX.graphicsQueueIndex;
	createInfo.flags = 0;

	GFX.commandPools.resize(GFX.maxFramesInFlight);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		VK_CHECK( vkCreateCommandPool(GFX.device, &createInfo, nullptr, &GFX.commandPools.at(i)), 
			"Failed to create command pool");	
	}

	createInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	VK_CHECK( vkCreateCommandPool(GFX.device, &createInfo, nullptr, &GFX.singleTimeCmdPool), 
		"Failed to create command pool");

	createInfo.queueFamilyIndex = GFX.transferQueueIndex;
	VK_CHECK( vkCreateCommandPool(GFX.device, &createInfo, nullptr, &GFX.transferCmdPool), 
		"Failed to create command pool");
}

void CommandBuffers::createCommandBuffers()
{
	GFX.shadingCmdBuffers.resize(GFX.maxFramesInFlight);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		VkCommandBufferAllocateInfo allocInfo {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.commandPool = GFX.commandPools[i];
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandBufferCount = 1;

		VK_CHECK( vkAllocateCommandBuffers(GFX.device, &allocInfo, 
				&GFX.shadingCmdBuffers[i]),
				"Command buffer allocation failed");
	}

	VkCommandBufferAllocateInfo allocInfo {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = 1;
	allocInfo.commandPool = GFX.singleTimeCmdPool;
	VK_CHECK( vkAllocateCommandBuffers(GFX.device, &allocInfo,
			&GFX.singleTimeCommands),
			"Command buffer allocation failed");

	allocInfo.commandPool = GFX.transferCmdPool;
	VK_CHECK( vkAllocateCommandBuffers(GFX.device, &allocInfo,
			&GFX.transferCmdBuffer),
			"Command buffer allocation failed");
}

inline void beginCommandBuffer(VkCommandBuffer cmdBuffer)
{
	VkCommandBufferBeginInfo beginInfo {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = 0;
	beginInfo.pInheritanceInfo = nullptr; //Relevant for secondary command buffers
	
	VK_CHECK (vkBeginCommandBuffer(cmdBuffer, &beginInfo), 
			"Failed to record command buffer (begin)");
}

void CommandBuffers::updateCommandBuffers(uint32 currentFrame, uint32 currentImage, ViewportData& vp)
{
	std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
	beginCommandBuffer(GFX.shadingCmdBuffers[currentFrame]);
	updateDepthCommands(currentFrame, vp);
	updateComputeCommands(currentFrame, vp);
	updateShadowCommands(currentFrame, vp);
	updateShadingCommands(currentFrame, vp);
	updateReflectionCommands(currentFrame);
//	copyToNextFrame(currentFrame);
	updateBloomCommands(currentFrame, vp);
	updatePostProcessCommands(currentFrame, vp);
	updateOutlineCommands(currentFrame, vp);
	updateTextCommands(currentFrame, vp);
	updateDebugVisualizationCommands(currentFrame, vp);
	
	if (vp.windowTarget) {
		updateSwapchainCommands(currentFrame, currentImage);
	} else {
		copyTextureImage(currentFrame, vp);
	}


	vkEndCommandBuffer(GFX.shadingCmdBuffers[currentFrame]);
}

void CommandBuffers::updateDepthCommands(uint32 currentFrame, ViewportData& vp)
{
	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	VkClearValue clearValue {};
	clearValue.depthStencil = {1.0f, 0};

	VkRenderPassBeginInfo renderPassInfo {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = GFX.mainShading.depthPass;
	renderPassInfo.framebuffer = vp.depthOnlyFramebuffers[currentFrame];
	renderPassInfo.renderArea.offset = {0, 0};
	renderPassInfo.renderArea.extent = VkExtent2D{ (uint32)vp.width, (uint32)vp.height};
	renderPassInfo.clearValueCount = 1;
	renderPassInfo.pClearValues = &clearValue;

	vkCmdBeginRenderPass(cmdBufferTmp, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

	vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.mainShading.depthPipeline);

	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.mainShading.depthPipelineLayout,
			0, 1, &GFX.cameraDescriptors[currentFrame], 0, nullptr);

	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.mainShading.depthPipelineLayout, 
			1, 1, &GFX.textureArraySet, 0, nullptr);

	meshDrawingCommands(cmdBufferTmp, GFX.mainShading.depthPipelineLayout, true, true, vp);

	vkCmdEndRenderPass(cmdBufferTmp);
}

void CommandBuffers::updateShadowCommands(uint32 currentFrame, ViewportData& vp)
{
	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	VkClearValue clearValue {};
	clearValue.depthStencil = {1.0f, 0};

	// Cascaded shadows
	if (GFX.cascadeShadowEnabled) {
		VkRenderPassBeginInfo shadowPassInfo {};

		shadowPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		shadowPassInfo.renderPass = GFX.cascadeShadowData.renderPass;
		shadowPassInfo.framebuffer = GFX.cascadeShadowData.frameBuffers[currentFrame];
		shadowPassInfo.renderArea.extent = 
			VkExtent2D{GFX.currentCascadeResolution, GFX.currentCascadeResolution};
		shadowPassInfo.clearValueCount = 1;
		shadowPassInfo.pClearValues = &clearValue;

		vkCmdBeginRenderPass(cmdBufferTmp, &shadowPassInfo, VK_SUBPASS_CONTENTS_INLINE);
	//	vkCmdSetDepthBias(cmdBufferTmp, 1.25f, 0.0f, 1.75f); //TODO: Make configurable
	
		vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS,
				GFX.cascadeShadowData.pipeline);

		vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, 
				GFX.cascadeShadowData.pipelineLayout, 0, 1, 
				&GFX.cascadeShadowData.shadowCamDesc[currentFrame], 0, nullptr);

		vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, 
				GFX.cascadeShadowData.pipelineLayout, 1, 1, 
				&GFX.textureArraySet, 0, nullptr);

		meshDrawingCommands(cmdBufferTmp, GFX.cascadeShadowData.pipelineLayout, true, false, vp);
		vkCmdEndRenderPass(cmdBufferTmp);
	}


	for (uint32 i = 0; i < GFX.omniShadowCount; ++i) {
		uint32 shadowIndex = currentFrame + GFX.maxFramesInFlight * i;

		VkRenderPassBeginInfo shadowPassInfo {};
	
		shadowPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		shadowPassInfo.renderPass = GFX.omniShadowData.renderPass;
		shadowPassInfo.framebuffer = GFX.omniShadowData.frameBuffers[shadowIndex];
		shadowPassInfo.renderArea.extent = 
			VkExtent2D{GFX.currentOmniResolution, GFX.currentOmniResolution};
		shadowPassInfo.clearValueCount = 1;
		shadowPassInfo.pClearValues = &clearValue;
	
		vkCmdBeginRenderPass(cmdBufferTmp, &shadowPassInfo, VK_SUBPASS_CONTENTS_INLINE);
//		vkCmdSetDepthBias(cmdBufferTmp, 1.25f, 0.0f, 1.75f); //TODO: Make configurable
		
		// Omni directional shadows
		vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, 
				GFX.omniShadowData.pipeline);

		vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, 
				GFX.omniShadowData.pipelineLayout, 0, 1, 
				&GFX.omniShadowData.shadowCamDesc[shadowIndex], 0, nullptr);
	
		vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, 
				GFX.omniShadowData.pipelineLayout, 1, 1, 
				&GFX.textureArraySet, 0, nullptr);

		meshDrawingCommands(cmdBufferTmp, GFX.omniShadowData.pipelineLayout, true, false, vp);
		vkCmdEndRenderPass(cmdBufferTmp);
	}
}

void CommandBuffers::updateComputeCommands(uint32 currentFrame, ViewportData& vp)
{
	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	// Tiled light culling
	vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.tiledForwardPass.pipeline);

	// Wait for depth pre-pass?
	
	//Camera data
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, 
			GFX.tiledForwardPass.pipelineLayout, 0, 1, 
			&GFX.cameraDescriptors[currentFrame], 0, nullptr);
	//Lighting data
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, 
			GFX.tiledForwardPass.pipelineLayout, 1, 1,
			&GFX.lightingDescriptors[currentFrame], 0, nullptr);

	//Depth Image
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, 
			GFX.tiledForwardPass.pipelineLayout, 2, 1, 
			&GFX.tiledForwardPass.depthSets[currentFrame], 0, nullptr);
	//Frustums and culling results
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, 
			GFX.tiledForwardPass.pipelineLayout, 3, 1,
			&vp.frustumSets[currentFrame], 0, nullptr);

	vkCmdDispatch(cmdBufferTmp, vp.TFGroupsX, vp.TFGroupsY, 1);

	// Culling output barrier
	VkBufferMemoryBarrier2 fwdPlusBarrier {};
	fwdPlusBarrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER_2;
	fwdPlusBarrier.pNext = nullptr;
	fwdPlusBarrier.srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
	fwdPlusBarrier.srcAccessMask = VK_ACCESS_2_SHADER_WRITE_BIT;
	fwdPlusBarrier.dstStageMask = VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT;
	fwdPlusBarrier.dstAccessMask = VK_ACCESS_2_SHADER_READ_BIT;
	fwdPlusBarrier.offset = 0;
	fwdPlusBarrier.size = VK_WHOLE_SIZE;
	fwdPlusBarrier.buffer = vp.cullingResultBuffers[currentFrame];

	// Transition image layout
	VkImageMemoryBarrier2 aoReadBarrier {};
	aoReadBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
	aoReadBarrier.pNext = nullptr;
	aoReadBarrier.srcStageMask = VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT;
	aoReadBarrier.srcAccessMask = VK_ACCESS_2_NONE;
	aoReadBarrier.dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
	aoReadBarrier.dstAccessMask = VK_ACCESS_2_SHADER_WRITE_BIT;
	aoReadBarrier.image = GFX.gtaoData.aoImg[currentFrame];
	aoReadBarrier.oldLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	aoReadBarrier.newLayout = VK_IMAGE_LAYOUT_GENERAL;
	aoReadBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	aoReadBarrier.subresourceRange.layerCount = 1;
	aoReadBarrier.subresourceRange.baseArrayLayer = 0;
	aoReadBarrier.subresourceRange.levelCount = 1;
	aoReadBarrier.subresourceRange.baseMipLevel = 0;

	VkDependencyInfo depInfo1 {};
	depInfo1.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
	depInfo1.pNext = nullptr;
	depInfo1.imageMemoryBarrierCount = (GFX.gtaoData.aoActive) ? 1 : 0;
	depInfo1.pImageMemoryBarriers = &aoReadBarrier;
	depInfo1.bufferMemoryBarrierCount = 0;//1;
	depInfo1.pBufferMemoryBarriers = &fwdPlusBarrier;
	depInfo1.memoryBarrierCount = 0;
	depInfo1.pMemoryBarriers = nullptr;

	vkCmdPipelineBarrier2(cmdBufferTmp, &depInfo1);

	// Ground truth ambient occlusion
	if (GFX.gtaoData.aoActive) {
		vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.gtaoData.pipeline);

		// Source depth image
		vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE,
				GFX.gtaoData.pipelineLayout, 0, 1,
				&GFX.gtaoData.depthSets[currentFrame], 0, nullptr);
		// AO output image
		vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE,
				GFX.gtaoData.pipelineLayout, 1, 1,
				&GFX.gtaoData.imageDescriptors[currentFrame], 0, nullptr);
		// Camera data
		vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, 
				GFX.gtaoData.pipelineLayout, 2, 1, 
				&GFX.cameraDescriptors[currentFrame], 0, nullptr);

		AmbientOcclusionData::PushConstData pushConst = {
			.width = vp.aoResX,
			.height = vp.aoResY
		};

		vkCmdPushConstants(cmdBufferTmp, GFX.gtaoData.pipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 
				0, sizeof(pushConst), &pushConst);

		vkCmdDispatch(cmdBufferTmp, vp.AOGroupsX, vp.AOGroupsY, 1);

		// Wait on AO write
		VkImageMemoryBarrier2 aoWriteBarrier {};
		aoWriteBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR;
		aoWriteBarrier.pNext = nullptr;
		aoWriteBarrier.srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT_KHR;
		aoWriteBarrier.srcAccessMask = VK_ACCESS_2_SHADER_WRITE_BIT_KHR;
		aoWriteBarrier.dstStageMask = VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT_KHR;
		aoWriteBarrier.dstAccessMask = VK_ACCESS_2_SHADER_READ_BIT_KHR;
		aoWriteBarrier.image = GFX.gtaoData.aoImg[currentFrame];
		aoWriteBarrier.oldLayout = VK_IMAGE_LAYOUT_GENERAL;
		aoWriteBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		aoWriteBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		aoWriteBarrier.subresourceRange.layerCount = 1;
		aoWriteBarrier.subresourceRange.baseArrayLayer = 0;
		aoWriteBarrier.subresourceRange.levelCount = 1;
		aoWriteBarrier.subresourceRange.baseMipLevel = 0;

		VkDependencyInfo depInfo {};
		depInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
		depInfo.pNext = nullptr;
		depInfo.imageMemoryBarrierCount = 1;
		depInfo.pImageMemoryBarriers = &aoWriteBarrier;
		depInfo.bufferMemoryBarrierCount = 1;
		depInfo.pBufferMemoryBarriers = &fwdPlusBarrier;
		depInfo.memoryBarrierCount = 0;
		depInfo.pMemoryBarriers = nullptr;

		vkCmdPipelineBarrier2(cmdBufferTmp, &depInfo);
	}
}

void CommandBuffers::updateShadingCommands(uint32 currentFrame, ViewportData& vp)
{
	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	std::array<VkClearValue, 3> clearValues;
	clearValues[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
	clearValues[1].color = {0.0f, 0.0f, 0.0f, 1.0f};
	clearValues[2].depthStencil = {1.0f, 0};

	VkRenderPassBeginInfo renderPassInfo {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderArea.offset = {0, 0};
	renderPassInfo.renderArea.extent = VkExtent2D{ (uint32)vp.width, (uint32)vp.height};
	renderPassInfo.renderPass = GFX.mainShading.renderPass;
	renderPassInfo.framebuffer = vp.mainFramebuffers[currentFrame];
	renderPassInfo.clearValueCount = clearValues.size();
	renderPassInfo.pClearValues = clearValues.data();

	vkCmdBeginRenderPass(cmdBufferTmp, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

	vkCmdBindPipeline(cmdBufferTmp,	VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.mainShading.graphicsPipeline);

	//Camera data
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.mainShading.pipelineLayout,
			0, 1, &GFX.cameraDescriptors[currentFrame], 0, nullptr);

	//Lighting data
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.mainShading.pipelineLayout,
			1, 1, &GFX.lightingDescriptors[currentFrame], 0, nullptr);

	// Textures
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.mainShading.pipelineLayout, 
			2, 1, &GFX.textureArraySet, 0, nullptr);

	// Omni & cascaded shadows, Ambient occlusion image
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.mainShading.pipelineLayout,
			3, 1, &GFX.mainShading.shadingDescriptors[currentFrame], 0, nullptr);

	//Frustums and culling results
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.mainShading.pipelineLayout,
			4, 1, &vp.frustumSets[currentFrame], 0, nullptr);

	meshDrawingCommands(cmdBufferTmp, GFX.mainShading.pipelineLayout, false, true, vp);

	vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.skyboxData.pipeline);

	//Camera data
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.skyboxData.layout,
			0, 1, &GFX.cameraDescriptors[currentFrame], 0, nullptr);

	// Textures
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.skyboxData.layout, 
			1, 1, &GFX.textureArraySet, 0, nullptr);

	VkDeviceSize offsets[] = {0};
	vkCmdBindVertexBuffers(cmdBufferTmp, 0, 1, &GFX.skyboxData.vertexBuffer, offsets);
	vkCmdBindIndexBuffer(cmdBufferTmp, GFX.skyboxData.indexBuffer, 0, VK_INDEX_TYPE_UINT32);

	vkCmdPushConstants(cmdBufferTmp, GFX.skyboxData.layout, VK_SHADER_STAGE_FRAGMENT_BIT,
			0, sizeof(int), &GFX.skyboxImgID);

	vkCmdDrawIndexed(cmdBufferTmp, 36, 1, 0, 0, 0);

	vkCmdEndRenderPass(cmdBufferTmp);
}

static bool first = false;
void CommandBuffers::updateReflectionCommands(uint32 currentFrame)
{
	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	VkImageMemoryBarrier2 b {};
	b.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2;
	b.pNext = nullptr;
	b.srcStageMask = VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT;
	b.srcAccessMask = VK_ACCESS_2_SHADER_READ_BIT;
	b.dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
	b.dstAccessMask = VK_ACCESS_2_SHADER_READ_BIT;
	b.image = GFX.mainShading.images[currentFrame];
	b.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	b.newLayout = VK_IMAGE_LAYOUT_GENERAL;
	b.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	b.subresourceRange.layerCount = 1;
	b.subresourceRange.baseArrayLayer = 0;
	b.subresourceRange.levelCount = 1;
	b.subresourceRange.baseMipLevel = 0;

	VkDependencyInfo depInfo {};
	depInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
	depInfo.pNext = nullptr;
	depInfo.imageMemoryBarrierCount = 1;
	depInfo.pImageMemoryBarriers = &b;
	depInfo.bufferMemoryBarrierCount = 0;
	depInfo.pBufferMemoryBarriers = nullptr;
	depInfo.memoryBarrierCount = 0;
	depInfo.pMemoryBarriers = nullptr;

	vkCmdPipelineBarrier2(cmdBufferTmp, &depInfo);

	// Previous frame won't be avalivalbe yet
	if (!first) {
		first = true;
		return;
	}

	// TODO: Add a barrier for the depth image
	// TODO: Hierarchjical depth buffer and renable ssr
	/*
	fullBarrier(cmdBufferTmp);
	
	vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.ssrData.pipeline);

	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.ssrData.layout, 
			0, 1, &GFX.ssrData.tracingDesc[currentFrame], 0, nullptr);

	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.ssrData.layout, 
			1, 1, &GFX.cameraDescriptors[currentFrame], 0, nullptr);

	vkCmdDispatch(cmdBufferTmp, GFX.ssrData.tracingGroupsX, GFX.ssrData.tracingGroupsY, 1);

	fullBarrier(cmdBufferTmp);

	vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.ssrData.resolvePipeline);

	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.ssrData.resolveLayout, 
			0, 1, &GFX.ssrData.resolveDesc[currentFrame], 0, nullptr);

	vkCmdDispatch(cmdBufferTmp, GFX.ssrData.tracingGroupsX, GFX.ssrData.tracingGroupsY, 1);*/

/* TODO:
	VkMemoryBarrier2 b2;
	b2 = {};
	b2.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER_2;
	b2.pNext = nullptr;
	b2.srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
	b2.srcAccessMask = VK_ACCESS_2_SHADER_READ_BIT;
	b2.dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
	b2.dstAccessMask = VK_ACCESS_2_SHADER_WRITE_BIT;

	VkDependencyInfo depInfo2 {};
	depInfo2.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
	depInfo2.pNext = nullptr;
	depInfo2.imageMemoryBarrierCount = 0;
	depInfo2.pImageMemoryBarriers = nullptr;
	depInfo2.bufferMemoryBarrierCount = 0;
	depInfo2.pBufferMemoryBarriers = nullptr;
	depInfo2.memoryBarrierCount = 1;
	depInfo2.pMemoryBarriers = &b2;

	vkCmdSetEvent2(cmdBufferTmp, GFX.ssrData.prevEvents[currentFrame], &depInfo2);*/

//	fullBarrier(cmdBufferTmp);
}

/*
void CommandBuffers::copyToNextFrame(uint32 currentFrame)
{
	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];
	uint32 nextFrame = (currentFrame + 1) % GFX.maxFramesInFlight;

	VkImageMemoryBarrier2 b[2];
	b[0] = {};
	b[0].sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR;
	b[0].pNext = nullptr;
	b[0].srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
	b[0].srcAccessMask = VK_ACCESS_2_SHADER_WRITE_BIT;
	b[0].dstStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
	b[0].dstAccessMask = VK_ACCESS_2_TRANSFER_READ_BIT;
	b[0].image = GFX.mainShading.images[currentFrame];
	b[0].newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
	b[0].oldLayout = VK_IMAGE_LAYOUT_GENERAL;
	b[0].subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	b[0].subresourceRange.layerCount = 1;
	b[0].subresourceRange.baseArrayLayer = 0;
	b[0].subresourceRange.levelCount = 1;
	b[0].subresourceRange.baseMipLevel = 0;
	
	b[1] = {};
	b[1].sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR;
	b[1].pNext = nullptr;
	b[1].srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
	b[1].srcAccessMask = VK_ACCESS_2_SHADER_READ_BIT;
	b[1].dstStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
	b[1].dstAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
	b[1].image = GFX.ssrData.prevImages[nextFrame];
	b[1].newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	b[1].oldLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	b[1].subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	b[1].subresourceRange.layerCount = 1;
	b[1].subresourceRange.baseArrayLayer = 0;
	b[1].subresourceRange.levelCount = 1;
	b[1].subresourceRange.baseMipLevel = 0;

	VkDependencyInfo depInfo {};
	depInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
	depInfo.pNext = nullptr;
	depInfo.imageMemoryBarrierCount = 2;
	depInfo.pImageMemoryBarriers = b;
	depInfo.bufferMemoryBarrierCount = 0;
	depInfo.pBufferMemoryBarriers = nullptr;
	depInfo.memoryBarrierCount = 0;
	depInfo.pMemoryBarriers = nullptr;

	vkCmdPipelineBarrier2(cmdBufferTmp, &depInfo);

	VkImageCopy imgCopy {};
	imgCopy.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	imgCopy.srcSubresource.layerCount = 1;
	imgCopy.srcSubresource.baseArrayLayer = 0;
	imgCopy.srcSubresource.mipLevel = 0;
	imgCopy.dstSubresource = imgCopy.srcSubresource;
	imgCopy.extent = GFX.internalRes;

	vkCmdCopyImage(cmdBufferTmp, GFX.mainShading.images[currentFrame], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			GFX.ssrData.prevImages[nextFrame], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &imgCopy);

	b[0].srcStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
	b[0].srcAccessMask = VK_ACCESS_2_TRANSFER_READ_BIT;
	b[0].dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
	b[0].dstAccessMask = VK_ACCESS_2_SHADER_WRITE_BIT;
	b[0].newLayout = VK_IMAGE_LAYOUT_GENERAL;
	b[0].oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;

	b[1].srcStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT;
	b[1].srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT;
	b[1].dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
	b[1].dstAccessMask = VK_ACCESS_2_SHADER_READ_BIT;
	b[1].newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	b[1].oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

	vkCmdPipelineBarrier2(cmdBufferTmp, &depInfo);

}*/

void CommandBuffers::updateBloomCommands(uint32 currentFrame, ViewportData& vp)
{
	// Aim for a final mip size >= 8 & <= 16
	int mipCount = log2(vp.height / 8);
	assert(mipCount <= GFX.bloomData.mipCount && "Viewpoort bloom mip levels exceed maximum mip count");

	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	if (!renderSettings.enableBloom) {
		return;
	}

	vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.bloomData.pipeline);

	BloomData::PushConstData pushConst;

	// Full image resolution is used to calculate the correct uv coordinates
	pushConst.outputResolution = ivec2{(int)GFX.internalRes.width, (int)GFX.internalRes.height};

	// Viewport res to determine group counts
	ivec2 scaledRes = ivec2(vp.width, vp.height);

	pushConst.sampleScale = 2.0f;
	pushConst.intensity = renderSettings.bloomIntensity;

	VkMemoryBarrier2 mb {};
	mb.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER_2;
	mb.pNext = nullptr;
	mb.srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
	mb.srcAccessMask = VK_ACCESS_2_SHADER_READ_BIT | VK_ACCESS_2_SHADER_WRITE_BIT;
	mb.dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
	mb.dstAccessMask = VK_ACCESS_2_SHADER_READ_BIT | VK_ACCESS_2_SHADER_WRITE_BIT;

	VkDependencyInfo depInfo {};
	depInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
	depInfo.pNext = nullptr;
	depInfo.bufferMemoryBarrierCount = 0;
	depInfo.pBufferMemoryBarriers = nullptr;
	depInfo.imageMemoryBarrierCount = 0;
	depInfo.pImageMemoryBarriers = nullptr;
	depInfo.memoryBarrierCount = 1;
	depInfo.pMemoryBarriers = &mb;

	// Downsample passes
	for (int i = 0; i < mipCount; ++i) {
		pushConst.outputResolution /= 2;
		int groupCountX = std::ceil((float)scaledRes.x / 8);
		int groupCountY = std::ceil((float)scaledRes.y / 8);

		pushConst.inTexelSize = vec2(1.0f / pushConst.outputResolution.x, 
				1.0f / pushConst.outputResolution.y);

		if (i == 0) {
			// Image source
			pushConst.mode = 0;
			vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.bloomData.layout,
					0, 1, &GFX.bloomData.srcDescriptors[currentFrame], 0, nullptr);
		} else {
			// Mip chain
			pushConst.mode = 1;
			vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.bloomData.layout,
					0, 1, &GFX.bloomData.inDescriptors[currentFrame][i - 1], 0, nullptr);
		}

		vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.bloomData.layout,
				1, 1, &GFX.bloomData.outDescriptors[currentFrame][i], 0, nullptr);

		vkCmdPushConstants(cmdBufferTmp, GFX.bloomData.layout, VK_SHADER_STAGE_COMPUTE_BIT, 
				0, sizeof(BloomData::PushConstData), &pushConst);

		vkCmdDispatch(cmdBufferTmp, groupCountX, groupCountY, 1);
		vkCmdPipelineBarrier2(cmdBufferTmp, &depInfo);
	}

	// Upsample
	ivec2 targetRes = ivec2{(int)GFX.internalRes.width, (int)GFX.internalRes.height};
	scaledRes = ivec2(vp.width, vp.height);

	for (int i =  mipCount - 1; i >= 0; --i) {
		float resScale = pow(0.5, i);
		pushConst.outputResolution = ivec2(targetRes.x * resScale, targetRes.y * resScale);
		int groupCountX = std::ceil((float)scaledRes.x / 8);
		int groupCountY = std::ceil((float)scaledRes.y / 8);

		pushConst.inTexelSize = vec2(1.0f / pushConst.outputResolution.x, 
				1.0f / pushConst.outputResolution.y);

		pushConst.mode = 2;
		vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.bloomData.layout,
					0, 1, &GFX.bloomData.inDescriptors[currentFrame][i], 0, nullptr);

		if (i == 0) {
			// Final image
			pushConst.mode = 2;
			vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.bloomData.layout,
				1, 1, &GFX.bloomData.dstDescriptors[currentFrame], 0, nullptr);		
		} else {
			// Mip chain
			pushConst.mode = 3;
			vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.bloomData.layout,
					1, 1, &GFX.bloomData.outDescriptors[currentFrame][i - 1], 0, nullptr);
		}

		vkCmdPushConstants(cmdBufferTmp, GFX.bloomData.layout, VK_SHADER_STAGE_COMPUTE_BIT, 
				0, sizeof(BloomData::PushConstData), &pushConst);

		vkCmdDispatch(cmdBufferTmp, groupCountX, groupCountY, 1);	
		vkCmdPipelineBarrier2(cmdBufferTmp, &depInfo);
	}
}

void CommandBuffers::copySwapchainImage(uint32 currentFrame, uint32 currentImage)
{
	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	// Transition swapchain image to transfer dst
	VkImageMemoryBarrier2KHR imgBarrier[2];
	imgBarrier[0] = {};
	imgBarrier[0].sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR;
	imgBarrier[0].pNext = nullptr;
	imgBarrier[0].srcStageMask = VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT;
	imgBarrier[0].srcAccessMask = 0;
	imgBarrier[0].dstStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
	imgBarrier[0].dstAccessMask = VK_ACCESS_2_TRANSFER_READ_BIT | 
		VK_ACCESS_2_TRANSFER_WRITE_BIT;
	imgBarrier[0].image = GFX.swapchainImages[currentImage];
	imgBarrier[0].oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	imgBarrier[0].newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	imgBarrier[0].subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	imgBarrier[0].subresourceRange.layerCount = 1;
	imgBarrier[0].subresourceRange.baseArrayLayer = 0;
	imgBarrier[0].subresourceRange.levelCount = 1;
	imgBarrier[0].subresourceRange.baseMipLevel = 0;

	/*
	imgBarrier[1] = {};
	imgBarrier[1].sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR;
	imgBarrier[1].pNext = nullptr;
	imgBarrier[1].srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
	imgBarrier[1].srcAccessMask = VK_ACCESS_2_SHADER_READ_BIT | VK_ACCESS_2_SHADER_WRITE_BIT ;
	imgBarrier[1].dstStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
	imgBarrier[1].dstAccessMask = VK_ACCESS_2_TRANSFER_READ_BIT | 
		VK_ACCESS_2_TRANSFER_WRITE_BIT;
	imgBarrier[1].image = GFX.mainShading.images[currentFrame];
	imgBarrier[1].oldLayout = VK_IMAGE_LAYOUT_GENERAL;
	imgBarrier[1].newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
	imgBarrier[1].subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	imgBarrier[1].subresourceRange.layerCount = 1;
	imgBarrier[1].subresourceRange.baseArrayLayer = 0;
	imgBarrier[1].subresourceRange.levelCount = 1;
	imgBarrier[1].subresourceRange.baseMipLevel = 0;*/

	VkDependencyInfo depInfo{};
	depInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
	depInfo.imageMemoryBarrierCount = 1;
	depInfo.pImageMemoryBarriers = imgBarrier;

	vkCmdPipelineBarrier2(cmdBufferTmp, &depInfo);

	VkImageBlit copyImage{};
	copyImage.srcOffsets[0] = { 0, 0, 0 };
	copyImage.srcOffsets[1] = { (int)GFX.internalRes.width, (int)GFX.internalRes.height, 1 };
	copyImage.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	copyImage.srcSubresource.mipLevel = 0;
	copyImage.srcSubresource.baseArrayLayer = 0;
	copyImage.srcSubresource.layerCount = 1;
	copyImage.dstOffsets[0] = { 0, 0, 0 };
	copyImage.dstOffsets[1] = { (int)GFX.surfaceExtent.width, (int)GFX.surfaceExtent.height, 1 };
	copyImage.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	copyImage.dstSubresource.mipLevel = 0;;
	copyImage.dstSubresource.baseArrayLayer = 0;
	copyImage.dstSubresource.layerCount = 1;

	vkCmdBlitImage(cmdBufferTmp, GFX.mainShading.images[currentFrame],
		VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
		GFX.swapchainImages[currentImage],
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		1, &copyImage, VK_FILTER_LINEAR);
}

void CommandBuffers::copyTextureImage(uint32 currentFrame, ViewportData& vp)
{

	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	MaterialManager::Texture* texture = materialManager.getImage(vp.targetImage);

	VkImageMemoryBarrier2KHR imgBarrier[1];
	imgBarrier[0] = {};
	imgBarrier[0].sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR;
	imgBarrier[0].pNext = nullptr;
	imgBarrier[0].srcStageMask = VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT;
	imgBarrier[0].srcAccessMask = 0;
	imgBarrier[0].dstStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
	imgBarrier[0].dstAccessMask = VK_ACCESS_2_TRANSFER_READ_BIT | 
		VK_ACCESS_2_TRANSFER_WRITE_BIT;
	imgBarrier[0].image = texture->texture;
	imgBarrier[0].oldLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	imgBarrier[0].newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	imgBarrier[0].subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	imgBarrier[0].subresourceRange.layerCount = 1;
	imgBarrier[0].subresourceRange.baseArrayLayer = 0;
	imgBarrier[0].subresourceRange.levelCount = 1;
	imgBarrier[0].subresourceRange.baseMipLevel = 0;

	VkDependencyInfo depInfo{};
	depInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
	depInfo.imageMemoryBarrierCount = 1;
	depInfo.pImageMemoryBarriers = imgBarrier;

	vkCmdPipelineBarrier2(cmdBufferTmp, &depInfo);

	VkImageBlit copyImage{};
	copyImage.srcOffsets[0] = { 0, 0, 0 };
	copyImage.srcOffsets[1] = { vp.width, vp.height, 1 };
	copyImage.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	copyImage.srcSubresource.mipLevel = 0;
	copyImage.srcSubresource.baseArrayLayer = 0;
	copyImage.srcSubresource.layerCount = 1;
	copyImage.dstOffsets[0] = { 0, 0, 0 };
	copyImage.dstOffsets[1] = { (int)texture->width, (int)texture->height, 1 };
	copyImage.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	copyImage.dstSubresource.mipLevel = 0;;
	copyImage.dstSubresource.baseArrayLayer = 0;
	copyImage.dstSubresource.layerCount = 1;

	vkCmdBlitImage(cmdBufferTmp, GFX.mainShading.images[currentFrame], 
		VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, texture->texture,
		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		1, &copyImage, VK_FILTER_LINEAR);

	imgBarrier[0].dstStageMask = VK_PIPELINE_STAGE_2_BOTTOM_OF_PIPE_BIT;
	imgBarrier[0].dstAccessMask = 0;
	imgBarrier[0].srcStageMask = VK_PIPELINE_STAGE_2_BLIT_BIT;
	imgBarrier[0].srcAccessMask = VK_ACCESS_2_TRANSFER_READ_BIT | 
		VK_ACCESS_2_TRANSFER_WRITE_BIT;
	imgBarrier[0].image = texture->texture;
	imgBarrier[0].newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	imgBarrier[0].oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

	vkCmdPipelineBarrier2(cmdBufferTmp, &depInfo);
}

void CommandBuffers::updatePostProcessCommands(uint32 currentFrame, ViewportData& vp)
{
	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	// Post process
	vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.postData.pipeline);

	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.postData.layout, 
			0, 1, &GFX.postData.descriptors[currentFrame], 0, nullptr);
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.postData.layout, 
			1, 1, &GFX.cameraDescriptors[currentFrame], 0, nullptr);

	PostData::PushConstData pushConst = {
		.width = vp.width,
		.height = vp.height
	};

	vkCmdPushConstants(cmdBufferTmp, GFX.postData.layout, VK_SHADER_STAGE_COMPUTE_BIT,
			0, sizeof(PostData::PushConstData), &pushConst);

	vkCmdDispatch(cmdBufferTmp, GFX.postData.groupCountX, GFX.postData.groupCountY, 1);


}

void CommandBuffers::updateDebugVisualizationCommands(uint32 currentFrame, ViewportData& vp)
{
	InstanceManager::TransferDataDst& tData = InstanceManager::getTransferData();
	
	DebugLines::updateVertexBuffer(tData.debugLines);
	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	VkRenderPassBeginInfo renderPassInfo {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderArea.offset = {0, 0};
	renderPassInfo.renderArea.extent = VkExtent2D{ (uint32)vp.width, (uint32)vp.height};
	renderPassInfo.renderPass = GFX.debugLine.renderPass;
	renderPassInfo.framebuffer = vp.debugFramebuffers[currentFrame];
	renderPassInfo.clearValueCount = 0;
	renderPassInfo.pClearValues = nullptr;

	vkCmdBeginRenderPass(cmdBufferTmp, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

	vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.debugLine.pipeline);

	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.debugLine.layout, 
				0, 1, &GFX.cameraDescriptors[currentFrame], 0, nullptr);

	VkDeviceSize offsets[] = {0};
	vkCmdBindVertexBuffers(cmdBufferTmp, 0, 1, &GFX.debugLine.vertexBuffer, offsets);

	vkCmdDraw(cmdBufferTmp, GFX.debugLine.actualCount, 1, 0, 0);

	vkCmdEndRenderPass(cmdBufferTmp);
}

void CommandBuffers::updateTextCommands(uint32 currentFrame, ViewportData& vp)
{
	if (!FontRendering::textObjects.size()) {
		return;
	}

	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	VkRenderPassBeginInfo renderPassInfo {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderArea.offset = {0, 0};
	renderPassInfo.renderArea.extent = VkExtent2D{ (uint32)vp.width, (uint32)vp.height};
	renderPassInfo.renderPass = GFX.fontData.renderPass;
	renderPassInfo.framebuffer = vp.debugFramebuffers[currentFrame]; // TODO: Different framebuffer?
	renderPassInfo.clearValueCount = 0;
	renderPassInfo.pClearValues = nullptr;

	vkCmdBeginRenderPass(cmdBufferTmp, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

	vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.fontData.pipeline);

	VkViewport viewport = {
		.x = 0,
		.y = 0,
		.width = (float)vp.width,
		.height = (float)vp.height,
		.minDepth = 1.0f,
		.maxDepth = 0.0f
	};
	vkCmdSetViewport(cmdBufferTmp, 0, 1, &viewport);

	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_GRAPHICS, GFX.fontData.layout, 
				0, 1, &GFX.textureArraySet, 0, nullptr);

	InstanceManager::TransferDataDst& tData = InstanceManager::getTransferData();
	for (auto& textInstance : tData.textTransforms) {
		if (textInstance.first == -1) {
			continue;
		}

		TextObject& text = FontRendering::textObjects[textInstance.first];
		TextFont& font = FontRendering::fonts[text.handle];

		if (text.deleted)
			continue;

		VkDeviceSize offsets[] = {0};
		vkCmdBindVertexBuffers(cmdBufferTmp, 0, 1, &text.vertexBuffer, offsets);
		vkCmdBindIndexBuffer(cmdBufferTmp, text.indexBuffer, 0, VK_INDEX_TYPE_UINT32);

		mat4 MVP = vp.camProps.proj * vp.camProps.view * textInstance.second;
		vkCmdPushConstants(cmdBufferTmp, GFX.fontData.layout, VK_SHADER_STAGE_VERTEX_BIT, 
				0, sizeof(mat4), &MVP);
		vkCmdPushConstants(cmdBufferTmp, GFX.fontData.layout, VK_SHADER_STAGE_FRAGMENT_BIT, 
				sizeof(mat4), sizeof(int), &font.atlasImage);

		vkCmdDrawIndexed(cmdBufferTmp, text.indexCount, 1, 0, 0, 0);
	}

	vkCmdEndRenderPass(cmdBufferTmp);
}

void CommandBuffers::updateOutlineCommands(uint32 currentFrame, ViewportData& vp)
{
	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	vkCmdBindPipeline(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.outlineData.pipeline);

	VkImageMemoryBarrier2KHR b {};
	b.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2_KHR;
	b.pNext = nullptr;
	b.dstStageMask = VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT_KHR;
	b.dstAccessMask = VK_ACCESS_2_SHADER_READ_BIT_KHR;
	b.srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT_KHR;
	b.srcAccessMask = VK_ACCESS_2_SHADER_READ_BIT_KHR;
	b.image = GFX.mainShading.images[currentFrame];
	b.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	b.oldLayout = VK_IMAGE_LAYOUT_GENERAL;
	b.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	b.subresourceRange.layerCount = 1;
	b.subresourceRange.baseArrayLayer = 0;
	b.subresourceRange.levelCount = 1;
	b.subresourceRange.baseMipLevel = 0;

	VkDependencyInfo depInfo {};
	depInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
	depInfo.pNext = nullptr;
	depInfo.imageMemoryBarrierCount = 0;
	depInfo.pImageMemoryBarriers = nullptr;
	depInfo.bufferMemoryBarrierCount = 0;
	depInfo.pBufferMemoryBarriers = nullptr;
	depInfo.memoryBarrierCount = 0;
	depInfo.pMemoryBarriers = nullptr;

	// Outline set
	vkCmdBindDescriptorSets(cmdBufferTmp, VK_PIPELINE_BIND_POINT_COMPUTE, GFX.outlineData.layout, 0, 1,
			&GFX.outlineData.descriptors[currentFrame], 0, nullptr);

	Outlines::PushConstantData pushConst;
	
	pushConst.colour = vec4(0.91, 0.57, 0.2, 1.0);
	pushConst.radius = 0.003f;
	pushConst.fadeoutRadius = 0;
	pushConst.width = vp.width;
	pushConst.height = vp.height;
	pushConst.ndcScale = vec2{(float)vp.width / GFX.internalRes.width, 
		(float)vp.height/ GFX.internalRes.height};

	// Prevent outline disappearing at low resolutions
	float texel = 1.0f / GFX.internalRes.height;
	pushConst.radius = std::max(pushConst.radius, texel * 1.1f);
	// 1px antialiasing
	pushConst.fadeoutRadius = std::max(pushConst.fadeoutRadius, texel);
	
	int radiusPx = GFX.internalRes.width * (pushConst.radius + pushConst.fadeoutRadius);
	int sampleCount = ceil(log2(radiusPx));
	pushConst.numSamples = sampleCount;
	pushConst.currentSample = 0;

	for (int i = 0; i < sampleCount + 2; ++i) {
		// Wait for previous compute shader invocation
		if (i != 0) {
			VkMemoryBarrier2 mb {};
			mb.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER_2;
			mb.pNext = nullptr;
			mb.srcStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
			mb.srcAccessMask = VK_ACCESS_2_SHADER_WRITE_BIT | VK_ACCESS_2_SHADER_READ_BIT;
			mb.dstStageMask = VK_PIPELINE_STAGE_2_COMPUTE_SHADER_BIT;
			mb.dstAccessMask = VK_ACCESS_2_SHADER_WRITE_BIT | VK_ACCESS_2_SHADER_READ_BIT;

			depInfo.imageMemoryBarrierCount = 0;
			depInfo.pImageMemoryBarriers = nullptr;
			depInfo.memoryBarrierCount = 1;
			depInfo.pMemoryBarriers = &mb;

			vkCmdPipelineBarrier2(cmdBufferTmp, &depInfo);
		}

		vkCmdPushConstants(cmdBufferTmp, GFX.outlineData.layout, VK_SHADER_STAGE_COMPUTE_BIT,
				0, sizeof(Outlines::PushConstantData), &pushConst);
		vkCmdDispatch(cmdBufferTmp, GFX.outlineData.groupCountX, GFX.outlineData.groupCountY, 1);
		pushConst.currentSample++;
	}

	depInfo.imageMemoryBarrierCount = 1;
	depInfo.pImageMemoryBarriers = &b;
	depInfo.memoryBarrierCount = 0;
	depInfo.pMemoryBarriers = nullptr;
	vkCmdPipelineBarrier2(cmdBufferTmp, &depInfo);
}

void CommandBuffers::updateSwapchainCommands(uint32 currentFrame, uint32 currentImage)
{
	VkCommandBuffer cmdBufferTmp = GFX.shadingCmdBuffers[currentFrame];

	// Copy image to swapchain
	// Blit seems to produce a nicer result than linear upsampling in the compute shader
	// The result was generally blurry and fireflies filled a 2x2 pixel area
	copySwapchainImage(currentFrame, currentImage);

	// Draw user interface
	VkRenderPassBeginInfo renderPassInfo {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderArea.offset = {0, 0};
	renderPassInfo.renderArea.extent = VkExtent2D{ GFX.surfaceExtent.width, GFX.surfaceExtent.height };
	renderPassInfo.renderPass = GFX.swapchainRenderPass;
	renderPassInfo.framebuffer = GFX.swapchainFramebuffers[currentImage];
	renderPassInfo.clearValueCount = 0;
	renderPassInfo.pClearValues = nullptr;
	vkCmdBeginRenderPass(cmdBufferTmp, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

	// Editor
#ifndef _DISABLE_EDITOR
	if (QGlobals.drawEditor) {	
		Editor::getDrawCommands(cmdBufferTmp);
	}
#endif

	vkCmdEndRenderPass(cmdBufferTmp);
}

void CommandBuffers::meshDrawingCommands(VkCommandBuffer cmdBuffer, VkPipelineLayout pipeline, 
		bool depthOnly, bool mainPass, ViewportData& vp)
{
	InstanceManager::TransferDataDst& tData = InstanceManager::getTransferData();

	// Set viewport state for non shadow passes
	if (mainPass) {
		VkViewport viewport = {
			.x = 0,
			.y = 0,
			.width = (float)vp.width,
			.height = (float)vp.height,
			.minDepth = 1.0f,
			.maxDepth = 0.0f
		};
		vkCmdSetViewport(cmdBuffer, 0, 1, &viewport);
	}

	for (MeshManager::Mesh& mesh : meshManager.meshes) {
		if (tData.instances.size() <= mesh.meshIndex) 
			continue;

		drawMesh(mesh, cmdBuffer, tData.instances[mesh.meshIndex], pipeline, depthOnly, mainPass);
	}
}

void CommandBuffers::drawMesh(MeshManager::Mesh& mesh, VkCommandBuffer cmdBuffer,
		std::vector<InstanceManager::MeshInstance>& instances, 
		VkPipelineLayout pipelineLayout, bool depthOnly, bool mainPass)
{
	if ( mesh.meshIndex < 0) return;

	VkDeviceSize offsets[] = {0};
	vkCmdBindVertexBuffers(cmdBuffer, 0, 1, &mesh.vertexBuffer, offsets);
	vkCmdBindIndexBuffer(cmdBuffer, mesh.indexBuffer, 0, VK_INDEX_TYPE_UINT32);

	int lastMaterial = -1;
	for (auto& instance : instances) {

		vkCmdPushConstants(cmdBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT,
			0, sizeof(PushConstantData), &instance.transform);

		if (!depthOnly) {
			vkCmdPushConstants(cmdBuffer, pipelineLayout, VK_SHADER_STAGE_FRAGMENT_BIT,
				sizeof(PushConstantData), sizeof(InstanceManager::MaterialProps),
				&instance.materialProps);
		} else {
			vkCmdPushConstants(cmdBuffer, pipelineLayout, VK_SHADER_STAGE_FRAGMENT_BIT,
				sizeof(PushConstantData), sizeof(uint32), &instance.materialProps.albedoID);
		}

		if (depthOnly && mainPass && instance.materialOptions.selected)
			vkCmdSetStencilTestEnable(cmdBuffer, VK_TRUE);
		else if (depthOnly && mainPass)
			vkCmdSetStencilTestEnable(cmdBuffer, VK_FALSE);

		if (mainPass && instance.materialOptions.doubleSided)
			vkCmdSetCullMode(cmdBuffer, VK_CULL_MODE_NONE);

		vkCmdDrawIndexed(cmdBuffer, mesh.indexCount, 1, 0, 0, 0);

		if (mainPass && instance.materialOptions.doubleSided)
			vkCmdSetCullMode(cmdBuffer, VK_CULL_MODE_BACK_BIT);

		if (depthOnly && mainPass && instance.materialOptions.selected)
			vkCmdSetStencilTestEnable(cmdBuffer, VK_FALSE);
	}
}

void CommandBuffers::cleanUp()
{
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vkDestroyCommandPool(GFX.device, GFX.commandPools[i], nullptr);
	}
	vkDestroyCommandPool(GFX.device, GFX.singleTimeCmdPool, nullptr);
	vkDestroyCommandPool(GFX.device, GFX.transferCmdPool, nullptr);
}

VkCommandBuffer CommandBuffers::beginSingleTimeCmd()
{
	vkWaitForFences(GFX.device, 1, &GFX.singleTimeCmdFence, VK_TRUE, UINT64_MAX);

	VkCommandBufferBeginInfo beginInfo {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(GFX.singleTimeCommands, &beginInfo);

	return GFX.singleTimeCommands;
}

void CommandBuffers::endSingleTimeCmd()
{
	vkEndCommandBuffer(GFX.singleTimeCommands);

	VkSubmitInfo submitInfo {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &GFX.singleTimeCommands;

	vkResetFences(GFX.device, 1, &GFX.singleTimeCmdFence);
	vkQueueSubmit(GFX.graphicsQueue, 1, &submitInfo, GFX.singleTimeCmdFence);
	vkWaitForFences(GFX.device, 1, &GFX.singleTimeCmdFence, VK_TRUE, UINT64_MAX);
}

VkCommandBuffer CommandBuffers::beginTransferCmd()
{
	VkCommandBufferBeginInfo beginInfo {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(GFX.transferCmdBuffer, &beginInfo);

	return GFX.transferCmdBuffer;
}

void CommandBuffers::endTransferCmd()
{
	vkEndCommandBuffer(GFX.transferCmdBuffer);

	VkSubmitInfo submitInfo {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &GFX.transferCmdBuffer;

	vkQueueWaitIdle(GFX.transferQueue);
	vkQueueSubmit(GFX.transferQueue, 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(GFX.transferQueue);
}

void CommandBuffers::fullBarrier(VkCommandBuffer cmdBuffer)
{
	VkMemoryBarrier2 memoryBarrier = {};
	memoryBarrier.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER_2;
	memoryBarrier.srcStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT_KHR;
	memoryBarrier.srcAccessMask = VK_ACCESS_2_MEMORY_READ_BIT_KHR |
	VK_ACCESS_2_MEMORY_WRITE_BIT_KHR;
	memoryBarrier.dstStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT_KHR;
	memoryBarrier.dstAccessMask = VK_ACCESS_2_MEMORY_READ_BIT_KHR |
	VK_ACCESS_2_MEMORY_WRITE_BIT_KHR;

	VkDependencyInfo dependencyInfo = {};
	dependencyInfo.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO;
	dependencyInfo.memoryBarrierCount = 1;
	dependencyInfo.pMemoryBarriers = &memoryBarrier;

	vkCmdPipelineBarrier2(cmdBuffer, &dependencyInfo);
}
