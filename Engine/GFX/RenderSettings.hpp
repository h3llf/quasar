#ifndef QUASAR_RENDER_SETTINGS_H
#define QUASAR_RENDER_SETTINGS_H

#include <string>
#include <atomic>
#include <vector>

#include "Core/Console.hpp"

class RenderSettings
{
public:
	void setupCVars(Console& console);

	// Debug
	std::atomic<uint32_t> deugLineDepthTest = true; 
	std::atomic<uint32_t> wireframe = false;
	std::atomic<uint32_t> showCascadeSplits = false;
	std::atomic<uint32_t> aoOnlyView = false;
	std::atomic<uint32_t> lightingHeatmap = false;
	std::atomic<uint32_t> whiteWorld = false;

	// Physical devices
	std::vector<std::string> deviceNames; // TODO: Thread safety
	std::atomic<uint32_t> selectedDevice = 0;

	// Display
	std::atomic<float> resolutionScale = 1.0f;
	std::atomic<uint32_t> vsync = false;

	std::atomic<uint32_t> width;
	std::atomic<uint32_t> height;

	// Shadows
	std::atomic<uint32_t> omniShadowRes = 1024;
	std::atomic<int> omniShadowCount = 2;
	std::atomic<uint32_t> cascadedShadowRes = 2048;
	std::atomic<uint32_t> numberCascades = 5;
	std::atomic<float> cascadeDist = 100.0f;
	std::atomic<float> lambda = 0.65f;
	std::atomic<uint32_t> numPCFSamples = 12;

	// Ambient occlusion
	std::atomic<uint32_t> enableGTAO = true;
	std::atomic<float> aoResolutionScale = 0.5f;

	// Bloom
	std::atomic<uint32_t> enableBloom = true;
	std::atomic<float> bloomIntensity = 0.04f;
};

extern RenderSettings renderSettings;

#endif //QUASAR_RENDER_SETTINGS_H
