#ifndef QUASAR_SWAPCHAIN_H
#define QUASAR_SWAPCHAIN_H

#include "RendererData.hpp"

class Swapchain
{
public:
	static void initSwapchain();
	static void createImages();
	static void transitionImages();
	static void cleanupSwapchain();
};

#endif //QUASAR_SWAPCHAIN_H
