#include "Window.hpp"
#include <cstdint>
#include <vulkan/vulkan_core.h>

using namespace quasar;

bool Window::createWindow()
{
	int x = QUASAR.windowSizeX;
	int y = QUASAR.windowSizeY;

	glfwInit();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

	GFX.window = glfwCreateWindow(x, y, QUASAR.appName, 0, 0);
	
	if(GFX.window)
		return true;

	return false;
}

void Window::createSurface()
{
	VK_CHECK(glfwCreateWindowSurface(GFX.instance, GFX.window, nullptr, &GFX.surface), 
			"Failed to create surface");

	VkBool32 supported = false;
	vkGetPhysicalDeviceSurfaceSupportKHR(GFX.physicalDevice, GFX.graphicsQueueIndex, GFX.surface, &supported);

	if (!supported) {
		Log::fatal("[Vulkan] Surface not supported");
	}

	GFX.surfaceExtent = getWindowExtent();
}

void Window::querySurfaceSupport()
{
	//Capabilities and size
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(GFX.physicalDevice, GFX.surface, &GFX.surfaceCapabilites);

	//Formats
	uint32 formatCount = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(GFX.physicalDevice, GFX.surface, &formatCount, nullptr);

	if (!formatCount) {
		Log::fatal("[Vulkan] No surface formats avaliable");
	}

	GFX.formats.resize(formatCount);
	vkGetPhysicalDeviceSurfaceFormatsKHR(GFX.physicalDevice, GFX.surface, &formatCount, GFX.formats.data());


	// UNORM format used to avoid double gamma correction
	GFX.surfaceFormat = GFX.formats.at(0);

	for (const VkSurfaceFormatKHR& avaliableFormat : GFX.formats) {
		if ((avaliableFormat.colorSpace == VK_COLORSPACE_SRGB_NONLINEAR_KHR) &&
				avaliableFormat.format == VK_FORMAT_B8G8R8A8_UNORM) {
			GFX.surfaceFormat = avaliableFormat;
			return;
		}
	}

	Log::error("Surface format not found");

}

VkExtent2D Window::getWindowExtent()
{
	if (GFX.surfaceCapabilites.currentExtent.width != UINT32_MAX) {
		return GFX.surfaceCapabilites.currentExtent;
	} else {
		int width, height;
		glfwGetFramebufferSize(GFX.window, &width, &height);

		VkExtent2D windowExtent = {
			static_cast<uint32>(width),
			static_cast<uint32>(height)
		};

		windowExtent.width = std::max(GFX.surfaceCapabilites.minImageExtent.width, 
				std::min(GFX.surfaceCapabilites.maxImageExtent.width, windowExtent.width));

		windowExtent.height = std::max(GFX.surfaceCapabilites.minImageExtent.height, 
				std::min(GFX.surfaceCapabilites.maxImageExtent.height, windowExtent.height));

		Log::out("ext: ", windowExtent.width, " min ", GFX.surfaceCapabilites.minImageExtent.width,
				" max ", GFX.surfaceCapabilites.maxImageExtent.width);

		return windowExtent;
	}
}
