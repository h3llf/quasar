#ifndef RENDER_TYPES_H
#define RENDER_TYPES_H

// TODO: Convert code to using these handles

#define WINDOW_IMAGE_HANDLE -1

// Renderer object handles
typedef int ViewportHandle;
typedef int ImageHandle;
typedef int MeshHandle;
typedef int FontHandle;
typedef int TextHandle;

#endif //RENDER_TYPES_H
