#ifndef QUASAR_DESCRIPTORS_H
#define QUASAR_DESCRIPTORS_H

#include "RendererData.hpp"

//https://github.com/vblanco20-1/vulkan-guide/blob/engine/extra-engine/vk_descriptors.h
class DescriptorBuilder
{
public:
	void bindBuffer(uint32 binding, VkDescriptorType type, VkDescriptorBufferInfo* info);

	void bindImage(uint32 binding, VkDescriptorType type, VkDescriptorImageInfo* imageInfo);

	void createLayoutBinding(uint32 binding, VkDescriptorType type, 
			VkShaderStageFlags stageFlags);

	void createLayout(VkDescriptorSetLayout* pLayout);
	void build(VkDescriptorSet* pSet, VkDescriptorSetLayout layout, VkDescriptorPool pool);

	std::vector<VkWriteDescriptorSet> writes;
	std::vector<VkDescriptorSetLayoutBinding> bindings;
};

class Descriptors
{
public:
	static void createDescriptorSetLayouts();

	//Creates a set with bindings for all textures and the uniform for a material
	static void updateTextureDescriptor(uint32 index, VkImageView imageView);

	// TODO: Set non arbitrary values for pool sizes or make/use an allocator
	static void createPersistantDescPool();
	static void createDescriptorPool();

	//Creates uniforms which will be used for the entire duration of the program:
	//Changes regularly so one is allocated per frame in flight
	//TODO: change to per swapchain image
	static void createUniformBuffers();
	//static void tmpUpdateUniformBuffers(uint32 currentImage);
	static void updateCamera(uint32 currentImage, ViewportHandle viewport);

	//Init descriptor sets that will only change once per frame
	static void initFrameDescriptor();
	static void updateFrameDescriptor();

	static void destroyUniforms();
};

#endif //QUASAR_DESCIPTORS_H
