#ifndef QUASAR_IMAGECREATION_H
#define QUASAR_IMAGECREATION_H

#include "RendererData.hpp"
#include <vulkan/vulkan_core.h>

class ImageCreation
{
public:
	static VkImageCreateInfo createImageInfo(VkFormat format, VkImageUsageFlags usage, 
			VkExtent3D extent, uint32 mipLevels = 1);

	static VkImageView createTextureImageView(VkFormat format, VkImage image, uint32 mipLevels);
	static VkImageView createImageView(VkFormat format, 
			VkImage image, 
			VkImageAspectFlags aspect,
			uint32 mipLevels, 
			VkImageViewType type = VK_IMAGE_VIEW_TYPE_2D, 
			uint32 layerCount = 1, 
			uint32 baseLayer = 0,
			uint32 baseMipLevel = 0);

	static void createTextureSampler(VkSampler* sampler, float maxLod);

	static void destroyImage(VkImage, VkImageView);

	static void transitionImageLayout(VkImage image, bool depth, VkImageLayout oldLayout, 
			VkImageLayout newLayout, uint32 layerCount, uint32 mipLevels = 1);

	static void generateMipmaps(VkImage image, int width, int height, uint32 levels);
};

#endif //QUASAR_IMAGECREATION_H
