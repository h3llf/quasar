#include "Swapchain.hpp"
#include "Drawable/MeshManager.hpp"
#include "RenderPass.hpp"
#include "Pipeline.hpp"
#include "CommandBuffers.hpp"
#include "Descriptors.hpp"
#include "Window.hpp"
#include "ImageCreation.hpp"
#include "RenderSettings.hpp"
#include <GLFW/glfw3.h>
#include <vulkan/vulkan_core.h>

void Swapchain::initSwapchain()
{
	Window::querySurfaceSupport();
	GFX.surfaceExtent = Window::getWindowExtent();

	// TODO: Add an option to adjust these
	const uint32 desiredMaxImageCount = 3;

	VkPresentModeKHR desiredPresentMode;
	VkPresentModeKHR fallbackPresentMode;

	if (renderSettings.vsync) {
		desiredPresentMode = VK_PRESENT_MODE_FIFO_RELAXED_KHR;
		fallbackPresentMode = VK_PRESENT_MODE_FIFO_KHR;
	}
	else {
		desiredPresentMode = VK_PRESENT_MODE_MAILBOX_KHR;
		fallbackPresentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
	}

	// Determine number of swapchain images
	uint32 sImageCount = desiredMaxImageCount;
	if (sImageCount > GFX.surfaceCapabilites.maxImageCount)
		sImageCount = GFX.surfaceCapabilites.maxImageCount;

	if (sImageCount < GFX.surfaceCapabilites.minImageCount + 1)
		sImageCount = GFX.surfaceCapabilites.minImageCount + 1;

	// Check avaliable present modes
	VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR; //Always avaliable
	{
		uint32 presentCount;
		vkGetPhysicalDeviceSurfacePresentModesKHR(GFX.physicalDevice, GFX.surface, 
				&presentCount, nullptr);
		std::vector<VkPresentModeKHR> presentModes(presentCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(GFX.physicalDevice, GFX.surface,
				&presentCount, presentModes.data());
	
		for (VkPresentModeKHR mode : presentModes) {
			if (mode == fallbackPresentMode) {
				presentMode = mode;
				continue;
			}

			if (mode == desiredPresentMode) {
				presentMode = mode;
				break;
			}
		}
	}

	VkSwapchainCreateInfoKHR createInfo {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = GFX.surface;
	createInfo.minImageCount = sImageCount;
	createInfo.imageFormat = GFX.surfaceFormat.format;
	createInfo.imageColorSpace = GFX.surfaceFormat.colorSpace;
	createInfo.imageExtent = GFX.surfaceExtent; //Get current surface size
	createInfo.imageArrayLayers = 1; // 2 for sterioscopic rendering
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	createInfo.queueFamilyIndexCount = 0;
	createInfo.pQueueFamilyIndices = nullptr; //Ignored if sharing mode is exclusive
	createInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR; //Primarily a mobile feature
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR; // not sure Used for window transparency????
	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE; //Prevents rendering parts of the image which are not visible
	createInfo.oldSwapchain = VK_NULL_HANDLE; //used for reconstructing swapchain

	VK_CHECK( vkCreateSwapchainKHR(GFX.device, &createInfo, nullptr, &GFX.swapchain), 
			"[Vulkan] Swapchain creation failed");

	RenderPass::createPostRenderPass(&GFX.swapchainRenderPass);
	createImages();

	if (GFX.maxFramesInFlight > sImageCount)
		GFX.maxFramesInFlight = sImageCount;

	GFX.numImages = sImageCount;
}

void Swapchain::createImages()
{
	uint32 sImageCount = 0;

	vkGetSwapchainImagesKHR(GFX.device, GFX.swapchain, &sImageCount, nullptr);
	GFX.swapchainImages.resize(sImageCount);
	vkGetSwapchainImagesKHR(GFX.device, GFX.swapchain, &sImageCount, GFX.swapchainImages.data());

	GFX.swapchainImageViews.resize(sImageCount);
	GFX.swapchainFramebuffers.resize(sImageCount);
	for (uint32 i = 0; i < sImageCount; ++i) {
		VkImageViewCreateInfo createInfo {};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.image = GFX.swapchainImages.at(i);
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		createInfo.format = GFX.surfaceFormat.format;
		createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;

		VK_CHECK( vkCreateImageView( GFX.device, &createInfo, nullptr, 
			&GFX.swapchainImageViews.at(i) ),
			"Failed to create swapchain image");


		std::array<VkImageView, 1> attatchments = {
			GFX.swapchainImageViews[i],
		};

		VkFramebufferCreateInfo fbCreateInfo {};
		fbCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		fbCreateInfo.renderPass = GFX.swapchainRenderPass;
		fbCreateInfo.attachmentCount = attatchments.size();
		fbCreateInfo.pAttachments = attatchments.data();
		fbCreateInfo.width = GFX.surfaceExtent.width;
		fbCreateInfo.height = GFX.surfaceExtent.height;
		fbCreateInfo.layers = 1;

		VK_CHECK( vkCreateFramebuffer(GFX.device, &fbCreateInfo, nullptr, 
			&GFX.swapchainFramebuffers[i]),
			"Failed to create framebuffer");

	}
}

void Swapchain::transitionImages()
{
	for (uint32 i = 0; i < GFX.numImages; ++i) {
		ImageCreation::transitionImageLayout(GFX.swapchainImages[i], false,
				VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, 1);
	}
}

void Swapchain::cleanupSwapchain()
{
	vkDestroyRenderPass(GFX.device, GFX.swapchainRenderPass, nullptr);
	vkDestroySwapchainKHR(GFX.device, GFX.swapchain, nullptr);

	for (uint32 i = 0; i < GFX.numImages; ++i) {
		vkDestroyImageView(GFX.device, GFX.swapchainImageViews[i], nullptr);
		vkDestroyFramebuffer(GFX.device, GFX.swapchainFramebuffers[i], nullptr);
	}
}
