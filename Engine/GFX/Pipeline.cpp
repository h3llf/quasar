#include "Pipeline.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include "Window.hpp"
#include "RendererData.hpp"
#include "RenderSettings.hpp"
#include "Shaders.hpp"
#include "Buffers.hpp"
#include <vulkan/vulkan_core.h>

using namespace quasar;

void Pipeline::initPipelines()
{
	//Depth prepass
	std::array<VkDescriptorSetLayout, 2> layoutsD = {
		GFX.cameraOnlyLayout,
		GFX.textureArrayLayout};

	VkDynamicState state[3] = {VK_DYNAMIC_STATE_CULL_MODE, VK_DYNAMIC_STATE_VIEWPORT,  
		VK_DYNAMIC_STATE_STENCIL_TEST_ENABLE};

	VkPipelineDynamicStateCreateInfo dynState = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
		.pNext = nullptr,
		.flags = 0,
		.dynamicStateCount = 3,
		.pDynamicStates = state
	};

	PipelineBuilder depthBuilder;
	depthBuilder.beginPipeline(&GFX.mainShading.depthPipeline, &GFX.mainShading.depthPipelineLayout, 
		&GFX.mainShading.depthPass);
	depthBuilder.addShaderStage("Shaders/depth_vs.spv", VK_SHADER_STAGE_VERTEX_BIT);
	depthBuilder.addShaderStage("Shaders/depth_fs.spv", VK_SHADER_STAGE_FRAGMENT_BIT);
	depthBuilder.setInputDescriptons(true);
	depthBuilder.setRasterizer(renderSettings.wireframe);
	depthBuilder.setViewportState(GFX.internalRes.width, GFX.internalRes.height);
	depthBuilder.setMultiSampling();
	depthBuilder.setColourBlending();
	depthBuilder.addPushConstantRange(0, sizeof(PushConstantData), VK_SHADER_STAGE_VERTEX_BIT);
	depthBuilder.addPushConstantRange(sizeof(PushConstantData), sizeof(uint32), 
			VK_SHADER_STAGE_FRAGMENT_BIT);
	depthBuilder.setPipelineLayout(layoutsD.data(), layoutsD.size());
	depthBuilder.setDepthStencil(VK_TRUE, VK_TRUE, VK_COMPARE_OP_LESS_OR_EQUAL, true);
	depthBuilder.createGraphicsPipeline(&dynState);

	struct LightingSpecConsts {
		VkBool32 enableAO = (VkBool32)renderSettings.enableGTAO;
		VkBool32 aoOnlyView = (VkBool32)renderSettings.aoOnlyView;
		VkBool32 lightingHeatmap = (VkBool32)renderSettings.lightingHeatmap;
		VkBool32 whiteWorld = (VkBool32)renderSettings.whiteWorld;
		VkBool32 showCascadeSplits = (VkBool32)renderSettings.showCascadeSplits;
		uint32 cascadeCount = GFX.currentCascadeCount;
		int numPCFSamples = renderSettings.numPCFSamples;
	} lightingSpecConsts;

	std::array<VkSpecializationMapEntry, 7> specializationEntries;
	specializationEntries[0] = {
		.constantID = 0,
		.offset  = offsetof(LightingSpecConsts, enableAO),
		.size = sizeof(VkBool32)
	};
	specializationEntries[1] = {
		.constantID = 1,
		.offset  = offsetof(LightingSpecConsts, aoOnlyView),
		.size = sizeof(VkBool32)
	};
	specializationEntries[2] = {
		.constantID = 2,
		.offset  = offsetof(LightingSpecConsts, lightingHeatmap),
		.size = sizeof(VkBool32)
	};
	specializationEntries[3] = {
		.constantID = 3,
		.offset  = offsetof(LightingSpecConsts, whiteWorld),
		.size = sizeof(VkBool32)
	};
	specializationEntries[4] = {
		.constantID = 4,
		.offset  = offsetof(LightingSpecConsts, showCascadeSplits),
		.size = sizeof(VkBool32)
	};
	specializationEntries[5] = {
		.constantID = 5,
		.offset  = offsetof(LightingSpecConsts, cascadeCount),
		.size = sizeof(uint32)
	};
	specializationEntries[6] = {
		.constantID = 6,
		.offset  = offsetof(LightingSpecConsts, numPCFSamples),
		.size = sizeof(uint32)
	};

	VkSpecializationInfo specInfo = {
		.mapEntryCount = specializationEntries.size(),
		.pMapEntries = specializationEntries.data(),
		.dataSize = sizeof(LightingSpecConsts),
		.pData = &lightingSpecConsts
	};

	// Stencil write always disabled
	dynState.dynamicStateCount = 2;

	//Main shading pass
	std::array<VkDescriptorSetLayout, 5> layoutsM = {
		GFX.cameraOnlyLayout,
		GFX.lightingLayout,
		GFX.textureArrayLayout,
		GFX.mainShading.shadingDescLayout,
		GFX.tiledForwardPass.frustumLayout};
	
	PipelineBuilder pipelineBuilder;
	pipelineBuilder.beginPipeline(&GFX.mainShading.graphicsPipeline, &GFX.mainShading.pipelineLayout, 
		&GFX.mainShading.renderPass);
	pipelineBuilder.addShaderStage("Shaders/LightingOpaque_vs.spv", VK_SHADER_STAGE_VERTEX_BIT);
	pipelineBuilder.addShaderStage("Shaders/LightingOpaque_fs.spv", VK_SHADER_STAGE_FRAGMENT_BIT,
		&specInfo);
	pipelineBuilder.setInputDescriptons(false);
	pipelineBuilder.setRasterizer(renderSettings.wireframe);
	pipelineBuilder.setViewportState(GFX.internalRes.width, GFX.internalRes.height);
	pipelineBuilder.setMultiSampling();
	pipelineBuilder.setColourBlending(true);
	pipelineBuilder.addPushConstantRange(0 ,sizeof(PushConstantData), VK_SHADER_STAGE_VERTEX_BIT);
	pipelineBuilder.addPushConstantRange(sizeof(PushConstantData),
			sizeof(InstanceManager::MaterialProps),
			VK_SHADER_STAGE_FRAGMENT_BIT);
	pipelineBuilder.setPipelineLayout(layoutsM.data(), layoutsM.size());
	// Exact depth value is known from depth pre-pass
	pipelineBuilder.setDepthStencil(VK_TRUE, VK_FALSE, VK_COMPARE_OP_EQUAL);
	pipelineBuilder.createGraphicsPipeline(&dynState);
}

// TODO: Shader file name error checking
void Pipeline::initShadowPipelines()
{
	std::array<VkDescriptorSetLayout, 2> layoutsPoint = {
		GFX.omniShadowData.shadowCamLayout,
		GFX.textureArrayLayout};

	PipelineBuilder builder;
	builder.beginPipeline(&GFX.omniShadowData.pipeline, &GFX.omniShadowData.pipelineLayout,
			&GFX.omniShadowData.renderPass);
	builder.addShaderStage("Shaders/pointshadow_vs.spv", VK_SHADER_STAGE_VERTEX_BIT);
	builder.addShaderStage("Shaders/pointshadow_fs.spv", VK_SHADER_STAGE_FRAGMENT_BIT);
	builder.setInputDescriptons(true);
	builder.setRasterizer();
	builder.setViewportState(GFX.currentOmniResolution, GFX.currentOmniResolution);
	builder.setMultiSampling();
	builder.addPushConstantRange(0, sizeof(PushConstantData), VK_SHADER_STAGE_VERTEX_BIT);
	builder.addPushConstantRange(sizeof(PushConstantData), sizeof(uint32), 
			VK_SHADER_STAGE_FRAGMENT_BIT);
	builder.setPipelineLayout(layoutsPoint.data(), layoutsPoint.size());
	builder.setDepthStencil(VK_TRUE, VK_TRUE, VK_COMPARE_OP_LESS_OR_EQUAL);
	//Skip blend state
	builder.createGraphicsPipeline();

	std::array<VkDescriptorSetLayout, 2> layoutsCascade = {
		GFX.cascadeShadowData.shadowCamLayout,
		GFX.textureArrayLayout};

	PipelineBuilder builder2;
	builder2.beginPipeline(&GFX.cascadeShadowData.pipeline, &GFX.cascadeShadowData.pipelineLayout,
			&GFX.cascadeShadowData.renderPass);
	builder2.addShaderStage("Shaders/cascadedshadow_vs.spv", VK_SHADER_STAGE_VERTEX_BIT);
	builder2.addShaderStage("Shaders/depth_fs.spv", VK_SHADER_STAGE_FRAGMENT_BIT);
	builder2.setInputDescriptons(true);
	builder2.setRasterizer();
	builder2.setViewportState(GFX.currentCascadeResolution, GFX.currentCascadeResolution);
	builder2.setMultiSampling();
	builder2.setColourBlending();
	builder2.addPushConstantRange(0, sizeof(PushConstantData), VK_SHADER_STAGE_VERTEX_BIT);
	builder2.addPushConstantRange(sizeof(PushConstantData), sizeof(uint32), 
			VK_SHADER_STAGE_FRAGMENT_BIT);
	builder2.setPipelineLayout(layoutsCascade.data(), layoutsCascade.size());
	builder2.setDepthStencil(VK_TRUE, VK_TRUE, VK_COMPARE_OP_LESS_OR_EQUAL);
	builder2.createGraphicsPipeline();
}

void Pipeline::initComputePipelines()
{
	//Tiled light culling
	std::array<VkDescriptorSetLayout, 4> cullingLayouts = {
		GFX.cameraOnlyLayout, GFX.lightingLayout,
		GFX.tiledForwardPass.depthLayout, GFX.tiledForwardPass.frustumLayout};
	
	PipelineBuilder computeBuilder = {};
	computeBuilder.addShaderStage("Shaders/lightculling_comp.spv", VK_SHADER_STAGE_COMPUTE_BIT);
	computeBuilder.beginPipeline(&GFX.tiledForwardPass.pipeline, &GFX.tiledForwardPass.pipelineLayout);
	computeBuilder.setPipelineLayout(cullingLayouts.data(), cullingLayouts.size());
	computeBuilder.createComputePipeline();
}

void Pipeline::initPostProcessPipelines()
{
	std::array<VkDescriptorSetLayout, 3> cullingLayouts = {
		GFX.gtaoData.depthLayout,
		GFX.gtaoData.descLayout,
		GFX.cameraOnlyLayout};

	PipelineBuilder builder;
	builder.addShaderStage("Shaders/GTAO_comp.spv", VK_SHADER_STAGE_COMPUTE_BIT);
	builder.beginPipeline(&GFX.gtaoData.pipeline, &GFX.gtaoData.pipelineLayout);
	builder.addPushConstantRange(0, sizeof(AmbientOcclusionData::PushConstData), VK_SHADER_STAGE_COMPUTE_BIT);
	builder.setPipelineLayout(cullingLayouts.data(), cullingLayouts.size());
	builder.createComputePipeline();
}

void PipelineBuilder::beginPipeline(VkPipeline *pipeline, VkPipelineLayout *pipelineLayout, 
		VkRenderPass* renderPass)
{
	this->pipeline = pipeline;
	this->pipelineLayout = pipelineLayout;
	this->renderPass = renderPass;
}

void PipelineBuilder::addShaderStage(const std::string& dir, VkShaderStageFlagBits stageFlags, 
	VkSpecializationInfo* specInfo)
{
	shaderModules.push_back( Shaders::createShaderModule(dir.c_str()) );

	VkPipelineShaderStageCreateInfo createInfo {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
		.stage = stageFlags,
		.module = shaderModules.back(),
		.pName = "main",
		.pSpecializationInfo = specInfo
	};

	shaderStages.push_back(createInfo);
}

void PipelineBuilder::setInputDescriptons(bool depthOnly)
{
	bindingDescription = Buffers::getBindDesc();
	attributeDescriptions = Buffers::getAttribDesc(depthOnly);

	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputInfo.vertexBindingDescriptionCount = 1;
	vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;

	vertexInputInfo.vertexAttributeDescriptionCount = (uint32)attributeDescriptions.size();
	vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;
}

void PipelineBuilder::setInputDescriptonLines()
{
	bindingDescription.binding = 0;
	bindingDescription.stride = sizeof(LineVertex);
	bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputInfo.vertexBindingDescriptionCount = 1;
	vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;

	attributeDescriptions.resize(2);
	attributeDescriptions[0] = VkVertexInputAttributeDescription {
		.location = 0,
		.binding = 0,
		.format = VK_FORMAT_R8G8B8A8_UNORM,
		.offset = 0
	};

	attributeDescriptions[1] = VkVertexInputAttributeDescription {
		.location = 1,
		.binding = 0,
		.format = VK_FORMAT_R32G32B32_SFLOAT,
		.offset = offsetof(LineVertex, position)
	};

	vertexInputInfo.vertexAttributeDescriptionCount = attributeDescriptions.size();
	vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;
}

void PipelineBuilder::setInputDescriptonsPosOnly()
{
	bindingDescription.binding = 0;
	bindingDescription.stride = sizeof(vec3);
	bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputInfo.vertexBindingDescriptionCount = 1;
	vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;

	attributeDescriptions.resize(1);
	attributeDescriptions[0] = VkVertexInputAttributeDescription {
		.location = 0,
		.binding = 0,
		.format = VK_FORMAT_R32G32B32_SFLOAT,
		.offset = 0
	};

	vertexInputInfo.vertexAttributeDescriptionCount = attributeDescriptions.size();
	vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;
}

void PipelineBuilder::setMultiSampling()
{
	/*Enabling multisampling requires enabling another GPU feature*/
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

	/* optional
	multisampling.minSampleShading = 1.0f; 
	multisampling.pSampleMask = nullptr; 
	multisampling.alphaToCoverageEnable = VK_FALSE; 
	multisampling.alphaToOneEnable = VK_FALSE;*/
}

void PipelineBuilder::setViewportState(uint32 width, uint32 height)
{
	/*TODO: Note that surface size and swapchain image size might not match in future
	 * Update it to use the actual swapchain size if this is the case.*/
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = width;
	viewport.height = height;
	viewport.minDepth = 1.0f;
	viewport.maxDepth = 0.0f;
	
	scissor.offset = {0, 0};
	scissor.extent = VkExtent2D{width, height};

	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;
}

void PipelineBuilder::setRasterizer(bool wireframe)
{
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE; //GPU feature required
	rasterizer.rasterizerDiscardEnable = VK_FALSE; //Disabled output to framebuffer
	rasterizer.polygonMode = (wireframe) ? VK_POLYGON_MODE_LINE : VK_POLYGON_MODE_FILL;
	rasterizer.lineWidth = 1.0f; // > 1.0 requires widelines feature
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
//	rasterizer.cullMode = VK_CULL_MODE_NONE;
	rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterizer.depthBiasEnable = VK_FALSE;

	// GCN Only optimization
	if (GFX.enableRasterizationOrder == true) {
		rasterizer.pNext = &amdRastOrder;
		amdRastOrder.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_RASTERIZATION_ORDER_AMD;
		amdRastOrder.rasterizationOrder = VK_RASTERIZATION_ORDER_RELAXED_AMD;
	}

	/* Optional
	rasterizer.depthBiasConstantFactor = 0.0f;
	rasterizer.depthBiasClamp = 0.0f;
	rasterizer.depthBiasSlopeFactor = 0.0f;*/
}

void PipelineBuilder::setColourBlending(bool gBuffer, VkBool32 blendEnable)
{
	VkColorComponentFlags colourWriteMask = 
		VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | 
		VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

	colourBlendAttach[0].colorWriteMask = colourWriteMask;
	colourBlendAttach[0].blendEnable = blendEnable;
	colourBlendAttach[1].colorWriteMask = colourWriteMask;
	colourBlendAttach[1].blendEnable = blendEnable;

	if (blendEnable) {
		colourBlendAttach[0].srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
		colourBlendAttach[0].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		colourBlendAttach[0].colorBlendOp = VK_BLEND_OP_ADD;
		colourBlendAttach[0].srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
		colourBlendAttach[0].dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
		colourBlendAttach[0].alphaBlendOp = VK_BLEND_OP_ADD;
	}

	colourBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colourBlending.logicOpEnable = VK_FALSE;
	colourBlending.logicOp = VK_LOGIC_OP_AND;
	colourBlending.attachmentCount = (gBuffer) ? 2 : 1;
	colourBlending.pAttachments = colourBlendAttach;
}

void PipelineBuilder::setDepthStencil(VkBool32 bDepthTest, VkBool32 bDepthWrite, VkCompareOp compareOp, bool stencilWrite)
{
	depthStencilInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencilInfo.pNext = nullptr;
	depthStencilInfo.depthTestEnable = bDepthTest;
	depthStencilInfo.depthWriteEnable = bDepthWrite;
	depthStencilInfo.depthCompareOp = compareOp;
	depthStencilInfo.depthBoundsTestEnable = VK_FALSE;
	depthStencilInfo.minDepthBounds = 0.0f;
	depthStencilInfo.maxDepthBounds = 1.0f;

	VkStencilOp stencilOp = (stencilWrite) ? VK_STENCIL_OP_REPLACE : VK_STENCIL_OP_KEEP;

	// Disabled by default but configured to fill stencil buffer if enabled
	depthStencilInfo.stencilTestEnable = VK_FALSE;
	depthStencilInfo.back.compareOp = VK_COMPARE_OP_ALWAYS;
	depthStencilInfo.back.failOp = stencilOp;
	depthStencilInfo.back.depthFailOp = stencilOp;
	depthStencilInfo.back.passOp = stencilOp;
	depthStencilInfo.back.compareMask = 0xff;
	depthStencilInfo.back.writeMask = 0xff;
	depthStencilInfo.back.reference = 255;
	depthStencilInfo.front = depthStencilInfo.back;
}

void PipelineBuilder::addPushConstantRange(uint32 offset, uint32 size, VkShaderStageFlagBits stage)
{
	VkPushConstantRange pushConstant {
		.stageFlags = (VkShaderStageFlags)stage,
		.offset = offset,
		.size = size
	};

	pushConstants.push_back(pushConstant);
}

void PipelineBuilder::setPipelineLayout(VkDescriptorSetLayout *layouts, uint32 descriptorCount)
{
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = descriptorCount;
	pipelineLayoutInfo.pSetLayouts = layouts;
	pipelineLayoutInfo.pPushConstantRanges = pushConstants.data();
	pipelineLayoutInfo.pushConstantRangeCount = pushConstants.size();

	VK_CHECK( vkCreatePipelineLayout(GFX.device, &pipelineLayoutInfo, nullptr, pipelineLayout),
			"Failed to create graphics pipeline layout");
}

void PipelineBuilder::createGraphicsPipeline(VkPipelineDynamicStateCreateInfo* pDynState)
{
	VkGraphicsPipelineCreateInfo pipelineInfo {
		.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
		.stageCount = (uint32)shaderStages.size(),
		.pStages = shaderStages.data(),
		.pVertexInputState = &vertexInputInfo,
		.pInputAssemblyState = &inputAssembly,
		.pViewportState = &viewportState,
		.pRasterizationState = &rasterizer,
		.pMultisampleState = &multisampling,
		.pDepthStencilState = &depthStencilInfo,
		.pColorBlendState = &colourBlending,
		.pDynamicState = pDynState,
		.layout = *pipelineLayout,
		.renderPass = *renderPass,
		.subpass = 0,
		.basePipelineHandle = VK_NULL_HANDLE,
	};

	VK_CHECK( vkCreateGraphicsPipelines(GFX.device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, pipeline),
			"Failed to create graphics pipeline");

	destroyShaderModules();
}

void PipelineBuilder::createComputePipeline()
{
	VkComputePipelineCreateInfo pipelineInfo {
		.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
		.stage = shaderStages[0],
		.layout = *pipelineLayout,
		.basePipelineHandle = VK_NULL_HANDLE
	};

	VK_CHECK( vkCreateComputePipelines(GFX.device, VK_NULL_HANDLE, 1, &pipelineInfo, 
			nullptr, pipeline), "Failed to create compute pipeline");

	destroyShaderModules();
}

void PipelineBuilder::destroyShaderModules()
{
	for (uint32 i = 0; i < shaderModules.size(); ++i) {
		vkDestroyShaderModule(GFX.device, shaderModules.at(i), nullptr);
	}
}
