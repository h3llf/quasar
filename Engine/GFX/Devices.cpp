#include "Devices.hpp"
#include "RendererData.hpp"
#include "RenderSettings.hpp"
#include <vulkan/vulkan_core.h>
#include <bitset>

void Devices::initDevice()
{
	uint32 deviceCount;
	vkEnumeratePhysicalDevices(GFX.instance, &deviceCount, nullptr);
	std::vector<VkPhysicalDevice> physicalDevices(deviceCount);
	vkEnumeratePhysicalDevices(GFX.instance, &deviceCount, physicalDevices.data());
	VkPhysicalDeviceProperties props;

	renderSettings.selectedDevice = std::min(renderSettings.selectedDevice.load(), deviceCount - 1);

	// List avaliable devices
	for (uint32 i = 0; i < physicalDevices.size(); ++i) {
		vkGetPhysicalDeviceProperties(physicalDevices[i], &props);
		renderSettings.deviceNames.push_back(props.deviceName);
	}

	vkGetPhysicalDeviceProperties(physicalDevices[renderSettings.selectedDevice], &props);
	GFX.physicalDevice = physicalDevices.at(renderSettings.selectedDevice);

	getMemoryProperties(&props);
	getImageFormats(&props);

	getQueueFamilies();
	std::array<VkDeviceQueueCreateInfo, 2> queueCreateInfo;

	float queuePriorities[1] = {1.0};
	queueCreateInfo[0] = VkDeviceQueueCreateInfo{};
	queueCreateInfo[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	queueCreateInfo[0].queueCount = 1;
	queueCreateInfo[0].queueFamilyIndex = GFX.graphicsQueueIndex;
	queueCreateInfo[0].pQueuePriorities = queuePriorities;

	if (GFX.enableStagingBuffer) {
		queueCreateInfo[1] = VkDeviceQueueCreateInfo{};
		queueCreateInfo[1].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo[1].queueCount = 1;
		queueCreateInfo[1].queueFamilyIndex = GFX.transferQueueIndex;
		queueCreateInfo[1].pQueuePriorities = queuePriorities;
	}

	VkPhysicalDeviceMultiviewFeatures multiview{};
	multiview.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES_KHR;
	multiview.pNext = nullptr;
	multiview.multiview = VK_TRUE;

	VkPhysicalDeviceVulkan13Features devFeatures_1_3 {};
	devFeatures_1_3.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES;
	devFeatures_1_3.synchronization2 = VK_TRUE;
	devFeatures_1_3.pNext = &multiview;

	VkPhysicalDeviceVulkan12Features devFeatures_1_2 {};
	devFeatures_1_2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
	devFeatures_1_2.pNext = &devFeatures_1_3;
	devFeatures_1_2.descriptorBindingPartiallyBound = VK_TRUE;
	devFeatures_1_2.descriptorBindingUpdateUnusedWhilePending = VK_TRUE; // Needed to update texture desc array
	devFeatures_1_2.descriptorIndexing = VK_TRUE;

	VkPhysicalDeviceFeatures2 devFeatures2 {};
	devFeatures2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
	devFeatures2.pNext = &devFeatures_1_2;
	devFeatures2.features.imageCubeArray = VK_TRUE;
	devFeatures2.features.samplerAnisotropy = VK_TRUE;
	devFeatures2.features.fillModeNonSolid = VK_TRUE;
	devFeatures2.features.wideLines = VK_TRUE;

	VkDeviceCreateInfo createInfo {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.queueCreateInfoCount = (GFX.enableStagingBuffer) ? 2 : 1;
	createInfo.pQueueCreateInfos = queueCreateInfo.data();
	createInfo.pNext = &devFeatures2; 
	//TODO: Check device extensions support
	createInfo.enabledExtensionCount = GFX.deviceExtensions.size();
	createInfo.ppEnabledExtensionNames = GFX.deviceExtensions.data();

	VK_CHECK(vkCreateDevice(GFX.physicalDevice, &createInfo, nullptr, &GFX.device), 
			"Failed to create device");

	vkGetDeviceQueue(GFX.device, GFX.graphicsQueueIndex, 0, &GFX.graphicsQueue);

	if (GFX.enableStagingBuffer) {
		vkGetDeviceQueue(GFX.device, GFX.transferQueueIndex, 0, &GFX.transferQueue);
	} else {
		GFX.transferQueue = GFX.graphicsQueue;
	}
}

void Devices::getQueueFamilies()
{
	uint32 familyCount;
	vkGetPhysicalDeviceQueueFamilyProperties(GFX.physicalDevice, &familyCount, nullptr);

	std::vector<VkQueueFamilyProperties> properties(familyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(GFX.physicalDevice, &familyCount, properties.data());

	/*Find a queue family supporting the graphics bit*/
	bool foundGfx = 0;
	bool foundTrans = 0;

	for (uint32 i = 0; i < familyCount; ++i) {
		// Main graphics queue
		if ((properties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) &&
				(properties[i].queueFlags & VK_QUEUE_COMPUTE_BIT)){
			GFX.graphicsQueueIndex = i;
			foundGfx = 1;
		}

		// Transfer queue
		// TODO: Check if most modern discrete GPUs support a dedicated transfer queue
		if (GFX.enableStagingBuffer && 
				(properties[i].queueFlags & VK_QUEUE_TRANSFER_BIT) &&
				!(properties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)) {
			GFX.transferQueueIndex = i;
			foundTrans = 1;
		}
	}

	if (!foundGfx) 
		Log::fatal("Couldn't find queue family supporting graphics");

	if (GFX.enableStagingBuffer && !foundTrans) 
		Log::fatal("Staging buffer enabled but no transfer queue avaliable");
}

void Devices::getMemoryProperties(VkPhysicalDeviceProperties* props)
{
	if (props->deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
		GFX.deviceOnlyProperties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		GFX.deviceOnlyBufferUsage = VK_BUFFER_USAGE_TRANSFER_DST_BIT;
		GFX.enableStagingBuffer = true;
	} else {
		GFX.deviceOnlyProperties = GFX.hostVisibleProperties;
		GFX.deviceOnlyBufferUsage = 0;
		GFX.enableStagingBuffer = false;
	}

	// Default if host visible device local isn't avaliable
	GFX.hostVisibleProperties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

	// Desired property if avaliable
	VkMemoryPropertyFlags hostVisDevLocal = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT |
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
	
	VkPhysicalDeviceMemoryProperties memProps;
	vkGetPhysicalDeviceMemoryProperties(GFX.physicalDevice, &memProps);
	for (uint32 i = 0; i < memProps.memoryTypeCount; ++i) {
		// Check for the avaliability of device local host coherent memory
		if ((memProps.memoryTypes[i].propertyFlags & hostVisDevLocal) == hostVisDevLocal) {
			GFX.stagingMemoryHeap = memProps.memoryTypes[i].heapIndex;
			GFX.hostVisibleProperties = hostVisDevLocal;
		}
	}

}

void Devices::getImageFormats(VkPhysicalDeviceProperties* props)
{
	// Prefer 24 bit depth buffer
	VkFormatProperties formatProps {};
	vkGetPhysicalDeviceFormatProperties(GFX.physicalDevice, VK_FORMAT_D24_UNORM_S8_UINT, &formatProps);
	if (!formatProps.optimalTilingFeatures) {
		GFX.depthStencilFormat = VK_FORMAT_D32_SFLOAT_S8_UINT;
	} else {
		GFX.depthStencilFormat = VK_FORMAT_D24_UNORM_S8_UINT;
	}

	/*
	vkGetPhysicalDeviceFormatProperties(GFX.physicalDevice, VK_FORMAT_D24_UNORM_S8_UINT, &formatProps);
	if (!formatProps.optimalTilingFeatures) {
		GFX.depthStencilFormat = VK_FORMAT_D32_SFLOAT_S8_UINT;
	} else {
		GFX.depthStencilFormat = VK_FORMAT_D24_UNORM_S8_UINT;
	}*/
}

bool Devices::checkDeviceExtensions(VkPhysicalDevice device)
{
	return true;
}
