#ifndef QUASAR_RENDER_DATA_H
#define QUASAR_RENDER_DATA_H

#include <chrono>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>
#include <vector>
#include <array>
#include <string.h>
#include <mutex>
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vk_mem_alloc.h>
#include "Core/Types.hpp"
#include "Core/Camera.hpp"
#include "RenderTypes.hpp"

#define VK_CHECK(result, msg) \
	if (result != VK_SUCCESS){ Log::out("[Vulkan] Error on line ", __LINE__, " in ", __FILE__, " ", msg); }

struct PushConstantData
{
	qml::mat4 data;
};

// Per camera framebuffers and optional images
struct ViewportData
{
	// Identifies deleted viewports in the viewport list
	bool deleted = false;

	// Target framebuffers
	std::vector<VkFramebuffer> debugFramebuffers;
	std::vector<VkFramebuffer> mainFramebuffers;
	std::vector<VkFramebuffer> depthOnlyFramebuffers;

	int width = 0;
	int height = 0;

	// Group counts for the tiled forward shader
	int TFGroupsX = 0;
	int TFGroupsY = 0;

	int AOGroupsX = 0;
	int AOGroupsY = 0;
	int aoResX = 0;
	int aoResY = 0;
	float aoScaleX = 0.0f;
	float aoScaleY = 0.0f;

	// Bloom mip levels, always < BloomData.mipCount
	int bloomMipLevels;

	int imageCount = 1;

	// Tiled forward resources
	std::vector<VkDescriptorSet> frustumSets;

	VkBuffer frustumBuffer;
	VmaAllocation frustumAlloc;
	VkBuffer tileDescBuffer;
	VmaAllocation tileDescAlloc;
	std::vector<VkBuffer> cullingResultBuffers;
	std::vector<VmaAllocation> cullingResultAlloc;

	Camera::CameraProperties camProps {};

	// Only used for rendering to images other than the swapchain
	VkSemaphore avaliableSemaphore;

	// Auto resize to window
	bool windowTarget = false;

	// Uses swapchain image if ID == -1
	// otherwise use a texture image at the correisponding index
	ImageHandle targetImage = WINDOW_IMAGE_HANDLE;
};

/*
struct ReflectionsData
{
	bool enabled = true;
//	float resolutionScale = 0.5f;
	float resolutionScale = 1.0f;

	// SSR Tracing
	VkPipeline pipeline;
	VkPipelineLayout layout;

	VkDescriptorSetLayout tracingDescLayout;
	std::vector<VkDescriptorSet> tracingDesc;

	// Resolve
	VkPipeline resolvePipeline;
	VkPipelineLayout resolveLayout;

	VkDescriptorSetLayout resolveDescLayout;
	std::vector<VkDescriptorSet> resolveDesc;

	VkFormat hitResultFormat = VK_FORMAT_R16G16_SFLOAT;
	std::vector<VkImage> hitResultImages;
	std::vector<VkImageView> hitResultImgViews;
	std::vector<VmaAllocation> hitResultImgAlloc;

	std::vector<VkImage> prevImages;
	std::vector<VkImageView> prevImgViews;
	std::vector<VmaAllocation> prevImgAlloc;
	std::vector<VkEvent> prevEvents;

	VkBuffer uniform;
	VmaAllocation uniformAlloc;

	int tracingGroupsX;
	int tracingGroupsY;
};*/

struct BloomData
{
	uint32_t mipCount = 7;

	VkDescriptorSetLayout inDescLayout;
	VkDescriptorSetLayout outDescLayout;

	// Shading image
	std::vector<VkDescriptorSet> srcDescriptors;
	std::vector<VkDescriptorSet> dstDescriptors;

	// Mip chain images
	std::vector<std::vector<VkDescriptorSet>> inDescriptors;
	std::vector<std::vector<VkDescriptorSet>> outDescriptors;

	// Mip chain images
	const VkFormat format = VK_FORMAT_B10G11R11_UFLOAT_PACK32;
	std::vector<VkImage> images;
	std::vector<VmaAllocation> imgAllocations;
	std::vector<std::vector<VkImageView>> imgViews;

	VkPipeline pipeline;
	VkPipelineLayout layout;

	VkSampler sampler;

	struct PushConstData
	{
		vec2 inTexelSize;
		ivec2 outputResolution;
		int mode;
		float sampleScale = 1.0;
		float intensity;
	};
};

struct PostData
{
	std::vector<VkDescriptorSet> descriptors;
	VkDescriptorSetLayout descLayout;

	VkPipeline pipeline;
	VkPipelineLayout layout;

	VkSampler sampler;

	int groupCountX;
	int groupCountY;

	struct PushConstData
	{
		int width;
		int height;
	};
};

struct OutlineData
{
	VkPipeline pipeline;
	VkPipelineLayout layout;

	VkFormat imgFormat = VK_FORMAT_R16G16_SFLOAT;

	// U/V buffers
	std::vector<VkImage> images;
	std::vector<VkImageView> imgViews;
	std::vector<VmaAllocation> imgAllocations;

	VkDescriptorSetLayout descLayout;
	std::vector<VkDescriptorSet> descriptors;

	int groupCountX;
	int groupCountY;
};

struct DebugLineData
{
	VkRenderPass renderPass;
	VkPipeline pipeline;
	VkPipelineLayout layout;

	// Vertex buffer is in host memory and updated every frame (when enabled)
	size_t bufferCount = 0;
	size_t actualCount = 0;
	VmaAllocation vertexAlloc = VK_NULL_HANDLE;
	VkBuffer vertexBuffer = VK_NULL_HANDLE;
};

struct SkyboxData
{
	VkPipeline pipeline;
	VkPipelineLayout layout;

	VmaAllocation vertexAlloc = VK_NULL_HANDLE;
	VkBuffer vertexBuffer = VK_NULL_HANDLE;
	VmaAllocation indexAlloc = VK_NULL_HANDLE;
	VkBuffer indexBuffer = VK_NULL_HANDLE;
};

struct MainShadingData
{
	// Image format
	VkFormat imgFormat = VK_FORMAT_B10G11R11_UFLOAT_PACK32;
	// TODO: Test 8 bit format for G-Buffer
	// Screen space normal + Roughness
	VkFormat gBufferFormat = VK_FORMAT_R16G16B16A16_SFLOAT;

	// Depth pre-pass
	VkRenderPass depthPass;
	VkPipelineLayout depthPipelineLayout;
	VkPipeline depthPipeline;

	// Main shading pass
	VkRenderPass renderPass;
	VkPipelineLayout pipelineLayout;
	VkPipeline graphicsPipeline;

	// Descriptors
	// Includes cascaded shadow, omni shadows and GTAO output
	VkDescriptorSetLayout shadingDescLayout;
	std::vector<VkDescriptorSet> shadingDescriptors;
	
	std::vector<VkImage> images;
	std::vector<VkImageView> imgViews;
	std::vector<VmaAllocation> imgAllocations;

	std::vector<VkImage> depthImages;
	std::vector<VkImageView> depImgViews;
	std::vector<VkImageView> depStenImgViews;
	std::vector<VkImageView> stenImgViews;
	std::vector<VmaAllocation> depImgAllocations;

	std::vector<VkImage> gBufferImages;
	std::vector<VkImageView> gBufferImgViews;
	std::vector<VmaAllocation> gBufferImgAllocations;

	// Camera, lighting and culling results use different shared descriptors
	// TODO: Add these buffers to this set
};

struct TiledForwardData
{
	const uint32 TILE_SIZE = 16;
	const uint32 maxLightsPerTile = 128;

	VkDescriptorSetLayout frustumLayout;

	VkDescriptorSetLayout depthLayout;
	std::vector<VkDescriptorSet> depthSets;

	VkPipeline pipeline = VK_NULL_HANDLE;
	VkPipelineLayout pipelineLayout = VK_NULL_HANDLE; 
};

struct ShadowPassData
{
	// 1 Image and allocation per frame in flight
	std::vector<VkImage> images;
	std::vector<VmaAllocation> allocations;
	std::vector<VkImageView> views;

	// 1 Camera per shadow mapped object
	std::vector<VkBuffer> cameraUniform;
	std::vector<VmaAllocation> cameraAllocation;
	std::vector<VkDescriptorSet> shadowCamDesc;
	VkDescriptorSetLayout shadowCamLayout;

	VkRenderPass renderPass;

	// 1 Image view per shadow per frame in flight
	// count = (frames_in_flight * shadow_count)
	std::vector<VkImageView> frameBufferViews; // Unused for cascaded shadows
	std::vector<VkFramebuffer> frameBuffers;

	VkPipeline pipeline;
	VkPipelineLayout pipelineLayout;
};


struct AmbientOcclusionData
{
	bool aoActive;

	// Shader specialization constants
	// Horizon based
	// A low preset might look like 3 slices 2 samples
	float numSlices = 9.0f;
	float numSamples = 3.0f; // Samples within a slice

	// Screen size / workgroup sizes
	float scale = 1.0f;
	const uint32 groupSize = 16;

	VkFormat format = VK_FORMAT_R16_UNORM;

	VkPipeline pipeline;
	VkPipelineLayout pipelineLayout;

	std::vector<VkImage> aoImg;
	std::vector<VmaAllocation> aoImgAlloc;
	std::vector<VkImageView> aoImgView;

	VkDescriptorSetLayout descLayout;
	std::vector<VkDescriptorSet> imageDescriptors;

	VkDescriptorSetLayout depthLayout;
	std::vector<VkDescriptorSet> depthSets;

	struct PushConstData
	{
		int width;
		int height;
	};
};

struct FontRenderingData
{
	// TODO: Check if separeate 2D & 3D pipelines are necessary
	VkPipeline pipeline;
	VkPipelineLayout layout;
	VkRenderPass renderPass;
};

/*All vulkan data is stored here and accesible to all renderer classes*/
struct GFX_DATA_TYPE
{
	/*Application / renderer settings*/
	const char* engineName = "Quasar";
	const float maxMipLod = 5.0f;
	const uint32 maxPointLights = 4096;
	const uint32 maxImageDescriptors = 16384;
	int skyboxImgID = 0;

	// TODO: Move to a scene/camera data structure
	float internalResScale = 1.0f;
	VkExtent3D internalRes;

	double timeSeconds;

	// Only useful on discrete hardware
	bool enableStagingBuffer = true;
	bool stagePerFrameData = false;

	// Asset upload synchronization
	std::mutex rendererSyncMutex;

	uint32 stagingMemoryHeap = 0;
	uint32 primaryMemoryHeap = 0;

	VkMemoryPropertyFlags hostVisibleProperties;
	VkMemoryPropertyFlags deviceOnlyProperties;
	VkBufferUsageFlags deviceOnlyBufferUsage;
	
	/*Instance*/
	const uint32 apiVer = VK_API_VERSION_1_3;
	VkInstance instance = VK_NULL_HANDLE;

	/*Validation*/
#ifdef NDEBUG
	bool enableValidationLayers = false;
#else
	bool enableValidationLayers = true;
#endif

	std::array<const char*, 1> validationLayers = {"VK_LAYER_KHRONOS_validation"};
	std::array<VkValidationFeatureEnableEXT, 1> validationFeatures;// =
			//{VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT};
	VkDebugUtilsMessengerEXT debugMessenger;

	/*Devices and queues*/
	VkDevice device = VK_NULL_HANDLE;
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
	std::vector<const char*> deviceExtensions = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME//,
		//VK_KHR_SYNCHRONIZATION_2_EXTENSION_NAME
	};

	bool enableRasterizationOrder = true;
	std::vector<const char*> optionalDeviceExtensions = {
		VK_AMD_RASTERIZATION_ORDER_EXTENSION_NAME // GCN only
	};

	uint32 graphicsQueueIndex;
	VkQueue graphicsQueue;

	uint32 transferQueueIndex;
	VkQueue transferQueue;

	/*Vulkan memory allocator*/
	VmaAllocator allocator;

	/*Window*/
	GLFWwindow* window = nullptr;
	VkSurfaceKHR surface = VK_NULL_HANDLE;
	VkSurfaceCapabilitiesKHR surfaceCapabilites {};
	std::vector<VkSurfaceFormatKHR> formats;
	VkSurfaceFormatKHR surfaceFormat {};
	VkExtent2D surfaceExtent;

	/*Swapchain*/
	VkSwapchainKHR swapchain = VK_NULL_HANDLE;
	VkRenderPass swapchainRenderPass;
	std::vector<VkImage> swapchainImages;
	std::vector<VkImageView> swapchainImageViews;
	std::vector<VkFramebuffer> swapchainFramebuffers;

	VkFormat depthStencilFormat;

	// Default swapchain viewport
//	ViewportData mainViewport;
	ViewportHandle mainViewportHandle;

	/*Descriptors*/
	VkDescriptorSetLayout cameraOnlyLayout;
	VkDescriptorSetLayout lightingLayout;
	VkDescriptorSetLayout textureArrayLayout;
	VkDescriptorPool descriptorPool; // Destroyed on swapchain destruction
	VkDescriptorPool persistantDescPool; // Not destroyed on window resize
	std::vector<VkDescriptorSet> cameraDescriptors;
	std::vector<VkDescriptorSet> lightingDescriptors;
	VkDescriptorSet textureArraySet;

	/*Command Buffers*/
	std::vector<VkCommandPool> commandPools;
	std::vector<VkCommandBuffer> shadingCmdBuffers;

	VkCommandPool singleTimeCmdPool;
	VkCommandBuffer singleTimeCommands;
	VkCommandPool transferCmdPool;
	VkCommandBuffer transferCmdBuffer;
	VkFence singleTimeCmdFence;

	/*Drawing frames*/
	std::vector<VkSemaphore> imageAvaliableSemaphores;
	std::vector<VkSemaphore> renderFinishedSemaphores;
	std::vector<VkFence> fencesInFlight;

	uint32 maxFramesInFlight = 4;
	uint32 numImages = 0;
	uint32 currentFrame = 0;
	bool framebufferResized = false;

	//Stores camera data, 1 uniform per frame in flight
	std::vector<VkBuffer> cameraUniformBuffers;
	std::vector<VmaAllocation> cameraUniformAllocations;

	//Texture samplers
	VkSampler textureSampler; //Using lod
	VkSampler aoSampler; // Clamp to edge, nearest
	VkSampler depthSampler;

	//Editor Objects
	VkDescriptorPool imguiPool;

	// Debug visualization
	DebugLineData debugLine {};

	//Main shading pass
	MainShadingData mainShading {};
	FontRenderingData fontData {};

	//Shadow Mapping
	ShadowPassData omniShadowData {};
	ShadowPassData cascadeShadowData {};
	//static const uint32 maxOmniShadows = 8;

	uint32 omniShadowCount;
	uint32 currentMaxOmniShadowCount;
	uint32 currentOmniResolution;
	bool cascadeShadowEnabled = false;
	uint32 currentCascadeCount;
	uint32 currentCascadeResolution;
	uint32 cascadeBufferSize;

	VkFormat shadowDepthFormat = VK_FORMAT_D16_UNORM;
//	VkFormat shadowDepthFormat = VK_FORMAT_D32_SFLOAT;

	//Tiled light culling
	TiledForwardData tiledForwardPass;

	// Ground truth ambient occlusion
	AmbientOcclusionData gtaoData;

	// Skybox
	SkyboxData skyboxData;

	// Outlines
	OutlineData outlineData;

	// Post process
	PostData postData;
	BloomData bloomData;
//	ReflectionsData ssrData;
};

//Declared in Renderer.cpp
extern GFX_DATA_TYPE GFX;

//TODO: Set up a non interrupting task system for renderer uploads
extern std::mutex rendererSyncMutex; //Locks the renderer thread while data is being uploaded

#endif
