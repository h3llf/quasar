#ifndef QUASAR_PIPELINE_H
#define QUASAR_PIPELINE_H

#include <vulkan/vulkan_core.h>
#include "RendererData.hpp"

class PipelineBuilder
{
public:
	void beginPipeline(VkPipeline* pipeline, VkPipelineLayout* pipelineLayout, 
			VkRenderPass* renderPass = nullptr);
	void addShaderStage(const std::string& dir, VkShaderStageFlagBits stageFlags, 
		VkSpecializationInfo* specInfo = nullptr);


	void createGraphicsPipeline(VkPipelineDynamicStateCreateInfo* pDynState = nullptr);
	void createComputePipeline();

	void setViewportState(uint32 width, uint32 height);
	void setRasterizer(bool wireframe = false);
	void setMultiSampling();
	void setInputDescriptons(bool depthOnly);
	void setInputDescriptonLines();
	void setInputDescriptonsPosOnly();

	void setColourBlending(bool gBuffer = false, VkBool32 blendEnable = VK_FALSE);

	void setDepthStencil(VkBool32 bDepthTest, VkBool32 bDepthWrite, 
			VkCompareOp compareOp = VK_COMPARE_OP_ALWAYS,
			bool stencilWrite = false);

	void addPushConstantRange(uint32 offset, uint32 size, VkShaderStageFlagBits stage);
	void setPipelineLayout(VkDescriptorSetLayout* layouts, uint32 descriptorCount);

	VkVertexInputBindingDescription bindingDescription;
	std::vector<VkVertexInputAttributeDescription> attributeDescriptions;
	VkPipelineVertexInputStateCreateInfo vertexInputInfo {};
	VkPipelineInputAssemblyStateCreateInfo inputAssembly {};
private:
	void destroyShaderModules();

	VkRenderPass* renderPass;
	VkPipeline* pipeline;
	VkPipelineLayout* pipelineLayout;
	VkPipelineLayoutCreateInfo pipelineLayoutInfo {};

	VkViewport viewport {};
	VkRect2D scissor {};
	VkPipelineMultisampleStateCreateInfo multisampling {};
	VkPipelineViewportStateCreateInfo viewportState {};
	VkPipelineRasterizationStateCreateInfo rasterizer {};

	VkPipelineColorBlendAttachmentState colourBlendAttach[2] {};
	VkPipelineColorBlendStateCreateInfo colourBlending {};

	VkPipelineDepthStencilStateCreateInfo depthStencilInfo {};

	// Optional / device specific
	VkPipelineRasterizationStateRasterizationOrderAMD amdRastOrder {};

	std::vector<VkShaderModule> shaderModules;
	std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
	std::vector<VkPushConstantRange> pushConstants;
};

class Pipeline
{
public:
	static void initPipelines();
	static void initShadowPipelines();
	static void initComputePipelines();
	static void initPostProcessPipelines();
};

#endif //QUASAR_PIPELINE_H
