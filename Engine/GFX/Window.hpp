#ifndef QUASAR_WINDOW_H
#define QUASAR_WINDOW_H

#include "RendererData.hpp"

class Window
{
public:
	Window();
	~Window();

	static bool createWindow();
	static void createSurface();

	static VkExtent2D getWindowExtent();
	static void querySurfaceSupport();
};

#endif //QUASAR_WINDOW_H

