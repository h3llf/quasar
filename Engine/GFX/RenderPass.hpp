#ifndef QUASAR_RENDERPASS_H
#define QUASAR_RENDERPASS_H

#include "RendererData.hpp"
#include <vulkan/vulkan_core.h>

class RenderPass
{
public:
	//TODO: Better abstraction
	static void createRenderPass(VkRenderPass* renderPass, bool clear = true, 
			bool transferDst = false, bool gBuffers = false);
	static void createPostRenderPass(VkRenderPass* renderPass);
	static void depthRenderPass(VkRenderPass* renderPass, uint32 viewNum, bool isShadowPass, bool useStencil);
};

#endif //QUASAR_RENDERPASS_H
