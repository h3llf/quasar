#include "RenderSettings.hpp"

void RenderSettings::setupCVars(Console& console)
{
	console.bindCVar("gfx_selected_device", &selectedDevice, true);
	console.bindCVar("gfx_render_scale", &resolutionScale, true);
	console.bindCVar("gfx_vsync", &vsync, true);
	console.bindCVar("gfx_ambient_occlusion", &enableGTAO, true);
	console.bindCVar("gfx_ambient_occlusion_res", &aoResolutionScale, true);
	console.bindCVar("gfx_omni_shadow_resolution", &omniShadowRes, true);
	console.bindCVar("gfx_omni_shadow_count", &omniShadowCount, true);
	console.bindCVar("gfx_cascaded_shadow_resolution", &cascadedShadowRes, true);
	console.bindCVar("gfx_number_shadow_cascades", &numberCascades, true);
	console.bindCVar("gfx_cascaded_shadow_dist", &cascadeDist, true);
	console.bindCVar("gfx_cascaded_shadow_lambda", &lambda, true);
	console.bindCVar("gfx_num_shadow_pcf_samples", &numPCFSamples, true);
	console.bindCVar("gfx_bloom", &enableBloom, true);
	console.bindCVar("gfx_bloom_intensity", &bloomIntensity, true);

	console.bindCVar("gfx_wireframe", &wireframe, false);
	console.bindCVar("gfx_ao_only", &aoOnlyView, false);
	console.bindCVar("gfx_lighting_heatmap", &lightingHeatmap, false);
	console.bindCVar("gfx_whiteworld", &whiteWorld, false);
	console.bindCVar("gfx_show_cascade_splits", &showCascadeSplits, false);
}

RenderSettings renderSettings;
