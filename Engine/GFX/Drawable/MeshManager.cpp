#include "MeshManager.hpp"

#include "GFX/Descriptors.hpp"
#include "GFX/Buffers.hpp"
#include "GFX/Drawable/MaterialManager.hpp"
#include <string.h>
#include <vulkan/vulkan_core.h>

using namespace quasar;
MeshManager meshManager;

int MeshManager::createMesh(std::vector<Vertex>* pVertices, std::vector<uint32>* pIndices)
{
	int index = -1;

	//Check for holes in meshes vector
	for (int  i = 0; i < meshes.size(); ++i) {
		if (meshes[i].meshIndex== -1) {
			index = i;
			break;
		}
	}

	if (index == -1) {
		index = meshes.size();
		meshes.push_back(Mesh{});
	}

	meshes[index].indexCount = pIndices->size();
	meshes[index].meshIndex = index;

	Buffers::createVertexBuffer(
			&meshes.at(index).vertexBuffer,
			&meshes.at(index).vertexAllocation, 
			pVertices);
	Buffers::createIndexBuffer(
			&meshes.at(index).indexBuffer,
			&meshes.at(index).indexAllocation,
			pIndices);

//	meshes[index].material = *pMaterial;

	return index;
}

void MeshManager::cleanupMeshes()
{
	for (int i = 0; i < meshes.size(); ++i) {
		vkDestroyBuffer(GFX.device, meshes[i].vertexBuffer, nullptr);
		vmaFreeMemory(GFX.allocator, meshes[i].vertexAllocation);
		vkDestroyBuffer(GFX.device, meshes[i].indexBuffer, nullptr);
		vmaFreeMemory(GFX.allocator, meshes[i].indexAllocation);
	}	
}

void MeshManager::destroyMesh(int index)
{
	vkDestroyBuffer(GFX.device, meshes[index].vertexBuffer, nullptr);
	vmaFreeMemory(GFX.allocator, meshes[index].vertexAllocation);
	vkDestroyBuffer(GFX.device, meshes[index].indexBuffer, nullptr);
	vmaFreeMemory(GFX.allocator, meshes[index].indexAllocation);
}
