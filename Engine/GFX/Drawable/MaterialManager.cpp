#include "MaterialManager.hpp"
#include "GFX/ImageCreation.hpp"
#include "GFX/Buffers.hpp"
#include "GFX/Descriptors.hpp"
#include "Core/Assets/Image.hpp"
#include "GFX/RenderTypes.hpp"
#include <cassert>
#include <mutex>
#include <string.h>
#include <vulkan/vulkan_core.h>

MaterialManager materialManager;

void MaterialManager::createDescriptor()
{
	// Create material descriptor
	VkDescriptorSetAllocateInfo allocInfo {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
		.pNext = nullptr,
		.descriptorPool = GFX.persistantDescPool,
		.descriptorSetCount = 1,
		.pSetLayouts = &GFX.textureArrayLayout
	};
	
	VK_CHECK( vkAllocateDescriptorSets(GFX.device, &allocInfo, &GFX.textureArraySet),
			"Failed to allocate texture array descriptor");
}

ImageHandle MaterialManager::addTextureImage(void* pixelData, uint32 width, uint32 height, 
		VkFormat imageFormat, uint32 bytesPerPx)
{
	std::lock_guard<std::mutex> lck(getImageSync);

	VkDeviceSize imageSize = width * height * bytesPerPx;

	ImageHandle index = getNextHandle();
	Texture& newImage = images[index];

	VkBuffer stagingBuffer;
	VmaAllocation stagingAllocation;
	Buffers::createStagingBuffer(imageSize, &stagingBuffer, &stagingAllocation);

	// Copy image to staging buffer
	void* data;
	vmaMapMemory(GFX.allocator, stagingAllocation, &data);
	memcpy(data, pixelData, (size_t)imageSize);
	vmaUnmapMemory(GFX.allocator, stagingAllocation);

	VkExtent3D extent = {width, height, 1};

	uint32 mipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(width, height)))) + 1;

	if (mipLevels > 1)
		mipLevels = std::min(static_cast<uint32>(GFX.maxMipLod), mipLevels);

	createImageInternal(width, height, imageFormat, bytesPerPx, newImage, mipLevels);

	ImageCreation::transitionImageLayout(newImage.texture, false, VK_IMAGE_LAYOUT_UNDEFINED,
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, mipLevels);

	Buffers::copyToImage(stagingBuffer, newImage.texture, extent);

	vkDestroyBuffer(GFX.device, stagingBuffer, nullptr);
	vmaFreeMemory(GFX.allocator, stagingAllocation);

	//Generate mipmaps
	ImageCreation::generateMipmaps(newImage.texture, (int)width, (int)height, mipLevels);


	if (index >= GFX.maxImageDescriptors) {
		Log::fatal("Cannot allocate more than ", GFX.maxImageDescriptors, 
				" images to the descriptor set");
	}

	Descriptors::updateTextureDescriptor(index, newImage.textureView);

	return index;
}

ImageHandle MaterialManager::addBlankImage(uint32 width, uint32 height)
{
	ImageHandle index = getNextHandle();
	Texture& newImage = images[index];

	createImageInternal(width, height, VK_FORMAT_R8G8B8A8_SRGB, 4, newImage, 1);
	Descriptors::updateTextureDescriptor(index, newImage.textureView);

	ImageCreation::transitionImageLayout(newImage.texture, false, VK_IMAGE_LAYOUT_UNDEFINED,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1, 1);

	return index;
}

void MaterialManager::createImageInternal(uint32 width, uint32 height, VkFormat imageFormat, 
		uint32 bytesPerPx, Texture& newImage, uint32 mipLevels)
{
	newImage = {
		.width = width,
		.height = height
	};

	VkExtent3D extent = {width, height, 1};

	VmaAllocationCreateInfo allocateInfo {};
	allocateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

	VkImageCreateInfo createInfo = ImageCreation::createImageInfo(imageFormat,
			VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | 
			VK_IMAGE_USAGE_TRANSFER_SRC_BIT, extent, mipLevels);
	
	vmaCreateImage(GFX.allocator, &createInfo, &allocateInfo, 
			&newImage.texture, &newImage.allocation, nullptr);

	//Create image view
	newImage.textureView = ImageCreation::createTextureImageView(imageFormat, newImage.texture, mipLevels);
}

ImageHandle MaterialManager::getNextHandle()
{
	// Check for free image slots
	ImageHandle index = 0;
	if (freeImageIndexs.size()) {
		index = freeImageIndexs.front();
		freeImageIndexs.pop();
		images[index] = {};
	} else {
		images.push_back({});
		index = images.size() - 1; 
	}

	return index;
}

void MaterialManager::destroyImage(ImageHandle imageID)
{
	// Images doesn't exist
	if (images[imageID].texture == VK_NULL_HANDLE)
		return;

	vkDestroyImage(GFX.device, images[imageID].texture, nullptr);
	vkDestroyImageView(GFX.device, images[imageID].textureView, nullptr);
	vmaFreeMemory(GFX.allocator, images[imageID].allocation);

	images[imageID].texture = VK_NULL_HANDLE;
	freeImageIndexs.push(imageID);
}

void MaterialManager::cleanupImages()
{
	for(int i = 0; i < images.size(); ++i) {
		destroyImage(i);
	}
}

