#ifndef QUASAR_LIGHTMANAGER_H
#define QUASAR_LIGHTMANAGER_H

#include "GFX/RendererData.hpp"

class LightManager
{
public:
	void createBuffer();
	void destroyBuffer();
	void updateBuffer(uint32 currentFrame);
	void createDescriptor();

	std::vector<VkBuffer> pointLightBuffers;
	std::vector <VmaAllocation> pointLightAllocations;
	std::vector <VkBuffer> directionalLightUniforms;
	std::vector <VmaAllocation> directionalLightAllocations;
};

extern LightManager lightManager;

#endif //QUASAR_LIGHTMANAGER_H
