#include "Core/Assets/AssetManager.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include "GFX/Passes/ShadowMapping.hpp"
#include "LightManager.hpp"
#include "GFX/Buffers.hpp"
#include "GFX/Descriptors.hpp"
#include "Utils/Timer.hpp"
#include <vulkan/vulkan_core.h>

using namespace quasar;
LightManager lightManager;

void LightManager::createBuffer()
{
	//Light array
	VkDeviceSize lightSize = GFX.maxPointLights * sizeof(PointLight) + sizeof(uint32) * 4;

	pointLightBuffers.resize(GFX.maxFramesInFlight);
	pointLightAllocations.resize(GFX.maxFramesInFlight);
	directionalLightUniforms.resize(GFX.maxFramesInFlight);
	directionalLightAllocations.resize(GFX.maxFramesInFlight);
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		if (GFX.stagePerFrameData) {
			Buffers::createBuffer(lightSize,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | GFX.deviceOnlyBufferUsage,
				GFX.deviceOnlyProperties, &pointLightBuffers[i], &pointLightAllocations[i]);
			Buffers::createBuffer(sizeof(DirectionalLight),
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | GFX.deviceOnlyBufferUsage,
				GFX.deviceOnlyProperties, &directionalLightUniforms[i],
				&directionalLightAllocations[i]);
		}
		else {
			Buffers::createBuffer(lightSize,
				VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, GFX.hostVisibleProperties,
				&pointLightBuffers[i], &pointLightAllocations[i]);
			Buffers::createBuffer(sizeof(DirectionalLight),
				VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, GFX.hostVisibleProperties,
				&directionalLightUniforms[i], &directionalLightAllocations[i]);
		}

		updateBuffer(i);
	}
}

void LightManager::destroyBuffer()
{
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vkDestroyBuffer(GFX.device, pointLightBuffers[i], nullptr);
		vmaFreeMemory(GFX.allocator, pointLightAllocations[i]);

		vkDestroyBuffer(GFX.device, directionalLightUniforms[i], nullptr);
		vmaFreeMemory(GFX.allocator, directionalLightAllocations[i]);
	}
}

void LightManager::updateBuffer(uint32 currentFrame)
{
	InstanceManager::getTransferData();
	InstanceManager::TransferDataDst& tData = InstanceManager::getTransferData();

	std::vector<PointLight>& pointLights = tData.pointLights;
	uint32 lightCount[4] = {(uint32)pointLights.size()}; // Array of 4 to satisfy 16 bit alignment

	// Omnidirectional shadows
	// TODO: Avoid iterating over each light
	GFX.omniShadowCount = 0;
	for (uint32 i = 0; i < lightCount[0]; ++i) {
		if (pointLights[i].shadowIndex > -1) {
			// Reconstruct radius
			// TODO: Make the constant 1.0f configurable
			float radius = sqrt(1.0f / pointLights[i].minLuminosity + 0.1f);

			pointLights[i].shadowIndex = GFX.omniShadowCount;
			ShadowResources::updateOmniDescriptors(pointLights[i].position,	radius, 
					GFX.omniShadowCount, currentFrame);

			GFX.omniShadowCount++;
			if (GFX.omniShadowCount == GFX.currentMaxOmniShadowCount)
				break;
		}
	}

	size_t bufferDataSizes[2] = {sizeof(uint32) * 4, lightCount[0] * sizeof(PointLight)};
	void* bufferData[2] = {lightCount, pointLights.data()};

	if (GFX.enableStagingBuffer && GFX.stagePerFrameData) {
		Buffers::copyToBufferStaging(pointLightBuffers[currentFrame], bufferDataSizes, bufferData, 2);
	} else {
		Buffers::copyToBuffer(pointLightBuffers[currentFrame], pointLightAllocations[currentFrame], 
				bufferDataSizes, bufferData, 2);
	}

	// Directional light
	DirectionalLight& dirLight = InstanceManager::getTransferData().dirLight;

	if (GFX.enableStagingBuffer && GFX.stagePerFrameData) {
		Buffers::copyToBufferStaging(directionalLightUniforms[currentFrame],
				sizeof(DirectionalLight), &dirLight);
	} else {
		Buffers::copyToBuffer(directionalLightUniforms[currentFrame], directionalLightAllocations[currentFrame],
				sizeof(DirectionalLight), &dirLight);
	}

	GFX.cascadeShadowEnabled = ((dirLight.colour != vec3(0.0f)) && (dirLight.shadowMapped));
}

void LightManager::createDescriptor()
{
	GFX.lightingDescriptors.resize(GFX.maxFramesInFlight);


	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		VkDescriptorBufferInfo pointLightInfo{};
		pointLightInfo.buffer = lightManager.pointLightBuffers[i];
		pointLightInfo.offset = 0;
		pointLightInfo.range = GFX.maxPointLights * sizeof(PointLight) + sizeof(uint32) * 4;

		VkDescriptorBufferInfo directionalLightInfo{};
		directionalLightInfo.buffer = lightManager.directionalLightUniforms[i];
		directionalLightInfo.offset = 0;
		directionalLightInfo.range = sizeof(DirectionalLight);

		DescriptorBuilder lighting;
		lighting.bindBuffer(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, &pointLightInfo);
		lighting.bindBuffer(1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, &directionalLightInfo);

		lighting.build(&GFX.lightingDescriptors[i], GFX.lightingLayout, GFX.persistantDescPool);
	}
}
