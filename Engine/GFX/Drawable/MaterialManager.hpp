#ifndef QUASAR_MATERIALMANAGER_H
#define QUASAR_MATERIALMANAGER_H

#include "GFX/RendererData.hpp"
#include <mutex>
#include <unordered_map>
#include <queue>
#include <vulkan/vulkan_core.h>

class MaterialManager
{
public:
	struct Texture
	{
		int ref = 0;
		VkImage texture;
		VkImageView textureView;
		VmaAllocation allocation;

		uint32 width = 0, height = 0;
	};

	ImageHandle addTextureImage(void* pixelData, uint32 width, uint32 height,
			VkFormat imageFormat, uint32 bytesPerPx);

	ImageHandle addBlankImage(uint32 width, uint32 height);

	void destroyImage(ImageHandle imageID);

	inline Texture* getImage(int imageID)
	{
		std::lock_guard<std::mutex> lck(getImageSync);

		if ((imageID < 0) || (imageID >= images.size())) {
			return nullptr;
		} else {
			return &images[imageID];
		}
	}

	inline VkImageView getImageView(int imageID) 
	{
		std::lock_guard<std::mutex> lck(getImageSync);
	
		if ((imageID < 0) || (imageID >= images.size())) {
			return VK_NULL_HANDLE;
		} else {
			return images[imageID].textureView;
		}	
	}

	void createDescriptor();

	void cleanupImages();

private:
	void createImageInternal(uint32 width, uint32 height, VkFormat imageFormat, 
			uint32 bytesPerPx, Texture& newImage, uint32 mipLevels);

	ImageHandle getNextHandle();

	std::vector<Texture> images;
	std::queue<ImageHandle> freeImageIndexs;

	std::mutex getImageSync;
};

extern MaterialManager materialManager;

#endif //QUASAR_MATERIALMANAGER_H
