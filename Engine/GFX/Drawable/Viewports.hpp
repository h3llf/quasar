#ifndef QUASAR_VIEWPORTS_H
#define QUASAR_VIEWPORTS_H

#include <queue>
#include "GFX/RendererData.hpp"

class Viewports
{
public:
	// Initialize a new viewport and return its index
	ViewportHandle addViewport(int width, int height, bool windowTarget);

	// Destroy all objects in a viewport and free its index
	void destroy(ViewportHandle handle);

	void recreate();
	void destroyAll();

	std::vector<ViewportData> viewportList;

private:
	void init(ViewportData& viewport);
	void deinit(ViewportData& viewport);

	std::queue<ViewportHandle> freeHandles;
};

extern Viewports viewports;

#endif // QUASAR_VIEWPORTS_H
