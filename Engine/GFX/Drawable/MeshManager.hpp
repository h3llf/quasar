#ifndef QUASAR_MESHMANAGER_H
#define QUASAR_MESHMANAGER_H

#include "GFX/RendererData.hpp"
#include "MaterialManager.hpp"
#include <vulkan/vulkan_core.h>

class MeshManager
{
public:

	struct Mesh
	{
		VkBuffer vertexBuffer, indexBuffer;
		VmaAllocation vertexAllocation, indexAllocation;
		uint32 indexCount = 0;
		
		int meshIndex = -1;
		/* TODO
		 * Specify transparency
		 * Multiple shader/pipeline support
		 * */
	
		~Mesh() {};
	};

	std::vector<Mesh> meshes;

	int createMesh(std::vector<quasar::Vertex>* pVertices, std::vector<uint32>* pIndices);

	/* Destroy all meshes */
	void cleanupMeshes();
	void destroyMesh(int meshIndex);

private:
	int modelID = 0;

};

extern MeshManager meshManager;

#endif //QUASAR_MESHMANAGER_H
