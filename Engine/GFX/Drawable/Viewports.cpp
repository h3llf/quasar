#include "Viewports.hpp"
#include "GFX/Passes/DebugLines.hpp"
#include "GFX/Passes/MainShading.hpp"
#include "GFX/Passes/TiledForward.hpp"
#include "GFX/Passes/AmbientOcclusion.hpp"
#include "GFX/RendererData.hpp"

using namespace quasar;
Viewports viewports;

// TODO: Viewport deletion and index reuse

ViewportHandle Viewports::addViewport(int width, int height, bool windowTarget)
{
	ViewportData vp = {
		.width = width,
		.height = height,
		.windowTarget = windowTarget
	};

	ViewportHandle handle = 0;
	if (freeHandles.size()) {
		handle = freeHandles.front();
		freeHandles.pop();
	} else {
		viewportList.push_back(vp);
		handle = viewportList.size() - 1;
	}


	init(viewportList[handle]);
	return handle;
}

void Viewports::init(ViewportData& viewport)
{
	// TODO: REMOVE ?
	viewport.imageCount = GFX.swapchainImages.size();

	// Resize to swapchain target
	if (viewport.width == 0 || viewport.height == 0 || viewport.windowTarget) {
		viewport.width = GFX.internalRes.width;
		viewport.height = GFX.internalRes.height;
	} else {
		viewport.width = std::min(viewport.width, (int)GFX.internalRes.width);
		viewport.height= std::min(viewport.height, (int)GFX.internalRes.height);
	}

	// Init the framebuffers and descriptors for each pass
	TiledForward::viewportInit(viewport);
	MainShading::initViewport(viewport);
	DebugLines::initViewport(viewport);
	AmbientOcclusion::initViewport(viewport);
}

void Viewports::destroy(ViewportHandle handle)
{
	if (handle < 0 || handle >= viewportList.size()) {
		return;
	}

	ViewportData& vp = viewportList[handle];
	deinit(vp);
	freeHandles.push(handle);
	vp.deleted = true;
}

void Viewports::destroyAll()
{
	for (ViewportData& vp : viewportList) {
		if (vp.deleted) {
			continue;
		}

		deinit(vp);
	}
}

void Viewports::recreate()
{
	destroyAll();

	for (ViewportData& vp : viewportList) {
		init(vp);
	}
}

void Viewports::deinit(ViewportData& viewport)
{
	TiledForward::cleanupViewport(viewport);
	MainShading::cleanupViewport(viewport);
	DebugLines::cleanupViewport(viewport);
}
