#ifndef QUASAR_RENDERER_H
#define QUASAR_RENDERER_H

#include <GLFW/glfw3.h>
#include <vector>
#include "Core/Assets/Font.hpp"
#include "Core/Assets/Model.hpp"
#include "Core/Console.hpp"
#include "GFX/RenderTypes.hpp"
#include "GFX/RendererData.hpp"

// Single interface for all renderer functions, only this file should be exposed to the game loop
// Also contains the renderer thread looop
class Renderer
{
public:
	static void initCVars(Console& console);
	static void init();
	static void startThread();

	// Recreates all images and pipelines associated with the current swapchain
	static void recreateSwapchain();

	static GLFWwindow* getWindow();
	static void cleanUp();

	static void drawFrame(ViewportHandle viewport);

	static MeshHandle uploadMesh(quasar::Model::Mesh* pMesh);
	static ImageHandle uploadImage(quasar::Image* image);
	static FontHandle uploadFont(quasar::TextFont* font);
	static TextHandle uploadText(const std::string& content, FontHandle font,
			float lineSpacing, float kerning, float maxWidth, uint32 colour);

	static void framebufferResizeCallback(GLFWwindow* window, int width, int height);

	// Returns the viewport index used for drawing to the window
	static ViewportHandle getWindowViewport();

	// Creates a new viewport for offscreen rendering to the specified texture
	// The viewport index is returned
	static ViewportHandle createTextureViewport(int width, int height);
	static void destroyTextureViewport(ViewportHandle handle);

private:
	static void renderLoop();
	static void updateRemovals();
};

#endif //QUASAR_RENDERER_H
