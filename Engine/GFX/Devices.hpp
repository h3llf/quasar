#ifndef QUASAR_DEVICES_H
#define QUASAR_DEVICES_H

#include "RendererData.hpp"

class Devices
{
public:
	static void initDevice();
	static void getQueueFamilies();

private:
	static void getMemoryProperties(VkPhysicalDeviceProperties* props);
	static void getImageFormats(VkPhysicalDeviceProperties* props);
	static bool checkDeviceExtensions(VkPhysicalDevice device);
};

#endif //QUASAR_DEVICES_H
