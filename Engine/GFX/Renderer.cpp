#include "Core/Assets/InstanceManager.hpp"
#include "GFX/Passes/FontRendering.hpp"
#include "GFX/RenderTypes.hpp"
#include "RendererData.hpp"
#include "RenderSettings.hpp"
#include "Editor/Editor.hpp"
#include "Editor/Profiler.hpp"
#include "Drawable/MeshManager.hpp"
#include "Drawable/LightManager.hpp"
#include "Drawable/MaterialManager.hpp"
#include "Drawable/Viewports.hpp"
#include "Initialization.hpp"
#include "Renderer.hpp"
#include "Devices.hpp"
#include "Utils/Timer.hpp"
#include "Window.hpp"
#include "Swapchain.hpp"
#include "Pipeline.hpp"
#include "RenderPass.hpp"
#include "CommandBuffers.hpp"
#include "Descriptors.hpp"
#include "Passes/ShadowMapping.hpp"
#include <GLFW/glfw3.h>
#include <atomic>
#include <cstdint>
#include <thread>
#include <mutex>
#include <chrono>
#include <vulkan/vulkan_core.h>

using namespace quasar;
using namespace std::chrono_literals;

/*Contains vulkan object data*/
GFX_DATA_TYPE GFX = {};

static std::atomic<bool> rendererRunning;
static std::thread renderThread;

static uint32 prof = 0;
static uint32 profRemovals = 0;
static uint32 profFence = 0;
static uint32 profDataTrans = 0;
static uint32 profLighting = 0;
static uint32 dataTransfer = 0;
static uint32 profAcquireImg = 0;
static uint32 profCmdBuff = 0;
static uint32 profDraw = 0;

static std::chrono::time_point<std::chrono::steady_clock> initTime;		

void Renderer::initCVars(Console& console)
{
	renderSettings.setupCVars(console);
}

void Renderer::init()
{
	Window::createWindow();
	glfwSetFramebufferSizeCallback(GFX.window, Renderer::framebufferResizeCallback);
	Initialization::initVulkan();

	// Create main viewport
	GFX.mainViewportHandle = viewports.addViewport(0, 0, true);

	initTime = std::chrono::steady_clock::now();
}

void Renderer::startThread()
{
	prof = Profiler::addThread("Renderer CPU", "fps");
	profRemovals = Profiler::addTask(prof, "Asset removals");
	profFence = Profiler::addTask(prof, "Wait for fence");
	profDataTrans = Profiler::addTask(prof, "Render data transfer");
	profLighting = Profiler::addTask(prof, "Lighting upload");
	profAcquireImg = Profiler::addTask(prof, "Accuire image");
	profCmdBuff = Profiler::addTask(prof, "Update command buffers");
	profDraw = Profiler::addTask(prof, "Other");

	vkDeviceWaitIdle(GFX.device);
	rendererRunning = true;
	renderThread = std::thread(renderLoop);
}

void Renderer::recreateSwapchain()
{
	std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
	GFX.framebufferResized = true;
}

ViewportHandle Renderer::getWindowViewport()
{
	std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
	return GFX.mainViewportHandle;
}

ViewportHandle Renderer::createTextureViewport(int width, int height)
{
	std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
	return viewports.addViewport(width, height, false);
}

void Renderer::destroyTextureViewport(ViewportHandle handle)
{
	std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
	if (handle != GFX.mainViewportHandle) {
		viewports.destroy(handle);
	}
}

void Renderer::renderLoop()
{
	while (rendererRunning) {
		{
			// Unload images, models from GPU memory
			Profiler::ScopedMeasurement psm(prof, profRemovals);
			updateRemovals();

			// Increment timer for shader use
			std::chrono::duration<double> seconds = std::chrono::steady_clock::now() - initTime;
			GFX.timeSeconds = seconds.count();
		}

		// Wait for previous frame to complete
		{
//			Profiler::ScopedMeasurement psm(prof, profFence);
			vkWaitForFences(GFX.device, 1, &GFX.fencesInFlight.at(GFX.currentFrame),
					VK_TRUE, UINT64_MAX);
		}

		// Update transfer data
		{
			Profiler::ScopedMeasurement psm(prof, profDataTrans);

			InstanceManager::semaphoreSignaled = false;
			InstanceManager::renderWaitSemaphore.acquire();

			// Wait for asset upload to complete
			std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);

			InstanceManager::updateTransferDst();
		}

		// Upload lighting data
		{
			Profiler::ScopedMeasurement psm(prof, profLighting);
			std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
			lightManager.updateBuffer(GFX.currentFrame);
		}

		InstanceManager::TransferDataDst& tData = InstanceManager::getTransferData();
	
		bool drawnToWindow = false;

		// Ensure the renderer can still run if no camera exists
		if (tData.cameras.size() == 0) {
			tData.cameras.push_back({{}, -1, -1, GFX.mainViewportHandle});
		}

		// Submit command buffers and present image
		for (const auto&[camProps, skybox, target, viewport] : tData.cameras) {
			// Prevent drawing to the window multiple times (causes bad flickering)
			// Also prevent mismatch between window/offscreen targets & viewports
			if (viewport == -1 || (drawnToWindow && viewport == GFX.mainViewportHandle) ||
				((target == WINDOW_IMAGE_HANDLE) != (viewport == GFX.mainViewportHandle)))
			{
				continue;
			}
			drawnToWindow |= (viewport == GFX.mainViewportHandle);

			ViewportData& vp = viewports.viewportList[viewport];

			// Update cascaded shadow frustums
			if (GFX.cascadeShadowEnabled) {
				ShadowResources::updateShadowCascades(tData.dirLight.rotation, 
					tData.dirLightYOffset, GFX.currentFrame, vp);
			}

			// Wait for previous frame to complete
			vkWaitForFences(GFX.device, 1, &GFX.fencesInFlight.at(GFX.currentFrame),
					VK_TRUE, UINT64_MAX);

			vp.camProps = camProps;
			vp.targetImage = target;
			GFX.skyboxImgID = skybox;
			Descriptors::updateCamera(GFX.currentFrame, viewport);
			drawFrame(viewport);
		}
	}
}

void Renderer::updateRemovals()
{
	// Last frame data
	InstanceManager::TransferDataDst& tData = InstanceManager::getTransferData();
	std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);

	// Ensure command buffers are no longer in use before removing assets
	if (tData.removedImageIDs.size() || tData.removedText.size()) {
		vkDeviceWaitIdle(GFX.device);
#ifndef _DISABLE_EDITOR
		Editor::clearImageCache();
#endif //_DISABLE_EDITOR
	}

	for (ImageHandle handle : tData.removedImageIDs) {
		materialManager.destroyImage(handle);
	}
	tData.removedImageIDs.clear();

	for (TextHandle handle: tData.removedText) {
		FontRendering::destroyText(handle);
	}
	tData.removedText.clear();
}

MeshHandle Renderer::uploadMesh(Model::Mesh *pMesh)
{
	std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
	return meshManager.createMesh(&pMesh->vertices, &pMesh->indices);
}

ImageHandle Renderer::uploadImage(Image* image)
{
	std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);

	if (image->imageUsage == Image::Blank) {
		return materialManager.addBlankImage(image->width, image->height);
	}

	VkFormat imageFormat;

	// Only uses 32 bit images as most modern GPUs only support this format
	switch (image->imageUsage) {
	case Image::Normal:
		imageFormat = VK_FORMAT_R8G8B8A8_UNORM;
		break;
	case Image::HDR:
		imageFormat = VK_FORMAT_R16G16B16A16_SFLOAT;
		break;
	default:
		imageFormat = VK_FORMAT_R8G8B8A8_SRGB;
	}

	uint32 bytesPerPx = 4;
	if (image->imageUsage == Image::HDR)
		bytesPerPx = 8;

	return materialManager.addTextureImage(image->pixels, image->width, image->height, 
		imageFormat, bytesPerPx);
}

FontHandle Renderer::uploadFont(quasar::TextFont *font)
{
	std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
	return FontRendering::uploadFont(font);
}

TextHandle Renderer::uploadText(const std::string &content, FontHandle font,
		float lineSpacing, float kerning, float maxWidth, uint32 colour)
{
	std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
	return FontRendering::uploadText(content, font, lineSpacing, kerning, maxWidth, colour);
}

void Renderer::cleanUp()
{
	{
		std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
		rendererRunning = false;
	}
	
	InstanceManager::renderWaitSemaphore.release();
	renderThread.join();

	vkDeviceWaitIdle(GFX.device);
	viewports.destroyAll();
	Initialization::cleanUp();
}

GLFWwindow* Renderer::getWindow()
{
	return GFX.window;
}

void Renderer::drawFrame(ViewportHandle viewport)
{
	uint32 imageIndex = 0;
	VkResult result;

	// No need to accuire image for offscreen target
	ViewportData& vp = viewports.viewportList[viewport];
	if (vp.windowTarget) {
		Profiler::ScopedMeasurement psm(prof, profAcquireImg);
		result = vkAcquireNextImageKHR(GFX.device, GFX.swapchain, UINT64_MAX,
				GFX.imageAvaliableSemaphores.at(GFX.currentFrame),
				VK_NULL_HANDLE, &imageIndex);

		// Wndow resizing if image is out of date
		if (result == VK_ERROR_OUT_OF_DATE_KHR) {
			std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
			Initialization::recreateSwapchain();
			viewports.recreate();
			return;
		} else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
			throw std::runtime_error("failed to acquire swap chain image!");
		}
	}

	{
		Profiler::ScopedMeasurement psm(prof, profCmdBuff);
		vkResetCommandPool(GFX.device, GFX.commandPools[GFX.currentFrame], 0);
		CommandBuffers::updateCommandBuffers(GFX.currentFrame, imageIndex, vp);
	}

	Profiler::ScopedMeasurement psm(prof, profDraw);

	VkSemaphore waitSemaphores[] = {GFX.imageAvaliableSemaphores.at(GFX.currentFrame)};
	VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_ALL_COMMANDS_BIT}; // TODO: Change?

	// Command buffer completion
	VkSemaphore signalSemaphores[] = {GFX.renderFinishedSemaphores.at(GFX.currentFrame)};

	// TODO: Add viewport wait semaphore for offscreen rendering

	VkSubmitInfo submitInfo {};
	// Wait until image is avaliable before writing colours
	submitInfo.sType  = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.waitSemaphoreCount = (vp.windowTarget) ? 1 : 0;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;
	
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &GFX.shadingCmdBuffers[GFX.currentFrame];

	submitInfo.signalSemaphoreCount = (vp.windowTarget) ? 1 : 0;
	submitInfo.pSignalSemaphores = signalSemaphores;

	vkResetFences(GFX.device, 1, &GFX.fencesInFlight[GFX.currentFrame]);

	// Submit queue and signal fence to indicate this frame has finished
	GFX.rendererSyncMutex.lock();
	VK_CHECK( vkQueueSubmit(GFX.graphicsQueue, 1, &submitInfo, GFX.fencesInFlight[GFX.currentFrame]),
			"Failed to submit command buffer");
	GFX.rendererSyncMutex.unlock();

	if (vp.windowTarget) {
		VkSwapchainKHR swapchains[] = {GFX.swapchain};

		VkPresentInfoKHR presentInfo {};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		// Semaphoes to wait on before presentation can happen
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = signalSemaphores;
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = swapchains;
		presentInfo.pImageIndices = &imageIndex;

		GFX.rendererSyncMutex.lock();
		result = vkQueuePresentKHR(GFX.graphicsQueue, &presentInfo);
		GFX.rendererSyncMutex.unlock();

		std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
		if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || GFX.framebufferResized) {
			Initialization::recreateSwapchain();
			viewports.recreate();
			GFX.framebufferResized = false;
		} else if (result != VK_SUCCESS) {
			Log::fatal("Failed to accuire swapchain image");
		}
	}

	GFX.currentFrame = (GFX.currentFrame + 1) % GFX.maxFramesInFlight;
}

void Renderer::framebufferResizeCallback(GLFWwindow* window, int width, int height) 
{
	std::lock_guard<std::mutex> lck(GFX.rendererSyncMutex);
	GFX.framebufferResized = true;
}
