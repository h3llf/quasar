#include "Buffers.hpp"
#include "RendererData.hpp"
#include "CommandBuffers.hpp"
#include <vulkan/vulkan_core.h>

using namespace quasar;

void Buffers::createBuffer(VkDeviceSize size,
		VkBufferUsageFlags usage,
		VkMemoryPropertyFlags properties,
		VkBuffer* buffer,
		VmaAllocation* allocation)

{
	VkBufferCreateInfo bufferInfo {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VmaAllocationCreateInfo allocInfo {};
	allocInfo.requiredFlags = properties;

	VK_CHECK( vmaCreateBuffer(GFX.allocator, &bufferInfo, &allocInfo, buffer, allocation, nullptr),
			"VMA buffer creation failed");
}

void Buffers::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size)
{
	VkCommandBuffer cmdBuffer = CommandBuffers::beginTransferCmd();

	VkBufferCopy copyRegion {};
	//optional src and dst offset
	copyRegion.size = size;
	vkCmdCopyBuffer(cmdBuffer, srcBuffer, dstBuffer, 1, &copyRegion);
	
	CommandBuffers::endTransferCmd();
}

void Buffers::copyToImage(VkBuffer buffer, VkImage image, VkExtent3D extent)
{
	VkCommandBuffer cmdBuffer = CommandBuffers::beginTransferCmd();

	VkBufferImageCopy region {};
	region.bufferOffset = 0;
	region.bufferRowLength = 0;
	region.bufferImageHeight = 0;
	region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	region.imageSubresource.mipLevel = 0;
	region.imageSubresource.baseArrayLayer = 0;
	region.imageSubresource.layerCount = 1;
	region.imageExtent = extent;

	vkCmdCopyBufferToImage(cmdBuffer, buffer, image, 
			VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

	CommandBuffers::endTransferCmd();
}

void Buffers::copyToBuffer(VkBuffer dstBuffer, VmaAllocation dstAlloc, size_t* size, 
		void** srcData, uint32 count)
{
	void* data;
	vmaMapMemory(GFX.allocator, dstAlloc, &data);
	uint32 currentOffset = 0;
	for (uint32 i = 0; i < count; ++i) {
		memcpy((char*)data + currentOffset, srcData[i], size[i]);
		currentOffset += size[i];
	}
	vmaUnmapMemory(GFX.allocator, dstAlloc);
}

void Buffers::copyToBuffer(VkBuffer dstBuffer, VmaAllocation dstAlloc, size_t size, void* srcData)
{
	void* data;
	vmaMapMemory(GFX.allocator, dstAlloc, &data);
	memcpy(data, srcData, size);
	vmaUnmapMemory(GFX.allocator, dstAlloc);
}

void Buffers::createStagingBuffer(size_t size, VkBuffer* buffer, VmaAllocation* alloc)
{
	// Check the allocation doesn't exceed avaliable memory
	VmaBudget budgets[VK_MAX_MEMORY_HEAPS];
	vmaGetHeapBudgets(GFX.allocator, budgets);

	VkMemoryPropertyFlags memProps;
	if (size> budgets[GFX.stagingMemoryHeap].budget) {
		memProps = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
	} else {
		memProps = GFX.hostVisibleProperties;
	}

	Buffers::createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, memProps, buffer, alloc);


}

void Buffers::copyToBufferStaging(VkBuffer dstBuffer, size_t* size, void** srcData, uint32 count)
{
	VkBuffer stagingBuffer;
	VmaAllocation stagingAllocation;

	uint32 totalSize = 0;
	for (uint32 i = 0; i < count; ++i) {
		totalSize += size[i];
	}
	uint32 currentOffset = 0;

	createStagingBuffer(totalSize, &stagingBuffer, &stagingAllocation);

	void* data;
	vmaMapMemory(GFX.allocator, stagingAllocation, &data);

	for (uint32 i = 0; i < count; ++i) {
		memcpy((char*)data + currentOffset, srcData[i], size[i]);
		currentOffset += size[i];
	}

	vmaUnmapMemory(GFX.allocator, stagingAllocation);
	copyBuffer(stagingBuffer, dstBuffer, totalSize);

	vkDestroyBuffer(GFX.device, stagingBuffer, nullptr);
	vmaFreeMemory(GFX.allocator, stagingAllocation);

}

void Buffers::copyToBufferStaging(VkBuffer dstBuffer, size_t size, void* srcData)
{
	VkBuffer stagingBuffer;
	VmaAllocation stagingAllocation;

	createStagingBuffer(size, &stagingBuffer, &stagingAllocation);

	void* data;
	vmaMapMemory(GFX.allocator, stagingAllocation, &data);
	memcpy(data, srcData, size);
	vmaUnmapMemory(GFX.allocator, stagingAllocation);

	copyBuffer(stagingBuffer, dstBuffer, size);

	vkDestroyBuffer(GFX.device, stagingBuffer, nullptr);
	vmaFreeMemory(GFX.allocator, stagingAllocation);
}

void Buffers::createVertexBuffer(VkBuffer *vertexBuffer, 
		VmaAllocation* vertexAllocation, 
		std::vector<Vertex> *vertices)
{
	VkDeviceSize bufferSize = sizeof(Vertex) * vertices->size();
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			vertexBuffer, vertexAllocation);

	copyToBufferStaging(*vertexBuffer, bufferSize, vertices->data());
}

void Buffers::createIndexBuffer(VkBuffer *indexBuffer,
		VmaAllocation *indexAllocation, 
		std::vector<uint32> *indices)
{
	VkDeviceSize bufferSize = sizeof(indices->at(0)) * indices->size();
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			indexBuffer, indexAllocation);

	copyToBufferStaging(*indexBuffer, bufferSize, indices->data());
}

VkVertexInputBindingDescription Buffers::getBindDesc()
{
	VkVertexInputBindingDescription bindDesc {};
	bindDesc.binding = 0;
	bindDesc.stride = sizeof(Vertex);
	bindDesc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	return bindDesc;
}

std::vector<VkVertexInputAttributeDescription> Buffers::getAttribDesc(bool depthOnly)
{
	std::vector<VkVertexInputAttributeDescription> attribDesc;
	
	VkVertexInputAttributeDescription positionDesc {
		.location = 0,
		.binding = 0,
		.format = VK_FORMAT_R32G32B32_SFLOAT,
		.offset = offsetof(Vertex, position)
	};

	VkVertexInputAttributeDescription normalDesc {
		.location = 1,
		.binding = 0,
		.format = VK_FORMAT_R32G32B32_SFLOAT,
		.offset = offsetof(Vertex, normal)
	};

	VkVertexInputAttributeDescription tangentDesc {
		.location = 2,
		.binding = 0,
		.format = VK_FORMAT_R32G32B32A32_SFLOAT,
		.offset = offsetof(Vertex, tangent)
	};

	VkVertexInputAttributeDescription uvDesc {
		.location = (depthOnly) ? 1u : 3u,
		.binding = 0,
		.format = VK_FORMAT_R32G32_SFLOAT,
		.offset = offsetof(Vertex, uv)
	};

	if (depthOnly) {
		attribDesc.push_back(positionDesc);
		attribDesc.push_back(uvDesc);
	} else {
		attribDesc.push_back(positionDesc);
		attribDesc.push_back(normalDesc);
		attribDesc.push_back(tangentDesc);
		attribDesc.push_back(uvDesc);
	}

	return attribDesc;
}

uint32 Buffers::findMemoryType(uint32 typeFilter, VkMemoryPropertyFlags properties)
{
	VkPhysicalDeviceMemoryProperties memProps;
	vkGetPhysicalDeviceMemoryProperties(GFX.physicalDevice, &memProps);

	for (uint32 i = 0; i < memProps.memoryTypeCount; ++i) {
		if ((typeFilter & (1 << i)) && //Suitability
				(memProps.memoryTypes[i].propertyFlags & properties)
				== properties) { //Check that all desired properties are present

			return i;
		}
	}

	Log::fatal("No suitable memory type for vertex buffer was found");
	return 1;
}
