#include "AmbientOcclusion.hpp"
#include "GFX/RenderSettings.hpp"
#include "GFX/ImageCreation.hpp"
#include "GFX/Descriptors.hpp"
#include "GFX/Pipeline.hpp"
#include <cmath>
#include <vulkan/vulkan_core.h>

void AmbientOcclusion::init()
{
	GFX.gtaoData.scale = renderSettings.aoResolutionScale;
	uint32 width = GFX.internalRes.width * GFX.gtaoData.scale;
	uint32 height = GFX.internalRes.height * GFX.gtaoData.scale;

	GFX.gtaoData.aoImg.resize(GFX.maxFramesInFlight);
	GFX.gtaoData.aoImgAlloc.resize(GFX.maxFramesInFlight);
	GFX.gtaoData.aoImgView.resize(GFX.maxFramesInFlight);
	GFX.gtaoData.imageDescriptors.resize(GFX.maxFramesInFlight);
	GFX.gtaoData.depthSets.resize(GFX.numImages);

	// Create image
	VkImageCreateInfo imgInfo = ImageCreation::createImageInfo(GFX.gtaoData.format,
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT |
			VK_IMAGE_USAGE_SAMPLED_BIT, VkExtent3D{width, height, 1});

	VmaAllocationCreateInfo imgAllocInfo {};
	imgAllocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
	imgAllocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vmaCreateImage(GFX.allocator, &imgInfo, &imgAllocInfo, &GFX.gtaoData.aoImg[i],
				&GFX.gtaoData.aoImgAlloc[i], nullptr);

		ImageCreation::transitionImageLayout(GFX.gtaoData.aoImg[i], false,
				VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1);

		GFX.gtaoData.aoImgView[i] = ImageCreation::createImageView(GFX.gtaoData.format,
				GFX.gtaoData.aoImg[i], VK_IMAGE_ASPECT_COLOR_BIT, 1);
	}

	// Descriptor layout
	// 2 layouts as maxFramesInFlight can be <= num images
	DescriptorBuilder builder;
	builder.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);
	builder.createLayout(&GFX.gtaoData.descLayout);

	builder = {};
	builder.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_COMPUTE_BIT);
	builder.createLayout(&GFX.gtaoData.depthLayout);

	// Descriptor creation
	createDescriptors();

	// Create pipeline
	Pipeline::initPostProcessPipelines();
}

void AmbientOcclusion::initViewport(ViewportData& vp)
{
	vp.aoResX = vp.width * GFX.gtaoData.scale;
	vp.aoResY = vp.height * GFX.gtaoData.scale;
	vp.AOGroupsX = std::ceil((float)vp.aoResX / GFX.gtaoData.groupSize);
	vp.AOGroupsY = std::ceil((float)vp.aoResY / GFX.gtaoData.groupSize);
}

void AmbientOcclusion::createDescriptors()
{
	// Image view and descriptor
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		DescriptorBuilder builder;

		VkDescriptorImageInfo imgDescInfo {};
		imgDescInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
		imgDescInfo.imageView = GFX.gtaoData.aoImgView[i];
		imgDescInfo.sampler = GFX.depthSampler;

		builder.bindImage(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, &imgDescInfo);
		builder.build(&GFX.gtaoData.imageDescriptors[i], GFX.gtaoData.descLayout,
				GFX.descriptorPool);

		VkDescriptorImageInfo depthImgInfo {};
		depthImgInfo.imageView = GFX.mainShading.depImgViews[i];
		//depthImgInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
		depthImgInfo.imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
		depthImgInfo.sampler = GFX.aoSampler;

		builder = {};
		builder.bindImage(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, &depthImgInfo);
		builder.build(&GFX.gtaoData.depthSets[i], GFX.gtaoData.depthLayout,
			GFX.descriptorPool);
	}
}

void AmbientOcclusion::createDepthSampler()
{
	VkSamplerCreateInfo samplerInfo {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;

	//Ansitropic filtering
	VkPhysicalDeviceProperties properties {};
	vkGetPhysicalDeviceProperties(GFX.physicalDevice, &properties);

	float desiredAnisotropy = 16.0f;
	desiredAnisotropy = std::min(desiredAnisotropy, properties.limits.maxSamplerAnisotropy);

	samplerInfo.anisotropyEnable = VK_FALSE;
	samplerInfo.maxAnisotropy = desiredAnisotropy;
	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	samplerInfo.unnormalizedCoordinates = VK_FALSE;
	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.mipLodBias = 0.0f;
	samplerInfo.minLod = 0.0f;
	samplerInfo.maxLod = 1.0f;

	vkCreateSampler(GFX.device, &samplerInfo, nullptr, &GFX.aoSampler);
}

void AmbientOcclusion::cleanup()
{
	vkDestroyPipeline(GFX.device, GFX.gtaoData.pipeline, nullptr);
	vkDestroyPipelineLayout(GFX.device, GFX.gtaoData.pipelineLayout, nullptr);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vkDestroyImageView(GFX.device, GFX.gtaoData.aoImgView[i], nullptr);
		vmaFreeMemory(GFX.allocator, GFX.gtaoData.aoImgAlloc[i]);
		vkDestroyImage(GFX.device, GFX.gtaoData.aoImg[i], nullptr);
	}

	vkDestroyDescriptorSetLayout(GFX.device, GFX.gtaoData.descLayout, nullptr);
	vkDestroyDescriptorSetLayout(GFX.device, GFX.gtaoData.depthLayout, nullptr);

}
