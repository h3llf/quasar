#ifndef QUASAR_OUTLINE_H
#define QUASAR_OUTLINE_H

#include "GFX/RendererData.hpp"
class Outlines
{
public:
	static void init();
	static void cleanup();

	struct PushConstantData
	{
		vec4 colour;
		int currentSample;
		int numSamples;
		int width;
		int height;
		float radius;
		float fadeoutRadius;
		vec2 ndcScale;
	};
};

#endif //QUASAR_OUTLINE_H
