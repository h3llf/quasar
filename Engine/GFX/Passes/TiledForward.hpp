#ifndef QUASAR_TILED_FORWARD_H
#define QUASAR_TILED_FORWARD_H

#include "Core/Camera.hpp"
#include "GFX/RendererData.hpp"

struct TileFrustum
{
	//left, right, up, down
	vec4 planes[4];
};

class TiledForward
{
public:
	static void init();
	static void viewportInit(ViewportData& viewport);
	static void cleanup();
	static void cleanupViewport(ViewportData& viewport);

private:
	static void genGridFrustums(std::vector<TileFrustum>* frustums, ViewportData& viewport);
	static vec4 computePlane(vec3 origin, vec3 p1, vec3 p2);

	static void createDescriptorLayout();

	static void createDescriptors(std::vector<TileFrustum>* frustums);
	static void createDescriptors(std::vector<TileFrustum>* frustums, ViewportData& viewport);
	static void createDepthDescriptor();
};

#endif //QUASAR_TILED_FORWARD_H
