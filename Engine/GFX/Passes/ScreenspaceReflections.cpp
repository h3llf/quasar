#include "ScreenspaceReflections.hpp"
#include "GFX/Descriptors.hpp"
#include "GFX/Buffers.hpp"
#include "GFX/ImageCreation.hpp"
#include "GFX/Pipeline.hpp"
#include <vulkan/vulkan_core.h>

void SSR::init()
{
	/*
	// Ray tracing at half resolution
	vec2 tracingRes = vec2(GFX.internalRes.width * GFX.ssrData.resolutionScale, 
			GFX.internalRes.height * GFX.ssrData.resolutionScale);

	GFX.ssrData.tracingGroupsX = std::ceil(tracingRes.x / 16.0f);
	GFX.ssrData.tracingGroupsY = std::ceil(tracingRes.y / 16.0f);

	GFX.ssrData.tracingDesc.resize(GFX.maxFramesInFlight);
	GFX.ssrData.resolveDesc.resize(GFX.maxFramesInFlight);
	GFX.ssrData.hitResultImages.resize(GFX.maxFramesInFlight);
	GFX.ssrData.hitResultImgViews.resize(GFX.maxFramesInFlight);
	GFX.ssrData.hitResultImgAlloc.resize(GFX.maxFramesInFlight);
	GFX.ssrData.prevImages.resize(GFX.maxFramesInFlight);
	GFX.ssrData.prevImgViews.resize(GFX.maxFramesInFlight);
	GFX.ssrData.prevImgAlloc.resize(GFX.maxFramesInFlight);
	GFX.ssrData.prevEvents.resize(GFX.maxFramesInFlight);

	Uniform uniform; 
	uniform.width = tracingRes.x;
	uniform.height = tracingRes.y;

	// Uniform buffer
	Buffers::createBuffer(sizeof(Uniform), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | 
			VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
			&GFX.ssrData.uniform, &GFX.ssrData.uniformAlloc);

	Buffers::copyToBufferStaging(GFX.ssrData.uniform, sizeof(Uniform), &uniform);

	// Layout
	DescriptorBuilder descBuilder;
	descBuilder.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_COMPUTE_BIT);
	descBuilder.createLayoutBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_COMPUTE_BIT);
	descBuilder.createLayoutBinding(2, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);
	descBuilder.createLayoutBinding(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_COMPUTE_BIT);
	descBuilder.createLayout(&GFX.ssrData.tracingDescLayout);

	descBuilder = {};
	descBuilder.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);
	descBuilder.createLayoutBinding(1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);
	descBuilder.createLayoutBinding(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_COMPUTE_BIT);
	descBuilder.createLayoutBinding(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_COMPUTE_BIT);
	descBuilder.createLayout(&GFX.ssrData.resolveDescLayout);

	for (int i = 0; i < GFX.maxFramesInFlight; ++i) {
		VkEventCreateInfo eventInfo {};
		eventInfo.sType = VK_STRUCTURE_TYPE_EVENT_CREATE_INFO;
		eventInfo.pNext = nullptr;
		eventInfo.flags = VK_EVENT_CREATE_DEVICE_ONLY_BIT;

		vkCreateEvent(GFX.device, &eventInfo, nullptr, &GFX.ssrData.prevEvents[i]);

		// TODO: Image arrays for > 1 spp
		// Result image
		VkImageCreateInfo imgInfo = ImageCreation::createImageInfo(GFX.ssrData.hitResultFormat, 
				VK_IMAGE_USAGE_STORAGE_BIT, GFX.internalRes);

		VmaAllocationCreateInfo allocInfo = {};
		allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
		allocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

		vmaCreateImage(GFX.allocator, &imgInfo, &allocInfo, &GFX.ssrData.hitResultImages[i], 
				&GFX.ssrData.hitResultImgAlloc[i], nullptr);

		GFX.ssrData.hitResultImgViews[i] = ImageCreation::createImageView(GFX.ssrData.hitResultFormat, 
				GFX.ssrData.hitResultImages[i], VK_IMAGE_ASPECT_COLOR_BIT, 1);

		ImageCreation::transitionImageLayout(GFX.ssrData.hitResultImages[i], false, 
				VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 1);

		// Previous frame image
		// TODO: Using the previous frame direcly is preferable
		VkImageCreateInfo prevImgInfo = ImageCreation::createImageInfo(GFX.mainShading.imgFormat, 
				VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, GFX.internalRes);

		vmaCreateImage(GFX.allocator, &prevImgInfo, &allocInfo, &GFX.ssrData.prevImages[i],
				&GFX.ssrData.prevImgAlloc[i], nullptr);

		GFX.ssrData.prevImgViews[i] = ImageCreation::createImageView(GFX.mainShading.imgFormat, 
				GFX.ssrData.prevImages[i], VK_IMAGE_ASPECT_COLOR_BIT, 1);

		ImageCreation::transitionImageLayout(GFX.ssrData.prevImages[i], false, 
				VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1);

		// Tracing descriptors
		// TODO: Change sampler to edge clamp
		VkDescriptorImageInfo depthInfo {};
		depthInfo.imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
		depthInfo.imageView = GFX.mainShading.depImgViews[i];
		depthInfo.sampler = GFX.textureSampler;

		VkDescriptorImageInfo gBufferInfo {};
		gBufferInfo .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		gBufferInfo .imageView = GFX.mainShading.gBufferImgViews[i];
		gBufferInfo .sampler = GFX.textureSampler;

		VkDescriptorImageInfo resultInfo {};
		resultInfo .imageLayout = VK_IMAGE_LAYOUT_GENERAL;
		resultInfo .imageView = GFX.ssrData.hitResultImgViews[i];//GFX.mainShading.imgViews[i]; // Change to result image
		resultInfo .sampler = GFX.textureSampler;

		VkDescriptorBufferInfo uniformInfo {};
		uniformInfo.buffer = GFX.ssrData.uniform;
		uniformInfo.offset = 0;
		uniformInfo.range = sizeof(Uniform);

		descBuilder = {};
		descBuilder.bindImage(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, &depthInfo);
		descBuilder.bindImage(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, &gBufferInfo);
		descBuilder.bindImage(2, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, &resultInfo);
		descBuilder.bindBuffer(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, &uniformInfo);
		descBuilder.build(&GFX.ssrData.tracingDesc[i], GFX.ssrData.tracingDescLayout, GFX.descriptorPool);

		VkDescriptorImageInfo shadingInfo {};
		shadingInfo .imageLayout = VK_IMAGE_LAYOUT_GENERAL;
		shadingInfo .imageView = GFX.mainShading.imgViews[i];
		shadingInfo .sampler = GFX.textureSampler;

		VkDescriptorImageInfo prevShadingInfo {};
		prevShadingInfo .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		prevShadingInfo .imageView = GFX.ssrData.prevImgViews[i];
		prevShadingInfo .sampler = GFX.textureSampler;

		descBuilder = {};
		descBuilder.bindImage(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, &resultInfo);
		descBuilder.bindImage(1, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, &shadingInfo);
		descBuilder.bindImage(2, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, &prevShadingInfo);
		descBuilder.bindBuffer(3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, &uniformInfo);
		descBuilder.build(&GFX.ssrData.resolveDesc[i], GFX.ssrData.resolveDescLayout, GFX.descriptorPool);
	}

	VkDescriptorSetLayout tracingLayouts[2] = {GFX.ssrData.tracingDescLayout, GFX.cameraOnlyLayout};

	PipelineBuilder builder;
	builder.beginPipeline(&GFX.ssrData.pipeline, &GFX.ssrData.layout);
	builder.addShaderStage("Shaders/SSSR_comp.spv", VK_SHADER_STAGE_COMPUTE_BIT);
	builder.setPipelineLayout(tracingLayouts, 2);
	builder.createComputePipeline();

//	VkDescriptorSetLayout resolveLayouts[2] = {GFX.ssrData.resolveDescLayout, GFX.cameraOnlyLayout};

	builder = {};
	builder.beginPipeline(&GFX.ssrData.resolvePipeline, &GFX.ssrData.resolveLayout);
	builder.addShaderStage("Shaders/SSSR_Resolve_comp.spv", VK_SHADER_STAGE_COMPUTE_BIT);
	builder.setPipelineLayout(&GFX.ssrData.resolveDescLayout, 1);
	builder.createComputePipeline();*/
}

void SSR::cleanup()
{
	/*
	vkDestroyPipeline(GFX.device, GFX.ssrData.pipeline, nullptr);
	vkDestroyPipelineLayout(GFX.device, GFX.ssrData.layout, nullptr);

	vkDestroyPipeline(GFX.device, GFX.ssrData.resolvePipeline, nullptr);
	vkDestroyPipelineLayout(GFX.device, GFX.ssrData.resolveLayout, nullptr);

	vkDestroyDescriptorSetLayout(GFX.device, GFX.ssrData.tracingDescLayout, nullptr);
	vkDestroyDescriptorSetLayout(GFX.device, GFX.ssrData.resolveDescLayout, nullptr);

	vmaDestroyBuffer(GFX.allocator, GFX.ssrData.uniform, GFX.ssrData.uniformAlloc);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vmaDestroyImage(GFX.allocator, GFX.ssrData.hitResultImages[i], GFX.ssrData.hitResultImgAlloc[i]);
		vkDestroyImageView(GFX.device, GFX.ssrData.hitResultImgViews[i], nullptr);

		vmaDestroyImage(GFX.allocator, GFX.ssrData.prevImages[i], GFX.ssrData.prevImgAlloc[i]);
		vkDestroyImageView(GFX.device, GFX.ssrData.prevImgViews[i], nullptr);

		vkDestroyEvent(GFX.device, GFX.ssrData.prevEvents[i], nullptr);
	}*/
}
