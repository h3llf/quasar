#ifndef QUASAR_SHADOWRESOURCES_H
#define QUASAR_SHADOWRESOURCES_H

#include "GFX/RendererData.hpp"

class ShadowResources
{
public:
	static void pointShadowInit();
	static void cascadedShadowInit();
	static void createDescLayouts();
	static void recreateDescriptors(ShadowPassData* shadowData, bool cubeMap);
	static void updateOmniDescriptors(vec3 lightPos, float radius, uint32 index, uint32 currentFrame);
	static void updateShadowCascades(vec3 direction, float yOffset, uint32 currentFrame, ViewportData& vp);
	static void cleanUp(ShadowPassData* shadowData);

private:
	struct OmniUniformData
	{
		qml::mat4 views[6];
		qml::vec3 lightPos;
		float radius;
	};
};

#endif //QUASAR_SHADOWRESOURCES_H
