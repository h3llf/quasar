#ifndef QUASAR_BLOOM_H
#define QUASAR_BLOOM_H

class BloomPass
{
public:
	static void init();
	static void cleanup();
};

#endif //QUASAR_BLOOM_H
