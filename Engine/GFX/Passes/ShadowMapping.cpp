#include "ShadowMapping.hpp"
#include "GFX/Buffers.hpp"
#include "GFX/Descriptors.hpp"
#include "GFX/Drawable/Viewports.hpp"
#include "GFX/ImageCreation.hpp"
#include "GFX/RenderPass.hpp"
#include "GFX/RenderSettings.hpp"
#include <cmath>
#include <cstdlib>
#include <limits>

void ShadowResources::createDescLayouts()
{
	DescriptorBuilder builder;
	builder.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 
			VK_SHADER_STAGE_VERTEX_BIT);
	builder.createLayout(&GFX.omniShadowData.shadowCamLayout);
	builder.createLayout(&GFX.cascadeShadowData.shadowCamLayout);
}

// TODO: Combine most point and cascaded shadow initialization
void ShadowResources::pointShadowInit()
{
	uint32 shadowCount = GFX.currentMaxOmniShadowCount;
	uint32 shadowRes = GFX.currentOmniResolution;

	GFX.omniShadowData.images.resize(GFX.maxFramesInFlight);
	GFX.omniShadowData.allocations.resize(GFX.maxFramesInFlight);
	GFX.omniShadowData.views.resize(GFX.maxFramesInFlight);
	GFX.omniShadowData.frameBuffers.resize(GFX.maxFramesInFlight * shadowCount);
	GFX.omniShadowData.frameBufferViews.resize(GFX.maxFramesInFlight * shadowCount);

	GFX.omniShadowData.cameraUniform.resize(GFX.maxFramesInFlight * shadowCount);
	GFX.omniShadowData.cameraAllocation.resize(GFX.maxFramesInFlight * shadowCount);
	GFX.omniShadowData.shadowCamDesc.resize(GFX.maxFramesInFlight * shadowCount);

	for (uint32 i = 0; i < GFX.maxFramesInFlight * shadowCount; ++i) {
		//Uniform for view and projection matrix
		if (GFX.stagePerFrameData) {
			Buffers::createBuffer(sizeof(OmniUniformData),
					VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | GFX.deviceOnlyBufferUsage,
					GFX.deviceOnlyProperties, &GFX.omniShadowData.cameraUniform[i],
					&GFX.omniShadowData.cameraAllocation[i]);
		} else {
			Buffers::createBuffer(sizeof(OmniUniformData),
					VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, GFX.hostVisibleProperties, 
					&GFX.omniShadowData.cameraUniform[i],
					&GFX.omniShadowData.cameraAllocation[i]);
		}
	}

	const uint32 numLayers = 6 * shadowCount;

	// Uniform buffer
	VkImageCreateInfo imgInfo = ImageCreation::createImageInfo(
			GFX.shadowDepthFormat, VK_IMAGE_USAGE_SAMPLED_BIT | 
			VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
			VkExtent3D{shadowRes, shadowRes, 1});

	imgInfo.arrayLayers = numLayers;
	imgInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;

	VmaAllocationCreateInfo imgAllocInfo {};
	imgAllocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
	imgAllocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

	// TODO: Only create the required number of images and use specialization constants in the shader
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vmaCreateImage(GFX.allocator, &imgInfo, &imgAllocInfo, &GFX.omniShadowData.images[i],
				&GFX.omniShadowData.allocations[i], nullptr);

		GFX.omniShadowData.views[i] = ImageCreation::createImageView(
				GFX.shadowDepthFormat, GFX.omniShadowData.images[i],
				VK_IMAGE_ASPECT_DEPTH_BIT, 1, VK_IMAGE_VIEW_TYPE_CUBE_ARRAY, numLayers);
		
		// Required to stop validation errors on unused images
		ImageCreation::transitionImageLayout(GFX.omniShadowData.images[i], true,
				VK_IMAGE_LAYOUT_UNDEFINED, 
				VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL, 
				numLayers);
	}

	//Render Pass
	RenderPass::depthRenderPass(&GFX.omniShadowData.renderPass, 6, true, false);

	//Framebuffer
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		for (uint32 j = 0; j < shadowCount; ++j) {
			uint32 layerIndex = i + GFX.maxFramesInFlight * j;

			GFX.omniShadowData.frameBufferViews[layerIndex] =
					ImageCreation::createImageView( GFX.shadowDepthFormat,
					GFX.omniShadowData.images[i], VK_IMAGE_ASPECT_DEPTH_BIT,
					1, VK_IMAGE_VIEW_TYPE_CUBE, 6, 6 * j);

			VkFramebufferCreateInfo fbCreateInfo {};
			fbCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			fbCreateInfo.renderPass = GFX.omniShadowData.renderPass;
			fbCreateInfo.attachmentCount = 1;
			fbCreateInfo.pAttachments = &GFX.omniShadowData.frameBufferViews[layerIndex];
			fbCreateInfo.width = shadowRes;
			fbCreateInfo.height = shadowRes;
			fbCreateInfo.layers = 1;

			VK_CHECK(vkCreateFramebuffer(GFX.device, &fbCreateInfo, nullptr,
					&GFX.omniShadowData.frameBuffers[layerIndex]),
					"Failed to create shadow framebuffer"); 
		}
	}
}

// TODO: Runtime editable cascade count using vulkan specialization constants
void ShadowResources::cascadedShadowInit()
{
	uint32 cascadeCount = GFX.currentCascadeCount;
	uint32 cascadeRes = GFX.currentCascadeResolution;

	GFX.currentCascadeCount = cascadeCount;
	GFX.currentCascadeResolution = cascadeRes;

	GFX.cascadeShadowData.images.resize(GFX.maxFramesInFlight);
	GFX.cascadeShadowData.allocations.resize(GFX.maxFramesInFlight);
	GFX.cascadeShadowData.views.resize(GFX.maxFramesInFlight);
	GFX.cascadeShadowData.frameBuffers.resize(GFX.maxFramesInFlight);
	GFX.cascadeShadowData.frameBufferViews.resize(0); //Unused

	// Only 1 cascaded shadow map is supported for now
	GFX.cascadeShadowData.cameraUniform.resize(GFX.maxFramesInFlight);
	GFX.cascadeShadowData.cameraAllocation.resize(GFX.maxFramesInFlight);
	GFX.cascadeShadowData.shadowCamDesc.resize(GFX.maxFramesInFlight);

	// TODO: Change back to using floats
	// Currently using vec4 for algnment reasons
	GFX.cascadeBufferSize = sizeof(mat4) * 8 + sizeof(vec4) * 8;
	uint32 camSize = GFX.cascadeBufferSize;

	// VP matrices and cascade splits
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		if (GFX.stagePerFrameData) {
			Buffers::createBuffer(camSize,
					VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | GFX.deviceOnlyBufferUsage,
					GFX.deviceOnlyProperties, &GFX.cascadeShadowData.cameraUniform[i],
					&GFX.cascadeShadowData.cameraAllocation[i]);
		} else {
			Buffers::createBuffer(camSize,
					VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, GFX.hostVisibleProperties, 
					&GFX.cascadeShadowData.cameraUniform[i],
					&GFX.cascadeShadowData.cameraAllocation[i]);
		}
	}

	// Shadow texture
	VkImageCreateInfo imgInfo = ImageCreation::createImageInfo(GFX.shadowDepthFormat,
			VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, 
			VkExtent3D{cascadeRes, cascadeRes, 1});

	imgInfo.arrayLayers = cascadeCount;

	VmaAllocationCreateInfo imgAllocInfo {};
	imgAllocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
	imgAllocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vmaCreateImage(GFX.allocator, &imgInfo, &imgAllocInfo, &GFX.cascadeShadowData.images[i],
				&GFX.cascadeShadowData.allocations[i], nullptr);
	
		GFX.cascadeShadowData.views[i] = ImageCreation::createImageView(
				GFX.shadowDepthFormat, GFX.cascadeShadowData.images[i],
				VK_IMAGE_ASPECT_DEPTH_BIT, 1, VK_IMAGE_VIEW_TYPE_2D_ARRAY, cascadeCount);

		// Prevent validation warnings
		ImageCreation::transitionImageLayout(GFX.cascadeShadowData.images[i], true, 
				VK_IMAGE_LAYOUT_UNDEFINED,
				VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL, cascadeCount);
	}

	// Render pass
	RenderPass::depthRenderPass(&GFX.cascadeShadowData.renderPass, cascadeCount, true, false);

	// Framebuffers
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		VkFramebufferCreateInfo fbCreateInfo {};
		fbCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		fbCreateInfo.renderPass = GFX.cascadeShadowData.renderPass;
		fbCreateInfo.attachmentCount = 1;
		fbCreateInfo.pAttachments = &GFX.cascadeShadowData.views[i];
		fbCreateInfo.width = cascadeRes;
		fbCreateInfo.height = cascadeRes;
		fbCreateInfo.layers = 1;
	
		VK_CHECK(vkCreateFramebuffer(GFX.device, &fbCreateInfo, nullptr,
				&GFX.cascadeShadowData.frameBuffers[i]),
				"Failed to create shadow framebuffer");

	}
	
}

void ShadowResources::recreateDescriptors(ShadowPassData* shadowData, bool cubeMap)
{
	uint32 cascadeCount = GFX.currentCascadeCount;

	// TODO: Switch to float rather than vec4
	uint32 camSize = cubeMap ? sizeof(OmniUniformData) : GFX.cascadeBufferSize;
	
	// TODO: Cleanup
	// Loop runs once for cascaded shadows
	VkDescriptorBufferInfo cameraInfo {};
	for (uint32 i = 0; i < shadowData->cameraUniform.size(); ++i) {
		DescriptorBuilder builder;
		cameraInfo = {};
		cameraInfo.buffer = shadowData->cameraUniform[i];
		cameraInfo.offset = 0;
		cameraInfo.range = camSize;
		builder.bindBuffer(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, &cameraInfo);

		builder.build(&shadowData->shadowCamDesc[i],
				shadowData->shadowCamLayout,
	 			GFX.descriptorPool);
	}
}

void ShadowResources::updateOmniDescriptors(qml::vec3 lightPos, float radius, uint32 index, uint32 currentFrame)
{
	OmniUniformData data;
	
	qml::mat4 proj = qml::perspective(qml::radians(90.0f), 1.0f, 0.01f, radius);
	data.radius = radius;
	data.lightPos = lightPos;

	data.views[0] = proj * qml::lookAt(lightPos, lightPos + qml::vec3(1.0f, 0.0f, 0.0f), 
			qml::vec3(0.0f, 1.0f, 0.0f));
	data.views[1] = proj * qml::lookAt(lightPos, lightPos + qml::vec3(-1.0f, 0.0f, 0.0f), 
			qml::vec3(0.0f, 1.0f, 0.0f));
	data.views[2] = proj * qml::lookAt(lightPos, lightPos + qml::vec3(0.0f, 1.0f, 0.0f),
			qml::vec3(0.0f, 0.0f, 1.0f));
	data.views[3] = proj * qml::lookAt(lightPos, lightPos + qml::vec3(0.0f, -1.0f, 0.0f), 
			qml::vec3(0.0f, 0.0f, -1.0f));

	//TODO: Investivate why +z and -z images need to be swapped
	data.views[4] = proj * qml::lookAt(lightPos, lightPos + qml::vec3(0.0f, 0.0f, -1.0f), 
			qml::vec3(0.0f, 1.0f, 0.0f));
	data.views[5] = proj * qml::lookAt(lightPos, lightPos + qml::vec3(0.0f, 0.0f, 1.0f), 
			qml::vec3(0.0f, 1.0f, 0.0f));

	uint32 bufferIndex = index * GFX.maxFramesInFlight + currentFrame;

	if (GFX.enableStagingBuffer && GFX.stagePerFrameData) {
		Buffers::copyToBufferStaging(GFX.omniShadowData.cameraUniform[bufferIndex], 
				sizeof(OmniUniformData), &data);
	} else {
		Buffers::copyToBuffer(GFX.omniShadowData.cameraUniform[bufferIndex],
				GFX.omniShadowData.cameraAllocation[bufferIndex],
				sizeof(OmniUniformData), &data);
	}
}

void ShadowResources::updateShadowCascades(vec3 direction, float yOffset, uint32 currentFrame, ViewportData& vp)
{
	// Setup shadow cascades, based upon SaschaWillems implementation
	// https://github.com/SaschaWillems/Vulkan/blob/master/examples/shadowmappingcascade/shadowmappingcascade.cpp
	// https://alextardif.com/shadowmapping.html

	// TODO: Avoid reallocating this each frame
	uint32 cascadeCount = GFX.currentCascadeCount;
	std::vector<float> cascadeSplits(cascadeCount);
	std::array<vec4, 8> cascadeSplitDepths;
	std::array<mat4, 8> viewProjMatrices;

	float near = vp.camProps.near;
	float far = std::min(vp.camProps.far, renderSettings.cascadeDist.load());
	float fov = vp.camProps.fov;

	float aspect = (float)GFX.surfaceExtent.width / (float)GFX.surfaceExtent.height;

	float ratio = far / near;
	float range = far - near;

	float lambda = renderSettings.lambda;

	// Calculate split depths
	for (uint32 i = 0; i < cascadeCount; ++i) {
		float p = (float)(i + 1) / cascadeCount;
		float log = near * std::pow(ratio, p);
		float uniform = near + range * p;
		float d = lambda * (log - uniform) + uniform;
		cascadeSplits[i] = (d - near);
	}

	// Split camera frustum into projections that match the cascade splits
	// TODO: Reuse calculation
	std::vector<mat4> invViewProjections;
	
	float lastSplitDist = near;
	for (uint32 i = 0; i < cascadeCount; ++i) {
		float splitDist = cascadeSplits[i];
		mat4 tmp = qml::perspective(fov, aspect, lastSplitDist, splitDist) * vp.camProps.view;
		invViewProjections.push_back(qml::invert(tmp));

		lastSplitDist = splitDist;
	}

	for (uint32 c = 0; c < cascadeCount; ++c) {
		float splitDist = cascadeSplits[c];

		vec3 frustumCorners[8] = {
			vec3(-1.0f,  1.0f, 0.0f),
			vec3( 1.0f,  1.0f, 0.0f),
			vec3( 1.0f, -1.0f, 0.0f),
			vec3(-1.0f, -1.0f, 0.0f),
			vec3(-1.0f,  1.0f, 1.0f),
			vec3( 1.0f,  1.0f, 1.0f),
			vec3( 1.0f, -1.0f, 1.0f),
			vec3(-1.0f, -1.0f, 1.0f),
		};

		// Transform the frustum corners into view space
		for (uint32 i = 0; i < 8; ++i) {
			vec4 invCorner = invViewProjections[c] * vec4(frustumCorners[i], 1.0f);
			frustumCorners[i] = invCorner.xyz / invCorner.w;
		}
		
		vec3 frustumCenter = vec3(0.0f);
		for (uint32 i = 0; i < 8; ++i) {
			frustumCenter += frustumCorners[i];
		}
		frustumCenter /= 8.0f;

		vec3 lightDir = -(direction.normalize());
		
		vec3 min = (std::numeric_limits<float>::max());
		vec3 max = (std::numeric_limits<float>::min());

		for (uint32 i = 0; i < 8; ++i) {
			for (uint32 j = 0; j < 3; ++j) {
				min[j] = std::min(frustumCorners[i][j], min[j]);
				max[j] = std::max(frustumCorners[i][j], max[j]);
			}
		}

		vec3 size = max - min;
		float radius = size.magnitude() / 2.0f;

		vec3 eye = frustumCenter - (lightDir * (yOffset + max.y));
		mat4 lightViewMat = qml::lookAt(eye, frustumCenter, vec3(0.0f, 1.0f, 0.0f));
		mat4 lightOrthoMat = qml::ortho(-radius, radius, radius, -radius, yOffset * 2.0f + max.y, 0.0f);

		cascadeSplitDepths[c].x = -splitDist;
		viewProjMatrices[c] = lightOrthoMat * lightViewMat;
	}

	// Copy buffer for depth pass
	size_t bufferDataSizes[2] = {sizeof(mat4) * 8, sizeof(vec4) * 8};
	void* bufferData[2] = {viewProjMatrices.data(), cascadeSplitDepths.data()};

	if (GFX.enableStagingBuffer && GFX.stagePerFrameData) {
		Buffers::copyToBufferStaging(GFX.cascadeShadowData.cameraUniform[currentFrame],
				bufferDataSizes, bufferData, 2);
	} else {
		Buffers::copyToBuffer(GFX.cascadeShadowData.cameraUniform[currentFrame], 
				GFX.cascadeShadowData.cameraAllocation[currentFrame],
				bufferDataSizes, bufferData, 2);
	}
}

void ShadowResources::cleanUp(ShadowPassData* shadowData)
{
	vkDestroyPipeline(GFX.device, shadowData->pipeline, nullptr);
	vkDestroyPipelineLayout(GFX.device, shadowData->pipelineLayout, nullptr);

	for (uint32 i = 0; i < shadowData->frameBuffers.size(); ++i) {
		vkDestroyFramebuffer(GFX.device, shadowData->frameBuffers[i], nullptr);
	}

	for (uint32 i = 0; i < shadowData->frameBufferViews.size(); ++i) {
		vkDestroyImageView(GFX.device, shadowData->frameBufferViews[i], nullptr);
	}

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vkDestroyImageView(GFX.device, shadowData->views[i], nullptr);
		vmaFreeMemory(GFX.allocator, shadowData->allocations[i]);
		vkDestroyImage(GFX.device, shadowData->images[i], nullptr);
	}

	vkDestroyDescriptorSetLayout(GFX.device, shadowData->shadowCamLayout, nullptr);

	for (uint32 i = 0; i < shadowData->cameraUniform.size(); ++i) {
		vkDestroyBuffer(GFX.device, shadowData->cameraUniform[i], nullptr);
		vmaFreeMemory(GFX.allocator, shadowData->cameraAllocation[i]);
	}

	vkDestroyRenderPass(GFX.device, shadowData->renderPass, nullptr);
}
