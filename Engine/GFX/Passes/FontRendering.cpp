#include "FontRendering.hpp"
#include "GFX/Buffers.hpp"
#include "GFX/Pipeline.hpp"
#include "GFX/RenderPass.hpp"
#include <X11/X.h>
#include <vulkan/vulkan_core.h>
#include <locale>
#include <codecvt>

using namespace quasar;

std::vector<TextObject> FontRendering::textObjects;
std::vector<TextFont> FontRendering::fonts;
std::queue<TextHandle> FontRendering::freeTextObjects;

void FontRendering::init()
{
	initPipeline();
}

TextHandle FontRendering::uploadText(const std::string &content, FontHandle fontHandle,
			float lineSpacing, float kerning, float maxWidth, uint32 colour)
{
	// Convert utfd 8 strings to 32 bit chars
	std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> cvt;
	std::u32string unicode = cvt.from_bytes(content);

	TextFont& font = fonts[fontHandle];

	std::vector<FontVertex> vertices;
	std::vector<uint32> indices;

	TextObject text = {
		.handle = fontHandle,
		.lineSpacing = lineSpacing,
		.kerning = kerning,
		.maxWidth = maxWidth
	};

	vertices.reserve(unicode.size() * 4);
	indices.reserve(unicode.size() * 6);

	for (auto it = unicode.begin(); it != unicode.end(); ++it) {
		char32_t ch = *it;

		// Newline characters
		if ((cvt.to_bytes(ch) == "\n")) {
			text.currentLine -= font.lineHeight * text.lineSpacing;
			text.currentAdvance = 0.0f;
			text.previous = 0;
		}

		// Glyph is not part of the atlas
		if (font.glyphIndices.size() < ch || font.glyphIndices[ch] == -1) {
			continue;
		}

		int index = font.glyphIndices[ch];
		TextFont::GlyphDescription& desc = font.glyphs[index];

		// Text Wrap
		if (text.currentAdvance > (desc.advance + maxWidth)) {
			text.currentLine -= font.lineHeight * text.lineSpacing;
			text.currentAdvance = 0.0f;
			text.previous = 0;
		}
		
		placeGlyph(vertices, indices, colour, text, font, desc);
		text.previous = ch;
	}
	text.indexCount = indices.size();

	createBuffers(vertices, indices, text);

	TextHandle textHandle;
	if (freeTextObjects.size()) {
		textHandle = freeTextObjects.front();
		freeTextObjects.pop();
		textObjects[textHandle] = text;
	} else {
		textHandle = textObjects.size();
		textObjects.push_back(text);
	}

	return textHandle;
}

void FontRendering::placeGlyph(std::vector<FontVertex>& vertices, std::vector<uint32>& indices,
			uint32 colour, TextObject& text, TextFont& font, TextFont::GlyphDescription& desc)
{
	// Quad indices
	indices.push_back(vertices.size() + 0);
	indices.push_back(vertices.size() + 3);
	indices.push_back(vertices.size() + 1);
	indices.push_back(vertices.size() + 0);
	indices.push_back(vertices.size() + 2);
	indices.push_back(vertices.size() + 3);

	vec2 translate = (vec2(-desc.tx, -desc.ty) * desc.scale) / static_cast<float>(font.glyphScale);
	vec2 size = (vec2(desc.w, desc.h)) / static_cast<float>(font.glyphScale);

	// Apply kerning offset
	if (text.previous != 0) {
		int c1 = font.glyphIndices[text.previous];
		int c2 = font.glyphIndices[desc.character];
		double kerningSpace = font.kerning[std::pair(c1, c2)] * text.kerning;
	}

	// Vertex positions
	float x1 = text.currentAdvance + translate.x;
	float y1 = text.currentLine + translate.y;
	float x2 = x1 + size.x;
	float y2 = y1 + size.y;

	vec3 pos[4] = {
		vec3(x1, y1, 0.0f),
		vec3(x1, y2, 0.0f),
		vec3(x2, y1, 0.0f),
		vec3(x2, y2, 0.0f)
	};

	// Atlas UV coordinates
	float uvx1 = static_cast<float>(desc.x) / font.width;
	float uvy1 = static_cast<float>(desc.y) / font.height;
	float uvx2 = uvx1 + static_cast<float>(desc.w) / font.width;
	float uvy2 = uvy1 + static_cast<float>(desc.h) / font.height;

	vec2 uv[4] = {
		vec2(uvx1, uvy1),
		vec2(uvx1, uvy2),
		vec2(uvx2, uvy1),
		vec2(uvx2, uvy2),
	};

	for (uint32 i = 0; i < 4; ++i) {
		vertices.push_back(FontVertex{pos[i], uv[i], colour});
	}

	text.currentAdvance += desc.advance;
}

void FontRendering::createBuffers(std::vector<FontVertex>& vertices, std::vector<uint32>& indices, TextObject& text)
{
	VkDeviceSize verticesSize = sizeof(FontVertex) * vertices.size();
	VkDeviceSize indicesSize = sizeof(uint32) * indices.size();

	Buffers::createBuffer(verticesSize, 
			VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
			&text.vertexBuffer, &text.vertexAlloc);
	Buffers::copyToBufferStaging(text.vertexBuffer, verticesSize, vertices.data());

	Buffers::createBuffer(indicesSize, 
			VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, 
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
			&text.indexBuffer, &text.indexAlloc);
	Buffers::copyToBufferStaging(text.indexBuffer, indicesSize, indices.data());


}

void FontRendering::destroyText(TextHandle handle)
{
	if (textObjects.size() <= handle || textObjects[handle].deleted) {
		return;
	}

	TextObject& text = textObjects[handle];
	text.deleted = true;
	vmaDestroyBuffer(GFX.allocator, text.vertexBuffer, text.vertexAlloc);
	vmaDestroyBuffer(GFX.allocator, text.indexBuffer, text.indexAlloc);

	freeTextObjects.push(handle);
}

void FontRendering::destroyAllText()
{
	for (TextHandle i = 0; i < textObjects.size(); ++i) {
		destroyText(i);
	}
}

FontHandle FontRendering::uploadFont(quasar::TextFont* font)
{
	fonts.push_back(*font);
	return fonts.size() - 1;
}

void FontRendering::initPipeline()
{
	VkDynamicState state[1] = {VK_DYNAMIC_STATE_VIEWPORT};

	VkPipelineDynamicStateCreateInfo dynState = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
		.pNext = nullptr,
		.flags = 0,
		.dynamicStateCount = 1,
		.pDynamicStates = state
	};

	RenderPass::createRenderPass(&GFX.fontData.renderPass, false, false, false);

	// Create pipeline
	PipelineBuilder builder {};
	builder.beginPipeline(&GFX.fontData.pipeline, &GFX.fontData.layout, &GFX.fontData.renderPass);
	builder.addShaderStage("Shaders/FontRendering_vs.spv", VK_SHADER_STAGE_VERTEX_BIT);
	builder.addShaderStage("Shaders/FontRendering_fs.spv", VK_SHADER_STAGE_FRAGMENT_BIT);
	builder.addPushConstantRange(0, sizeof(mat4), VK_SHADER_STAGE_VERTEX_BIT);
	builder.addPushConstantRange(sizeof(mat4), sizeof(int), VK_SHADER_STAGE_FRAGMENT_BIT);

	// TODO: Redo builder setInputDescriptons function to support customization

	// Setup input description for text vertex
	builder.bindingDescription = {};
	builder.bindingDescription .binding = 0;
	builder.bindingDescription .stride = sizeof(FontVertex);
	builder.bindingDescription .inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	builder.vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	builder.vertexInputInfo.vertexBindingDescriptionCount = 1;
	builder.vertexInputInfo.pVertexBindingDescriptions = &builder.bindingDescription;

	builder.attributeDescriptions.resize(3);
	builder.attributeDescriptions[0] = {
		.location = 0,
		.binding = 0,
		.format = VK_FORMAT_R32G32B32_SFLOAT,
		.offset = 0
	};

	builder.attributeDescriptions[1] = {
		.location = 1,
		.binding = 0,
		.format = VK_FORMAT_R32G32_SFLOAT,
		.offset = offsetof(FontVertex, texCoord)
	};

	builder.attributeDescriptions[2] = {
		.location = 2,
		.binding = 0,
		.format = VK_FORMAT_R8G8B8A8_UNORM,
		.offset = offsetof(FontVertex, colour)
	};

	builder.vertexInputInfo.vertexAttributeDescriptionCount = builder.attributeDescriptions.size();
	builder.vertexInputInfo.pVertexAttributeDescriptions = builder.attributeDescriptions.data();

	builder.inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	builder.inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	builder.inputAssembly.primitiveRestartEnable = VK_FALSE;

	std::array<VkDescriptorSetLayout, 1> layouts = {
		GFX.textureArrayLayout
	};

	builder.setViewportState(GFX.internalRes.width, GFX.internalRes.height);
	builder.setRasterizer(false);
	builder.setMultiSampling();
	builder.setColourBlending(false, VK_TRUE);
	builder.setPipelineLayout(layouts.data(), layouts.size());
	builder.setDepthStencil(true, false, VK_COMPARE_OP_LESS_OR_EQUAL);
	builder.createGraphicsPipeline(&dynState);
}

void FontRendering::cleanup()
{
	vkDestroyPipeline(GFX.device, GFX.fontData.pipeline, nullptr);
	vkDestroyPipelineLayout(GFX.device, GFX.fontData.layout, nullptr);
	vkDestroyRenderPass(GFX.device, GFX.fontData.renderPass, nullptr);
}
