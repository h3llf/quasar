#include "TiledForward.hpp"
#include "GFX/Descriptors.hpp"
#include "GFX/Buffers.hpp"
#include "GFX/Pipeline.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include "Core/Camera.hpp"

void TiledForward::init()
{
	createDescriptorLayout();
	createDepthDescriptor();
	Pipeline::initComputePipelines();
}

void TiledForward::viewportInit(ViewportData &viewport)
{
	std::vector<TileFrustum> frustums;
	genGridFrustums(&frustums, viewport);
	createDescriptors(&frustums, viewport);
}

void TiledForward::cleanup()
{
	vkDestroyPipeline(GFX.device, GFX.tiledForwardPass.pipeline, nullptr);
	vkDestroyPipelineLayout(GFX.device, GFX.tiledForwardPass.pipelineLayout, nullptr);

	vkDestroyDescriptorSetLayout(GFX.device, GFX.tiledForwardPass.frustumLayout, nullptr);
	vkDestroyDescriptorSetLayout(GFX.device, GFX.tiledForwardPass.depthLayout, nullptr);
}

void TiledForward::cleanupViewport(ViewportData& viewport)
{
	vmaDestroyBuffer(GFX.allocator, viewport.frustumBuffer,
			viewport.frustumAlloc);
	vmaDestroyBuffer(GFX.allocator, viewport.tileDescBuffer, 
			viewport.tileDescAlloc);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vmaDestroyBuffer(GFX.allocator, viewport.cullingResultBuffers[i],
				viewport.cullingResultAlloc[i]);
	}
}

// https://www.3dgep.com/forward-plus/#Forward
void TiledForward::genGridFrustums(std::vector<TileFrustum>* frustums, ViewportData& viewport)
{
	uint32 width = viewport.width;
	uint32 height = viewport.height;
	uint32 size = GFX.tiledForwardPass.TILE_SIZE;
	uint32 rows = (int)std::ceil((double)height / size);
	uint32 columns = (int)std::ceil((double)width/ size);
	uint32 tileCount = rows * columns;

	viewport.TFGroupsX = columns;
	viewport.TFGroupsY = rows;

	frustums->resize(tileCount);

	float xStep = float(size) / float(width);
	float yStep = float(size) / float(height);
	float xPos = 0.0f;
	float yPos = 1.0f;
	float near = viewport.camProps.near;
	float far = viewport.camProps.far;

	Camera::genProjection(width, height, &viewport.camProps);

	qml::mat4 invProj = qml::invert(viewport.camProps.proj);	
	vec4 points[4];

	for (uint32 i = 0; i < tileCount; ++i) {
		if ( i && !(i % (columns))) {
			yPos -= yStep;
			xPos = 0.0f;
		}

		//Generate points in screen space
		points[0] = vec4(xPos, yPos, -1.0f, 1.0f); //UL
		points[1] = vec4(xPos + xStep, yPos, -1.0f, 1.0f); //UR
		points[2] = vec4(xPos + xStep, yPos - yStep, -1.0f, 1.0f); //LR
		points[3] = vec4(xPos, yPos - yStep, -1.0f, 1.0f); //LL

		xPos += xStep;

		for (int p = 0; p < 4; ++p) {
			//Convert to clip space
			points[p].xy = vec2(points[p].x, 1.0f - points[p].y) * 2.0f - vec2(1.0f);

			//Translate to viewspace
			points[p] = invProj * points[p];
			points[p] = points[p] / points[p].w;
		}

		vec3 origin = vec3(0.0f);

		frustums->at(i).planes[0] = computePlane(origin, points[3].xyz, points[0].xyz);
		frustums->at(i).planes[1] = computePlane(origin, points[1].xyz, points[2].xyz);
		frustums->at(i).planes[2] = computePlane(origin, points[0].xyz, points[1].xyz);
		frustums->at(i).planes[3] = computePlane(origin, points[2].xyz, points[3].xyz);
	}
}

vec4 TiledForward::computePlane(vec3 origin, vec3 p1, vec3 p2)
{
	vec4 plane;

	vec3 v0 = p1 - origin;
	vec3 v1 = p2 - origin;

	//Calculate normal
	plane.xyz = qml::cross(v0, v1).normalize();

	//Distance to origin
//	plane.w = dot(plane.xyz, origin);

	return plane;
}

void TiledForward::createDescriptorLayout()
{
	DescriptorBuilder builder;
	//Tile list
	builder.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
			VK_SHADER_STAGE_COMPUTE_BIT);
	//Culling results
	builder.createLayoutBinding(1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 
			VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_COMPUTE_BIT);
	//Tile count + window size description
	builder.createLayoutBinding(2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 
			VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_COMPUTE_BIT);

	builder.createLayout(&GFX.tiledForwardPass.frustumLayout);

	//Depth image descriptor layout
	builder = {};
	builder.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
		VK_SHADER_STAGE_COMPUTE_BIT);

	builder.createLayout(&GFX.tiledForwardPass.depthLayout);
}


struct TileDesc {
	int groupCountX;
	int groupCountY;
	int width;
	int height;
};

void TiledForward::createDescriptors(std::vector<TileFrustum>* frustums, ViewportData& viewport)
{
	viewport.frustumSets.resize(GFX.maxFramesInFlight);
	viewport.cullingResultBuffers.resize(GFX.maxFramesInFlight);
	viewport.cullingResultAlloc.resize(GFX.maxFramesInFlight);

	VkDeviceSize tileListSize = frustums->size() * sizeof(TileFrustum);
	VkDeviceSize cullingResultSize = sizeof(uint32) * (GFX.tiledForwardPass.maxLightsPerTile+1) 
		* frustums->size();
	VkDeviceSize tileDescSize = sizeof(int) * 4;

	//Frustum data
	Buffers::createBuffer(tileListSize, 
			VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, 
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
			&viewport.frustumBuffer, &viewport.frustumAlloc);

	Buffers::copyToBufferStaging(viewport.frustumBuffer, 
			tileListSize, frustums->data());

	//Group count and window size
	Buffers::createBuffer(tileDescSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, 
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
			&viewport.tileDescBuffer, &viewport.tileDescAlloc);

	TileDesc tileDesc = {
		viewport.TFGroupsX,
		viewport.TFGroupsY,
		viewport.width,
		viewport.height
	};

	Buffers::copyToBufferStaging(viewport.tileDescBuffer, sizeof(TileDesc), &tileDesc);

	//Frustum info and result descriptor, per frame in flight.
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		//Light culling results buffer
		Buffers::createBuffer(cullingResultSize, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, 
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
				&viewport.cullingResultBuffers[i], 
				&viewport.cullingResultAlloc[i]);

		//Create descriptor set
		VkDescriptorBufferInfo frustumInfo {};
		frustumInfo.buffer = viewport.frustumBuffer;
		frustumInfo.offset = 0;
		frustumInfo.range = tileListSize;
	
		VkDescriptorBufferInfo cullingResultInfo {};
		cullingResultInfo.buffer = viewport.cullingResultBuffers[i];
		cullingResultInfo.offset = 0;
		cullingResultInfo.range = cullingResultSize;
	
		VkDescriptorBufferInfo tileDescInfo {};
		tileDescInfo.buffer = viewport.tileDescBuffer;
		tileDescInfo.offset = 0;
		tileDescInfo.range = tileDescSize;
	
		DescriptorBuilder builder;
		builder.bindBuffer(0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, &frustumInfo);
		builder.bindBuffer(1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, &cullingResultInfo);
		builder.bindBuffer(2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, &tileDescInfo);
	
		builder.build(&viewport.frustumSets[i], GFX.tiledForwardPass.frustumLayout, 
				GFX.descriptorPool);
	}
}

void TiledForward::createDepthDescriptor()
{
	GFX.tiledForwardPass.depthSets.resize(GFX.maxFramesInFlight);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		// Create depth image descriptor
		DescriptorBuilder builder {};
		VkDescriptorImageInfo imageInfo{};
		imageInfo.sampler = GFX.depthSampler;
		imageInfo.imageView = GFX.mainShading.depImgViews[i];
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

		builder.bindImage(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, &imageInfo);
		builder.build(&GFX.tiledForwardPass.depthSets[i], GFX.tiledForwardPass.depthLayout,
			GFX.descriptorPool);
	}
}
