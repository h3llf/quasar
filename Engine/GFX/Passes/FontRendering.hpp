#ifndef QUASAR_FONT_RENDERING_H
#define QUASAR_FONT_RENDERING_H

#include "GFX/RenderTypes.hpp"
#include "GFX/RendererData.hpp"
#include "Core/Assets/Font.hpp"

struct FontVertex
{
	vec3 position;
	vec2 texCoord;
	uint32 colour;
};

// Allocations and buffers used to store font textures
struct TextObject
{
	bool deleted = false;

	VkBuffer vertexBuffer;
	VmaAllocation vertexAlloc;
	VkBuffer indexBuffer;
	VmaAllocation indexAlloc;
	uint32 indexCount = 0;

	FontHandle handle;

	float lineSpacing;
	float kerning;
	float maxWidth;

	// Positions to control font placement
	float currentAdvance = 0.0f;
	float currentLine = 0.0f;

	char32_t previous = 0;
};

class FontRendering
{
public:
	// Initialize pipelie & renderpass
	static void init();
	static void cleanup();

	// Generates a renderable vertex buffer from the given string
	static TextHandle uploadText(const std::string& content, FontHandle fontHandle, 
			float lineSpacing, float kerning, float maxWidth, uint32 colour);
	static std::vector<TextObject> textObjects;

	static void destroyText(TextHandle handle);
	static void destroyAllText();

// Font Management
	static FontHandle uploadFont(quasar::TextFont* font);
	static std::vector<quasar::TextFont> fonts;
private:
	// Adds a new character to the index/vertex buffers
	static void placeGlyph(std::vector<FontVertex>& vertices, std::vector<uint32>& indices, uint32 colour,
			TextObject& text, quasar::TextFont& font, quasar::TextFont::GlyphDescription& desc);

	static void createBuffers(std::vector<FontVertex>& vertices, std::vector<uint32>& indices,
			TextObject& text);

	static void initPipeline();
	static std::queue<TextHandle> freeTextObjects;
};

#endif //QUASAR_FONT_RENDERING_H
