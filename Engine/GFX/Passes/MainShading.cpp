#include "MainShading.hpp"
#include "GFX/RendererData.hpp"
#include "GFX/Descriptors.hpp"
#include "GFX/ImageCreation.hpp"
#include <vulkan/vulkan_core.h>

void MainShading::createDescLayouts()
{
	// Opaque and (transparent) shading pass layouts
	DescriptorBuilder builder;
	// Omni shadow image
	builder.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 
			VK_SHADER_STAGE_FRAGMENT_BIT);
	// Cascaded shadow image
	builder.createLayoutBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			VK_SHADER_STAGE_FRAGMENT_BIT);
	// Cascaded shadow frustums
	builder.createLayoutBinding(2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
			VK_SHADER_STAGE_FRAGMENT_BIT);
	// Ambient occlusion result
	builder.createLayoutBinding(3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			VK_SHADER_STAGE_FRAGMENT_BIT);
	builder.createLayout(&GFX.mainShading.shadingDescLayout);

	GFX.mainShading.shadingDescriptors.resize(GFX.maxFramesInFlight);
}

void MainShading::init()
{
	GFX.mainShading.images.resize(GFX.maxFramesInFlight);
	GFX.mainShading.imgViews.resize(GFX.maxFramesInFlight);
	GFX.mainShading.imgAllocations.resize(GFX.maxFramesInFlight);
	GFX.mainShading.depthImages.resize(GFX.maxFramesInFlight);
	GFX.mainShading.depImgViews.resize(GFX.maxFramesInFlight);
	GFX.mainShading.depImgAllocations.resize(GFX.maxFramesInFlight);
	GFX.mainShading.depStenImgViews.resize(GFX.maxFramesInFlight);
	GFX.mainShading.stenImgViews.resize(GFX.maxFramesInFlight);

	GFX.mainShading.gBufferImages.resize(GFX.maxFramesInFlight);
	GFX.mainShading.gBufferImgViews.resize(GFX.maxFramesInFlight);
	GFX.mainShading.gBufferImgAllocations.resize(GFX.maxFramesInFlight);

	// Create images and framebuffers
	VkImageCreateInfo imgInfo = ImageCreation::createImageInfo(GFX.mainShading.imgFormat, 
		VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT |
		VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_STORAGE_BIT, GFX.internalRes);

	VkImageCreateInfo depthImgInfo = ImageCreation::createImageInfo(GFX.depthStencilFormat,
		VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, GFX.internalRes);

	VkImageCreateInfo gBUferImgInfo = ImageCreation::createImageInfo(GFX.mainShading.gBufferFormat, 
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, GFX.internalRes);


	VmaAllocationCreateInfo imgAllocInfo {};
	imgAllocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
	imgAllocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		// Colour attatchment
		vmaCreateImage(GFX.allocator, &imgInfo, &imgAllocInfo, &GFX.mainShading.images[i],
			&GFX.mainShading.imgAllocations[i], nullptr);
		GFX.mainShading.imgViews[i] = ImageCreation::createImageView(GFX.mainShading.imgFormat,
			GFX.mainShading.images[i], VK_IMAGE_ASPECT_COLOR_BIT, 1);
		
		// Depth attachment
		vmaCreateImage(GFX.allocator, &depthImgInfo, &imgAllocInfo, &GFX.mainShading.depthImages[i],
			&GFX.mainShading.depImgAllocations[i], nullptr);
		GFX.mainShading.depImgViews[i] = ImageCreation::createImageView(GFX.depthStencilFormat,
			GFX.mainShading.depthImages[i], VK_IMAGE_ASPECT_DEPTH_BIT, 1);
		GFX.mainShading.depStenImgViews[i] = ImageCreation::createImageView(GFX.depthStencilFormat,
			GFX.mainShading.depthImages[i], VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT, 1);
		GFX.mainShading.stenImgViews[i] = ImageCreation::createImageView(GFX.depthStencilFormat,
			GFX.mainShading.depthImages[i], VK_IMAGE_ASPECT_STENCIL_BIT, 1);

		// G Buffer creation
		vmaCreateImage(GFX.allocator, &gBUferImgInfo, &imgAllocInfo, 
				&GFX.mainShading.gBufferImages[i], 
				&GFX.mainShading.gBufferImgAllocations[i], nullptr);

		GFX.mainShading.gBufferImgViews[i] = ImageCreation::createImageView(
			GFX.mainShading.gBufferFormat, GFX.mainShading.gBufferImages[i], 
			VK_IMAGE_ASPECT_COLOR_BIT, 1);
	}
}

void MainShading::recreateDescriptors()
{
	// Descriptor set
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		DescriptorBuilder builder;

		// Omni shadow image
		VkDescriptorImageInfo omniImgInfo {};
		omniImgInfo.imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
		omniImgInfo.imageView = GFX.omniShadowData.views[i];
		omniImgInfo.sampler = GFX.depthSampler;
		builder.bindImage(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, &omniImgInfo);

		// Cascaded shadow image
		VkDescriptorImageInfo cascadeImgInfo {};
		cascadeImgInfo.imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
		cascadeImgInfo.imageView = GFX.cascadeShadowData.views[i];
		cascadeImgInfo.sampler = GFX.depthSampler;
		builder.bindImage(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, &cascadeImgInfo);

		// Cascaced shadow frustums
		uint32 cascadeCount = GFX.currentCascadeCount;
		uint32 camSize = GFX.cascadeBufferSize;

		VkDescriptorBufferInfo cascadeCamInfo {};
		cascadeCamInfo.buffer = GFX.cascadeShadowData.cameraUniform[i];
		cascadeCamInfo.offset = 0;
		cascadeCamInfo.range = camSize;	

		builder.bindBuffer(2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, &cascadeCamInfo);

		// Ambient occlusion
		VkDescriptorImageInfo aoImgInfo {};
		aoImgInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		aoImgInfo.imageView = GFX.gtaoData.aoImgView[i];
		aoImgInfo.sampler = GFX.depthSampler;
		builder.bindImage(3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, &aoImgInfo);

		// Build descriptor set
		builder.build(&GFX.mainShading.shadingDescriptors[i], GFX.mainShading.shadingDescLayout,
				GFX.descriptorPool);
	}
}

void MainShading::cleanup()
{
	vkDestroyPipeline(GFX.device, GFX.mainShading.depthPipeline, nullptr);
	vkDestroyPipeline(GFX.device, GFX.mainShading.graphicsPipeline, nullptr);
	vkDestroyPipelineLayout(GFX.device, GFX.mainShading.depthPipelineLayout, nullptr);
	vkDestroyPipelineLayout(GFX.device, GFX.mainShading.pipelineLayout, nullptr);
	vkDestroyRenderPass(GFX.device, GFX.mainShading.depthPass, nullptr);
	vkDestroyRenderPass(GFX.device, GFX.mainShading.renderPass, nullptr);

	// Images & views
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vmaDestroyImage(GFX.allocator, GFX.mainShading.images[i], 
				GFX.mainShading.imgAllocations[i]);
		vkDestroyImageView(GFX.device, GFX.mainShading.imgViews[i], nullptr);

		vmaDestroyImage(GFX.allocator, GFX.mainShading.depthImages[i], 
				GFX.mainShading.depImgAllocations[i]);
		vkDestroyImageView(GFX.device, GFX.mainShading.depImgViews[i], nullptr);
		vkDestroyImageView(GFX.device, GFX.mainShading.depStenImgViews[i], nullptr);
		vkDestroyImageView(GFX.device, GFX.mainShading.stenImgViews[i], nullptr);

		vmaDestroyImage(GFX.allocator, GFX.mainShading.gBufferImages[i], 
				GFX.mainShading.gBufferImgAllocations[i]);
		vkDestroyImageView(GFX.device, GFX.mainShading.gBufferImgViews[i], nullptr);
	}

	vkDestroyDescriptorSetLayout(GFX.device, GFX.mainShading.shadingDescLayout, nullptr);
}

void MainShading::initViewport(ViewportData &vp)
{
	vp.depthOnlyFramebuffers.resize(GFX.maxFramesInFlight);
	vp.mainFramebuffers.resize(GFX.maxFramesInFlight);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		std::array<VkImageView, 3> attachments = {GFX.mainShading.imgViews[i], 
			GFX.mainShading.depStenImgViews[i], GFX.mainShading.gBufferImgViews[i]};


		VkFramebufferCreateInfo fbCreateInfo {};
		fbCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		fbCreateInfo.renderPass = GFX.mainShading.renderPass;
		fbCreateInfo.attachmentCount = attachments.size();
		fbCreateInfo.pAttachments = attachments.data();
		fbCreateInfo.width = GFX.internalRes.width;
		fbCreateInfo.height = GFX.internalRes.height;
		fbCreateInfo.layers = 1;
		
		VK_CHECK( vkCreateFramebuffer(GFX.device, &fbCreateInfo, nullptr, 
				&vp.mainFramebuffers[i]), "Failed to create shading pass frame buffer");

		fbCreateInfo.renderPass = GFX.mainShading.depthPass;
		fbCreateInfo.attachmentCount = 1;
		fbCreateInfo.pAttachments = &GFX.mainShading.depStenImgViews[i];

		VK_CHECK(vkCreateFramebuffer(GFX.device, &fbCreateInfo, nullptr, 
				&vp.depthOnlyFramebuffers[i]), "Failed to create shading pass frame buffer");
	}
}

void MainShading::cleanupViewport(ViewportData &vp)
{
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vkDestroyFramebuffer(GFX.device, vp.mainFramebuffers[i], nullptr);
		vkDestroyFramebuffer(GFX.device, vp.depthOnlyFramebuffers[i], nullptr);
	}
}
