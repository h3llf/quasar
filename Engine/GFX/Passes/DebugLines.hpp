#ifndef QUASAR_DEBUG_LINES_H
#define QUASAR_DEBUG_LINES_H

#include <GFX/RendererData.hpp>

class DebugLines
{
public:
	static void updateVertexBuffer(std::vector<quasar::LineVertex>& lines);
	static void init();
	static void initViewport(ViewportData& vp);

	static void cleanup();
	static void cleanupViewport(ViewportData& vp);

private:
	static void allocateVertexBuffer(size_t size);
};

#endif //QUASAR_DEBUG_LINES_H
