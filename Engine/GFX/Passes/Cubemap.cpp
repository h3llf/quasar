#include "Cubemap.hpp"
#include "GFX/Pipeline.hpp"
#include "GFX/Buffers.hpp"
#include "GFX/RenderPass.hpp"
#include <vulkan/vulkan_core.h>

vec3 vertices[8] = {
	// Top
	{-1.0f, 1.0f, -1.0f}, //bl 0
	{-1.0f, 1.0f, 1.0f},  //br 1
	{1.0f, 1.0f, -1.0f},  //fl 2
	{1.0f, 1.0f, 1.0f},   //fr 3
	
	// Bottom
	{-1.0f, -1.0f, -1.0f}, //bl 4
	{-1.0f, -1.0f, 1.0f},  //br 5
	{1.0f, -1.0f, -1.0f},  //fl 6
	{1.0f, -1.0f, 1.0f},   //fr 7
};

uint32 indices[36] = {
	1, 0, 2, 1, 2, 3, // Top
	0, 1, 5, 5, 4, 0, // Back
	6, 4, 5, 5, 7, 6, // Bottom
	7, 3, 2, 2, 6, 7, // Front
	2, 0, 4, 4, 6, 2, // Left
	5, 1, 3, 3, 7, 5  // Right
};

void Cubemapping::initSkyboxPass()
{
	// Create vertex buffer
	VkDeviceSize bufferSize = sizeof(vec3) * 8;
	Buffers::createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, 
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
			&GFX.skyboxData.vertexBuffer, &GFX.skyboxData.vertexAlloc);
	Buffers::copyToBufferStaging(GFX.skyboxData.vertexBuffer, bufferSize, vertices);

	bufferSize = sizeof(uint32) * 36;
	Buffers::createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, 
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
			&GFX.skyboxData.indexBuffer, &GFX.skyboxData.indexAlloc);
	Buffers::copyToBufferStaging(GFX.skyboxData.indexBuffer, bufferSize, indices);

	// Create renderPass
//	RenderPass::createRenderPass(&GFX.skyboxData.renderPass, false);

	// Use equirectangular map directly for now
	// TODO: Generate cubemap texture
	std::array<VkDescriptorSetLayout, 2> layouts = {
		GFX.cameraOnlyLayout,
		GFX.textureArrayLayout
	};

	VkDynamicState state[1] = {VK_DYNAMIC_STATE_VIEWPORT};

	VkPipelineDynamicStateCreateInfo dynState = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
		.pNext = nullptr,
		.flags = 0,
		.dynamicStateCount = 1,
		.pDynamicStates = state
	};


	PipelineBuilder builder;
	builder.beginPipeline(&GFX.skyboxData.pipeline, &GFX.skyboxData.layout, &GFX.mainShading.renderPass);
	builder.addShaderStage("Shaders/Cubemap_vs.spv", VK_SHADER_STAGE_VERTEX_BIT);
	builder.addShaderStage("Shaders/Cubemap_fs.spv", VK_SHADER_STAGE_FRAGMENT_BIT);
	builder.setInputDescriptonsPosOnly();
	builder.setRasterizer(false);
	builder.setViewportState(GFX.internalRes.width, GFX.internalRes.height);
	builder.setMultiSampling();
	builder.setColourBlending(true);
	builder.setDepthStencil(VK_TRUE, VK_FALSE, VK_COMPARE_OP_LESS_OR_EQUAL);
	builder.addPushConstantRange(0, sizeof(int), VK_SHADER_STAGE_FRAGMENT_BIT);
	builder.setPipelineLayout(layouts.data(), layouts.size());
	builder.createGraphicsPipeline(&dynState);
}

void Cubemapping::cleanup()
{
	vkDestroyPipeline(GFX.device, GFX.skyboxData.pipeline, nullptr);
	vkDestroyPipelineLayout(GFX.device, GFX.skyboxData.layout, nullptr);
//	vkDestroyRenderPass(GFX.device, GFX.skyboxData.renderPass, nullptr);

	vkDestroyBuffer(GFX.device, GFX.skyboxData.vertexBuffer, nullptr);
	vmaFreeMemory(GFX.allocator, GFX.skyboxData.vertexAlloc);

	vkDestroyBuffer(GFX.device, GFX.skyboxData.indexBuffer, nullptr);
	vmaFreeMemory(GFX.allocator, GFX.skyboxData.indexAlloc);
}
