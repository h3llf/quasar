#include "Bloom.hpp"
#include "GFX/ImageCreation.hpp"
#include "GFX/Descriptors.hpp"
#include "GFX/Pipeline.hpp"
#include "GFX/RendererData.hpp"
#include <vulkan/vulkan_core.h>

void BloomPass::init()
{
	// Aim for a vertical resolution of 8 for the smallest mip level
	GFX.bloomData.mipCount = log2(GFX.internalRes.height / 8);
	
	GFX.bloomData.images.resize(GFX.maxFramesInFlight);
	GFX.bloomData.imgAllocations.resize(GFX.maxFramesInFlight);
	GFX.bloomData.imgViews.resize(GFX.maxFramesInFlight);
	GFX.bloomData.inDescriptors.resize(GFX.maxFramesInFlight);
	GFX.bloomData.outDescriptors.resize(GFX.maxFramesInFlight);
	GFX.bloomData.srcDescriptors.resize(GFX.maxFramesInFlight);
	GFX.bloomData.dstDescriptors.resize(GFX.maxFramesInFlight);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		GFX.bloomData.imgViews[i].resize(GFX.bloomData.mipCount);
		GFX.bloomData.inDescriptors[i].resize(GFX.bloomData.mipCount);
		GFX.bloomData.outDescriptors[i].resize(GFX.bloomData.mipCount);
	}

	DescriptorBuilder descBuild;
	descBuild.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_COMPUTE_BIT);
	descBuild.createLayout(&GFX.bloomData.inDescLayout);

	descBuild = DescriptorBuilder();
	descBuild.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);
	descBuild.createLayout(&GFX.bloomData.outDescLayout);

	VkSamplerCreateInfo samplerInfo = {
		.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
		.pNext = nullptr,
		.flags = 0,
		.magFilter = VK_FILTER_LINEAR,
		.minFilter = VK_FILTER_LINEAR,
		.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
		.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
		.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
		.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
		.mipLodBias = 0.0f,
		.anisotropyEnable = VK_TRUE,
		.maxAnisotropy = 16.0f
	};
	vkCreateSampler(GFX.device, &samplerInfo, nullptr, &GFX.bloomData.sampler);

	// Create mip chain 
	VkExtent3D resolution = GFX.internalRes;
	resolution.width /= 2.0f;
	resolution.height /= 2.0f;

	VkImageCreateInfo imgInfo = ImageCreation::createImageInfo(GFX.bloomData.format, 
			VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_STORAGE_BIT, 
			resolution, GFX.bloomData.mipCount);

	VmaAllocationCreateInfo allocInfo {};
	allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
	allocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vmaCreateImage(GFX.allocator, &imgInfo, &allocInfo, &GFX.bloomData.images[i], 
				&GFX.bloomData.imgAllocations[i], nullptr);
		
		// Might need to transition
		ImageCreation::transitionImageLayout(GFX.bloomData.images[i], false, VK_IMAGE_LAYOUT_UNDEFINED, 
				VK_IMAGE_LAYOUT_GENERAL, 1, GFX.bloomData.mipCount);

		// 1 View per mip level
//		DescriptorBuilder descBuild;
		VkDescriptorImageInfo shadingImgInfo {
			.sampler = GFX.bloomData.sampler,
			.imageView = GFX.mainShading.imgViews[i],
			.imageLayout = VK_IMAGE_LAYOUT_GENERAL,
		};
		descBuild = DescriptorBuilder();
		descBuild.bindImage(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, &shadingImgInfo);
		descBuild.build(&GFX.bloomData.srcDescriptors[i], GFX.bloomData.inDescLayout, GFX.descriptorPool);

		descBuild = DescriptorBuilder();
		descBuild.bindImage(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, &shadingImgInfo);
		descBuild.build(&GFX.bloomData.dstDescriptors[i], GFX.bloomData.outDescLayout, GFX.descriptorPool);

		for (uint32 j = 0; j < GFX.bloomData.mipCount; ++j) {
			GFX.bloomData.imgViews[i][j] = ImageCreation::createImageView(GFX.bloomData.format, 
					GFX.bloomData.images[i], VK_IMAGE_ASPECT_COLOR_BIT, 
					1, VK_IMAGE_VIEW_TYPE_2D, 1, 0, j);

			VkDescriptorImageInfo mipImgInfo{
				.sampler = GFX.bloomData.sampler,
				.imageView = GFX.bloomData.imgViews[i][j],
				.imageLayout = VK_IMAGE_LAYOUT_GENERAL,
			};

			descBuild = DescriptorBuilder();
			descBuild.bindImage(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, &mipImgInfo);
			descBuild.build(&GFX.bloomData.outDescriptors[i][j], GFX.bloomData.outDescLayout,
					GFX.descriptorPool);

			descBuild = DescriptorBuilder();
			descBuild.bindImage(0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, &mipImgInfo);
			descBuild.build(&GFX.bloomData.inDescriptors[i][j], GFX.bloomData.inDescLayout,
					GFX.descriptorPool);
		}
	}

	VkDescriptorSetLayout layouts[2] = {
		GFX.bloomData.inDescLayout,
		GFX.bloomData.outDescLayout
	};

	// compute pipelineS
	PipelineBuilder pipeline {};
	pipeline.beginPipeline(&GFX.bloomData.pipeline, &GFX.bloomData.layout);
	pipeline.addShaderStage("Shaders/Bloom_comp.spv", VK_SHADER_STAGE_COMPUTE_BIT);
	pipeline.addPushConstantRange(0, sizeof(BloomData::PushConstData), VK_SHADER_STAGE_COMPUTE_BIT);
	pipeline.setPipelineLayout(layouts, 2);
	pipeline.createComputePipeline();
}

void BloomPass::cleanup()
{
	vkDestroySampler(GFX.device, GFX.bloomData.sampler, nullptr);

	vkDestroyPipelineLayout(GFX.device, GFX.bloomData.layout, nullptr);
	vkDestroyPipeline(GFX.device, GFX.bloomData.pipeline, nullptr);

	vkDestroyDescriptorSetLayout(GFX.device, GFX.bloomData.inDescLayout, nullptr);
	vkDestroyDescriptorSetLayout(GFX.device, GFX.bloomData.outDescLayout, nullptr);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vmaDestroyImage(GFX.allocator, GFX.bloomData.images[i], GFX.bloomData.imgAllocations[i]);

		for (uint32 j = 0; j < GFX.bloomData.mipCount; ++j) {
			vkDestroyImageView(GFX.device, GFX.bloomData.imgViews[i][j], nullptr);
		}
	}
}
