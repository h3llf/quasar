#include "Outline.hpp"
#include "GFX/RendererData.hpp"
#include "GFX/ImageCreation.hpp"
#include "GFX/Descriptors.hpp"
#include "GFX/Buffers.hpp"
#include "GFX/Pipeline.hpp"
#include <cmath>
#include <vulkan/vulkan_core.h>

void Outlines::init()
{
	// Create outline texture R16G16
	GFX.outlineData.images.resize(GFX.maxFramesInFlight);
	GFX.outlineData.imgViews.resize(GFX.maxFramesInFlight);
	GFX.outlineData.imgAllocations.resize(GFX.maxFramesInFlight);
	GFX.outlineData.descriptors.resize(GFX.maxFramesInFlight);

	VkImageCreateInfo imgInfo {
		.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
		.pNext = nullptr,
		.flags = 0,
		.imageType = VK_IMAGE_TYPE_2D,
		.format = GFX.outlineData.imgFormat,
		.extent = GFX.internalRes,
		.mipLevels = 1,
		.arrayLayers = 1,
		.samples = VK_SAMPLE_COUNT_1_BIT,
		.tiling = VK_IMAGE_TILING_OPTIMAL,
		.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_STORAGE_BIT,
		.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
	};

	VmaAllocationCreateInfo imgAllocInfo {};
	imgAllocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
	imgAllocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vmaCreateImage(GFX.allocator, &imgInfo, &imgAllocInfo, &GFX.outlineData.images[i],
				&GFX.outlineData.imgAllocations[i], nullptr);
	
		ImageCreation::transitionImageLayout(GFX.outlineData.images[i], false, 
				VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL, 1, 1);

		GFX.outlineData.imgViews[i] = ImageCreation::createImageView(GFX.outlineData.imgFormat, 
				GFX.outlineData.images[i], VK_IMAGE_ASPECT_COLOR_BIT, 1);
	}

	// Descriptor set layout
	DescriptorBuilder desc;
	desc.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);
	desc.createLayoutBinding(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_COMPUTE_BIT);
	desc.createLayoutBinding(2, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);
	desc.createLayout(&GFX.outlineData.descLayout);

	// Descriptors
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		desc = {};
		VkDescriptorImageInfo mainInfo {};
		mainInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
		mainInfo.imageView = GFX.mainShading.imgViews[i];
		mainInfo.sampler = GFX.textureSampler;
		desc.bindImage(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, &mainInfo);

		VkDescriptorImageInfo stencilInfo {};
		stencilInfo.imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;//VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		stencilInfo.imageView = GFX.mainShading.stenImgViews[i];
		stencilInfo.sampler = GFX.depthSampler;
		desc.bindImage(1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, &stencilInfo);

		VkDescriptorImageInfo outlineInfo{};
		outlineInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
		outlineInfo.imageView = GFX.outlineData.imgViews[i];
		outlineInfo.sampler = GFX.textureSampler;
		desc.bindImage(2, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, &outlineInfo);

		desc.build(&GFX.outlineData.descriptors[i], GFX.outlineData.descLayout, GFX.descriptorPool);
	}

	// compute pipelineS
	PipelineBuilder pipeline {};
	pipeline.beginPipeline(&GFX.outlineData.pipeline, &GFX.outlineData.layout);
	pipeline.addShaderStage("Shaders/Outline_comp.spv", VK_SHADER_STAGE_COMPUTE_BIT);
	pipeline.addPushConstantRange(0, sizeof(PushConstantData), VK_SHADER_STAGE_COMPUTE_BIT);
	pipeline.setPipelineLayout(&GFX.outlineData.descLayout, 1);
	pipeline.createComputePipeline();

	// Compute shader group count
	GFX.outlineData.groupCountX = std::ceil(static_cast<float>(GFX.internalRes.width) / 16.0f);
	GFX.outlineData.groupCountY = std::ceil(static_cast<float>(GFX.internalRes.height) / 16.0f);
}

void Outlines::cleanup()
{
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vkDestroyImageView(GFX.device, GFX.outlineData.imgViews[i], nullptr);
		vmaDestroyImage(GFX.allocator, GFX.outlineData.images[i], GFX.outlineData.imgAllocations[i]);
	}

	vkDestroyDescriptorSetLayout(GFX.device, GFX.outlineData.descLayout, nullptr);

	vkDestroyPipeline(GFX.device, GFX.outlineData.pipeline, nullptr);
	vkDestroyPipelineLayout(GFX.device, GFX.outlineData.layout, nullptr);
}
