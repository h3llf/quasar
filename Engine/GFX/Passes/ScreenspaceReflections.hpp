#ifndef QUASAR_SCREENSPACE_REFLECTIONS_H
#define QUASAR_SCREENSPACE_REFLECTIONS_H

class SSR
{
public:
	static void init();
	static void cleanup();
	
	struct Uniform
	{
		int width;
		int height;
	};
};

#endif //QUASAR_SCREENSPACE_REFLECTIONS_H
