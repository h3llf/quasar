#include "DebugLines.hpp"
#include <vulkan/vulkan_core.h>
#include "GFX/Buffers.hpp"
#include "GFX/Pipeline.hpp"
#include "GFX/RenderPass.hpp"
#include "GFX/RenderSettings.hpp"

using namespace quasar;

const size_t allocationBuffer = 512;

void DebugLines::allocateVertexBuffer(size_t size)
{
	vkDeviceWaitIdle(GFX.device);

	if (GFX.debugLine.vertexBuffer != VK_NULL_HANDLE) {
		vkDestroyBuffer(GFX.device, GFX.debugLine.vertexBuffer, nullptr);
		vmaFreeMemory(GFX.allocator, GFX.debugLine.vertexAlloc);

		GFX.debugLine.vertexBuffer = VK_NULL_HANDLE;
		GFX.debugLine.vertexAlloc = VK_NULL_HANDLE;
	}

	size += allocationBuffer;
	Buffers::createBuffer(size * sizeof(LineVertex), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, 
			VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
			&GFX.debugLine.vertexBuffer, &GFX.debugLine.vertexAlloc);

	GFX.debugLine.bufferCount = size;
}

void DebugLines::updateVertexBuffer(std::vector<LineVertex> &lines)
{
	// Resize buffer
	if (GFX.debugLine.bufferCount <= lines.size()) {
		allocateVertexBuffer(lines.size());
	}

	if (lines.size()) {
		// Copy vertices
		void* data;
		vmaMapMemory(GFX.allocator, GFX.debugLine.vertexAlloc, &data);
		memcpy(data, lines.data(), lines.size() * sizeof(LineVertex));
		vmaUnmapMemory(GFX.allocator, GFX.debugLine.vertexAlloc);
	}

	GFX.debugLine.actualCount = lines.size();
}

void DebugLines::init()
{
	RenderPass::createRenderPass(&GFX.debugLine.renderPass, false, true);

	// Dynamic state to allow viewport resizing
	VkDynamicState state[1] = {VK_DYNAMIC_STATE_VIEWPORT};

	VkPipelineDynamicStateCreateInfo dynState = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
		.pNext = nullptr,
		.flags = 0,
		.dynamicStateCount = 1,
		.pDynamicStates = state
	};

	PipelineBuilder builder;
	builder.beginPipeline(&GFX.debugLine.pipeline, &GFX.debugLine.layout, &GFX.debugLine.renderPass);
	builder.addShaderStage("Shaders/DebugLines_vs.spv", VK_SHADER_STAGE_VERTEX_BIT);
	builder.addShaderStage("Shaders/DebugLines_fs.spv", VK_SHADER_STAGE_FRAGMENT_BIT);
	builder.setInputDescriptonLines();
	builder.setRasterizer(false);
	builder.setViewportState(GFX.internalRes.width, GFX.internalRes.height);
	builder.setMultiSampling();
	builder.setColourBlending();
	builder.setPipelineLayout(&GFX.cameraOnlyLayout, 1);
	if (renderSettings.deugLineDepthTest) {
		builder.setDepthStencil(VK_TRUE, VK_FALSE, VK_COMPARE_OP_LESS_OR_EQUAL);
	} else {
		builder.setDepthStencil(VK_FALSE, VK_FALSE, VK_COMPARE_OP_ALWAYS);
	}
	builder.createGraphicsPipeline(&dynState);
}

void DebugLines::initViewport(ViewportData &vp)
{
	vp.debugFramebuffers.resize(GFX.maxFramesInFlight);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		std::array<VkImageView, 2> attachments = {GFX.mainShading.imgViews[i], 
			GFX.mainShading.depStenImgViews[i]};

		VkFramebufferCreateInfo fbCreateInfo {};
		fbCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		fbCreateInfo.renderPass = GFX.debugLine.renderPass;
		fbCreateInfo.attachmentCount = attachments.size();
		fbCreateInfo.pAttachments = attachments.data();
		fbCreateInfo.width = vp.width;
		fbCreateInfo.height = vp.height;
		fbCreateInfo.layers = 1;
		
		VK_CHECK( vkCreateFramebuffer(GFX.device, &fbCreateInfo, nullptr, &vp.debugFramebuffers[i]), 
			"Failed to create shading pass frame buffer");
	}

}

void DebugLines::cleanup()
{
	vkDestroyPipeline(GFX.device, GFX.debugLine.pipeline, nullptr);
	vkDestroyPipelineLayout(GFX.device, GFX.debugLine.layout, nullptr);
	vkDestroyRenderPass(GFX.device, GFX.debugLine.renderPass, nullptr);

	if (GFX.debugLine.vertexBuffer != VK_NULL_HANDLE) {
		vmaDestroyBuffer(GFX.allocator, GFX.debugLine.vertexBuffer, GFX.debugLine.vertexAlloc);

		GFX.debugLine.vertexBuffer = VK_NULL_HANDLE;
		GFX.debugLine.vertexAlloc = VK_NULL_HANDLE;
	}

	GFX.debugLine.bufferCount = 0;
	GFX.debugLine.actualCount = 0;
}

void DebugLines::cleanupViewport(ViewportData &vp)
{
	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vkDestroyFramebuffer(GFX.device, vp.debugFramebuffers[i], nullptr);
	}
}
