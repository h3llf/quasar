#ifndef QUASAR_AMBIENT_OCCLUSION_H
#define QUASAR_AMBIENT_OCCLUSION_H

#include "GFX/RendererData.hpp"

class AmbientOcclusion
{
public:
	static void init();
	static void initViewport(ViewportData& vp);
	static void createDepthSampler();
	static void createDescriptors();
	static void cleanup();
private:
};

#endif //QUASAR_AMBIENT_OCCLUSION_H
