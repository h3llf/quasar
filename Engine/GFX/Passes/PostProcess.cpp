#include "PostProcess.hpp"
#include "GFX/RendererData.hpp"
#include "GFX/Descriptors.hpp"
#include "GFX/Buffers.hpp"
#include "GFX/Pipeline.hpp"
#include <vulkan/vulkan_core.h>

void PostProcess::init()
{
	GFX.postData.descriptors.resize(GFX.maxFramesInFlight);

	/*
	PostProcessUniform uniform;
	uniform.width = GFX.internalRes.width;
	uniform.height = GFX.internalRes.height;*/

	GFX.postData.groupCountX = std::ceil((float)GFX.internalRes.width / 16.0f);
	GFX.postData.groupCountY = std::ceil((float)GFX.internalRes.height / 16.0f);

	VkSamplerCreateInfo samplerInfo = {
		.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
		.pNext = nullptr,
		.flags = 0,
		.magFilter = VK_FILTER_NEAREST,
		.minFilter = VK_FILTER_NEAREST,
		.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
		.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
		.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
		.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
		.mipLodBias = 0.0f,
		.anisotropyEnable = VK_FALSE,
		.maxAnisotropy = 16.0f
	};
	vkCreateSampler(GFX.device, &samplerInfo, nullptr, &GFX.postData.sampler);

	// Main image descriptors
	DescriptorBuilder desc;
	desc.createLayoutBinding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_COMPUTE_BIT);
	desc.createLayout(&GFX.postData.descLayout);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		VkDescriptorImageInfo shadingImg {};
		shadingImg.imageLayout = VK_IMAGE_LAYOUT_GENERAL;//VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		shadingImg.imageView = GFX.mainShading.imgViews[i];
		shadingImg.sampler = GFX.postData.sampler;

		desc = DescriptorBuilder();
		desc.bindImage(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, &shadingImg);
		desc.build(&GFX.postData.descriptors[i], GFX.postData.descLayout, GFX.descriptorPool);
	}

	// Compute pipeline
	VkDescriptorSetLayout layouts[2] = {
		GFX.postData.descLayout,
		GFX.cameraOnlyLayout
	};
	
	PipelineBuilder pipeline;
	pipeline.beginPipeline(&GFX.postData.pipeline, &GFX.postData.layout);
	pipeline.addShaderStage("Shaders/PostProcess_comp.spv", VK_SHADER_STAGE_COMPUTE_BIT);
	pipeline.addPushConstantRange(0, sizeof(PostData::PushConstData), VK_SHADER_STAGE_COMPUTE_BIT);
	pipeline.setPipelineLayout(layouts, 2);
	pipeline.createComputePipeline();
}

void PostProcess::cleanup()
{
	vkDestroyDescriptorSetLayout(GFX.device, GFX.postData.descLayout, nullptr);

	vkDestroyPipelineLayout(GFX.device, GFX.postData.layout, nullptr);
	vkDestroyPipeline(GFX.device, GFX.postData.pipeline, nullptr);

	vkDestroySampler(GFX.device, GFX.postData.sampler, nullptr);
}
