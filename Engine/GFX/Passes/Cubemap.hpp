#ifndef QUASAR_CUBEMAP_H
#define QUASAR_CUBEMAP_H

class Cubemapping
{
public:
	static void initSkyboxPass();
	static void cleanup();
};

#endif // QUASAR_CUBEMAP_H
