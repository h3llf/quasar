#ifndef QUASAR_MAIN_SHADING_H
#define QUASAR_MAIN_SHADING_H

#include "GFX/RendererData.hpp"

class MainShading
{
public:		
	static void createDescLayouts();
	static void init();
	static void recreateDescriptors();
	static void cleanup();

	static void initViewport(ViewportData& vp);
	static void cleanupViewport(ViewportData& vp);
private:
};

#endif //QUASAR_MAIN_SHADING_H
