#include "Initialization.hpp"
#include "GFX/Drawable/MaterialManager.hpp"
#include "Validation.hpp"
#include "Devices.hpp"
#include "Swapchain.hpp"
#include "Pipeline.hpp"
#include "RenderPass.hpp"
#include "CommandBuffers.hpp"
#include "Window.hpp"
#include "Drawable/LightManager.hpp"
#include "Descriptors.hpp"
#include "ImageCreation.hpp"
#include "Passes/TiledForward.hpp"
#include "Passes/ShadowMapping.hpp"
#include "Passes/AmbientOcclusion.hpp"
#include "Passes/MainShading.hpp"
#include "Passes/DebugLines.hpp"
#include "Passes/Cubemap.hpp"
#include "Passes/Outline.hpp"
#include "Passes/PostProcess.hpp"
#include "Passes/Bloom.hpp"
#include "Passes/ScreenspaceReflections.hpp"
#include "Passes/FontRendering.hpp"
#include "RendererData.hpp"
#include "Editor/Editor.hpp"
#include "RenderSettings.hpp"

#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"

using namespace quasar;

void Initialization::initVulkan()
{
	//Init
	initInstance();
	Validation::setupDebugMessenger();
	Devices::initDevice();
	initVMA();

	Window::createSurface();

	ImageCreation::createTextureSampler(&GFX.depthSampler, 1.0f);
	Descriptors::createDescriptorSetLayouts();
	Descriptors::createPersistantDescPool();
	materialManager.createDescriptor();
	Swapchain::initSwapchain();
	createSyncObjects();

	AmbientOcclusion::createDepthSampler();

	ImageCreation::createTextureSampler(&GFX.textureSampler, GFX.maxMipLod);
	initSwapchainDependencies();
	lightManager.createBuffer();
	lightManager.createDescriptor();
	Descriptors::initFrameDescriptor();

	// Main shading pass init
	MainShading::recreateDescriptors();
	Pipeline::initPipelines();
	DebugLines::init();

	Log::out("Vulkan initalized!");
}

void Initialization::cleanUp()
{
	materialManager.cleanupImages();
	lightManager.destroyBuffer();
	cleanupSwapchainDependencies();
	FontRendering::destroyAllText();

	vkDestroyDescriptorPool(GFX.device, GFX.persistantDescPool, nullptr);

	vkDestroySampler(GFX.device, GFX.textureSampler, nullptr);
	vkDestroySampler(GFX.device, GFX.aoSampler, nullptr);

	vkDestroyDescriptorSetLayout(GFX.device, GFX.cameraOnlyLayout, nullptr);
	vkDestroyDescriptorSetLayout(GFX.device, GFX.lightingLayout, nullptr);
	vkDestroyDescriptorSetLayout(GFX.device, GFX.textureArrayLayout, nullptr);

#ifndef _DISABLE_EDITOR
	Editor::cleanUp();
#endif //_DISABLE_EDITOR
       
	meshManager.cleanupMeshes();

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {
		vkDestroySemaphore(GFX.device, GFX.imageAvaliableSemaphores.at(i), nullptr);
		vkDestroySemaphore(GFX.device, GFX.renderFinishedSemaphores.at(i), nullptr);
		vkDestroyFence(GFX.device, GFX.fencesInFlight.at(i), nullptr);
	}
	vkDestroyFence(GFX.device, GFX.singleTimeCmdFence, nullptr);

	vkDestroySampler(GFX.device, GFX.depthSampler, nullptr);
	//ShadowResources::cleanUp(&GFX.omniShadowData);
	//ShadowResources::cleanUp(&GFX.cascadeShadowData);

	vmaDestroyAllocator(GFX.allocator);

	vkDestroyDevice(GFX.device, nullptr);

	if (GFX.enableValidationLayers)
		Validation::destroyDebugMessenger(GFX.instance, GFX.debugMessenger, nullptr);

	vkDestroySurfaceKHR(GFX.instance, GFX.surface, nullptr);
	vkDestroyInstance(GFX.instance, nullptr);

	glfwDestroyWindow(GFX.window);
	glfwTerminate();
}

void Initialization::initSwapchainDependencies()
{
	float resScale = renderSettings.resolutionScale;
	uint32 width = GFX.surfaceExtent.width * resScale;
	uint32 height = GFX.surfaceExtent.height * resScale;
	renderSettings.width = width;
	renderSettings.height = height;
	GFX.internalRes = VkExtent3D(width, height, 1);

	GFX.currentCascadeCount = renderSettings.numberCascades;
	GFX.currentCascadeResolution = renderSettings.cascadedShadowRes;
	GFX.currentMaxOmniShadowCount = renderSettings.omniShadowCount;
	GFX.currentOmniResolution = renderSettings.omniShadowRes;

	Descriptors::createUniformBuffers();
	Descriptors::createDescriptorPool();
	CommandBuffers::createCommandPool();
	CommandBuffers::createCommandBuffers();
	MainShading::createDescLayouts();
	ShadowResources::createDescLayouts();

	RenderPass::depthRenderPass(&GFX.mainShading.depthPass, 1, false, true);
	RenderPass::createRenderPass(&GFX.mainShading.renderPass, true, false, true);

	Swapchain::transitionImages();
	MainShading::init();
	TiledForward::init();
	GFX.gtaoData.aoActive = renderSettings.enableGTAO;
	AmbientOcclusion::init();
	Outlines::init();
	BloomPass::init();
	SSR::init();
	PostProcess::init();

	//Shadow passes
	ShadowResources::pointShadowInit();
	ShadowResources::cascadedShadowInit();
	ShadowResources::recreateDescriptors(&GFX.omniShadowData, true);
	ShadowResources::recreateDescriptors(&GFX.cascadeShadowData, false);
	Pipeline::initShadowPipelines();

	Cubemapping::initSkyboxPass();
	FontRendering::init();

#ifndef _DISABLE_EDITOR
	Editor::setViewportImages();
#endif //_DISABLE_EDITOR
}

void Initialization::cleanupSwapchainDependencies()
{
	Swapchain::cleanupSwapchain();

	ShadowResources::cleanUp(&GFX.omniShadowData);
	ShadowResources::cleanUp(&GFX.cascadeShadowData);

	TiledForward::cleanup();
	AmbientOcclusion::cleanup();
	MainShading::cleanup();
	DebugLines::cleanup();
	Cubemapping::cleanup();
	Outlines::cleanup();
	BloomPass::cleanup();
	SSR::cleanup();
	PostProcess::cleanup();
	FontRendering::cleanup();

	CommandBuffers::cleanUp();
	Descriptors::destroyUniforms();
	vkDestroyDescriptorPool(GFX.device, GFX.descriptorPool, nullptr);
}

void Initialization::recreateSwapchain()
{
	vkDeviceWaitIdle(GFX.device);

	int width, height;
	glfwGetFramebufferSize(GFX.window, &width, &height);

	while (!(width || height)) {
		glfwGetFramebufferSize(GFX.window, &width, &height);
		glfwWaitEvents();
	}

	cleanupSwapchainDependencies();

	Swapchain::initSwapchain();
	initSwapchainDependencies();
	Descriptors::initFrameDescriptor();
	ShadowResources::recreateDescriptors(&GFX.omniShadowData, true);
	ShadowResources::recreateDescriptors(&GFX.cascadeShadowData, false);

	//MainShading::createDescLayouts();
	MainShading::recreateDescriptors();
	Pipeline::initPipelines();
	DebugLines::init();
}

void Initialization::initInstance()
{
	if (GFX.enableValidationLayers && !Validation::checkValidationLayers()) {
		Log::error("[Vulkan] Requested validation layers are not avalivalbe");
		GFX.enableValidationLayers = false;
	}

	VkApplicationInfo appInfo{};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pEngineName = GFX.engineName;
	appInfo.pApplicationName = QUASAR.appName;
	appInfo.apiVersion = GFX.apiVer;
	appInfo.applicationVersion = VK_MAKE_VERSION(0, 0, 0);

	VkValidationFeaturesEXT validationFeatures;
	validationFeatures.sType = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT;
	validationFeatures.enabledValidationFeatureCount = GFX.validationFeatures.size();
	validationFeatures.pEnabledValidationFeatures = GFX.validationFeatures.data();

	VkInstanceCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pNext = &validationFeatures;
	createInfo.pApplicationInfo = &appInfo;

	std::vector<const char*> extensions = getRequiredExtensions();
	createInfo.enabledExtensionCount = extensions.size();
	createInfo.ppEnabledExtensionNames = extensions.data();

	VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
	if (GFX.enableValidationLayers) {
		createInfo.enabledLayerCount = GFX.validationLayers.size();
		createInfo.ppEnabledLayerNames = GFX.validationLayers.data();

		Validation::populateCreateInfo(debugCreateInfo);
		createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*) &debugCreateInfo;
	} else {
		createInfo.enabledLayerCount = 0;
		createInfo.pNext = nullptr;
	}

	VK_CHECK( vkCreateInstance(&createInfo, nullptr, &GFX.instance), 
			"Instance creation failed. ");

}

void Initialization::initVMA()
{
	VmaVulkanFunctions functionsInfo {};
	functionsInfo.vkGetInstanceProcAddr = vkGetInstanceProcAddr;
	functionsInfo.vkGetDeviceProcAddr = vkGetDeviceProcAddr;

	VmaAllocatorCreateInfo allocatorInfo {};
	allocatorInfo.vulkanApiVersion = GFX.apiVer;
	allocatorInfo.physicalDevice = GFX.physicalDevice;
	allocatorInfo.device = GFX.device;
	allocatorInfo.instance = GFX.instance;
	allocatorInfo.pVulkanFunctions = &functionsInfo;
	//future enabled extensions in flags

	VK_CHECK( vmaCreateAllocator(&allocatorInfo, &GFX.allocator), 
			"Failed to create allocator");
}

std::vector<const char*> Initialization::getRequiredExtensions()
{
	uint32 glfwExtCount = 0;
	const char** glfwExts;
	glfwExts = glfwGetRequiredInstanceExtensions(&glfwExtCount);

	std::vector<const char*> extensions(glfwExts, glfwExts + glfwExtCount);

	if(GFX.enableValidationLayers) {
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		extensions.push_back(VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME);
	}

	return extensions;
}

void Initialization::createSyncObjects()
{
	//Create semaphores
	VkSemaphoreCreateInfo semaphoreInfo {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceInfo {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	GFX.imageAvaliableSemaphores.resize(GFX.maxFramesInFlight);
	GFX.renderFinishedSemaphores.resize(GFX.maxFramesInFlight);
	GFX.fencesInFlight.resize(GFX.maxFramesInFlight);

	for (uint32 i = 0; i < GFX.maxFramesInFlight; ++i) {

		VK_CHECK( 
			(vkCreateSemaphore(GFX.device, &semaphoreInfo, nullptr, &GFX.imageAvaliableSemaphores.at(i)) |
			vkCreateSemaphore(GFX.device, &semaphoreInfo, nullptr, &GFX.renderFinishedSemaphores.at(i)) |
			vkCreateFence(GFX.device, &fenceInfo, nullptr, &GFX.fencesInFlight.at(i))),
			"Sync object creation failed");
	}

	VK_CHECK(vkCreateFence(GFX.device, &fenceInfo, nullptr, &GFX.singleTimeCmdFence), 
			"Failed to create one time command fence");
}
