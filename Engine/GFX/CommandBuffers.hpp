#ifndef QUASAR_COMMANDBUFFERS_H
#define QUASAR_COMMANDBUFFERS_H

#include "Core/Assets/InstanceManager.hpp"
#include "Drawable/MeshManager.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "GFX/RenderTypes.hpp"
#include "GFX/RendererData.hpp"

class CommandBuffers
{
public:
	static void createCommandPool();
	static void createCommandBuffers();

	//Updates mesh and descriptor bindings for a specific frame in flight
	static void updateCommandBuffers(uint32 currentFrame, uint32 currentImage, ViewportData& vp);
	static void cleanUp();

	static void updateDepthCommands(uint32 currentFrame, ViewportData& vp);
	static void updateShadowCommands(uint32 currentFrame, ViewportData& vp);
	static void updateComputeCommands(uint32 currentFrame, ViewportData& vp);
	static void updateReflectionCommands(uint32 currentFrame);
	static void copyToNextFrame(uint32 currentFrame);
	static void updateShadingCommands(uint32 currentFrame, ViewportData& vp);
	static void updateBloomCommands(uint32 currentFrame, ViewportData& vp);
	static void updatePostProcessCommands(uint32 currentFrame, ViewportData& vp);
	static void updateDebugVisualizationCommands(uint32 currentFrame, ViewportData& vp);
	static void updateOutlineCommands(uint32 currentFrame, ViewportData& vp);
	static void updateTextCommands(uint32 currentFrame, ViewportData& vp);
	static void updateSwapchainCommands(uint32 currentFrame, uint32 currentImage);
	static void copySwapchainImage(uint32 currentFrame, uint32 currentImage);
	static void copyTextureImage(uint32 currentFrame, ViewportData& vp);

	static VkCommandBuffer beginSingleTimeCmd();
	static void endSingleTimeCmd();

	static VkCommandBuffer beginTransferCmd();
	static void endTransferCmd();

private:
	static void meshDrawingCommands(VkCommandBuffer cmdBuffer, VkPipelineLayout pipeline,
			bool depthOnly, bool mainPass, ViewportData& vp);

	static void drawMesh(MeshManager::Mesh& mesh, VkCommandBuffer cmdBuffer,
			std::vector<quasar::InstanceManager::MeshInstance>& instances,
			VkPipelineLayout pipelineLayout, bool depthOnly, bool mainPass);
	static void instanceDrawingCommand(VkCommandBuffer cmdBuffer);

	static void fullBarrier(VkCommandBuffer cmdBuffer);
};

#endif //QUASAR_COMMANDBUFFERS_H
