#ifndef QUASAR_NETWORKING_CLIENT_H
#define QUASAR_NETWORKING_CLIENT_H

#include "TransportLayer.hpp"

namespace quasar
{

class NetworkClient
{
public:

private:
	TransportLayer* transport = nullptr;
};

};

#endif //QUASAR_NETWORKING_CLIENT_H
