#ifndef QUASAR_NETWORKING_SERVER_H
#define QUASAR_NETWORKING_SERVER_H

#include "TransportLayer.hpp"

namespace quasar
{

class NetworkServer
{
public:

private:
	TransportLayer* transport = nullptr;
};

};

#endif //QUASAR_NETWORKING_SERVER_H
