#ifndef QUASAR_NETWORKING_TRANSPORT_H
#define QUASAR_NETWORKING_TRANSPORT_H

#include <cstdint>
#include <string>

namespace quasar
{

class TransportLayer
{
public:
	void connect(const std::string& address, uint16_t port);
};

};

#endif // QUASAR_NETWORKING_TRANSPORT_H
