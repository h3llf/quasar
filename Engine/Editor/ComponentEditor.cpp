#include "ComponentEditor.hpp"
#include "Core/ECS/Reflection.hpp"
#include "Editor/EditorSettings.hpp"
#include "Elements.hpp"
#include "AssetBrowser.hpp"
#include "Editor/ImageCache.hpp"
#include "GFX/RenderTypes.hpp"
#include "Platform/FileDialog.hpp"
#include "Core/ECS/UtilECS.hpp"
#include "imgui.h"
#include "imgui_internal.h"
#include "imgui/misc/cpp/imgui_stdlib.h"

using namespace quasar;

ComponentType ComponentEditor::rClickComponent = 0;

void ComponentEditor::drawComponentEditor()
{
	ImGui::Begin("Entity Editor");
	if (uiData.selectedEntity) {
		ContextECS::EntityEntry& entry = uiData.ecs->context.entities[uiData.selectedEntity.index];

		drawEntityInfo(entry);
		componentContextMenu();
		ImGuiTreeNodeFlags treeFlags = ImGuiTreeNodeFlags_DefaultOpen | 
			ImGuiTreeNodeFlags_AllowItemOverlap;

		// Display information for each component
		for (uint32 type = 0; type < uiData.ecs->context.componentPools.size(); ++type) {
			ComponentPool* pool = uiData.ecs->context.componentPools[type];

			if (!pool)
				continue;

			if (!pool->entityHasComponent(uiData.selectedEntity))
				continue;

			ComponentBase* comp = pool->atEntity(uiData.selectedEntity.index);
			std::string& name = ComponentRegistry::componentNames[type];

			bool open = false;
			bool btn = false;

			char btnText[64];
			sprintf(btnText, "%s##%d", ICON_FA_GEAR, type);
			Editor::collapsingHeaderBtn(name.c_str(), open, treeFlags, btnText, btn);

			if (btn) {
				rClickComponent = type;
				ImGui::OpenPopup("Component context menu");
			}

			// Display component information
			if (open) {
				displayComponent(comp, type);
				ImGui::NewLine();
			}
		}
	} else {
		const char* text = "Entity not selected";
		float xOffset = ImGui::GetContentRegionAvail().x / 2.0f;
		xOffset -= ImGui::CalcTextSize(text).x / 2.0f;
		float yOffset = ImGui::GetContentRegionAvail().y / 2.0f;
		
		ImGui::SetCursorPos(ImVec2(xOffset, yOffset));
		ImGui::PushStyleColor(ImGuiCol_Text, uiColours.textGrey);
		ImGui::TextUnformatted(text);
		ImGui::PopStyleColor();
	}

	ImGui::End();
}

void ComponentEditor::drawEntityInfo(ContextECS::EntityEntry& entry)
{
	const char* addBtnTxt = " " ICON_FA_SQUARE_PLUS " Add  ";

	float winPadding = ImGui::GetStyle().WindowPadding.x;

	ImVec2 txtSize = ImGui::CalcTextSize(addBtnTxt);
	txtSize.x += uiData.btnPadding * 2;

	// Entity name & add btn
	char nameBuffer[4096];
	strcpy(nameBuffer, entry.name.c_str());
	ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x - txtSize.x - winPadding);
	ImGui::InputText("##Entity name", nameBuffer, 4096);
	entry.name = nameBuffer;

	ImGui::SameLine();
	if (ImGui::Button(addBtnTxt)) {
		ImGui::OpenPopup("Add components dialog");
	}
	addComponentDialog(entry.entity);

	// Entity info
	ImGui::PushStyleColor(ImGuiCol_Text, uiColours.textGrey);
	ImGui::Text("Index %u  ID: %u  Components: %d", entry.entity.index, entry.entity.ID, entry.compCount);
	ImGui::PopStyleColor();

	ImGui::SeparatorEx(ImGuiSeparatorFlags_Horizontal);

}

void ComponentEditor::addComponentDialog(Entity entity)
{
	if (ImGui::BeginPopup("Add components dialog")) {
		if (ImGui::Selectable(ICON_FA_FOLDER " Load")) {
			std::string fileName = SystemDialog::getFile("*.component", false);
			if (!fileName.empty()) {
				ECSUtil::readComponentDisk(uiData.ecs, entity, fileName);
			}
		}
		ImGui::SeparatorEx(ImGuiSeparatorFlags_Horizontal);

		for (int i = 0; i < ComponentRegistry::componentNames.size(); ++i) {
			if (ImGui::Selectable(ComponentRegistry::componentNames[i].c_str())) {
				uiData.ecs->addComponentInternal(entity, i, nullptr);
			}
		}

		ImGui::EndPopup();
	}
}

void ComponentEditor::componentContextMenu()
{
	if (ImGui::BeginPopup("Component context menu")) {
		if (ImGui::Selectable(ICON_FA_ARROWS_ROTATE "Refresh")) {
			uiData.ecs->refresh(uiData.selectedEntity, rClickComponent);
		}

		if (ImGui::Selectable(ICON_FA_FLOPPY_DISK "Save")) {
			std::string fileName = SystemDialog::getFile("*.component", true);
			if (!fileName.empty()) {
				ECSUtil::writeComponentDisk(uiData.ecs, fileName, uiData.selectedEntity, 
						rClickComponent);
			}
		}

		ImGui::PushStyleColor(ImGuiCol_Header, uiColours.red);
		ImGui::PushStyleColor(ImGuiCol_HeaderHovered, uiColours.redHover);
		ImGui::PushStyleColor(ImGuiCol_HeaderActive, uiColours.redActive);
		if (ImGui::Selectable(ICON_FA_TRASH_CAN "Delete")) {
			uiData.ecs->remove(uiData.selectedEntity, rClickComponent);
		}
		ImGui::PopStyleColor(3);
		ImGui::EndPopup();
	}
}

void ComponentEditor::displayComponent(ComponentBase* comp, ComponentType type)
{
	ImGui::Separator();
	
	// TODO: Improve this somehow
	if (displayComponentHelper<CTransform>(comp, type)) {
		return;
	} else if (displayComponentHelper<CModel>(comp, type)) {
		return;
	} else if (displayComponentHelper<CMesh>(comp, type)) {
		return;
	} else if (displayComponentHelper<CMaterialPBR>(comp, type)) {
		return;
	} else if (displayComponentHelper<CRenderable>(comp, type)) {
		return;
	} else if (displayComponentHelper<CPointLight>(comp, type)) {
		return;
	} else if (displayComponentHelper<CDirectionalLight>(comp, type)) {
		return;
	} else if (displayComponentHelper<CRigidbody>(comp, type)) {
		return;
	} else if (displayComponentHelper<CColliderShape>(comp, type)) {
		return;
	} else if (displayComponentHelper<CCamera>(comp, type)) {
		return;
	} else if (displayComponentHelper<CText>(comp, type)) {
		return;
	} else if (displayComponentHelper<CAudioSource>(comp, type)) {
		return;
	} else {
		if (ComponentRegistry::reflectionData[type]) {
			displayComponent(comp, *ComponentRegistry::reflectionData[type]);
		} else {
			ImGui::Text("Component not implemented");
		}
	}
}

// TODO: Make getHash constexpt
static const uint64 intHash = Reflect::getHashFromTypename<int>();
static const uint64 floatHash = Reflect::getHashFromTypename<float>();
static const uint64 boolHash = Reflect::getHashFromTypename<bool>();
static const uint64 stringHash = Reflect::getHashFromTypename<std::string>();
static const uint64 imageHandleHash = Reflect::getHashFromTypename<ImageHandle>();
static const uint64 viewportHandleHash = Reflect::getHashFromTypename<ViewportHandle>();
static const uint64 vec2Hash = Reflect::getHashFromTypename<vec2>();
static const uint64 vec3Hash = Reflect::getHashFromTypename<vec3>();
static const uint64 vec4Hash = Reflect::getHashFromTypename<vec4>();

void ComponentEditor::displayComponent(ComponentBase* comp, Reflect::FieldList& fl)
{
	char* compPtr = reinterpret_cast<char*>(comp) + sizeof(ComponentBase);

	char label[2048];
	if (ImGui::BeginTable("Reflected component table", 2, ImGuiTableFlags_Resizable)) {
		for (Reflect::Field& fd : fl.fieldData) {
			if ((fd.editorOptions & Reflect::EDITOR_READ) != Reflect::EDITOR_READ) {
				continue;
			}

			Editor::tableText("%s", fd.fieldName.c_str());

			char* valPtr = compPtr + fd.offset;
			snprintf(label, 2048, "##inputreflcomp%s", fd.fieldName.c_str());

			ImGui::BeginDisabled(!(fd.editorOptions & Reflect::EDITOR_WRITE));
			if (fd.valueType.typeHash == boolHash) {
				ImGui::Checkbox(label, reinterpret_cast<bool*>(valPtr));
			} else 	if (fd.valueType.typeHash == intHash) {
				ImGui::InputInt(label, reinterpret_cast<int*>(valPtr));
			} else 	if (fd.valueType.typeHash == floatHash) {
				ImGui::InputFloat(label, reinterpret_cast<float*>(valPtr));
			} else if (fd.valueType.typeHash == stringHash) {
				ImGui::InputText(label, reinterpret_cast<std::string*>(valPtr));
			} else if (fd.valueType.typeHash ==  vec2Hash) {
				Editor::vectorInput(label, reinterpret_cast<vec2*>(valPtr));
			} else if (fd.valueType.typeHash ==  vec3Hash) {
				Editor::vectorInput(label, reinterpret_cast<vec3*>(valPtr));
			} else if (fd.valueType.typeHash ==  vec4Hash) {
				Editor::vectorInput(label, reinterpret_cast<vec4*>(valPtr));
			}

			ImGui::EndDisabled();
		}

		ImGui::EndTable();
	}
}


static bool showCTM = false;
static vec3 euler = vec3(0.0f);
void ComponentEditor::displayComponent(CTransform* cTransform)
{
	if (ImGui::BeginTable("Transform comp table", 2, ImGuiTableFlags_Resizable)) {
		Editor::tableText("Position");
		Editor::vectorInput("CTran pos", &cTransform->position);
		Editor::tableText("Rotation");
		euler = quatToEuler(cTransform->rotation);
		Editor::vectorInput("CTran rot", &euler);
//		Editor::vectorInput(&cTransform->rotation);
		cTransform->rotation = eulerToQuat(euler);
		Editor::tableText("Scale");
		Editor::vectorInput("CTran scale", &cTransform->scale);	
		ImGui::EndTable();

		if (ImGui::CollapsingHeader("Transformation matrix")) {
			ImGui::Text("%s", cTransform->ctm.str().c_str());
		}
	}

}

void ComponentEditor::displayComponent(CModel* cModel)
{
	// TODO: Display a preview and name
	if (ImGui::BeginTable("CModel component table", 2, ImGuiTableFlags_Resizable)) {
		Editor::tableText("Has meshes");
		ImGui::Checkbox("##CModel has meshes", &cModel->hasMeshes);

		Editor::tableText("Folder");
		ImGui::Text("%s", cModel->modelFolder.c_str());

		Editor::tableText("Filename");
		ImGui::Text("%s", cModel->modelName.c_str());

		ImGui::EndTable();

		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("ASSET_BROWSER_MODEL_DRAG")) {
				AssetBrowser::AssetPayload modelAsset = 
					*reinterpret_cast<AssetBrowser::AssetPayload*>(payload->Data);

				std::string path = modelAsset.identifier;
				cModel->modelFolder = path.substr(0, path.find_last_of('/') + 1);
				cModel->modelName = path.substr(path.find_last_of('/') + 1, path.npos);
				uiData.ecs->refresh(uiData.selectedEntity, TypeInfo<CModel>::type);
			}
		}
	}
}

// Replace with auto generated?
void ComponentEditor::displayComponent(CMesh* cMesh)
{
	if (ImGui::BeginTable("CMesh table", 2, ImGuiTableFlags_Resizable)) {
		Editor::tableText("Mesh Index");
		ImGui::Text("%d", cMesh->modelMeshIndex);

		Editor::tableText("Show BVH");
		ImGui::Checkbox("##preview bvh check", &cMesh->previewBVH);

		ImGui::EndTable();
	}
}

void ComponentEditor::displayComponent(CMaterialPBR* cMat)
{
	if (ImGui::BeginTable("CMaterialPBR table", 2, ImGuiTableFlags_Resizable)) {
		ImGui::TableNextColumn();
		ImGui::Dummy(ImVec2(0.0f, 8.0f));
		displayMaterialImage(cMat->albedoImage, &cMat->materialProps.albedoID, Image::Albedo);
		ImGui::TableNextColumn();
		ImGui::Dummy(ImVec2(0.0f, 8.0f));
		ImGui::Text("Albedo");
		ImGui::SeparatorEx(ImGuiSeparatorFlags_Horizontal);
		ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
		ImGui::ColorEdit4("##Albedo colour edit", &cMat->materialProps.baseColour.x, 
				ImGuiColorEditFlags_Float);
	
		ImGui::TableNextColumn();
		ImGui::Dummy(ImVec2(0.0f, 8.0f));
		displayMaterialImage(cMat->metalRoughImage, &cMat->materialProps.metalRoughID, Image::MetallicRoughness);
		ImGui::TableNextColumn();
		ImGui::Dummy(ImVec2(0.0f, 8.0f));
		ImGui::Text("Metallic roughness");
		ImGui::SeparatorEx(ImGuiSeparatorFlags_Horizontal);
		ImGui::Text("Metal");
		ImGui::SameLine();
		ImGui::InputFloat("##Metallic", &cMat->materialProps.metallicFactor);
		ImGui::Text("Rough");
		ImGui::SameLine();
		ImGui::InputFloat("##Roughness", &cMat->materialProps.roughnessFactor);
	
		ImGui::TableNextColumn();
		ImGui::Dummy(ImVec2(0.0f, 8.0f));
		displayMaterialImage(cMat->normalImage, &cMat->materialProps.normalID, Image::Normal);
		ImGui::TableNextColumn();
		ImGui::Dummy(ImVec2(0.0f, 8.0f));
		ImGui::Text("Normal");
		ImGui::SeparatorEx(ImGuiSeparatorFlags_Horizontal);
		ImGui::Text("Scale");
		ImGui::SameLine();
		ImGui::SliderFloat("##Normal scale", &cMat->materialProps.normalScale, -1.0f, 1.0f);
	
		ImGui::TableNextColumn();
		ImGui::Dummy(ImVec2(0.0f, 8.0f));
		displayMaterialImage(cMat->emissiveImage, &cMat->materialProps.emissiveID, Image::Emissive);
		ImGui::TableNextColumn();
		ImGui::Dummy(ImVec2(0.0f, 8.0f));
		ImGui::Text("Emissive");
		ImGui::SeparatorEx(ImGuiSeparatorFlags_Horizontal);
		ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
		ImGui::ColorEdit3("##Emissive colour edit", &cMat->materialProps.emissiveFactor.x, 
			ImGuiColorEditFlags_Float | ImGuiColorEditFlags_HDR);

		ImGui::TableNextColumn();
		ImGui::Dummy(ImVec2(0.0f, 8.0f));
		ImGui::Text("Double sided");
		ImGui::TableNextColumn();
		ImGui::Checkbox("##material doublesided", &cMat->materialOptions.doubleSided);

		ImGui::EndTable();
	}
}

void ComponentEditor::displayComponent(CCamera* cCam)
{
	if (ImGui::BeginTable("CCamera table", 2, ImGuiTableFlags_Resizable)) {

		if (cCam->targetImage != WINDOW_IMAGE_HANDLE) {
			Editor::tableText("Viewport Width");
			ImGui::InputInt("##viewport width input", &cCam->width);

			Editor::tableText("Viewport Height");
			ImGui::InputInt("##viewport height input", &cCam->height);
		}

		Editor::tableText("Viewport target");
		displayMaterialImage(cCam->targetImgName, &cCam->targetImage, Image::Blank);
		ImGui::Text("%i", cCam->targetImage);

		Editor::tableText("Skybox texture");
		displayMaterialImage(cCam->skyboxName, &cCam->skyboxImgID, Image::HDR);

		Editor::tableText("Ambient colour");
		ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
		ImGui::ColorEdit3("##CCam ambient color", &cCam->camProps.ambientColour.x, 
			ImGuiColorEditFlags_Float | ImGuiColorEditFlags_HDR);

		Editor::tableText("Gamma");
		ImGui::InputFloat("##Gamma float", &cCam->camProps.gamma);

		Editor::tableText("Exposure");
		ImGui::InputFloat("##Exposure float", &cCam->camProps.exposure);

		Editor::tableText("FOV");
		float fov = qml::degrees(cCam->camProps.fov);
		ImGui::InputFloat("##FOV float", &fov);
		cCam->camProps.fov = qml::radians(fov);

		Editor::tableText("Near plane");
		ImGui::InputFloat("##Near plane float", &cCam->camProps.near);

		Editor::tableText("Far plane");
		ImGui::InputFloat("##Far plane float", &cCam->camProps.far);

		Editor::tableText("Enabled");
		ImGui::Checkbox("##Cam Enabled bool", &cCam->enabled);

		Editor::tableText("Offscreen");
		ImGui::Checkbox("##Cam offscreen bool", &cCam->offscreen);

		ImGui::EndTable();
	}
}

constexpr float materialImgSize = 96.0f;
void ComponentEditor::displayMaterialImage(std::string& name, int* imageID, quasar::Image::ImageUsage usage)
{
	ImVec2 imgSize;
	if (usage == Image::HDR) {
		imgSize = ImVec2(materialImgSize * 2, materialImgSize);
	} else {
		imgSize = ImVec2(materialImgSize, materialImgSize);
	}

	drawCheckerboard(imgSize);
	ImTextureID textureImage;
	if (ImageCache::getImage(*imageID, &textureImage)) {
		ImGui::Image(textureImage, imgSize);
	} else {
		ImGui::Dummy(imgSize);
	}

	char btnLabel[16];
	sprintf(btnLabel, ICON_FA_XMARK "##clearTex%i", usage);
	if (ImGui::IsItemHovered() || ImGui::GetHoveredID() == ImGui::GetID(btnLabel)) {
		float btnSize = ImGui::CalcTextSize(ICON_FA_XMARK).x + uiData.btnPadding;

		ImGui::SameLine();
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() - btnSize - uiData.winPadding);

		ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 0.0f);
		ImGui::PushStyleColor(ImGuiCol_Button, 0xaa000000);
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, uiColours.redActive);
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, uiColours.redHover);
		ImGui::PushStyleColor(ImGuiCol_Text, uiColours.textFG);
		bool result = ImGui::Button(btnLabel, ImVec2(btnSize, btnSize));
		ImGui::PopStyleColor(4);
		ImGui::PopStyleVar();

		if (result) {
			name = "";
			*imageID = -1;
		}
	}

	if (ImGui::BeginDragDropTarget()) {
		if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("ASSET_BROWSER_IMAGE_DRAG")) {
			AssetBrowser::AssetPayload imageAsset = 
				*reinterpret_cast<AssetBrowser::AssetPayload*>(payload->Data);

			name = imageAsset.identifier;
			quasar::Image* img = uiData.appCtx.assetMgr->getImage(imageAsset.identifier);
			*imageID = img->rendererImageID;
		}
		ImGui::EndDragDropTarget();
	}

	ImGui::Text("File: %s", name.c_str());
}


void ComponentEditor::drawCheckerboard(ImVec2 size)
{
	ImDrawList* draw = ImGui::GetWindowDrawList();

	ImVec2 pos = ImGui::GetCursorScreenPos();
	ImVec2 p = pos;

	uint32_t colours[2] = {0xff999999, 0xffdddddd};

	const int squares = 8;
	for (int i = 0; i < squares; ++i) {
		for (int j = 0; j < squares; ++j) {
			ImVec2 ext = {p.x + size.x / squares, p.y + size.y / squares};
			draw->AddRectFilled(p, ext, colours[(i + j) % 2]);

			p.x += size.x / squares;
		}

		p.y += size.y / squares;
		p.x = pos.x;
	}
}

void ComponentEditor::displayComponent(CRenderable* cR)
{
	if (ImGui::BeginTable("Renderable component table", 2, ImGuiTableFlags_Resizable)) {
		Editor::tableText("Renderer mesh ID");
		ImGui::Text("%d", cR->meshID);

		Editor::tableText("Instance ID");
		ImGui::Text("%d", cR->instanceID);

		ImGui::EndTable();
	}
}

void ComponentEditor::displayComponent(CPointLight* cPLight)
{
	if (ImGui::BeginTable("CPointLight table", 2, ImGuiTableFlags_Resizable)) {
		Editor::tableText("Colour");
		ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
		ImGui::ColorEdit3("##pointlight colour edit", &cPLight->colour.x, ImGuiColorEditFlags_Float);

		Editor::tableText("Intensity");
		ImGui::InputFloat("##point light intensity float", &cPLight->intensity);

		Editor::tableText("Distance");
		ImGui::InputFloat("##point light distance", &cPLight->distance);

		Editor::tableText("Shadow mapped");
		ImGui::Checkbox("##point light shadow check", &cPLight->shadowMapped);

		Editor::tableText("Renderer ID");
		ImGui::Text("%d", cPLight->lightID);

		ImGui::EndTable();
	}
}

void ComponentEditor::displayComponent(CDirectionalLight* cDLight)
{
	if (ImGui::BeginTable("CDirectionalLight table", 2, ImGuiTableFlags_Resizable)) {
		Editor::tableText("Colour");
		ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
		ImGui::ColorEdit3("##dirlight colour edit", &cDLight->colour.x, ImGuiColorEditFlags_Float);

		Editor::tableText("Intensity");
		ImGui::InputFloat("##dir light intensity float", &cDLight->intensity);

		Editor::tableText("Shadow mapped");
		ImGui::Checkbox("##dir light shadow check", &cDLight->shadowMapped);

		Editor::tableText("Nearplane offset");
		ImGui::InputFloat("##dir light near plane", &cDLight->yOffset);

		Editor::tableText("Renderer ID");
		ImGui::Text("%d", cDLight->lightID);

		ImGui::EndTable();
	}
}

void ComponentEditor::displayComponent(CRigidbody* cRigid)
{
	if (ImGui::BeginTable("CRigidbody table", 2, ImGuiTableFlags_Resizable)) {
		Editor::tableText("Is kinematic");
		ImGui::Checkbox("##rigidbody is kinematic check", &cRigid->kinematic);

		Editor::tableText("Linear velocity");
		Editor::vectorInput("CRigid linvel", &cRigid->linearVelocity);

		Editor::tableText("Angular velocity");
		Editor::vectorInput("CRigid angval", &cRigid->angularVelocity);

		Editor::tableText("Mass");
		ImGui::DragFloat("##rigidbody mass slider", &cRigid->mass, 1.0f, 0.0f, 0.0f, "%.3fkg");

		Editor::tableText("Rigidbody ID");
		ImGui::Text("%d", cRigid->ID);

		ImGui::EndTable();

		float width = (ImGui::GetContentRegionAvail().x - uiData.framePadding) / 2.0f;
		if (ImGui::Button("Add triangle meshes", ImVec2(width, 0.0f))) {
			cRigid->kinematic = true;
			uiData.ecs->refresh<CRigidbody>(uiData.selectedEntity);
			SRigidbody::addMeshColliders(uiData.ecs, uiData.selectedEntity, false);
		}

		ImGui::SameLine();
		ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x - uiData.framePadding);
		if (ImGui::Button("Add convex meshes", ImVec2(width, 0.0f))) {
			SRigidbody::addMeshColliders(uiData.ecs, uiData.selectedEntity, true);
		}
	}
	
}

void ComponentEditor::displayComponent(CColliderShape* cCol)
{
	if (ImGui::BeginTable("CBoxCollider table", 2, ImGuiTableFlags_Resizable)) {
		Editor::tableText("Collider");
		ImGui::Combo("##CCollider collider combo", (int*)&cCol->colliderType, 
				PhysicsController::physicsColliderNames, 5);

		switch (cCol->colliderType) {
		case PHYSICS_BOX_COLLIDER:
			Editor::tableText("Size");
			Editor::vectorInput("CCol size", &cCol->size);
			break;
		case PHYSICS_CAPSULE_COLLIDER:	
			Editor::tableText("Half height");
			ImGui::InputFloat("##CCollider half height", &cCol->halfHeight);
		case PHYSICS_SPHERE_COLLIDER:	
			Editor::tableText("Radius");
			ImGui::InputFloat("##CCollider radius", &cCol->radius);
			break;
		}

		Editor::tableText("Shape ID");
		ImGui::Text("%d", cCol->ID);

		ImGui::EndTable();
	}
}

void ComponentEditor::displayComponent(quasar::CText* cText)
{
	if (ImGui::BeginTable("Ctext table", 2, ImGuiTableFlags_Resizable)) {
		Editor::tableText("String content");
		ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
		ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 0.0f);
		ImGui::InputTextMultiline("##CText contennt input", &cText->content);
		ImGui::PopStyleVar();

		float height = ImGui::CalcTextSize("l").y + 2.0f * uiData.winPadding;

		Editor::tableText("Font");
		ImGui::PushStyleColor(ImGuiCol_ChildBg, uiColours.grey);
		ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 10.0f);
		ImGui::BeginChild("font drag dst", ImVec2(0, height), true, ImGuiWindowFlags_AlwaysUseWindowPadding);
		ImGui::Text("%s", cText->fontName.c_str());
		ImGui::EndChild();
		ImGui::PopStyleVar();
		ImGui::PopStyleColor();

		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("ASSET_BROWSER_FONT_DRAG")) {
				AssetBrowser::AssetPayload fontAsset = 
					*reinterpret_cast<AssetBrowser::AssetPayload*>(payload->Data);

				cText->fontName = fontAsset.identifier;
				cText->font = uiData.appCtx.assetMgr->getFont(fontAsset.identifier)->fontHandle;
			}
			ImGui::EndDragDropTarget();
		}

		Editor::tableText("Vertex colour");
		ImVec4 col4 = ImGui::ColorConvertU32ToFloat4(cText->vertexColour);
		ImGui::ColorEdit4("##CText vertex colour", &col4.x, ImGuiColorEditFlags_DisplayHex | 
				ImGuiColorEditFlags_AlphaPreview | ImGuiColorEditFlags_AlphaBar);
		cText->vertexColour = ImGui::ColorConvertFloat4ToU32(col4);

		Editor::tableText("Wrap width");
		ImGui::InputFloat("##CText max width", &cText->maxWidth);

		Editor::tableText("Line spacing");
		ImGui::InputFloat("##CText line spacing", &cText->lineSpacing);

		Editor::tableText("Kerning");
		ImGui::InputFloat("##CText kerning", &cText->kerning);

		ImGui::EndTable();
	}
}

void ComponentEditor::displayComponent(CAudioSource* cAudio)
{
	if (ImGui::BeginTable("CAudioSource table", 2, ImGuiTableFlags_Resizable)) {
		Editor::tableText("Audio file");

		float height = ImGui::CalcTextSize("l").y + 2.0f * uiData.winPadding;
		ImGui::PushStyleColor(ImGuiCol_ChildBg, uiColours.grey);
		ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 10.0f);
		ImGui::BeginChild("font drag dst", ImVec2(0, height), true, ImGuiWindowFlags_AlwaysUseWindowPadding);
		ImGui::Text("%s", cAudio->filename.c_str());
		ImGui::EndChild();
		ImGui::PopStyleVar();
		ImGui::PopStyleColor();

		if (ImGui::BeginDragDropTarget()) {
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("ASSET_BROWSER_AUDIO_DRAG")) {
				AssetBrowser::AssetPayload fontAsset = 
					*reinterpret_cast<AssetBrowser::AssetPayload*>(payload->Data);

				cAudio->filename = fontAsset.identifier;
			}
			ImGui::EndDragDropTarget();
		}

		Editor::tableText("Loop");
		ImGui::Checkbox("##cAudio loop", &cAudio->loop);

		Editor::tableText("Play on creation");
		ImGui::Checkbox("##cAudio play oncreate", &cAudio->playOnCreate);

		Editor::tableText("Gain");
		ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
		ImGui::SliderFloat("##cAudio gain", &cAudio->gain, 0.0f, 1.0f);

		Editor::tableText("Pitch");
		ImGui::InputFloat("##cAudio pitch", &cAudio->pitch);

		Editor::tableText("Positional");
		ImGui::Checkbox("##cAudio positional", &cAudio->positional);

		Editor::tableText("Should update");
		cAudio->shouldUpdate |= ImGui::Button("Update Now##caudio");

		Editor::tableText("Always update");
		ImGui::Checkbox("##cAudio always update", &cAudio->alwaysUpdate);

		ImGui::EndTable();
	}
}
