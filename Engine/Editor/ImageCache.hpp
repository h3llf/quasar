#ifndef QUASAR_IMAGE_CACHE_H
#define QUASAR_IMAGE_CACHE_H

#include "imgui.h"
#include <unordered_map>

class ImageCache
{
public:
	// Clears all image descriptors from the cache, (Used when images are deleted)
	static void resetCache();

	static bool getImage(int imageID, ImTextureID* pTexID);

	static std::unordered_map<int, ImTextureID> cache;
};

#endif //QUASAR_IMAGE_CACHE_H
