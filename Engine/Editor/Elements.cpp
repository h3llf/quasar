#include "Elements.hpp"
#include "Editor/EditorSettings.hpp"
#include "IconFontCppHeaders/IconsFontAwesome6.h"
#include "imgui.h"
#include "imgui_internal.h"
#include "imgui/misc/cpp/imgui_stdlib.h"
#include <cmath>
#include <cstdio>

void Editor::button(const char *name, const char *desc, bool *state, ImVec2 buttonSize,
		uint32 colour)
{
	ImGui::PushStyleColor(ImGuiCol_Button, colour);

	if (ImGui::Button(name, buttonSize)) {
		*state = !*state;
	}

	ImGui::PopStyleColor();

	if (ImGui::IsItemHovered()) {
		ImGui::PushFont(uiData.textIconSmall);
		ImGui::SetTooltip(desc);
		ImGui::PopFont();
	}
}

void Editor::toggleButton(const char* name, const char* desc, bool* state, ImVec2 buttonSize,
		uint32 activeCol, uint32 inactiveCol)
{
	if (*state) {
		ImGui::PushStyleColor(ImGuiCol_Button, activeCol);
	} else {
		ImGui::PushStyleColor(ImGuiCol_Button, inactiveCol);
	}

	if (ImGui::Button(name, buttonSize)) {
		*state = !*state;
	}

	ImGui::PopStyleColor();

	if (ImGui::IsItemHovered()) {
		ImGui::PushFont(uiData.textIconSmall);
		ImGui::SetTooltip("%s", desc);
		ImGui::PopFont();
	}
}

void Editor::toggleButtonIcon(const char* nameActive, const char* nameInactive, const char *desc, 
		bool *state, ImVec2 buttonSize)
{
	const char* name;

	if (*state) {
		name = nameActive;
	} else {
		name = nameInactive;
	}

	if (ImGui::Button(name, buttonSize)) {
		*state = !*state;
	}

	if (ImGui::IsItemHovered()) {
		ImGui::PushFont(uiData.textIconSmall);
		ImGui::SetTooltip("%s", desc);
		ImGui::PopFont();
	}
}

void Editor::toggleSwitch(const char* leftName, const char* rightName, bool* state, const char* tooltip)
{	
	float width = std::max(ImGui::CalcTextSize(leftName).x, ImGui::CalcTextSize(rightName).x) * 2.0f +
		ImGui::CalcTextSize(" ").x * 12;
	width += 4.0f * uiData.framePadding;

	float fontSize = uiData.textIconSmall->FontSize;
	float yOffset = fontSize + 2.0f * uiData.framePadding;

	// TODO: Move to settings
	const float border = 3.0f;

	ImVec2 p1 = ImGui::GetCursorScreenPos();
	p1.x += border + 2.0f;
	p1.y += border + 2.0f;
	ImVec2 p2 = ImVec2(p1.x + width, p1.y + yOffset);
	ImVec2 p3 = p1;
	ImVec2 p4 = p2;
	if (*state) {
		p3.x += width / 2.0f;
	} else {
		p4.x -= width / 2.0f;
	}

	// Border
	p1.x -= border;
	p1.y -= border;
	p2.x += border;
	p2.y += border;

	ImDrawList* draw = ImGui::GetWindowDrawList();
	draw->AddRectFilled(p1, p2, uiColours.bgTrans, 100.0f);
	draw->AddRectFilled(p3, p4, uiColours.button, 100.0f);

	// Draw text relative to center
	ImVec2 txtPos;
	txtPos.y = p3.y + uiData.framePadding;
	txtPos.x = (*state) ? p3.x : p4.x;
	txtPos.x -= width / 4.0f + ImGui::CalcTextSize(leftName).x / 2.0f;
	ImGui::SetCursorScreenPos(txtPos);
	ImGui::Text("%s", leftName);

	txtPos.x = (*state) ? p3.x : p4.x;
	txtPos.x += width / 4.0f - ImGui::CalcTextSize(rightName).x / 2.0f;
	ImGui::SetCursorScreenPos(txtPos);
	ImGui::Text("%s", rightName);

	ImVec2 btnSize = ImVec2(p2.x - p1.x, p2.y - p1.y);
	ImGui::SetCursorScreenPos(p1);
	if (ImGui::InvisibleButton(leftName, btnSize)) {
		*state = !*state;
	};

	if (tooltip[0] != '\0' && ImGui::IsItemHovered()) {
		ImGui::SetTooltip("%s", tooltip);
	}
}

void Editor::buttonExclusive(const char* name, const char* desc, int* state, int ID, ImVec2 buttonSize,
		uint32 activeCol, uint32 inactiveCol, bool colourText)
{
	ImGuiCol col = (colourText) ? ImGuiCol_Text : ImGuiCol_Button;

	if (*state == ID) {
		ImGui::PushStyleColor(col, activeCol);
	} else {
		ImGui::PushStyleColor(col, inactiveCol);
	}

	if (ImGui::Button(name, buttonSize)) {
		*state = ID;
	}

	ImGui::PopStyleColor();

	if (desc && ImGui::IsItemHovered()) {
		ImGui::PushFont(uiData.textIconSmall);
		ImGui::SetTooltip("%s", desc);
		ImGui::PopFont();
	}
}

void Editor::collapsingHeaderBtn(const char *name, bool &open, ImGuiTreeNodeFlags flags, 
		const char *btnText, bool &btn)
{
	float textSize = ImGui::CalcTextSize(btnText, NULL, true).x;
	float settingBtnOffset = ImGui::GetContentRegionAvail().x - textSize;
		
	ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 0.0f);
	ImGui::PushStyleColor(ImGuiCol_Header, uiColours.childBG);
	ImGui::PushFont(uiData.textLarge);
	open = ImGui::CollapsingHeader(name, flags);
	ImGui::PopFont();
	ImGui::PopStyleColor();
	ImGui::PopStyleVar();

	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, 0x00000000);
	ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0.0f, 0.0f));
	float btnSize = textSize - 1.0f;
	ImGui::SetCursorPosX(settingBtnOffset);
	ImGui::SetCursorPosY(ImGui::GetCursorPosY() + 2.0f);
	btn = ImGui::Button(btnText, ImVec2(btnSize, btnSize));
	ImGui::PopStyleColor();
	ImGui::PopStyleVar();
}

bool Editor::collapsingHeader(const char *label)
{
	ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 0.0f);	
	ImGui::PushFont(uiData.textLarge);
	bool res = ImGui::CollapsingHeader(label);
	ImGui::PopFont();
	ImGui::PopStyleVar();
	return res;
}

void Editor::searchBox(const char* label, std::string &input, const char *hint, float width)
{
	ImVec2 cursorPos = ImGui::GetCursorScreenPos();

	// Set clear button location
	float btnSize = ImGui::CalcTextSize(ICON_FA_CIRCLE_XMARK).x + uiData.framePadding * 1.0f;
	cursorPos.x += width - btnSize;
	cursorPos.y += uiData.framePadding;

	// Check if the mouse is over the button location
	ImVec2 mouseCursor = ImGui::GetMousePos();
	mouseCursor.x -= cursorPos.x;
	mouseCursor.y -= cursorPos.y;
	mouseCursor.x -= btnSize / 2.0f;
	mouseCursor.y -= btnSize / 2.0f;
	bool btnHovered = (abs(mouseCursor.x) <= btnSize / 2.0f) && (abs(mouseCursor.y) <= btnSize / 2.0f);

	ImGuiInputTextFlags flags = ImGuiInputTextFlags_AutoSelectAll;

	// Equivilent to an invisible button without overlap contraints
	uint32 textColour = uiColours.textGrey;
	if (btnHovered && ImGui::IsMouseDown(ImGuiMouseButton_Left)) {
		textColour = uiColours.redActive;
		input.clear();

		// Prevent text being written to input next frame
		flags |= ImGuiInputTextFlags_ReadOnly;
	} else if (btnHovered) {
		textColour = uiColours.red;
	}

	// Search bar
	ImGui::SetNextItemWidth(width);
	ImGui::InputTextWithHint(label, hint, &input, flags);

	// Clear button icon
	// Don't draw if there's nothing to clear
	if (!input.empty()) {
		ImGui::GetWindowDrawList()->AddText(cursorPos, textColour, ICON_FA_CIRCLE_XMARK);
	}
}

void Editor::sliderIntAtomic(const char* label, std::atomic<uint32_t>* v, uint32_t min, uint32_t max, 
		const char* format, bool powerTwo)
{
	int vBuffer;

	if (powerTwo) {
		vBuffer = std::log2(v->load());
		// Ignore label format as this is replaced
		format = "";
	} else {
		vBuffer = *v;
	}

	ImGui::SliderInt(label, &vBuffer, min, max, format);

	if (powerTwo) {
		vBuffer = 1 << vBuffer;

		// Make the result appear in the middle of the slider
		char newLabel[64];
		sprintf(newLabel, "%d", vBuffer);
		*v = vBuffer;
		ImGui::SameLine();
		float offset = ImGui::GetItemRectSize().x / 2.0f;
		offset += ImGui::CalcTextSize(newLabel).x / 2.0f;
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() - offset);
		ImGui::TextUnformatted(newLabel);
	} else {
		*v = vBuffer;
	}
}
