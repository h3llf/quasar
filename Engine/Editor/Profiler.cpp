#include "Profiler.hpp"
#include "Editor/Elements.hpp"
#include "imgui.h"
#include "implot.h"
#include <mutex>
#include <string>
#include <sstream>

int Profiler::maxDataPoints = 512;
float Profiler::updateRate = 0.5f;
std::vector<Profiler::ThreadData> Profiler::threadData {};

uint32 Profiler::addThread(std::string name, std::string unit)
{
	std::lock_guard<std::mutex> lck(uiData.editorSyncMutex);
	
	threadData.push_back(ThreadData{name, unit});
	return threadData.size() - 1;
}

uint32 Profiler::addTask(int threadNum, std::string taskName)
{
	std::lock_guard<std::mutex> lck(uiData.editorSyncMutex);
	
	ThreadData& data = threadData[threadNum];

	data.tasks.push_back((Task{taskName}));
	data.tasks.back().dataPoints.resize(maxDataPoints);
	data.tasks.back().cumulativeDataPoints.resize(maxDataPoints);
	return threadData[threadNum].tasks.size() - 1;
}

void Profiler::uploadTime(double duration, const int threadNum, const int taskNum)
{
	std::lock_guard<std::mutex> lck(uiData.editorSyncMutex);

	// Thread total
	ThreadData& tData = threadData[threadNum];
	tData.totalDelta += duration;
	if (taskNum == 0 ) {
		tData.maxDataPoints = maxDataPoints;
		tData.nextPlot++;
		tData.nextPlot = tData.nextPlot % tData.maxDataPoints;

		tData.tickCount++;
		if (tData.totalDelta >= updateRate * 1000) {
			tData.measurement = tData.totalDelta / tData.tickCount; 
			tData.tickCount = 0;
			tData.totalDelta = 0.0;
		}
	}

	// Per task
	Task& task = tData.tasks[taskNum];
	task.dataPoints.resize(tData.maxDataPoints);
	task.cumulativeDataPoints.resize(tData.maxDataPoints);

	task.dataPoints[tData.nextPlot] = duration;

	if (taskNum > 0) {
		task.cumulativeDataPoints[tData.nextPlot] = 
			threadData[threadNum].tasks[taskNum - 1].cumulativeDataPoints[tData.nextPlot] + duration;
	} else {
		task.cumulativeDataPoints[tData.nextPlot] = duration;
	}

	task.totalDelta += duration;
	task.tickCount++;

	if (task.totalDelta >= updateRate * 1000) {
		task.measurement = task.totalDelta / tData.tickCount; 
		task.tickCount = 0;
		task.totalDelta = 0.0;
	}
}

bool Profiler::showSettings = false;
int Profiler::chartType = 0;
float Profiler::plotHeight = 150.0f;
bool Profiler::legendOutside = true;
const char* Profiler::chartNames[] = {
	ICON_FA_CHART_AREA "Cumulative",
	ICON_FA_CHART_LINE "Line",
	ICON_FA_CHART_PIE "Pie"
};

void Profiler::drawProfiler()
{
	ImGuiWindowFlags winFlags = ImGuiWindowFlags_None;
	ImGui::Begin("Profiler", &uiData.profilerVisible, winFlags);
	ImGui::PushStyleColor(ImGuiCol_Button, 0x000000);
	Editor::toggleButtonIcon(ICON_FA_GEAR ICON_FA_CARET_UP, ICON_FA_GEAR ICON_FA_CARET_DOWN, 
			"Profiler settings", &showSettings);
	ImGui::PopStyleColor();

	if (showSettings) {
		drawSettings();
	}

	for (ThreadData& tData : threadData) {
		ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 0.0f);

		char header[256];
		snprintf(header, 256, "%s %lf ms %lf %s ###plot%s", tData.threadName.c_str(), tData.measurement, 
				1000.0 / tData.measurement, tData.unit.c_str(), tData.threadName.c_str());

		bool open = ImGui::CollapsingHeader(header, ImGuiTreeNodeFlags_DefaultOpen);
		ImGui::PopStyleVar();
		if (open) {
			ImPlotAxisFlags axisFlags = ImPlotAxisFlags_AutoFit;
			ImPlotFlags plotFlags = ImPlotFlags_NoFrame | ImPlotFlags_NoTitle;
			ImPlotLegendFlags legendFlags = (legendOutside) ? 
				ImPlotLegendFlags_Outside : ImPlotLegendFlags_None;

			if (ImPlot::BeginPlot(tData.threadName.c_str(), ImVec2(-1, plotHeight), plotFlags)) {
				ImPlot::SetupAxis(ImAxis_Y1, "ms", axisFlags);
				ImPlot::SetupAxis(ImAxis_X1, "", axisFlags | ImPlotAxisFlags_NoDecorations);
				ImPlot::SetupLegend(ImPlotLocation_NorthWest, legendFlags);

				switch (chartType) {
				case 0:
					drawShadedPlot(tData);
					break;
				case 1:
					drawLinePlot(tData);
					break;
				default:
					drawShadedPlot(tData);
					break;
				}
				ImPlot::EndPlot();
			}
		}
	}

	ImGui::End();
}

void Profiler::drawSettings()
{
	if (ImGui::BeginTable("Transform comp table", 2, ImGuiTableFlags_Resizable)) {
		ImGui::TableNextColumn();
		ImGui::Text("Chart Type");
		ImGui::TableNextColumn();
		ImGui::SetNextItemWidth(256.0f);
		ImGui::Combo("##Select chart type", &chartType, chartNames, 2);

		ImGui::TableNextColumn();
		ImGui::Text("Legend outside");
		ImGui::TableNextColumn();
		ImGui::Checkbox("##Legend outside checkbox", &legendOutside);

		ImGui::TableNextColumn();
		ImGui::Text("Data points");
		ImGui::TableNextColumn();
		ImGui::SetNextItemWidth(256.0f);
		ImGui::SliderInt("##Max Data points", &maxDataPoints, 16, 2048);
		ImGui::TableNextColumn();
		ImGui::Text("Frametime frequency");
		ImGui::TableNextColumn();
		ImGui::SetNextItemWidth(256.0f);
		ImGui::SliderFloat("##Frametime refresh rate slider", &updateRate, 0.0f, 20.0f, "%.1fs");
		ImGui::TableNextColumn();
		ImGui::Text("Graph height");
		ImGui::TableNextColumn();
		ImGui::SetNextItemWidth(256.0f);
		ImGui::SliderFloat("##Graph height slider", &plotHeight, 100.0f, 500.0f, "%.0fpx");
		ImGui::EndTable();
	}
}

void Profiler::drawShadedPlot(ThreadData& tData) {
	for (int i = tData.tasks.size() - 1; i >= 0; --i) {
		Task& tsk = tData.tasks[i];
		if (!tsk.cumulativeDataPoints.size()) {
			continue;
		}
		ImPlot::PlotShaded(tsk.taskName.c_str(), tsk.cumulativeDataPoints.data(), 
				tsk.cumulativeDataPoints.size(), 0, 1, 0, 0, tData.nextPlot);
	}
}

void Profiler::drawLinePlot(ThreadData& tData)
{
	for (int i = 0; i < tData.tasks.size(); ++i) {
		Task& tsk = tData.tasks[i];
		if (!tsk.dataPoints.size()) {
			continue;
		}
		ImPlot::PlotLine(tsk.taskName.c_str(), tsk.dataPoints.data(), 
				tsk.dataPoints.size(), 1, 0, 0, tData.nextPlot);
	}

}
