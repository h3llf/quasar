#ifndef QUASAR_PROJECT_GENERATOR_H
#define QUASAR_PROJECT_GENERATOR_H

#include "Core/QuasarProject.hpp"
#include <filesystem>
#include <unordered_map>
#include <string>

class ProjectGenerator
{
public:
	static void generateProject(const std::string& path, const std::string& name, bool createFolder);
	static bool loadProject(const std::string& path);

private:
	static void projectStartup(ProjectHeader* ph);

	// Creates a source file to serve as the entry pont
	static void generateInitFile(const std::filesystem::path& path);

	// Writes the .quasar project file
	static void createJsonFile(const std::filesystem::path& path, const std::string& name);

	// Re-creates the CMakeLists on each startup (Quasar absolute system path)
	// TODO: Move the engine managed potion of this file to an include
	static void initCmakeFiles(const std::filesystem::path& path, const std::string& name);

	// Generates source and header files from the class list
	static void generateSourceFile(const std::filesystem::path& path, const std::string& name,
			const std::string& relativeDir = "");
	static void generateHeaderFile(const std::filesystem::path& path, const std::string& name,
			const std::string& relativeDir = "");

	static void writeFile(const std::filesystem::path& file, char* data);

	// Initialize build and assset directories
	static void setupDirectoryTree(const std::filesystem::path& dir);

	static inline const char CPP_INIT_TEMPLATE[] = {
		"#include <Core/Application.hpp>\n"
		"%s\n"
		"// Do not edit! This file will be overwritten by the project system\n\n"
		"%s\n"
		"extern \"C\"\n"
		"{\n"
		"\tvoid init(quasar::ApplicationContext& appContext)\n"
		"\t{\n"
		"%s"
		"\t}\n"
		"}\n"
	};

	static inline const char HEADER_FILE_TEMPLATE[] = {
		"#ifndef %s\n"
		"#define %s\n"
		"#include <Core/Application.hpp>\n\n"
		"class %s\n"
		"{\n"
		"public:\n"
		"\t// Called when the library is loaded\n"
		"\tvoid init(quasar::ApplicationContext& ctx);\n"
		"};\n"
		"#endif\n"
	};

	static inline const char SOURCE_FILE_TEMPLATE[] = {
		"#include \"%s.hpp\"\n"
		"using namespace quasar;\n\n"
		"void %s::init(ApplicationContext& ctx)\n"
		"{\n"
		"\t// Register ecs functions here\n"
		"}\n"
	};

	static inline const char CMAKE_TEMPLATE[] = {
		"cmake_minimum_required(VERSION 3.16)\n"
		"project(%s)\n\n"
		"# Do not edit! This file will be overwritten by the project system\n"
		"set(QUASAR_RELATIVE_DIR %s/)\n"
		"include(${QUASAR_RELATIVE_DIR}cmake/GameBuild.cmake)\n"
		"include(${QUASAR_RELATIVE_DIR}cmake/CompilerOptions.cmake)\n"
		"include(${QUASAR_RELATIVE_DIR}cmake/IncludeDirectories.cmake)\n\n"
		"message(${QUASAR_RELATIVE_DIR}Engine)\n"
		"file(GLOB_RECURSE GAME_SRC source/*.cpp)\n"
		"link_directories(${QUASAR_RELATIVE_DIR}build)\n"
		"add_library(${PROJECT_NAME} SHARED ${GAME_SRC})\n"
		"target_link_libraries(${PROJECT_NAME} Quasar)\n\n"
		"# Randomize pdb name to avoid locking\n"
		"if(${CMAKE_SYSTEM_NAME} MATCHES \"Windows\")\n"
		"RANDOMIZE_PDB_NAME(${PROJECT_NAME})\n"
		//"target_link_options(${PROJECT_NAME} PRIVATE -PDBALTPATH:./${PROJECT_NAME}.pbd)\n"
		"endif()\n"

//		"target_compile_options(${PROJECT_NAME} PUBLIC -fPIC)\n"
	};

	static inline const std::filesystem::path directoryTree[] = {
		"build",
		"source",
		"Assets",
		"Assets/Models",
		"Assets/Textures",
		"Assets/Entities",
	};
};

#endif //QUASAR_PROJECT_GENERATOR_H
