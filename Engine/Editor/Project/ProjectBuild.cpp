#include "ProjectBuild.hpp"
#include "Core/QuasarProject.hpp"
#include "Editor/EditorSettings.hpp"

void ProjectBuild::runBuildCommands()
{
	ProjectHeader* ph = uiData.currentProj;
	std::string buildNotify = "Build started on " + ph->projectName;
	std::string spacer = std::string(buildNotify.size(), '=');

	Log::out('\n', spacer);
	Log::out(buildNotify);
	Log::out(spacer, '\n');

#ifdef __linux__
	cmakeCommandsLinux();
#endif // __linux__
}

void ProjectBuild::openProject()
{
#ifdef __linux__
	openProjectLinux();
#endif // __linux__
}

void ProjectBuild::cmakeCommandsLinux()
{
	ProjectHeader* ph = uiData.currentProj;

	// CD to project
	std::string cmd = "cd \"";
	cmd += ph->directory;
	cmd += "\"; ";

	// CMake init & build
	cmd += "cd build; cmake ../; cmake --build ./ &";

	system(cmd.c_str());
}

void ProjectBuild::openProjectLinux()
{
	// TODO: Check for the presence of xdg-open

	ProjectHeader* ph = uiData.currentProj;
	std::string cmd = "xdg-open ";
	cmd += ph->directory / "source/";
	cmd += ph->sourceFiles.begin()->first + ".cpp &";

	system(cmd.c_str());
}
