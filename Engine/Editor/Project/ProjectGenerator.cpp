#include "ProjectGenerator.hpp"
#include "Core/QuasarProject.hpp"
#include "Editor/EditorSettings.hpp"
#include "Editor/Elements.hpp"
#include "imgui.h"
#include "imgui_internal.h"
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <filesystem>
#include <iomanip>
#include <ctime>
#include <fstream>
#include <ios>
#include "json.hpp"

namespace fs = std::filesystem;

void ProjectGenerator::generateProject(const std::string& path, const std::string& name, bool createFolder)
{
	ProjectHeader* ph = uiData.currentProj;
	ph->shouldClose = true;

	fs::path fullPath = path;

	if (createFolder) {
		fullPath /= name;
	}

	fs::create_directory(fullPath);

	char timeDateBuffer[128];
	std::tm* tm;
	time_t t;
	time(&t);
	tm = ::localtime(&t);
	std::strftime(timeDateBuffer, 64, "%d.%m.%Y %H:%M:%S", tm);

	// Clean up the name
	std::string binaryName = name;
	std::replace(binaryName.begin(), binaryName.end(), ' ', '_');
	if (!isalpha(binaryName[0])) {
		binaryName = 's' + binaryName;
	}

	*ph = {
		.projectName = name,
		.description = "Blank project",
		.binaryName = binaryName,
		.createdOn = timeDateBuffer,
		.directory = fullPath,
	};
	ph->canonicalDir = fs::canonical(fs::path(ph->directory)).string();

	// Default class
	ph->sourceFiles.insert(std::make_pair(binaryName, ProjectClassDef{}));

	projectStartup(ph);

	generateInitFile(fullPath / "source");
	for (const auto& classEntry : ph->sourceFiles)
	{
		generateSourceFile(fullPath / "source", classEntry.first);
		generateHeaderFile(fullPath / "source", classEntry.first);
	}
}

bool ProjectGenerator::loadProject(const std::string &path)
{
	ProjectHeader* ph = uiData.currentProj;
	ph->shouldClose = true;

	std::ifstream fileIn(path);
	if (!fileIn.is_open()) {
		Log::error("Failed to open project file: ", path);
		return false;
	}

	nlohmann::json jsonHeader;
	fileIn >> jsonHeader;
	fileIn.close();

	ph->projectName = jsonHeader["Project name"];
	ph->description = jsonHeader["Description"];
	ph->binaryName = jsonHeader["Binary name"];
	ph->createdOn = jsonHeader["Created on"];

	// Update directory
	ph->directory = fs::path(path).parent_path();
	ph->canonicalDir = fs::canonical(fs::path(ph->directory)).string();

	ph->sourceFiles.clear();
	for (auto& sourceFile : jsonHeader["Source files"]) {
		ProjectClassDef pcd = {
			.relativeDir = sourceFile["directory"]
		};

		ph->sourceFiles.insert(std::make_pair(sourceFile["name"], pcd));
	}

	projectStartup(ph);

	return true;
}

void ProjectGenerator::projectStartup(ProjectHeader* ph)
{
	setupDirectoryTree(ph->directory);

	// Create or update files
	initCmakeFiles(ph->directory, ph->binaryName);
	createJsonFile(ph->directory, ph->projectName);
	ph->loaded = true;

	// Create symbolic link
	if (fs::exists("./Assets") || fs::is_symlink("./Assets")) {
		fs::remove("./Assets");
	}

	if (!fs::exists(ph->directory / "Assets")) {
		Log::error("Failed to generate project directories");
		return;
	}

	fs::create_symlink(ph->directory / "Assets", "./Assets");
}

void ProjectGenerator::createJsonFile(const std::filesystem::path& path, const std::string& name)
{
	ProjectHeader* ph = uiData.currentProj;

	nlohmann::json jsonHeader = {
		{"Project name", ph->projectName},
		{"Description", ph->description},
		{"Binary name", ph->binaryName},
		{"Created on", ph->createdOn}
	};

	int index = 0;
	for (const auto& classEntry : ph->sourceFiles) {
		jsonHeader["Source files"][index]["name"] = classEntry.first;
		jsonHeader["Source files"][index]["directory"] = classEntry.second.relativeDir;
		index++;
	}

	fs::path projFile = path / (name + ".quasar");
	std::ofstream fileOut(projFile, std::ios::trunc);
	
	if (!fileOut.is_open()) {
		Log::error("Failed to open ", projFile, " for writing");
		return;
	}

	fileOut << jsonHeader;
	fileOut.close();
}

void ProjectGenerator::generateInitFile(const std::filesystem::path& path)
{
	ProjectHeader* ph = uiData.currentProj;

	std::string includes = "";
	for (const auto& classEntry : ph->sourceFiles) {
		includes += "#include \"" + classEntry.first + ".hpp\"\n";
	}

	std::string classDefinitionList = "";
	int count = 0;
	for (const auto& classEntry : ph->sourceFiles) {
		classDefinitionList += classEntry.first + " classVar" + std::to_string(count++) + ";\n";
	}

	std::string classInitCalls = "";
	for (int i = 0; i < count; ++i) {
		classInitCalls += "\t\tclassVar" + std::to_string(i) + ".init(appContext);\n";
	}

	size_t minStrSize = sizeof(CPP_INIT_TEMPLATE) + includes.size() + classDefinitionList.size() + 
		classInitCalls.size() + 128;
	char* initFileStr = new char[minStrSize];
	snprintf(initFileStr, minStrSize, CPP_INIT_TEMPLATE, includes.c_str(), classDefinitionList.c_str(), 
			classInitCalls.c_str());

	writeFile(path / "InitializeProject.cpp", initFileStr);
}

void ProjectGenerator::initCmakeFiles(const std::filesystem::path& path, const std::string& name)
{
	fs::create_directory(fs::path(path / "build/"));
	
	// Get quasar binary dir
	// Assumes quasar is located within build and the source files are present
	fs::path workingDir = fs::current_path().parent_path();

	// Validate path contains files in correct relative locations
	fs::path quasarBinPath = workingDir;
#ifdef __linux__
	quasarBinPath += fs::path("/build/libQuasar.so");
#elif _WIN32
	quasarBinPath += fs::path("\\build\\Quasar.dll");
#endif
	if (!fs::exists(quasarBinPath)) {
		Log::error("Failed to generate CMakeLists.txt, Couldn't locate quasar binary at ", quasarBinPath);
		return;
	}

	fs::path quasarSourceFiles = workingDir;
#ifdef __linux__
	quasarSourceFiles += fs::path("/Engine/");
#elif _WIN32
	quasarSourceFiles += fs::path("\\Engine\\");
#endif
	if (!fs::exists(quasarSourceFiles)) {
		Log::error("Failed to generate CMakeLists.txt, Couldn't source files at ", quasarSourceFiles);
		return;
	}

	std::string quasarDir = workingDir.string();
	// Cmake expects unix style paths
#ifdef _WIN32
	std::replace(quasarDir.begin(), quasarDir.end(), '\\', '/');
#endif

	size_t minStrSize = sizeof(CMAKE_TEMPLATE) + name.size() + quasarDir.size() + 128;
	char* initFileStr = new char[minStrSize];
	snprintf(initFileStr, minStrSize, CMAKE_TEMPLATE, name.c_str(), quasarDir.c_str());

	fs::path projFile = path / "CMakeLists.txt";

	writeFile(projFile, initFileStr);
}

void ProjectGenerator::generateSourceFile(const std::filesystem::path& path, const std::string& name, 
			const std::string& relativeDir)
{
	size_t minStrSize = sizeof(SOURCE_FILE_TEMPLATE) + name.size() * 2.0f + 128;
	char* initFileStr = new char[minStrSize];
	snprintf(initFileStr, minStrSize, SOURCE_FILE_TEMPLATE, name.c_str(), name.c_str());

	std::string projFile = relativeDir + name + ".cpp";
	writeFile(path / projFile, initFileStr);
}

void ProjectGenerator::generateHeaderFile(const std::filesystem::path& path, const std::string& name, 
			const std::string& relativeDir)
{
	std::string guards = name + "_H";
	std::transform(guards.begin(), guards.end(), guards.begin(), ::toupper);

	size_t minStrSize = sizeof(HEADER_FILE_TEMPLATE) + 2 * guards.size() + name.size() + 128;
	char* initFileStr = new char[minStrSize];
	snprintf(initFileStr, minStrSize, HEADER_FILE_TEMPLATE, guards.c_str(), guards.c_str(), name.c_str());

	std::string projFile = relativeDir + name + ".hpp";
	writeFile(path / projFile, initFileStr);
}

void ProjectGenerator::writeFile(const std::filesystem::path& file, char* data)
{
	std::ofstream fileOut(file, std::ios::trunc | std::ios::out);
	
	if (!fileOut.is_open()) {
		// Retrieve error message
		int errorNum = errno;
		const char* errorMessage = std::strerror(errorNum);

		Log::error("Failed to open ", file, " for writing.\n", errorMessage);
		return;
	}

	fileOut << data;
	fileOut.close();
	delete [] data;
}

void ProjectGenerator::setupDirectoryTree(const std::filesystem::path& dir)
{
	for (const fs::path& newDir : directoryTree) {
		if (!fs::exists(dir / newDir)) {
			fs::create_directory(dir / newDir);

			if (!fs::exists(dir / newDir)) {
				Log::error("Failed to create directory: ", dir / newDir);
			}
		}
	}
}
