#include "ProjectManagement.hpp"
#include "IconFontCppHeaders/IconsFontAwesome6.h"
#include "imgui.h"
#include "Editor/EditorSettings.hpp"
#include "Editor/Elements.hpp"
#include <filesystem>
#include "Platform/FileDialog.hpp"
#include "ProjectGenerator.hpp"
#include "imgui_internal.h"

nlohmann::json ProjectManagement::projects;

static char directoryInput[4096];
static char nameInput[4096] = "New Project";
static bool createFolderCheck = true;

namespace fs = std::filesystem;

static const float padding = 8.0f;
static const float winWidth = 500.0f;

void ProjectManagement::drawProjectDialog()
{
	ImGui::SetNextWindowSize(ImVec2(winWidth, 0.0f));
	ImVec2 dispSize = ImGui::GetIO().DisplaySize;
	ImGui::SetNextWindowPos(ImVec2(dispSize.x / 2.0f, dispSize.y / 2.0f), ImGuiCond_Always, ImVec2(0.5f, 0.5f));

	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(32.0f, 32.0f));
	if (ImGui::BeginPopupModal("load/new projectDialog", NULL, ImGuiWindowFlags_NoDecoration)) {

		ImGui::PushStyleVar(ImGuiStyleVar_CellPadding, ImVec2(0.0f, 0.0f));
		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.0, 0.0f));

		ImGui::PushStyleColor(ImGuiCol_Button, 0x00000000);
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, uiColours.header);
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, uiColours.tabActive);
//		ImGui::PushFont(uiData.iconLarge);

		ImVec2 btnSize = ImVec2(96.0f, 0.0f);
		float offset = ImGui::GetContentRegionAvail().x - 2.0f * btnSize.x;
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + offset);

		Editor::buttonExclusive(ICON_FA_FILE_CIRCLE_PLUS " New", nullptr, &uiData.projectDialogCreateMode, 1, 
				btnSize, uiColours.buttonHover, uiColours.textFG, true);
		ImGui::SameLine();
		Editor::buttonExclusive(ICON_FA_FOLDER_OPEN " Load", nullptr, &uiData.projectDialogCreateMode, 0,
				btnSize, uiColours.buttonHover, uiColours.textFG, true);

//		ImGui::PopFont();
		ImGui::PopStyleColor(3);
		ImGui::PopStyleVar(2);

		ImGui::Dummy(ImVec2(0.0f, padding));

		if (uiData.projectDialogCreateMode == 0) {
			drawLoadProject();
		} else {
			drawGenerateProject();
		}
		ImGui::EndPopup();
	}
	ImGui::PopStyleVar();
}

void ProjectManagement::drawGenerateProject()
{
	ImGui::PushStyleColor(ImGuiCol_Border, uiColours.red);
	ImGui::Text("Name");
	ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);
	ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(padding, padding));
	ImGui::InputText("##project name input", nameInput, sizeof(nameInput));
	ImGui::PopStyleVar();

	ImGui::Checkbox("Create folder", &createFolderCheck);
	ImGui::Dummy(ImVec2(0.0f, padding));

	bool exists = fs::exists(fs::path(directoryInput));
	if (!exists)
		ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 1.0f);

	ImGui::Text("Directory");
	ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x - 
			ImGui::CalcTextSize(ICON_FA_FOLDER).x - 2.0f * padding);
	ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(padding, padding));
	ImGui::InputText("##project directory input", directoryInput, sizeof(directoryInput));
	ImGui::PopStyleVar();

	if (!exists)
		ImGui::PopStyleVar();

	// Directory button aligned to right of input text
	ImGui::SameLine();
	ImGui::SetCursorPosY(ImGui::GetCursorPosY() + padding / 2.0f);
	ImGui::PushStyleColor(ImGuiCol_Button, uiColours.frameBG);
	if (ImGui::Button(ICON_FA_FOLDER)) {
		std::string dir = SystemDialog::getDirectory();
		if (!dir.empty()) {
			snprintf(directoryInput, sizeof(directoryInput), "%s/", dir.c_str());
		}
	}
	ImGui::PopStyleColor();

	ImGui::PushStyleColor(ImGuiCol_Text, uiColours.red);
	if (!exists)
		ImGui::Text("Invalid directory");

	ImGui::PopStyleColor(2);
	ImGui::Dummy(ImVec2(0.0f, padding));

	// Size and center buttons
	float btnWidth = 96.0f;
	float btnOffset = ImGui::GetContentRegionAvail().x - 2.0f * btnWidth - padding;
	ImGui::SetCursorPosX(ImGui::GetCursorPosX() + btnOffset);

	ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0.0f, padding));
	if (ImGui::Button("Cancel", ImVec2(btnWidth, 0.0f)))
		uiData.projectDialogVisible = false;
	ImGui::SameLine();
	ImGui::BeginDisabled(!exists);
	if (ImGui::Button("Create", ImVec2(btnWidth, 0.0f))) {
		uiData.projectDialogVisible = false;
		ProjectGenerator::generateProject(directoryInput, nameInput, createFolderCheck);
		writeRecentProjects(true);
	}
	ImGui::EndDisabled();
	ImGui::PopStyleVar();
}

void ProjectManagement::drawLoadProject()
{
	ImGui::PushFont(uiData.textLarge);
	ImGui::Text("Recent Projects");
	ImGui::PopFont();

	ImGui::Dummy(ImVec2(0.0f, padding / 2.0f));

	ImGui::PushStyleColor(ImGuiCol_ChildBg, 0xff101010);
	ImGui::BeginChild("Recent projects child", ImVec2(0.0f, 300.0f));
	ImGui::Indent(padding / 2.0f);
	drawRecentProjects();
	ImGui::Unindent();
	ImGui::EndChild();
	ImGui::PopStyleColor();

	ImGui::Dummy(ImVec2(0.0f, padding));

	float btnWidth = 96.0f;
	ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0.0f, padding));
	float btnOffset = ImGui::GetContentRegionAvail().x - btnWidth;
	if (ImGui::Button("Import", ImVec2(btnWidth, 0.0f))) {
		std::string projDir = SystemDialog::getFile("*.quasar", false);
		if (!projDir.empty()) {
			if (ProjectGenerator::loadProject(projDir)) {
				uiData.projectDialogVisible = false;
				writeRecentProjects(true);
			}
		}
	}
	ImGui::SameLine(ImGui::GetCursorPosX() + btnOffset);

	if (ImGui::Button("Cancel", ImVec2(btnWidth, 0.0f))) {
		uiData.projectDialogVisible = false;
	}
	ImGui::PopStyleVar();
}

void ProjectManagement::getRecentProjects()
{
	if (projects.empty()) {
		std::ifstream fileIn("Recent_Projects.json");
		if (!fileIn.is_open()) {
			return;
		}

		fileIn >> projects;
		fileIn.close();
	}
}

void ProjectManagement::drawRecentProjects()
{
	if (projects.contains("recent")) {
		for (const auto& proj : projects["recent"]) {
			drawProjectBtn(proj["name"], proj["directory"]);
		}
	}
}

void ProjectManagement::drawProjectBtn(const std::string& name, const std::filesystem::path& directory)
{	
	// Remove entries that no longer exist
	if (!fs::exists(fs::path(directory))) {
		projects["recent"].erase(projects["recent"].find(directory));
		writeRecentProjects(false);
		return;
	}

	ImGui::Dummy(ImVec2(0.0f, padding / 2.0f));
	ImGui::PushFont(uiData.textLarge);
	float yOffset = ImGui::CalcTextSize("/").y;
	bool pressed = ImGui::Selectable(name.c_str(), false, ImGuiSelectableFlags_AllowItemOverlap, ImVec2(0.0f, 2.0f * yOffset));
	ImGui::PopFont();

	// Move dir inside selectable box
	ImGui::SetCursorPosY(ImGui::GetCursorPosY() - yOffset);

	// Convert to absolute path for readability
	ImGui::PushStyleColor(ImGuiCol_Text, uiColours.textGrey);
	ImGui::Text("%s", fs::canonical(fs::path(directory)).string().c_str());
	ImGui::PopStyleColor();

	if (pressed && ProjectGenerator::loadProject(directory / (name + ".quasar"))) {
		uiData.projectDialogVisible = false;
	}
}


void ProjectManagement::writeRecentProjects(bool addCurrent)
{
	ProjectHeader* ph = uiData.currentProj;
	if (addCurrent && fs::exists(ph->directory)) {
		// Check for duplicate entries
		bool found = false;
		for (const auto& entry : projects["recent"]) {
			if (!fs::exists(entry["directory"])) {
				continue;
			}

			if (fs::equivalent(entry["directory"], ph->directory)) {
				found = true;
				break;
			}
		}

		if (!found) {
			projects["recent"].push_back({{"directory", ph->directory}, {"name", ph->projectName}});
		}
	}

	std::ofstream fileOut("Recent_Projects.json", std::ios::trunc);
	if (fileOut.is_open()) {
		fileOut << projects;
	} else {
		Log::error("Failed to open Recent_Projects.json for writing");
	}
	fileOut.close();
}
