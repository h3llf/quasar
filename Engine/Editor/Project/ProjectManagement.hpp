#ifndef QUASAR_PROJECT_MANAGEMENT_H
#define QUASAR_PROJECT_MANAGEMENT_H

#include <string>
#include <filesystem>
#include "json.hpp"

class ProjectManagement
{
public:
	// Opens a dialog to load or create a new project
	static void drawProjectDialog();
	static bool displayLoadTab;

	// Reads a list of recent project names & directories from Recent_Projects.json
	static void getRecentProjects();
private:
	static void drawLoadProject();
	static void drawGenerateProject();

	static void drawRecentProjects();
	static void drawProjectBtn(const std::string& name, const std::filesystem::path& directory);

	static void writeRecentProjects(bool addCurrent);

	static nlohmann::json projects;
};

#endif //QUASAR_PROJECT_MANAGEMENT_H
