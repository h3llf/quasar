#ifndef PROJECT_BUILD_H
#define PROJECT_BUILD_H

class ProjectBuild
{
public:
	static void runBuildCommands();
	static void openProject();

private:
	// TODO: Move to platform/ ?
	static void cmakeCommandsLinux();
	static void openProjectLinux();
};

#endif //PROJECT_BUILD_H
