#include "EditorGizmos.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/ECS/Types.hpp"
#include "Elements.hpp"
#include "EditorSettings.hpp"
#include "ImGuizmo.h"
#include "Utils/Math/MatrixOps.hpp"
#include "Utils/Math/Quaternion.hpp"
#include "Utils/Math/Transform.hpp"
#include "imgui.h"

using namespace quasar;

// TODO: Move out of Editor namespace
bool EditorGizmos::drawGuizmos()
{
	ECS* ecs = uiData.ecs;

	drawGuizmoOptions();

	if (!ecs->exists(uiData.editorCamera) || !ecs->has<CCamera>(uiData.editorCamera)) {
		return false;
	}

	ImGuiIO& io = ImGui::GetIO();
	ImGuizmo::SetRect(0.0f, 0.0f, io.DisplaySize.x, io.DisplaySize.y);

	CCamera& cCam = ecs->get<CCamera>(uiData.editorCamera);
	mat4 proj = cCam.proj;
	mat4 view = cCam.view;
	proj[1][1] *= -1.0f; // Flip Y-axis

//	mat4 ident = mat4::identity();
//	ImGuizmo::DrawGrid(view.dataArray, proj.dataArray, ident.dataArray, 10.0f);

	if (!ecs->exists(uiData.selectedEntity) ||
		!ecs->has<CTransform>(uiData.selectedEntity) ||
		!uiData.opNum) 
	{
		return false;
	}

	CTransform& tOut = ecs->get<CTransform>(uiData.selectedEntity);

	mat4 matrixBuffer = tOut.ctm;
	vec3 position = tOut.ctm[3].xyz;
	vec3 degrees;
	vec3 scale;

	// Check that the gizmo doesnt end up behind the camera
	// ImGuizmo culling doesn't seem to work
	// https://github.com/CedricGuillemet/ImGuizmo/pull/212
	vec4 camSpacePos = (proj * view) * vec4(position, 1.0f);
	if (camSpacePos.w < 0.0f) {
		return false;
	}

	bool isUsing = ImGuizmo::Manipulate(view.dataArray, proj.dataArray, uiData.currentGizmoOp, 
			uiData.currentGizmoMode, matrixBuffer.dataArray, nullptr);

	isUsing |= ImGuizmo::IsUsing();

	if (!ImGuizmo::IsOver(ImGuizmo::TRANSLATE)) {
	} else {
	}

	// Undo hierarchy transformations
	Entity parent = ecs->getParent(uiData.selectedEntity);
	if (parent && ecs->has<CTransform>(parent)) {
		CTransform& pT = ecs->get<CTransform>(parent);
		mat4 inverse = qml::invert(pT.ctm);
		matrixBuffer = inverse * matrixBuffer;
	}

	ImGuizmo::DecomposeMatrixToComponents(matrixBuffer.dataArray, position.data, degrees.data, scale.data);

	// TODO: Individual axis flags
	switch (uiData.currentGizmoOp) {
	case ImGuizmo::OPERATION::TRANSLATE:
		tOut.position = position;
		break;
	case ImGuizmo::OPERATION::ROTATE:
		tOut.rotation = qml::eulerToQuat(qml::degToRad(degrees));
		break;
	case ImGuizmo::OPERATION::SCALE:
		tOut.scale = scale;
		break;
	default:
		break;
	}

	return isUsing;
}

void EditorGizmos::drawGuizmoOptions()
{
	float iSize = uiData.iconTxtSize + uiData.btnPadding * 2;
	float winPadding = ImGui::GetStyle().WindowPadding.x;

	ImVec2 winSize = {0.0f, 0.0f};
	ImVec2 winPos = {uiData.viewportExtent.x - iSize - winPadding * 2.0f + uiData.viewportPos.x, 
		uiData.viewportPos.y - winPadding};
	ImVec2 bSize = {iSize, iSize};

	ImGuiWindowFlags winFlags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoBackground | 
		ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | 
		ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoDocking;

	// Transform operation setting window
	ImGui::SetNextWindowSize(winSize);
	ImGui::SetNextWindowPos(winPos);
	ImGui::Begin("Translate mode", NULL, winFlags);

	ImGui::PushFont(uiData.iconLarge);
	ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 4.0f);

	// Mode switch buttons, only 1 can be active at once
	Editor::buttonExclusive(ICON_FA_ARROW_POINTER, "Select mode", &uiData.opNum, 0, bSize,
		uiColours.button, uiColours.bgTrans);
	Editor::buttonExclusive(ICON_FA_ARROWS_UP_DOWN_LEFT_RIGHT, "Move", &uiData.opNum, 1, bSize,
		uiColours.button, uiColours.bgTrans);
	Editor::buttonExclusive(ICON_FA_ROTATE, "Rotate", &uiData.opNum, 2, bSize,
		uiColours.button, uiColours.bgTrans);
	Editor::buttonExclusive(ICON_FA_COMPRESS, "Scale", &uiData.opNum, 3, bSize,
		uiColours.button, uiColours.bgTrans);
	Editor::buttonExclusive(ICON_FA_VECTOR_SQUARE, "Transform", &uiData.opNum, 4, bSize,
		uiColours.button, uiColours.bgTrans);

	switch(uiData.opNum) {
	case 1:
		uiData.currentGizmoOp = ImGuizmo::TRANSLATE;
		break;
	case 2:
		uiData.currentGizmoOp = ImGuizmo::ROTATE;
		break;
	case 3:
		uiData.currentGizmoOp = ImGuizmo::SCALE;
		break;
	case 4:
		uiData.currentGizmoOp = ImGuizmo::UNIVERSAL;
		break;
	}

	ImGui::PopFont();
	ImGui::PopStyleVar(1);
	ImGui::End();

	// Pivot and mode (orientation) setting window
	const float winWidth = 96;
	ImGui::SetNextWindowSize(ImVec2(winWidth + winPadding * 2.0f, 64.0f));
	ImGui::SetNextWindowPos(ImVec2(uiData.barRightExtent + 16.0f, uiData.viewportPos.y));
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
	ImGui::Begin("Gizmo options", NULL, winFlags);

	ImGui::PushStyleColor(ImGuiCol_FrameBg, uiColours.bgTrans);

	const char* modeItems[] = {"Global", "Local"};
	ImGui::SameLine();
	ImGui::SetNextItemWidth(winWidth);
	ImGui::Combo("##Transform Orientation", &uiData.orientationNum, modeItems, 2);
	if (ImGui::IsItemHovered() && !ImGui::IsItemActive()) {
		ImGui::SetTooltip("Transform orientation");
	}

	switch (uiData.orientationNum) {
	case 0:
		uiData.currentGizmoMode = ImGuizmo::WORLD;
		break;
	case 1:
		uiData.currentGizmoMode = ImGuizmo::LOCAL;
		break;
	}

	ImGui::PopStyleColor();
	ImGui::PopStyleVar();
	ImGui::End();
}
