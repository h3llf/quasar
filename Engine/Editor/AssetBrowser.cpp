#include "AssetBrowser.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "EditorSettings.hpp"
#include "Elements.hpp"
#include "ImageCache.hpp"
#include "GFX/Drawable/MaterialManager.hpp"
#include "IconFontCppHeaders/IconsFontAwesome6.h"
#include "backends/imgui_impl_vulkan.h"
#include "imgui.h"
#include "imgui_internal.h"
#include <cmath>
#include <cstring>
#include <string>
#include <filesystem>
#include "Platform/FileDialog.hpp"
#include "ThumbnailGen.hpp"

using namespace quasar;

std::unordered_map<std::string, bool> AssetBrowser::fileTypeFilters;

int AssetBrowser::filterSelection = 0;
int AssetBrowser::nrColumns = 12.0f;
char AssetBrowser::searchTxt[4096];
bool AssetBrowser::showFilters = false;

AssetBrowser::AssetPayload AssetBrowser::assetPayloadBuffer;

void AssetBrowser::drawAssetBrowser()
{
	ImGui::Begin("Asset Browser");

	// Show/Hide filters
	ImGui::PushStyleColor(ImGuiCol_Button, 0x000000);
	Editor::toggleButtonIcon(ICON_FA_FILTER ICON_FA_CARET_UP, ICON_FA_FILTER ICON_FA_CARET_DOWN, 
			"Filter file types", &showFilters);
	ImGui::PopStyleColor();

	// Search bar
	ImGui::SameLine();
	ImGui::SetNextItemWidth(256);
	ImGui::InputTextWithHint("##Search", ICON_FA_MAGNIFYING_GLASS " Search", searchTxt, 4096);

	ImGui::SameLine();
	ImGui::SeparatorEx(ImGuiSeparatorFlags_Vertical);

	// Change icon column count
	ImGui::SameLine();
	ImGui::SetNextItemWidth(128.0f);
	ImGui::SliderInt("##Asset icon size", &nrColumns, 1, 64, "%i columns");

	// Import button aligned to right
	const char* importBtnTxt = "  " ICON_FA_FOLDER_PLUS " Import  ";
	float btnPos = ImGui::GetContentRegionAvail().x - ImGui::CalcTextSize(importBtnTxt).x;
	ImGui::SameLine(btnPos);
	if (ImGui::Button(importBtnTxt)) {
		importAssets();
	}

	// Set filter values
	bool first = true;
	if (showFilters) {
		for (auto& ffilter : fileTypeFilters) {
			if (!first) {
				ImGui::SameLine();
			} else {
				first = false;
			}
			ImGui::Checkbox(ffilter.first.c_str(), &ffilter.second);
		}
	}
	
	ImGui::Separator();


	ImGui::BeginChild("Image browser", ImGui::GetContentRegionAvail());
	ImGui::Dummy(ImVec2(0.0f, uiData.winPadding));
	ImGui::Indent(uiData.winPadding);
	drawAssetIcons();
	ImGui::EndChild();

	ImGui::End();
}

void AssetBrowser::drawAssetIcons()
{
	// Get asset list
	std::vector<std::pair<std::string, ImageHandle>> images;
	std::vector<std::pair<std::string, int>> models;
	std::vector<std::pair<std::string, FontHandle>> fonts;
	std::vector<std::string> audioFiles;
	uiData.appCtx.assetMgr->getImageList(&images);
	uiData.appCtx.assetMgr->getModelList(&models);
	uiData.appCtx.assetMgr->getFontList(&fonts);
	uiData.appCtx.assetMgr->getAudioList(&audioFiles);

	bool firstEntry = true;
	float iconTextHeight = uiData.normalTxtSize * 3.0f + 4.0f;

	if (ImGui::BeginTable("Asset browser table", nrColumns)) {
		for (std::pair<std::string, FontHandle>& font: fonts) {
			contentBox(font.first, uiData.iconImages[2].image->rendererImageID, 
					iconTextHeight, "ASSET_BROWSER_FONT_DRAG");
		}

		for (std::string& filename: audioFiles) {
			contentBox(filename, uiData.iconImages[3].image->rendererImageID,
					iconTextHeight, "ASSET_BROWSER_AUDIO_DRAG");
		}

		for (std::pair<std::string, int>& model : models) {
			contentBox(model.first, ThumbnailGen::getImage(model.first), iconTextHeight, 
					"ASSET_BROWSER_MODEL_DRAG");
		}

		for (std::pair<std::string, ImageHandle>& img: images) {
			contentBox(img.first, img.second, iconTextHeight, "ASSET_BROWSER_IMAGE_DRAG");
		}

		ImGui::EndTable();
	}
}

void AssetBrowser::contentBox(const std::string& path, ImageHandle imageID, float height, const char* payloadName)
{
	if (shouldFilterType(path) || imageID < 0) {
		return;
	}

	if (searchTxt[0] != '\0' && (path.find(searchTxt) == std::string::npos)) {
		return;
	}

	ImGui::TableNextColumn();

	std::string name = path.substr(path.find_last_of("/") + 1);
	float width = ImGui::GetContentRegionAvail().x - 12.0f;

	ImGui::PushStyleColor(ImGuiCol_ChildBg, uiColours.lighterBG);
	ImGui::PushStyleColor(ImGuiCol_Border, uiColours.frameBG);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));

	ImGui::BeginChild(path.c_str(), ImVec2(width, width + height), true,
			ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoScrollbar);

	ImGui::PushStyleColor(ImGuiCol_ChildBg, uiColours.frameBG);
	ImGui::BeginChild((path + " background").c_str(), ImVec2(width, width));

	ImTextureID preview;
	if (ImageCache::getImage(imageID, &preview)) {
		ImGui::Image(preview, ImVec2(width, width));
	} else {
		ImGui::Dummy(ImVec2(width, height));
	}

	if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_SourceAllowNullID)) {
		assetPayloadBuffer = {path};

		ImGui::SetDragDropPayload(payloadName, &assetPayloadBuffer, sizeof(AssetPayload));
		ImGui::SetTooltip("%s", name.c_str());
		ImGui::EndDragDropSource();
	}

	ImGui::EndChild();
	ImGui::PopStyleColor();

	ImGui::SetNextItemWidth(width);

	ImGui::PushTextWrapPos(ImGui::GetCursorPos().x + width - 4.0f);
	ImGui::Indent(4.0f);
	ImGui::Text("%s", name.c_str());
	ImGui::Unindent();
	ImGui::PopTextWrapPos();

	ImGui::EndChild();
	ImGui::Dummy(ImVec2(0.0f, 4.0f));
	ImGui::PopStyleVar();
	ImGui::PopStyleColor(2);
}

bool AssetBrowser::shouldFilterType(std::string name)
{
	// Get file extension
	std::string ftype = name.substr(name.find_last_of('.') + 1, name.size() - 1);

	// Add file type to filter list if it's not found
	fileTypeFilters.try_emplace(ftype, true);

	// Skip if unchecked
	return !fileTypeFilters[ftype];
}

void AssetBrowser::importAssets()
{
	// TODO: Move hard coded format lists
	std::vector<std::string> files = SystemDialog::getFileMulti("*.hdr *.exr *.png *.jpg *.gltf *.ttf *.wav *.ogg");

	size_t pos = std::string::npos;
	size_t start = 0;

	Log::out("Files: ", files.size());
	for (std::string& file : files)
	{
		// Make path relative to the symlinks (Some file pickers don't return the symlink path)
		std::string path = file;
		auto rel = std::filesystem::relative(path, "./EngineAssets");
		if (!rel.empty() && rel.string()[0] != '.') {
			path = "EngineAssets" / rel;
		}

		rel = std::filesystem::relative(path, "./Assets");
		if (!rel.empty() && rel.string()[0] != '.') {
			path = "Assets" / rel;
		}

		std::string extension = path.substr(path.find_last_of('.') + 1);

		// TODO: Add the ability to specify usage
		if (extension == "jpg" || extension == "png") {
			uiData.appCtx.assetMgr->addImageLoadTask(path, Image::ImageUsage::Albedo);
		} else if (extension == "hdr" || extension == "exr") {
			uiData.appCtx.assetMgr->addImageLoadTask(path, Image::ImageUsage::HDR);
		} else if (extension == "gltf") {
			std::string directory = path.substr(0, path.find_last_of('/') + 1);
			std::string fileName = path.substr(path.find_last_of('/') + 1);
			uiData.appCtx.assetMgr->addModelLoadTask(directory, fileName);
		} else if (extension == "ttf") {
			uiData.appCtx.assetMgr->addFontLoadTask(path);
		} else if (extension == "wav" || extension == "ogg") {
			uiData.appCtx.assetMgr->addAudioLoadTask(path);
		} else {
			Log::error("Failed to load asset file with unrecognized extension: ", extension);
		}
	}

}
