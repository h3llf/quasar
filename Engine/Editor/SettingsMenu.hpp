#ifndef QUASAR_SETTINGS_MENU_H
#define QUASAR_SETTINGS_MENU_H

#include "Core/ECS/ECS.hpp"

class SettingsMenu
{
public:
	static void drawSettingsMenu();

private:
	static void drawGraphicalSettings();
	static void drawProjectSettings();
	static void drawSceneList(ECS* ecs, const char* header);
	static void drawServerSettings();
	static void drawEditorSettings();
};

#endif //QUASAR_SETTINGS_MENU_H
