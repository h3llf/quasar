#include "ThumbnailGen.hpp"
#include "Client/Components/CRenderable.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include "Core/Components/CCamera.hpp"
#include "Core/Components/CModel.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/Components/CAssetPolling.hpp"
#include "Editor/EditorSettings.hpp"
#include <limits>

std::unordered_map<std::string, ImageHandle> ThumbnailGen::thumbnails;
std::unordered_map<std::string, quasar::AssetHandle> ThumbnailGen::assetHandles;

using namespace quasar;

// TODO: Hide raw thumbnail images from asset browser
// TODO: Hide camera and generator entities

void ThumbnailGen::init()
{
	uiData.appCtx.cECS->registerUpdateFunction(SThumbnail::onUpdate, UPDATE_EDITOR, 20);
	uiData.appCtx.cECS->registerListener<CAssetPolling>(SThumbnail::onAssetsLoad, LISTENER_ON_ASSET_LOAD);

	CCamera cCam {};
	cCam.camProps.fov = qml::radians(90.0f);
	cCam.camProps.exposure = 2.0f;
	cCam.skyboxName = "../Assets/qwantani_puresky_4k.hdr";
	cCam.offscreen = true;
	cCam.enabled = false;
	cCam.width = 512;
	cCam.height = 512;

	CTransform cT {};

	SThumbnail::cameraEntity = uiData.appCtx.cECS->makeEntity(cCam, cT);
	uiData.appCtx.cECS->renameEntity(SThumbnail::cameraEntity, "Thumbnail camera");
	uiData.appCtx.cECS->setParent(SThumbnail::cameraEntity, uiData.hiddenEntity);
}

ImageHandle ThumbnailGen::getImage(const std::string &modelPath)
{
	if (thumbnails.contains(modelPath)) {
		return thumbnails.at(modelPath);
	}

	// Wait for the model to load
	/*
	AssetLoadStatus modelStat = uiData.appCtx.assetMgr->queryModelStatus(modelPath);
	if (modelStat != ASSET_LOADED) {
		return -1;
	}*/

	if (!assetHandles.contains(modelPath)) {
		const std::string imgName = modelPath + "_thumbnail";
		assetHandles[modelPath] = uiData.appCtx.assetMgr->addBlankImageTask(imgName, 512, 512);
	}

	// Wait for the blank thumbnail to be created
	AssetHandle& handle = assetHandles[modelPath];
	uiData.appCtx.assetMgr->queryLoadStatus(handle);
	if (handle.loadStatus == ASSET_LOADED){
		thumbnails[modelPath] = uiData.appCtx.assetMgr->getImage(handle)->rendererImageID;
		generateThumbnail(modelPath);
		return thumbnails.at(modelPath);
	}

	return -1;
}

void ThumbnailGen::generateThumbnail(const std::string &modelPath)
{
	ECS* ecs = uiData.appCtx.cECS;

	// Should be guarnteed to exist
	Model* model = uiData.appCtx.assetMgr->getModel(modelPath);

	// Tempoarily take the image far away from the origin
	// TODO: Render mask system to avoid this
	CTransform cT = {vec3(-1000.0f, 1000.0f, -1000.0f)};
	Entity root = ecs->makeEntity(cT);
	uiData.appCtx.cECS->setParent(root, uiData.hiddenEntity);
	ecs->renameEntity(root, "Thumbnail generator");

	ECSUtil::createModelEntities(uiData.appCtx.assetMgr, uiData.appCtx.cECS, model->modelPath, 
			model->modelName, root);

	ICThumbnailGenerator icTG = {thumbnails.at(modelPath)};
	ecs->add(root, icTG);

	CAssetPolling& cAP = SAssetPolling::getCAssetPolling(ecs, root);
	HierarchyIterator it = ecs->hierarchyIterator(root);
	for (Entity entity : it) {
		if (!ecs->has<CMaterialPBR>(entity)) {
			continue;
		}

		CMaterialPBR& cMat = ecs->get<CMaterialPBR>(entity);
		if (!cMat.albedoImage.empty())
			cAP.addHandle(cMat.albedoHandle);
		if (!cMat.metalRoughImage.empty())
			cAP.addHandle(cMat.metalRoughHandle);
		if (!cMat.normalImage.empty())
			cAP.addHandle(cMat.normalHandle);
		if (!cMat.emissiveImage.empty())
			cAP.addHandle(cMat.emissiveHandle);
	}

	//ecs->add(root, cAP);

	// Nothing to load, trigger event immediately
	if (cAP.pollingList.empty()) {
		ecs->triggerListenerEvents(root, TypeInfo<CAssetPolling>::type, LISTENER_ON_ASSET_LOAD);
	}
}

bool checkLoading(const AssetHandle& image)
{
	return  image.loadStatus == quasar::ASSET_IN_PROGRESS || 
		image.loadStatus == quasar::ASSET_QUEUED;
}

// Find the bounding box of a model in world space after mesh transformations
// Used to properly frame the thumbnail
void getModelBounds(ECS* ecs, Entity root, vec3& min, vec3& max, vec3& center)
{
	max = vec3(std::numeric_limits<float>::lowest());
	min = vec3(std::numeric_limits<float>::max());

	HierarchyIterator it = ecs->hierarchyIterator(root);
	for (Entity entity : it) {
		if (!ecs->has<CTransform>(entity) || !ecs->has<CMesh>(entity)) {
			continue;
		}

		CTransform& cT = ecs->get<CTransform>(entity);
		CMesh& cM = ecs->get<CMesh>(entity);

		vec4 mmin = cT.ctm * vec4(cM.min, 1.0f);
		vec4 mmax = cT.ctm * vec4(cM.max, 1.0f);

		min = qml::min(mmin.xyz, min);
		max = qml::max(mmax.xyz, max);
	}

	vec3 diff = max - min;
	center = max - (diff / 2.0f);
	min -= center;
	max -= center;
}

void SThumbnail::onAssetsLoad(ECS *ecs, Entity entity)
{
	if (!ecs->has<ICThumbnailGenerator>(entity)) {
		return;
	}

	ICThumbnailGenerator& icTG = ecs->get<ICThumbnailGenerator>(entity);

	// Enable drawing for this thumbnail
	icTG.ready = true;
}

// Draws one thumbnail at a time
void SThumbnail::onUpdate(ECS* ecs, double delta)
{
	EntityView view = ecs->view<ICThumbnailGenerator, CModel>();

	for (auto[entity, icTG, cM] : view.components())
	{
		if (currentlyDrawing && icTG.currentlyDrawing) {
			uint64 frameCount = InstanceManager::getFrameCount();
			
			// Wait for the image to be created
			if ((frameCount - icTG.startingFrameCount) > 3) {
				// Disable the camera and unset the target
				CCamera& cC = ecs->get<CCamera>(cameraEntity);
				cC.enabled = false;
				cC.targetImage = WINDOW_IMAGE_HANDLE;
				// Remove the model
				ecs->destroyEntity(entity);
				// Unlock thumbnail updates
				currentlyDrawing = false;
			}

			return;
		}

		if (icTG.ready && !currentlyDrawing) {
			getModelBounds(ecs, entity, icTG.min, icTG.max, icTG.center);

			// Use a bounding sphere for now
			// TODO: Fit the camera frustum to the bounding box 
			float length = (icTG.max - icTG.min).magnitude();
			CCamera& cC = ecs->get<CCamera>(cameraEntity);
			cC.targetImage = icTG.target;
			cC.enabled = true;

			CTransform& cT = ecs->get<CTransform>(cameraEntity);
			vec3 lookDir = vec3(-0.5f, -0.5f, -1.0f).normalize();
			cT.position = icTG.center + vec3(length * 0.60) * -lookDir,
			cT.rotation = qml::getRotationBetween(cC.forward, lookDir);

			icTG.startingFrameCount = InstanceManager::getFrameCount();
			icTG.currentlyDrawing = true;
			currentlyDrawing = true;
			return;
		}
	}
}
