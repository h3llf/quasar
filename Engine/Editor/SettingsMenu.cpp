#include "SettingsMenu.hpp"
#include "Core/ECS/UtilECS.hpp"
#include "Core/Globals.hpp"
#include "Core/Components/CScene.hpp"
#include "Editor/Project/ProjectManagement.hpp"
#include "Platform/ApplicationLaunch.hpp"
#include "Platform/FileDialog.hpp"
#include "EditorSettings.hpp"
#include "Elements.hpp"
#include "GFX/Renderer.hpp"
#include "GFX/RenderSettings.hpp"
#include "imgui.h"
#include "imgui_internal.h"
#include <cstdio>

using namespace quasar;

int settingsTab;
void SettingsMenu::drawSettingsMenu()
{
	ImGui::Begin("Settings");
	
	ImGui::PushStyleVar(ImGuiStyleVar_CellPadding, ImVec2(0.0f, 0.0f));
	ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.0, 0.0f));
	if (ImGui::BeginTable("Settings tab columns", 3)) {
		ImGui::PushStyleColor(ImGuiCol_Button, 0x00000000);
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, uiColours.header);
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, uiColours.tabActive);
		ImGui::PushFont(uiData.textLarge);

		ImGui::TableNextColumn();
		ImVec2 btnSize = ImVec2(ImGui::GetContentRegionAvail().x, 0.0f);
		Editor::buttonExclusive("Graphics", "Renderer settings", &settingsTab, 0,
				btnSize, uiColours.buttonHover, uiColours.textFG, true);
		ImGui::TableNextColumn();
		Editor::buttonExclusive("Project", "Project & scene setup", &settingsTab, 1, 
				btnSize, uiColours.buttonHover, uiColours.textFG, true);
		ImGui::TableNextColumn();
		Editor::buttonExclusive("Editor", "Editor settings", &settingsTab, 2,
				btnSize, uiColours.buttonHover, uiColours.textFG, true);

		ImGui::PopFont();
		ImGui::PopStyleColor(3);
		ImGui::EndTable();
	}
	ImGui::PopStyleVar(2);

	ImGui::Dummy(ImVec2(0.0f, uiData.winPadding));

	ImGui::BeginChild("settings menu child");

	switch (settingsTab) {
	case 0:
		drawGraphicalSettings();
		break;
	case 1:
		drawProjectSettings();
		break;
	case 2:
		drawEditorSettings();
		break;
	}

	ImGui::EndChild();

	ImGui::End();
}

void SettingsMenu::drawGraphicalSettings()
{
	uint32 width = ImGui::GetContentRegionAvail().x;

	std::string& selectedDevStr = renderSettings.deviceNames[renderSettings.selectedDevice];

	// Display settings
	bool openDevice = Editor::collapsingHeader("Device");
	ImGui::PushStyleColor(ImGuiCol_Text, uiColours.textGrey);
	ImGui::SameLine();
	ImGui::Text(" (restart required)");
	ImGui::PopStyleColor();
	if (openDevice) {
		ImGui::SetNextItemWidth(width);
		if (ImGui::BeginCombo("##Devices", selectedDevStr.c_str())) {
			for (uint32 i = 0; i < renderSettings.deviceNames.size(); ++i) {
				bool isSelected = i == renderSettings.selectedDevice;
				ImGui::Selectable(renderSettings.deviceNames[i].c_str(), &isSelected);
				if (isSelected)
					renderSettings.selectedDevice = i;
			}
			ImGui::EndCombo();
		}
		ImGui::Dummy(ImVec2(0.0f, uiData.winPadding));
	}
	
	if (Editor::collapsingHeader("Display")) {
		char currentResStr[128];
		sprintf(currentResStr, "%dx%d", renderSettings.width.load(), renderSettings.height.load());

		ImGui::Text("Resolution scale");
		ImGui::SameLine(ImGui::GetContentRegionAvail().x - ImGui::CalcTextSize(currentResStr).x);
		ImGui::Text("%s", currentResStr);
		ImGui::SetNextItemWidth(width);
		Editor::sliderFloatAtomic("##Resolution Scale", &renderSettings.resolutionScale, 0.1f, 4.0f, "%.2fx");
		
		Editor::checkboxAtomic("Enable VSync", &renderSettings.vsync);
		ImGui::Dummy(ImVec2(0.0f, uiData.winPadding));
	}

	if (Editor::collapsingHeader("Ambient Occlusion")) {
		Editor::checkboxAtomic("Enable AO", &renderSettings.enableGTAO);
		ImGui::Text("AO resolution");
		ImGui::SetNextItemWidth(width);
		Editor::sliderFloatAtomic("##AO resolution Scale", &renderSettings.aoResolutionScale, 0.1f, 1.0f, "%.1fx");
		ImGui::Dummy(ImVec2(0.0f, uiData.winPadding));
	}

	if (Editor::collapsingHeader("Bloom")) {
		Editor::checkboxAtomic("Enable bloom", &renderSettings.enableBloom);
		ImGui::Text("Intensity");
		ImGui::SetNextItemWidth(width);
		Editor::sliderFloatAtomic("##Bloom intensity", &renderSettings.bloomIntensity, 0.01f, 1.0f, "%.2f");
		ImGui::Dummy(ImVec2(0.0f, uiData.winPadding));
	}

	if (Editor::collapsingHeader("Shadows")) {
		ImGui::Text("Omnidirectional resolution");
		ImGui::SetNextItemWidth(width);
		Editor::sliderIntAtomic("##Omni shadow res", &renderSettings.omniShadowRes, 5, 12, "", true);

		ImGui::Text("Directional resolution");
		ImGui::SetNextItemWidth(width);
		Editor::sliderIntAtomic("##Cascaded shadow res", &renderSettings.cascadedShadowRes, 5, 12, "", true);

		ImGui::Text("Shadow cascades");
		ImGui::SetNextItemWidth(width);
		Editor::sliderIntAtomic("##Shadow cascades", &renderSettings.numberCascades, 1, 8);

		float cascadeDist = renderSettings.cascadeDist;
		ImGui::Text("Distance");
		ImGui::SameLine();
		ImGui::SetNextItemWidth(64.0f);
		ImGui::InputFloat("##Distance", &cascadeDist, 0.0f, 0.0f, "%.1f");
		renderSettings.cascadeDist = std::clamp(cascadeDist, 0.0f, 1000.0f);
		ImGui::SameLine();
		float lambda = renderSettings.lambda;
		ImGui::Text("Lambda");
		ImGui::SameLine();
		ImGui::SetNextItemWidth(64.0f);
		ImGui::InputFloat("##Lambda", &lambda);
		renderSettings.lambda= std::clamp(lambda, 0.0f, 1.0f);

		ImGui::Text("PCF samples");
		ImGui::SetNextItemWidth(width);
		Editor::sliderIntAtomic("##PCF samples", &renderSettings.numPCFSamples, 1, 64);
		ImGui::Dummy(ImVec2(0.0f, uiData.winPadding));
	}

	if (Editor::collapsingHeader("Debug")) {
		// Collider vis should be moved to editor settings as it doesnt change renderer state
		Editor::checkboxAtomic("Line depth test", &renderSettings.deugLineDepthTest);
		Editor::checkboxAtomic("wireframe", &renderSettings.wireframe);
#ifndef _DEBUG
		ImGui::BeginDisabled();
#endif //_DEBUUG
		Editor::checkboxAtomic("AO only", &renderSettings.aoOnlyView);
		Editor::checkboxAtomic("Lighting heatmap", &renderSettings.lightingHeatmap);
		Editor::checkboxAtomic("White world", &renderSettings.whiteWorld);
		Editor::checkboxAtomic("Show cascade splits", &renderSettings.showCascadeSplits);
#ifndef _DEBUG
		ImGui::EndDisabled();
#endif //_DEBUG
		ImGui::Dummy(ImVec2(0.0f, uiData.winPadding));
	}

	if (ImGui::Button("  Apply  ")) {
		Renderer::recreateSwapchain();
	}


}

void SettingsMenu::drawProjectSettings()
{
	/*
	ImGui::PushFont(uiData.textLarge);
	ImGui::PushStyleColor(ImGuiCol_Text, uiColours.textGrey);
	ImGui::Text("Project not loaded");
	ImGui::PopStyleColor();
	ImGui::PopFont();*/

	float btnWidth = ImGui::GetContentRegionAvail().x / 2.0f - uiData.framePadding;
		
	if (ImGui::Button("New project", ImVec2(btnWidth, 0.0f))) {
		uiData.projectDialogVisible = true;
		uiData.projectDialogCreateMode = 1;
	}

	ImGui::SameLine();
	if (ImGui::Button("Load project", ImVec2(btnWidth, 0.0f))) {
		uiData.projectDialogVisible = true;
		uiData.projectDialogCreateMode = 0;
	}

	if (uiData.currentProj && uiData.currentProj->loaded) {
		ImGui::Dummy(ImVec2(0.0f, uiData.winPadding));

		ImGui::PushFont(uiData.textLarge);
		ImGui::Text("%s", uiData.currentProj->projectName.c_str());
		ImGui::PopFont();

		float xOffset = ImGui::GetContentRegionAvail().x;
		xOffset -= ImGui::CalcTextSize(ICON_FA_FOLDER_OPEN).x + uiData.btnPadding;
		ImGui::SameLine(xOffset);
		bool openf = false;
		Editor::button(ICON_FA_FOLDER_OPEN, "Open project folder", &openf, ImVec2(0.0f, 0.0f), 0x00000000);
		if (openf) {
			LaunchApplication::openFolder(uiData.currentProj->directory);
		}

		ImGui::PushStyleColor(ImGuiCol_Text, uiColours.textGrey);
		ImGui::Text("%s", uiData.currentProj->canonicalDir.c_str());
		ImGui::PopStyleColor();
	}

	ImGui::Dummy(ImVec2(0.0f, uiData.winPadding));

	drawSceneList(uiData.appCtx.cECS, "Client Scenes");
	drawSceneList(uiData.appCtx.sECS, "Server Scenes");
}

void SettingsMenu::drawSceneList(ECS* ecs, const char* header)
{
	ImGui::PushFont(uiData.textLarge);
	ImGui::Text("%s", header);
	ImGui::PopFont();
	if (ImGui::BeginTable("projcet settings table", 2, ImGuiTableFlags_Resizable)) {
		ImGui::Indent();

		bool found = false;
		EntityView view = ecs->view<CSceneRoot>().begin();
		for (Entity entity: view) {
			found = true;
			ContextECS::EntityEntry& entry = ecs->context.entities[entity.index];
			ImGui::TableNextColumn();
			ImGui::Text("Scene root");
			ImGui::TableNextColumn();		
			ImGui::Text("%s", entry.name.c_str());
			
			CSceneRoot& cScene = ecs->get<CSceneRoot>(entity);
			ImGui::TableNextColumn();		
			ImGui::Text("File name");
			ImGui::TableNextColumn();

			// Select scene file
			ImGui::PushStyleColor(ImGuiCol_Button, uiColours.grey);
			if (ImGui::Button(ICON_FA_FOLDER)) {
				std::string filePath = SystemDialog::getFile("*.entity", true);
				ECSUtil::addFileExtension(filePath, ".entity");
				snprintf(cScene.sceneFileName, sizeof(cScene.sceneFileName), 
						"%s", filePath.c_str());

				ECSUtil::writeEntityDisk(ecs, filePath, entity, true);
			}
			ImGui::PopStyleColor();
			if (ImGui::IsItemHovered()) {
				ImGui::SetTooltip("Select save location");
			}
			ImGui::SameLine();

			if (cScene.sceneFileName[0] == '\0') {
				ImGui::Text("No path specified");
			} else {
				ImGui::Text("%s", cScene.sceneFileName);
			}
		}

		ImGui::Unindent();
		ImGui::EndTable();

		if (!found) {
			ImGui::PushStyleColor(ImGuiCol_Text, uiColours.textGrey);
			ImGui::Text("No CSceneRoot components present");
			ImGui::PopStyleColor();
		}
	}
}

void SettingsMenu::drawEditorSettings()
{
	float width = ImGui::GetContentRegionAvail().x;
	uiData.resetDocks |= ImGui::Button("Reset Window Layout", ImVec2(width, 0.0f));

	Editor::checkboxAtomic("Collider visualization", &QGlobals.colliderVisualization);
	ImGui::Checkbox("Menu bar shown", &uiData.menuBarVisible);
}

void SettingsMenu::drawServerSettings()
{
	ImGui::Text("Server settings");
}
