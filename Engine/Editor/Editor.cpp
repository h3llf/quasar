#include "Editor.hpp"
#include "AssetBrowser.hpp"
#include "Core/ApplicationContext.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "Editor/ImageCache.hpp"
#include "Editor/Project/ProjectGenerator.hpp"
#include "Editor/ThumbnailGen.hpp"
#include "Elements.hpp"
#include "EditorSettings.hpp"
#include "EntityHierarchy.hpp"
#include "ComponentEditor.hpp"
#include "GLFW/glfw3.h"
#include "Profiler.hpp"
#include "AssetBrowser.hpp"
#include "EditorGizmos.hpp"
#include "SettingsMenu.hpp"
#include "Picking.hpp"
#include "Project/ProjectManagement.hpp"
#include "Project/ProjectBuild.hpp"

#include "Core/Globals.hpp"
#include "Core/ECS/UtilECS.hpp"
#include "Core/Components/CScene.hpp"
#include "GFX/CommandBuffers.hpp"
#include "IconFontCppHeaders/IconsFontAwesome6.h"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_vulkan.h"
#include "ImGuizmo.h"
#include "imgui.h"
#include "imgui_internal.h"
#include "implot.h"

using namespace quasar;

Editor::EditorData uiData {};
//Editor::EditorColours uiColours {};

void Editor::init(GLFWwindow* window, Console& console)
{
	std::lock_guard<std::mutex> lck(uiData.editorSyncMutex);
	
	const int numPoolSize = 11;
	VkDescriptorPoolSize poolSizes[numPoolSize] = {
		{ VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
		// Work around for imgui vulkan impl running out of descriptors
		// TODO: Modify the implementation to use image arrays?
		{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 100000 },
		{ VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
		{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
		{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
		{ VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
	};

	VkDescriptorPoolCreateInfo poolInfo {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
	poolInfo.maxSets = 100000;
	poolInfo.poolSizeCount = numPoolSize;
	poolInfo.pPoolSizes = poolSizes;

	VK_CHECK(vkCreateDescriptorPool(GFX.device, &poolInfo, nullptr, &GFX.imguiPool),
			"Failed to create imgui descriptor pool");

	ImGui::CreateContext();
	ImPlot::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;

	ImVector<ImWchar> ranges;
	ImFontGlyphRangesBuilder builder;
	builder.AddRanges(io.Fonts->GetGlyphRangesDefault());
	builder.AddChar(0x3b1); // Alpha
	builder.AddChar(0x3b2); // Beta
	builder.AddChar(0x2022);
	builder.BuildRanges(&ranges);

	// TODO: Icon font glyph ranges

	static const ImWchar icons_ranges[] = { ICON_MIN_FA, ICON_MAX_16_FA, 0 };
	ImFontConfig icons_config; 
	icons_config.MergeMode = true;
	icons_config.GlyphOffset = ImVec2(0.0f, 1.0f);
	icons_config.PixelSnapH = false;
	icons_config.GlyphMinAdvanceX = uiData.iconTxtSize;
	icons_config.GlyphMaxAdvanceX = uiData.iconTxtSize;

	io.Fonts->AddFontFromFileTTF("../lib/fonts/DroidSans.ttf", uiData.normalTxtSize);
	io.Fonts->AddFontFromFileTTF("../lib/fonts/fa-solid-900.ttf", uiData.normalTxtSize,
			&icons_config, icons_ranges);
	io.Fonts->Build();
	uiData.textIconSmall = io.Fonts->Fonts[0];

	io.Fonts->AddFontFromFileTTF("../lib/fonts/DroidSans.ttf", uiData.headerTxtSize, 
			NULL, ranges.Data);
	io.Fonts->AddFontFromFileTTF("../lib/fonts/fa-regular-400.ttf", uiData.headerTxtSize,
				&icons_config, icons_ranges);	
	io.Fonts->Build();
	uiData.textLarge = io.Fonts->Fonts[1];

	icons_config.GlyphOffset = ImVec2(0.0f, 0.0f);
	icons_config.MergeMode = false;
	io.Fonts->AddFontFromFileTTF("../lib/fonts/fa-solid-900.ttf", uiData.iconTxtSize, 
			&icons_config, icons_ranges);
	io.Fonts->Build();
	uiData.iconLarge = io.Fonts->Fonts[2];

	ImGui_ImplGlfw_InitForVulkan(window, true);
	ImGui_ImplVulkan_InitInfo initInfo {};
	initInfo.Instance = GFX.instance;
	initInfo.PhysicalDevice = GFX.physicalDevice;
	initInfo.Device = GFX.device;
	initInfo.Queue = GFX.graphicsQueue;
	// TODO: Get image count on init and on resize
	// ImGui_ImplVulkan_SetMinImageCount(uint32_t min_image_count);
	initInfo.DescriptorPool = GFX.imguiPool;
	initInfo.MinImageCount = 4;
	initInfo.ImageCount = 4;
	initInfo.MSAASamples = VK_SAMPLE_COUNT_1_BIT;

	ImGui_ImplVulkan_Init(&initInfo, GFX.swapchainRenderPass);

	ImGuiStyle* styleP = &ImGui::GetStyle();
	styleP->FrameRounding = 10.0f;
	styleP->GrabRounding = 10.0f;
	styleP->WindowPadding = ImVec2{uiData.winPadding, uiData.winPadding};
	styleP->FramePadding = ImVec2(uiData.framePadding, uiData.framePadding);
	styleP->FrameBorderSize = 0.0f;
	styleP->TabRounding = 0.0f;
	styleP->WindowBorderSize = 0.0f;

	ImVec4* colours = styleP->Colors;
	colours[ImGuiCol_WindowBg] = ImGui::ColorConvertU32ToFloat4(uiColours.bg);
	colours[ImGuiCol_TitleBg] = ImGui::ColorConvertU32ToFloat4(uiColours.bg);
	colours[ImGuiCol_TitleBgActive] = ImGui::ColorConvertU32ToFloat4(uiColours.bg);
	colours[ImGuiCol_ChildBg] = ImGui::ColorConvertU32ToFloat4(uiColours.childBG);
	colours[ImGuiCol_Header] = ImGui::ColorConvertU32ToFloat4(uiColours.header);

	colours[ImGuiCol_Tab] = ImGui::ColorConvertU32ToFloat4(uiColours.tabInactive);
	colours[ImGuiCol_TabActive] = ImGui::ColorConvertU32ToFloat4(uiColours.tabActive);
	colours[ImGuiCol_TabUnfocused] = ImGui::ColorConvertU32ToFloat4(uiColours.tabInactive);
	colours[ImGuiCol_TabUnfocusedActive] = ImGui::ColorConvertU32ToFloat4(uiColours.tab);
	colours[ImGuiCol_TabHovered] = ImGui::ColorConvertU32ToFloat4(uiColours.buttonHover);

	colours[ImGuiCol_ResizeGrip] = ImGui::ColorConvertU32ToFloat4(uiColours.button);
	colours[ImGuiCol_ResizeGripHovered] = ImGui::ColorConvertU32ToFloat4(uiColours.buttonHover);
	colours[ImGuiCol_ResizeGripActive] = ImGui::ColorConvertU32ToFloat4(uiColours.buttonActive);

	colours[ImGuiCol_FrameBg] = ImGui::ColorConvertU32ToFloat4(uiColours.txtInput);
//	colours[ImGuiCol_Border] = ImGui::ColorConvertU32ToFloat4(uiColours.frameBG);
	colours[ImGuiCol_PlotLines] = ImGui::ColorConvertU32ToFloat4(uiColours.green);
	colours[ImGuiCol_PlotHistogram] = ImGui::ColorConvertU32ToFloat4(uiColours.green);

	colours[ImGuiCol_Text] = ImGui::ColorConvertU32ToFloat4(uiColours.textFG);

	colours[ImGuiCol_Button] = ImGui::ColorConvertU32ToFloat4(uiColours.grey);
	colours[ImGuiCol_ButtonHovered] = ImGui::ColorConvertU32ToFloat4(uiColours.buttonHover);
	colours[ImGuiCol_ButtonActive] = ImGui::ColorConvertU32ToFloat4(uiColours.buttonActive);

	colours[ImGuiCol_ModalWindowDimBg] = ImGui::ColorConvertU32ToFloat4(uiColours.modalBG);

	uiData.sampler = GFX.textureSampler;

	console.bindCVar("editor_reset_docks", &uiData.resetDocks, true);
}

void Editor::loadImageIcons()
{
	std::lock_guard<std::mutex> lck(uiData.editorSyncMutex);
	
	size_t count = uiData.iconImages.size();
	std::vector<AssetHandle> handles(count);

	// Load each image
	for (uint32 i = 0; i < count; ++i) {
		handles[i] = uiData.appCtx.assetMgr->addImageLoadTask(uiData.iconImages[i].fileName, 
				Image::ImageUsage::Albedo);
	}

	bool loading = true;
	while (loading) {
		loading = false;
		for (AssetHandle& asset: handles) {
			uiData.appCtx.assetMgr->queryLoadStatus(asset);
			if (asset.loadStatus == quasar::ASSET_QUEUED || 
					asset.loadStatus == quasar::ASSET_IN_PROGRESS)
			{
				loading = true;
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}

	for (uint32 i = 0; i < count; ++i) {
		uiData.iconImages[i].image = uiData.appCtx.assetMgr->getImage(handles[i]);
	}
}

void Editor::deinitECS()
{
	if (uiData.hiddenEntity) {
		uiData.appCtx.cECS->destroyEntity(uiData.hiddenEntity);
		uiData.hiddenEntity = Entity::null;
	}
}

void Editor::initECS(const quasar::ApplicationContext& appCtx)
{
	uiData.appCtx = appCtx;

	if (uiData.hiddenEntity) {
		appCtx.cECS->destroyEntity(uiData.hiddenEntity);
	}

	uiData.hiddenEntity = appCtx.cECS->makeEntity();

	CTransform eTransform {qml::vec3(0.0f, 5.0f, 0.0f)};
	CEditorCamController eController {};
	CCamera eCamera {};
	eCamera.skyboxName = "../Assets/qwantani_puresky_4k.hdr";
//	eCamera.skyboxName = "qwantani_puresky_4k.hdr";
	uiData.editorCamera = uiData.appCtx.cECS->makeEntity(eTransform, eController, eCamera);
	uiData.appCtx.cECS->renameEntity(uiData.editorCamera, "Editor camera");
	uiData.appCtx.cECS->setParent(uiData.editorCamera, uiData.hiddenEntity);

	ThumbnailGen::init();
}

void Editor::initProject(ProjectHeader* project)
{
	ProjectManagement::getRecentProjects();
	uiData.currentProj = project;
}

void Editor::buildDocks(ImGuiID dockID, ImGuiDockNodeFlags flags)
{
	ImGuiID id = dockID;
//	ImGui::DockBuilderRemoveNode(id);

	ImGuiIO io = ImGui::GetIO();
	id = ImGui::DockBuilderAddNode(id, ImGuiDockNodeFlags_DockSpace);
	ImGui::DockBuilderSetNodeSize(id, io.DisplaySize);
//	ImGui::DockBuilderSetNodePos(id, ImVec2(0.0f, 0.0f));

	ImGuiID dockViewport = id;
	ImGuiID dockHierarchy = ImGui::DockBuilderSplitNode(id, ImGuiDir_Left, 0.27f, nullptr, &id);
	ImGuiID dockEntEdit = ImGui::DockBuilderSplitNode(dockHierarchy, ImGuiDir_Down, 0.5f, nullptr, &dockHierarchy);
	ImGuiID dockAsset = ImGui::DockBuilderSplitNode(id, ImGuiDir_Down, 0.4f, nullptr, &id);
	ImGuiID dockSettings = ImGui::DockBuilderSplitNode(id, ImGuiDir_Right, 0.25f, nullptr, &id);

	ImGui::DockBuilderDockWindow("Viewport", dockViewport);
	ImGui::DockBuilderDockWindow("Hierarchy", dockHierarchy);
	ImGui::DockBuilderDockWindow("Entity Editor", dockEntEdit);
	ImGui::DockBuilderDockWindow("Asset Browser", dockAsset);
	ImGui::DockBuilderDockWindow("Profiler", dockAsset);
	ImGui::DockBuilderDockWindow("Settings", dockSettings);

	ImGui::DockBuilderFinish(id);
}

int  once = 0;
void Editor::dockSpace()
{
	ImGuiWindowFlags winFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse |
		ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoBringToFrontOnFocus |
		ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoNavFocus | ImGuiWindowFlags_NoResize;

	ImGuiDockNodeFlags dockFlags = ImGuiDockNodeFlags_None;

	float menuBarSize = (uiData.menuBarVisible) ? uiData.menuBarHeight : 0.0f;

	ImVec2 displaySize = ImGui::GetIO().DisplaySize;
	displaySize.y -= menuBarSize;

	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
	ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
	ImGui::SetNextWindowSize(displaySize);
	ImGui::SetNextWindowPos(ImVec2(0.0f, menuBarSize));
	ImGui::SetNextWindowBgAlpha(0.0f);
	ImGui::Begin("Dockspace", NULL, winFlags);

	// Cannot build dockspace on the first frame
	// TODO: Fix this
	static ImGuiID dockSpace = 0;
	dockSpace = ImGui::GetID("Base Dockspace");
	if (uiData.resetDocks && once > 2) {
		// Setup initial widow layout
		buildDocks(dockSpace, dockFlags);
		uiData.resetDocks = false;
	} else {
		once++;
	}

	ImGui::DockSpace(dockSpace, displaySize, dockFlags);

	ImGui::End();
	ImGui::PopStyleVar(2);
}

void Editor::initFonts()
{
	VkCommandBuffer cmdBuffer = CommandBuffers::beginSingleTimeCmd();
	ImGui_ImplVulkan_CreateFontsTexture(cmdBuffer);
	CommandBuffers::endSingleTimeCmd();

	ImGui_ImplVulkan_DestroyFontUploadObjects();
}

vec3 data = {1.0f, 2.0f, 3.0f};

void Editor::draw(GLFWwindow* window)
{
	std::lock_guard<std::mutex> lck(uiData.editorSyncMutex);
	uiData.ecs = (uiData.ecsSelection == EditorData::SELECTION_ECS_CLIENT) ? 
		uiData.appCtx.cECS : uiData.appCtx.sECS;

	// Prevent editor being drawn twice in a single frame
	if (uiData.editorDrawn)
		return;

	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
	dockSpace();
	ImGuizmo::BeginFrame();
	ImGuizmo::SetOrthographic(false);

	if (uiData.menuBarVisible) {
		drawMenuBar();
	}

	drawToolbar();

	if (uiData.profilerVisible)
		Profiler::drawProfiler();

	if (uiData.assetBrowserVisible)
		AssetBrowser::drawAssetBrowser();

	if (uiData.entityHierarchyVisible)
		EntityHierarchy::drawEntityHierarchy();

	if (uiData.settingsVisible)
		SettingsMenu::drawSettingsMenu();

	if (uiData.projectDialogVisible) {
		ImGui::OpenPopup("load/new projectDialog");
		ProjectManagement::drawProjectDialog();
	}

	{
		ImGui::SetNextWindowBgAlpha(0.0f);
		ImGuiWindowFlags winFlags = ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoBackground;

		// Keep viewport focused when mouse is captured
		if (!InputState::cursorVisible) {
			ImGui::SetNextWindowFocus();
		}

		ImGui::Begin("Viewport", NULL, winFlags);

		ImVec2 region = ImGui::GetWindowContentRegionMin();
		uiData.viewportExtent = ImGui::GetWindowSize();
		uiData.viewportExtent.x -= region.x;
		uiData.viewportExtent.y -= region.y;
		uiData.viewportPos = ImGui::GetWindowPos();
		uiData.viewportPos.y += region.y;
		uiData.viewportPos.x += region.x;

		ImGuizmo::SetDrawlist(ImGui::GetWindowDrawList());
		bool isNotUsing = !EditorGizmos::drawGuizmos();

		isNotUsing &= ImGui::IsWindowHovered() && !ImGui::IsAnyItemHovered() &&
			ImGui::IsMouseClicked(ImGuiMouseButton_Left);

		int width, height;
		glfwGetWindowSize(window, &width, &height);
		if (isNotUsing) {
			MousePick::checkIntersections(ImGui::GetMousePos(), width, height);
		}

		ImGui::End();
	}

	if (uiData.componentEditorVisible) {
		ComponentEditor::drawComponentEditor();
	}

	ImGui::Render();
	uiData.editorDrawn = true;
}

void Editor::getDrawCommands(VkCommandBuffer cmdBuffer)
{
	std::lock_guard<std::mutex> lck(uiData.editorSyncMutex);
	ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), cmdBuffer);
	uiData.editorDrawn = false;
}

void Editor::cleanUp()
{
	std::lock_guard<std::mutex> lck(uiData.editorSyncMutex);
	ImPlot::DestroyContext();
	ImGui_ImplVulkan_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
	vkDestroyDescriptorPool(GFX.device, GFX.imguiPool, nullptr);
}

void Editor::setViewportImages()
{
	//TODO: REMOVE
	return;

	// TODO: Reenable viewport
	Log::out("Setting viewport images");

	std::vector<VkImageView>& imgViews = GFX.mainShading.imgViews;

	uiData.viewportImages.resize(imgViews.size());

	Log::out("Img views: ", imgViews.size());

	for (uint32 i = 0; i < imgViews.size(); ++i) {
		uiData.viewportImages[i] = ImGui_ImplVulkan_AddTexture(uiData.sampler, imgViews[i], 
				VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
	}
}

void Editor::clearImageCache()
{
	std::lock_guard<std::mutex> lck(uiData.editorSyncMutex);
	ImageCache::resetCache();
	uiData.editorDrawn = false;
}

std::mutex* Editor::getEditorMutex()
{
	return &uiData.editorSyncMutex;
}

void Editor::drawToolbar()
{
	const float WINDOW_PADDING = 8.0f;

	float iSize = uiData.iconTxtSize + uiData.btnPadding * 2;
	ImVec2 bSize = ImVec2{iSize, iSize};
	int buttonCount = 5;
	float barWidth = (buttonCount + 2) * (bSize.x + 8.0f) + WINDOW_PADDING;
	float barHeight = bSize.y + WINDOW_PADDING;

	ImVec2 barSize = ImVec2{barWidth, barHeight};

	float xOffset = uiData.viewportExtent.x / 2.0f + uiData.viewportPos.x;
	uiData.barRightExtent = xOffset + barSize.x / 2.0f;
	xOffset -= barSize.x / 2;
	uiData.barLeftExtent = xOffset;

	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 4.0f);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(WINDOW_PADDING / 2.0f, WINDOW_PADDING / 2.0f));
	ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
	ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 4.0f);
	ImGui::PushStyleColor(ImGuiCol_WindowBg, uiColours.bgTrans);

	ImGuiWindowFlags winFlags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoNav |
		ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoCollapse;
	ImGui::SetNextWindowSize(barSize);
	ImGui::SetNextWindowPos(ImVec2{xOffset, uiData.viewportPos.y});
	ImGui::Begin("Main Toolbar", NULL, winFlags);

	ImGui::PushFont(uiData.iconLarge);

	toggleButton(ICON_FA_SITEMAP, "Toggle entity hierarchy", &uiData.entityHierarchyVisible, bSize,
			uiColours.button, 0x00000000);
	ImGui::SameLine();
	toggleButton(ICON_FA_WRENCH, "Toggle entity editor", &uiData.componentEditorVisible, bSize,
			uiColours.button, 0x00000000);
	ImGui::SameLine();
	toggleButton(ICON_FA_BOX_OPEN, "Toggle asset browser", &uiData.assetBrowserVisible, bSize,
			uiColours.button, 0x00000000);
	ImGui::SameLine();
	toggleButton(ICON_FA_GAUGE_HIGH, "Toggle profiler", &uiData.profilerVisible, bSize,
			uiColours.button, 0x00000000);
	ImGui::SameLine();
	toggleButton(ICON_FA_SLIDERS, "Toggle settings menu", &uiData.settingsVisible, bSize,
		uiColours.button, 0x00000000);

	ImGui::SameLine();
	ImGui::SeparatorEx(ImGuiSeparatorFlags_Vertical);

	drawRunControls(bSize);

	ImGui::PopFont();
	ImGui::PopStyleColor();
	ImGui::PopStyleVar(4);
	ImGui::End();

	// TODO: Re-enable once networking is re-added
	/*
	const float winWidth = 160.0f;
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
	ImGui::SetNextWindowPos(ImVec2(uiData.barLeftExtent - 160.0f - uiData.winPadding, uiData.viewportPos.y));
	ImGui::SetNextWindowSize(ImVec2(160.0f, 0.0f));
	ImGui::Begin("ECS toggle", NULL, winFlags | ImGuiWindowFlags_NoBackground);

	bool last = uiData.ecsSelection;
	toggleSwitch("Client", "Server", &uiData.ecsSelection, "");
	if (last != uiData.ecsSelection) {
		// Deselct entity
		EntityHierarchy::setSelectedEntity(Entity::null);
	}

	ImGui::PopStyleVar();
	ImGui::End();*/
}

void Editor::drawRunControls(ImVec2& bSize)
{
	ImGui::BeginDisabled(uiData.sceneFirstRun);
	ImGui::SameLine();
	bool stopBtn = false;
	button(ICON_FA_STOP, "Reload scene", &stopBtn, bSize, 0x00000000);
	ImGui::EndDisabled();

	// Reload scenes on stop
	if (stopBtn) {
		uiData.sceneFirstRun = true;
		QGlobals.updateGame = false;


		EntityView view = uiData.appCtx.sECS->view<CSceneRoot>();
		for (Entity entity : view) {
			CSceneRoot cS = uiData.appCtx.sECS->get<CSceneRoot>(entity);

			if (cS.sceneFileName[0] != '\0') {
				uiData.appCtx.sECS->destroyEntity(entity);
				ECSUtil::readEntityDisk(uiData.appCtx.sECS, cS.sceneFileName);
			}
		}
	}

	ImGui::SameLine();
	bool pauseBtn = QGlobals.updateGame;
	ImGui::PushStyleColor(ImGuiCol_Button, 0x000000);
	toggleButtonIcon(ICON_FA_PAUSE, ICON_FA_PLAY, "Toggle the game loop", 
			&pauseBtn, bSize);
	ImGui::PopStyleColor();

	// Save scenes to the disk before running 
	if (pauseBtn && pauseBtn != QGlobals.updateGame && uiData.sceneFirstRun) {
		bool sceneFound = false;

		EntityView view = uiData.appCtx.sECS->view<CSceneRoot>();
		for (Entity entity : view) {
			sceneFound = true;
			CSceneRoot& cS = uiData.appCtx.sECS->get<CSceneRoot>(entity);
			ContextECS::EntityEntry& entry = uiData.ecs->context.entities[entity.index];

			if (cS.sceneFileName[0] == '\0' || !ECSUtil::writeEntityDisk(uiData.appCtx.sECS, 
						std::string(cS.sceneFileName), entity, true)) 
			{
				Log::error("Scene \"", entry.name, "\" has an invalid file path and cannot be saved");
			}
		}

		// Prevent running if the scenes cant be saved
		if (!sceneFound) {
			Log::out("[Warning] No CSceneRoot components, entities can't be saved");
		}
	}

	if (pauseBtn) {
		// Unlock the reset button
		uiData.sceneFirstRun = false;
	}

	QGlobals.updateGame = pauseBtn;


}

void Editor::drawMenuBar()
{

	std::string projectName;
	if (uiData.currentProj->loaded) {
		projectName = uiData.currentProj->projectName;
	} else {
		projectName = "No Project";
	}

	ImGuiWindowFlags winFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoDecoration |
			ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
	ImVec2 winSize = ImVec2(ImGui::GetIO().DisplaySize.x, uiData.menuBarHeight);
	ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
	ImGui::SetNextWindowSize(winSize);
	ImGui::Begin("##Menu bar", NULL, winFlags);

	float buttonsOffset = 160.0f; // TODO: Hard coded alignment
	float winWidth = ImGui::GetWindowWidth() - uiData.winPadding * 2.0f;
	
	// Version info
	ImGui::SameLine();
	ImGui::Text("Quasar");
	ImGui::PushStyleColor(ImGuiCol_Text, uiColours.textGrey);
	ImGui::SameLine();
	ImGui::Text("Alpha");
	ImGui::PopStyleColor();

	// Status indicator
	ImGui::SameLine(ImGui::GetWindowContentRegionMax().x - buttonsOffset);
	uint32 statusCol = (uiData.currentProj->libLoaded) ? uiColours.green : uiColours.red;
	ImGui::PushStyleColor(ImGuiCol_Text, statusCol);
	// Align indicator to buttons
	ImGui::SetCursorPosY(ImGui::GetCursorPosY() + uiData.framePadding);
	ImGui::PushFont(uiData.textLarge);
//	ImGui::Text(ICON_FA_CIRCLE);
	ImGui::Text("•");
	ImGui::PopFont();
	ImGui::PopStyleColor();

	if (ImGui::IsItemHovered()) {
		if (uiData.currentProj->libLoaded) {
			ImGui::SetTooltip("Project library running");
		} else {
			ImGui::SetTooltip("Project lirbary not running");
		}
	}

	ImGui::SameLine();
	ImGui::SeparatorEx(ImGuiSeparatorFlags_Vertical);

	ImGui::BeginDisabled(!uiData.currentProj->loaded);

	// Compile project
	bool compileBtn = false;;
	ImGui::SameLine();
	Editor::button("Build " ICON_FA_CODE, "Compile and run project", &compileBtn, ImVec2(0.0f, 0.0f), 0x00000000);

	if (compileBtn) {
		ProjectBuild::runBuildCommands();
	}

	bool openBtn = false;
	ImGui::SameLine();
	Editor::button("Open " ICON_FA_ARROW_UP_RIGHT_FROM_SQUARE, "Opens project in the default application", 
			&openBtn, ImVec2(0.0f, 0.0f), 0x00000000);
	if (openBtn) {
		ProjectBuild::openProject();
	}

	// Project title
	ImGui::PushFont(uiData.textLarge);
	float nameWidth = ImGui::CalcTextSize(projectName.c_str()).x;
	ImGui::SameLine(winWidth - nameWidth - buttonsOffset);
	ImGui::Text("%s", projectName.c_str());
	ImGui::PopFont();

	ImGui::EndDisabled();

	ImGui::End();
}
