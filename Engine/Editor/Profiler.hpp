#ifndef QUASAR_PROFILER_H
#define QUASAR_PROFILER_H

#include "EditorIncludes.hpp"
#include <vector>
#include <array>
#include <chrono>
#include <string>

class Profiler
{
public:
	/* Names of threads and tasks to be displayed in the renderer, timings will be 
	 * catergotised by tasks within individual threads */
	static uint32 addThread(std::string threadName, std::string unit);
	static uint32 addTask(int threadNum, std::string taskName);

	static void uploadTime(double duration, const int threadNum, const int taskNum);

	static void drawProfiler();

	// Uploads a time on destruction
	struct ScopedMeasurement
	{
		std::chrono::high_resolution_clock::time_point t0;
		int thread = 0;
		int task = 0;

		ScopedMeasurement(int threadNum, int taskNum) :
			t0(std::chrono::high_resolution_clock::now()),
			thread(threadNum), task(taskNum){}

		~ScopedMeasurement()
		{
			std::chrono::high_resolution_clock::time_point t1 =
				std::chrono::high_resolution_clock::now();
			
			uint32 duration = std::chrono::duration_cast<std::chrono::microseconds>
				(t1 - t0).count();

			Profiler::uploadTime(duration / 1000.0f, thread, task);
		}
	};

	static int maxDataPoints;
	static float updateRate;

private:
	struct Task
	{
		//Histogram
		std::string taskName;

		std::vector<float> dataPoints {};
		std::vector<float> cumulativeDataPoints {};

		//TPS reading
		double totalDelta = 0.0;
		uint32 tickCount = 0;
		double measurement = 0.0;
	};

	struct ThreadData
	{

		std::string threadName;
		std::string unit;
		std::vector<Task> tasks {};

		int maxDataPoints = 512;
		uint32 nextPlot = 0;
		double totalDelta = 0.0;
		uint32 tickCount = 0;
		double measurement = 0.0;
	};

	static bool showSettings;
	static int chartType;
	static float plotHeight;
	static bool legendOutside;
	static const char* chartNames[];

	static void drawShadedPlot(ThreadData& tData);
	static void drawLinePlot(ThreadData& tData);

	static void drawSettings();

	static std::vector<ThreadData> threadData;
};

#endif //QUASAR_PROFILER_H
