#ifndef QUASAR_ASSET_BROWSER_H
#define QUASAR_ASSET_BROWSER_H

#include "EditorSettings.hpp"
#include "Elements.hpp"
#include "GFX/Drawable/MaterialManager.hpp"
#include "imgui.h"
#include <unordered_map>
#include <vulkan/vulkan_core.h>

class AssetBrowser
{
public:
	// Draw an asset browser window
	static void drawAssetBrowser();

	static std::unordered_map<std::string, bool> fileTypeFilters;

	struct AssetPayload
	{
		std::string identifier;
	};

	static AssetPayload assetPayloadBuffer;

private:
	static void drawAssetIcons();
	static bool shouldFilterType(std::string fileName);
	static void importAssets();

	enum AssetType
	{
		GLTF,
		IMAGE,
		ENTITY
	};

	static void contentBox(const std::string& path, int imageID, float height, const char* payloadName);

	static int filterSelection;
	static char searchTxt[4096];
	static int nrColumns;

	static bool showFilters;
};

#endif //QUASAR_ASSET_BROWSER_H
