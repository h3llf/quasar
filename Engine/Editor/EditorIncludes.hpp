#ifndef QUASAR_EDITOR_INCLUDES_H
#define QUASAR_EDITOR_INCLUDES_H

#include "Core/Types.hpp"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_vulkan.h"
#include "imgui.h"
#include "EditorSettings.hpp"

#endif //QUASAR_EDITOR_INCLUDES_H
