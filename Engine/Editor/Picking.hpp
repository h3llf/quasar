#ifndef QUASAR_EDITOR_PICKING_H
#define QUASAR_EDITOR_PICKING_H

#include "EditorSettings.hpp"

class MousePick
{
public:
	static void checkIntersections(ImVec2 moustPos, int width, int height);
	static void drawTest();
};

#endif //QUASAR_EDITOR_PICKING_H
