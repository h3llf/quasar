#ifndef THUMBNAIL_GEN_H
#define THUMBNAIL_GEN_H

#include <unordered_map>
#include <string>
#include "Core/ECS/ECS.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "Core/ECS/Reflection.hpp"
#include "GFX/RenderTypes.hpp"

class ThumbnailGen
{
public:
	static void init();
	static ImageHandle getImage(const std::string& modelPath);

private:
	static void generateThumbnail(const std::string &modelPath);
	static std::unordered_map<std::string, ImageHandle> thumbnails;
	static std::unordered_map<std::string, quasar::AssetHandle> assetHandles;
};

struct ICThumbnailGenerator
{
	ImageHandle target;
	uint64 startingFrameCount = 0;
	bool ready = false;
	bool currentlyDrawing = false;

	// Bounds
	vec3 min = vec3(0.0f);
	vec3 max = vec3(0.0f);
	vec3 center = vec3(0.0f);

	// Don't preserve data or serialize
	BEGIN_CLASS()
	FIELD(ready, Reflect::EDITOR_READ, Reflect::SERIALIZE_DISABLE)
	END_CLASS()
};

// Waits for the model & textures to fully load then creates the thumbnail
class SThumbnail
{
public:
	static void onUpdate(ECS* ecs, double delta);
	static void onAssetsLoad(ECS* ecs, Entity entity);

	static inline bool currentlyDrawing = false;
	static inline Entity cameraEntity;
};

#endif //THUMBNAIL_GEN_H
