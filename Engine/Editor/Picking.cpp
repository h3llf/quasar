#include "Picking.hpp"
#include "Core/ECS/Types.hpp"
#include "EntityHierarchy.hpp"
#include "Core/Types.hpp"
#include "Editor/EditorSettings.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include <limits>

using namespace quasar;

void MousePick::checkIntersections(ImVec2 mousePos, int width, int height)
{
	if (!uiData.appCtx.cECS->has<CCamera>(uiData.editorCamera)) {
		return;
	}

	CCamera& cC = uiData.appCtx.cECS->get<CCamera>(uiData.editorCamera);

	// Check aganst mesh AABB
	EntityView view = uiData.ecs->view<CMesh, CTransform>();

	// View space ray dir
	vec3 dir = vec3(mousePos.x / width, mousePos.y / height, -1.0);
	dir.xy = dir.xy * 2.0f - vec2(1.0f);

	vec4 world = qml::invert(cC.proj) * vec4(dir, 1.0);
	world /= world.w;
//	world *= cC.camProps.far;
	world = qml::invert(cC.view) * world;
	world /= world.w;
	
	Entity closestEnt = Entity::null;
	float closest = std::numeric_limits<float>::max();

	for (auto[entity, cM, cT] : view.components()) {
		// Convert into object local space
		mat4 invCtm = qml::invert(cT.ctm);
		vec3 lStart = (invCtm * vec4(cC.camProps.viewPos, 1.0f)).xyz;
		vec3 lEnd = (invCtm * vec4(world.xyz, 1.0f)).xyz;
		vec3 lRay = (lStart - lEnd).normalize();
		vec3 invRay = vec3(1.0f) / lRay;

		float dist = 0.0f;
		if (BVH::rayBoxIntersect(cM.min, cM.max, lStart, invRay)) {
			if (BVH::raycastBVH(cM.node, lStart, invRay, lRay, &dist)) {
				if (dist <= 0.0f) {
					continue;
				}
				// Recover world space distance
				vec3 intersectPt = lStart + (lRay * dist);
				vec3 worldIntersectPt = (cT.ctm * vec4(intersectPt, 1.0)).xyz - cC.camProps.viewPos;
				float worldDist = worldIntersectPt.magnitude();
				if (worldDist < closest) {
					closest = worldDist;
					closestEnt = entity;
				}
			}
		}		
	}

	if (closestEnt) {
		EntityHierarchy::setSelectedEntity(closestEnt);
	}
}

void MousePick::drawTest()
{
	auto& v = InstanceManager::getDebugLines();

//	v.push_back(LineVertex{0xff00ff00, pos});
//	v.push_back(LineVertex{0xff0000ff, pos + dir2});
}
