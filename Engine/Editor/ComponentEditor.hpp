#ifndef QUASAR_COMPONENT_EDITOR_H
#define QUASAR_COMPONENT_EDITOR_H

#include "Core/ECS_Headers.hpp"
#include "EditorIncludes.hpp"

class ComponentEditor
{
public:
	static void drawComponentEditor();

private:
	static void drawEntityInfo(ContextECS::EntityEntry& entry);

	template<typename T>
	static bool displayComponentHelper(ComponentBase* comp, ComponentType type)
	{
		if (type == TypeInfo<T>::type) {
			T* c = &reinterpret_cast<Component<T>*>(comp)->data;
			ImGui::PushFont(uiData.textIconSmall);
			displayComponent(c);
			ImGui::PopFont();
			return true;
		}

		return false;

	}

	static void addComponentDialog(Entity entity);
	static void componentContextMenu();
	
	static void displayComponent(ComponentBase* comp, ComponentType type);
	static void displayComponent(ComponentBase* comp, Reflect::FieldList& fl);
	static void displayComponent(quasar::CTransform* cTransform);
	static void displayComponent(quasar::CModel* cModel);
	static void displayComponent(quasar::CMesh* cMesh);
	static void displayComponent(quasar::CMaterialPBR* cMat);
	static void displayComponent(quasar::CCamera* cCam);
	static void displayComponent(quasar::CRenderable* cR);
	static void displayComponent(quasar::CPointLight* cPLight);
	static void displayComponent(quasar::CDirectionalLight* cDLight);
	static void displayComponent(quasar::CRigidbody* cRigid);
	static void displayComponent(quasar::CColliderShape* cCol);
	static void displayComponent(quasar::CText* cText);
	static void displayComponent(quasar::CAudioSource* cAudio);

	static void displayMaterialImage(std::string& name, int* imageID, quasar::Image::ImageUsage usage);
	static void drawCheckerboard(ImVec2 size);


	static ComponentType rClickComponent;
};

#endif // QUASAR_COMPONENT_EDITOR_H
