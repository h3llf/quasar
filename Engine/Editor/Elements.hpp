#ifndef QUASAR_ELEMENTS_H
#define QUASAR_ELEMENTS_H

#include "Core/Types.hpp"
#include "Editor/EditorSettings.hpp"
#include "EditorIncludes.hpp"
#include "Utils/Math/Vector.hpp"
#include "imgui.h"
#include <limits>
#include <string>

namespace Editor
{
	enum VectorUsage
	{
		VEC_USAGE_COLOUR,
		VEC_USAGE_POS
	};

	void button(const char* name, const char* desc, bool* state, ImVec2 buttonSize = ImVec2(0.0f, 0.0f),
			uint32 col = uiColours.button);

	// Toggleable button that inverts the state bool and changes colour when enabled
	void toggleButton(const char* name, const char* desc, bool* state, ImVec2 buttonSize = ImVec2(0.0f, 0.0f),
			uint32 activeCol = uiColours.button, uint32 inactiveCol = uiColours.bg);

	// Toggleable button that switches between an active and inactive icon
	void toggleButtonIcon(const char* nameActive, const char* nameInactive,
			const char* desc, bool* state, ImVec2 buttonSize = ImVec2(0.0f, 0.0f));

	void toggleSwitch(const char* leftName, const char* rightName, bool* state, const char* tooltip = "");

	// Sets state to ID if clicked, only changes colour when state == ID
	void buttonExclusive(const char* name, const char* desc, int* state, int ID,
			ImVec2 buttonSize = ImVec2(0.0f, 0.0f),	uint32 activeCol = uiColours.button, 
			uint32 inactiveCol = uiColours.bg, bool colourText = false);

	void collapsingHeaderBtn(const char* name, bool& open, ImGuiTreeNodeFlags flags, 
			const char* btnText, bool& btn);

	bool collapsingHeader(const char* label);

	void searchBox(const char* label, std::string& input, const char* hint, float width);

	// Custom vector input element
	template <typename T, int size>
	void vectorInput(const char* identifier, qml::vectorType<T, size>* vec, VectorUsage usage = VEC_USAGE_POS)
	{
		ImVec2 startSize = ImGui::GetCursorScreenPos();

		for (uint32 i = 0; i < size; ++i) {
			std::string fieldName;
			uint32_t colour;

			switch(i) {
			case 0:
				fieldName = (usage == VEC_USAGE_COLOUR) ? "R" : "X";
				colour = uiColours.red;
				break;
			case 1:
				fieldName = (usage == VEC_USAGE_COLOUR) ? "G" : "Y";
				colour = uiColours.green;
				break;
			case 2:
				fieldName = (usage == VEC_USAGE_COLOUR) ? "B" : "Z";
				colour = uiColours.blue;
				break;
			case 3:
				fieldName = (usage == VEC_USAGE_COLOUR) ? "A" : "W";
				colour = uiColours.grey;
				break;
			default:
				fieldName = std::to_string(size);
				colour = uiColours.grey;
			}

			// TODO: Make label scale properly with font size
			// TODO: Make items scale with different padding values

			const float labelPadding = 4.0f;
			const float inputWidth = 48.0f;

			float fontSize = uiData.textIconSmall->FontSize;
			float yOffset = fontSize + 2.0f * uiData.framePadding;
			float labelWidth = fontSize + labelPadding;
			
			float txtPadding = ImGui::CalcTextSize(fieldName.c_str()).x / 2.0f;

			ImVec2 boxSize = ImVec2{startSize.x + labelWidth + yOffset, startSize.y + yOffset};
			ImVec2 txtOffset = ImVec2{startSize.x + (labelWidth + labelPadding) / 2.0f - txtPadding, 
				startSize.y + uiData.framePadding};

			// Labels with colourd background
			ImDrawList* draw = ImGui::GetWindowDrawList();
			draw->AddRectFilled(startSize, boxSize, colour, 10.0f, ImDrawFlags_RoundCornersLeft);
			draw->AddText(txtOffset, uiColours.textFG, fieldName.c_str());
			startSize.x += labelWidth;
			ImGui::SetCursorScreenPos(startSize);

			// Each input needs its own ID
			std::stringstream ss;
			ss << "##" << i << identifier;

			ImGui::SetNextItemWidth(inputWidth);
			ImGui::InputFloat(ss.str().c_str(), &vec->data[i], 0.0f, 0.0f, "%.3f");
			startSize.x += inputWidth + labelPadding;

			if (i < size - 1)
				ImGui::SameLine();
		}

		// Colour picker

		// Field lock btn

	}

	// TODO: Add flags to the following functions as needed
	
	inline void checkboxAtomic(const char* label, std::atomic<uint32_t>* v)
	{
		bool enable = *v;
		ImGui::Checkbox(label, &enable);
		*v = enable;
	}

	inline void checkboxAtomic(const char* label, std::atomic<bool>* v)
	{
		bool enable = *v;
		ImGui::Checkbox(label, &enable);
		*v = enable;
	}

	inline void sliderFloatAtomic(const char* label, std::atomic<float>* v, float min, float max,
			const char* format)
	{
		float val = *v;
		ImGui::SliderFloat(label, &val, min, max, format);
		*v = val;
	}

	// Passes the parameters to ImGui::Text and places table next column before and after the call
	template <typename ... Args>
	inline void tableText(const char* fmt, Args ... args){
		ImGui::TableNextColumn();
		ImGui::Text(fmt, args...);
		ImGui::TableNextColumn();
	}

	void sliderIntAtomic(const char* label, std::atomic<uint32_t>* v, uint32_t min, uint32_t max, 
			const char* format = "%d", bool powerTwo = false);
};

#endif //QUASAR_ELEMENTS_H
