#include "EntityHierarchy.hpp"
#include "Elements.hpp"
#include "Client/Components/CRenderable.hpp"
#include "Core/ECS/Types.hpp"
#include "Core/Globals.hpp"
#include "Editor/ImageCache.hpp"
#include "EditorSettings.hpp"
#include "AssetBrowser.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include "GFX/Renderer.hpp"
#include "Elements.hpp"
#include "IconFontCppHeaders/IconsFontAwesome6.h"
#include "imgui.h"
#include <bitset>
#include <cstdio>
#include <cstring>
#include <sstream>
#include <string>

#include "Platform/FileDialog.hpp"
#include "Core/ECS/UtilECS.hpp"
#include "imgui_internal.h"

using namespace quasar;

bool EntityHierarchy::selectedVisited;
Entity EntityHierarchy::rClickEntity = Entity::null;
bool EntityHierarchy::shouldOpenPopup = false;
bool entityReparentHover = false;
std::string EntityHierarchy::searchInput;
std::stack<Entity> EntityHierarchy::selectedParents;

void EntityHierarchy::drawEntityHierarchy()
{
	selectedVisited = false;
	ImGui::Begin("Hierarchy");

	float addBtnSize = ImGui::CalcTextSize(ICON_FA_FOLDER_PLUS).x + uiData.framePadding * 2;
	float searchWidth = ImGui::GetContentRegionAvail().x - addBtnSize - uiData.framePadding * 2;
	Editor::searchBox("##Entity search", searchInput, ICON_FA_MAGNIFYING_GLASS " Search", searchWidth);

	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, 0x00000000);
	if (ImGui::Button(ICON_FA_FOLDER_PLUS "##entity add")) {
		ImGui::OpenPopup("Add entity menu");

	}
	ImGui::PopStyleColor();

	addEntityMenu();

	// Clear selection on escape
	if (!ImGui::IsAnyItemActive() && ImGui::IsKeyPressed(ImGuiKey_Escape)) {
		setSelectedEntity(Entity::null);
	}

	ImGui::PushStyleColor(ImGuiCol_ChildBg, 0xff1a1a1a);
	ImGui::BeginChild("Hierarchy list", ImVec2(0.0f, 0.0f));
	entityReparentHover = false;

	// Draw context menu
	entityContextMenu();

	if (shouldOpenPopup) {
		ImGui::OpenPopup("Entity context menu");
		shouldOpenPopup = false;
	}

	// Traverse all root level entities
	for (ContextECS::EntityEntry& entry : uiData.ecs->context.entities) {
		// Add root level entities
		if (!entry.parent && searchInput.empty()) {
			addTreeNode(entry.entity);
		} else if (!searchInput.empty() && entry.name.find(searchInput) != std::string::npos) {
			addTreeNode(entry.entity, true);
		}
	}
	// Keep viewport focused when mouse is captured

	// Unselect on collapse or deletion
	if (searchInput.empty() && !selectedVisited && uiData.selectedEntity != Entity::null) {
		setSelectedEntity(Entity::null);
	}

	ImGui::EndChild();
	ImGui::PopStyleColor();

	// Unparent entities dragged to the base window
	// Only enable this target if others in the hierarchy are not active to avoid conflicts
	if (!entityReparentHover && ImGui::BeginDragDropTarget()) {
		ImGui::SetTooltip("Unparent entity");
		if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("Entity_Entry_Drag")) {
			Entity srcEntity = *reinterpret_cast<Entity*>(payload->Data);
			uiData.ecs->setParent(srcEntity, Entity::null);
		}
		ImGui::EndDragDropTarget();
	}

	ImGui::End();
}


void EntityHierarchy::setSelectedEntity(Entity entity)
{
	// Un-highlight
	if (uiData.selectedEntity && uiData.ecs->has<CMaterialPBR>(uiData.selectedEntity)) {
		uiData.ecs->get<CMaterialPBR>(uiData.selectedEntity).materialOptions.selected = 0;
	}

	// Set highlight
	if (entity && uiData.ecs->has<CMaterialPBR>(entity)) {
		uiData.ecs->get<CMaterialPBR>(entity).materialOptions.selected = 1;
	}

	// Clear expansion list
	for (uint32 i = 0; i < selectedParents.size(); ++i) {
		selectedParents.pop();
	}

	// Add each parent to the tree expansion list
	// Unless entity has been set to null entity
	Entity parent = entity;
	while (entity && (parent = uiData.ecs->getParent(parent))) {
		selectedParents.push(parent);
	}

	uiData.selectedEntity = entity;
}

void EntityHierarchy::addEntityMenu()
{
	if (ImGui::BeginPopup("Add entity menu")) {
		if (ImGui::Selectable(ICON_FA_FOLDER " Load")) {
			// Load entity from disk
			std::string fileName = SystemDialog::getFile("*.entity", false);

			if (!fileName.empty()) {
				Entity newEnt = ECSUtil::readEntityDisk(uiData.ecs, fileName);
				uiData.ecs->setParent(newEnt, uiData.selectedEntity);
			}
		}

		if (ImGui::Selectable(ICON_FA_SQUARE_PLUS " New")) {
			// Add entity
			Entity newEnt = uiData.ecs->makeEntity();
			// Selected entity or NullEntity
			uiData.ecs->setParent(newEnt, uiData.selectedEntity);
		}
		ImGui::EndPopup();
	}
}

void EntityHierarchy::entityContextMenu()
{
	if (ImGui::BeginPopup("Entity context menu")) {
		if (ImGui::Selectable(ICON_FA_FLOPPY_DISK "Save")) {
			// Save entity to disk
			std::string fileName = SystemDialog::getFile("*.entity", true);
			if (!fileName.empty()) {
				ECSUtil::writeEntityDisk(uiData.ecs, fileName, rClickEntity, true);
			}
		}

		ImGui::PushStyleColor(ImGuiCol_Header, uiColours.red);
		ImGui::PushStyleColor(ImGuiCol_HeaderHovered, uiColours.redHover);
		ImGui::PushStyleColor(ImGuiCol_HeaderActive, uiColours.redActive);
		if (ImGui::Selectable(ICON_FA_TRASH_CAN "Delete")) {
			uiData.ecs->destroyEntity(rClickEntity);
		}
		ImGui::PopStyleColor(3);
		ImGui::EndPopup();
	}
}

void EntityHierarchy::addTreeNode(Entity entity, bool alwaysLeaf)
{
	// Deleted entity
	if (!entity || entity == uiData.hiddenEntity) {
		return;
	}

	ContextECS::EntityEntry& entry = uiData.ecs->context.entities[entity.index];

	char name[2048];
	if (!entry.name.empty()) {
		snprintf(name, 2048, "%s %s ## %u", getEntityIcon(entity, uiData.ecs), entry.name.c_str(), entity.ID); 
	} else {
		sprintf(name, "Unnamed Entity");
	}

	ImGuiTreeNodeFlags nodeFlags = ImGuiTreeNodeFlags_OpenOnArrow |
		ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_SpanAvailWidth;

	if (uiData.selectedEntity == entity) {
		nodeFlags |= ImGuiTreeNodeFlags_Selected;
	}

	bool nodeExpanded = false;

	// The tree should expand on entity selection
	if (!alwaysLeaf && selectedParents.size() && selectedParents.top() == entity) {
		selectedParents.pop();
		ImGui::SetNextItemOpen(true);
	}

	// Display this entity
	if (entry.children.size() && !alwaysLeaf) {
		nodeExpanded = ImGui::TreeNodeEx(name, nodeFlags);
	} else {
		nodeFlags |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
		ImGui::TreeNodeEx(name, nodeFlags);
	}

	// Check for right click
	if (ImGui::IsItemHovered() && ImGui::IsMouseClicked(ImGuiMouseButton_Right)) {
		rClickEntity = entity;
		// OpenPopup won't work here if tree is open
		shouldOpenPopup = true;
	}

	// Select or deselect when node is clicked
	if (ImGui::IsItemClicked() && !ImGui::IsItemToggledOpen()) {
		if (entity != uiData.selectedEntity)
			setSelectedEntity(entity);
		else
			setSelectedEntity(Entity::null);
	}

	// Drag and drop entities to reparent
	// Source
	if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None)) {
		ImGui::SetTooltip("Reparent entity");
		ImGui::SetDragDropPayload("Entity_Entry_Drag", &entity, sizeof(Entity));
		ImGui::EndDragDropSource();
	}

	// Reparent to destination
	if (ImGui::BeginDragDropTarget()) {
		if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("Entity_Entry_Drag")) {
			Entity srcEntity = *reinterpret_cast<Entity*>(payload->Data);
			uiData.ecs->setParent(srcEntity, entity);
		}
		ImGui::EndDragDropTarget();
		entityReparentHover = true;
	}

	// Recursively display child entities
	if (nodeExpanded) {
		for (Entity cEnt : entry.children) {
			addTreeNode(cEnt);
		}

		ImGui::TreePop();
	}

	if (entity == uiData.selectedEntity)
		selectedVisited = true;
}

const char* EntityHierarchy::getEntityIcon(Entity entity, ECS* ecs)
{
	if (ecs->has<CCamera>(entity)) {
		return ICON_FA_VIDEO;
	} else if (ecs->has<CModel>(entity)) {
		return ICON_FA_CUBES;
	} else if (ecs->has<CPointLight>(entity)) {
		return ICON_FA_LIGHTBULB;
	} else if (ecs->has<CDirectionalLight>(entity)) {
		return ICON_FA_SUN;
	}

	return ICON_FA_CUBE;
}
