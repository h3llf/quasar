#ifndef QUASAR_EDITOR_GIZMOS_H
#define QUASAR_EDITOR_GIZMOS_H

class EditorGizmos
{
public:
	static bool drawGuizmos();
	static void drawGuizmoOptions();
};

#endif //QUASAR_EDITOR_GIZMOS_H
