#ifndef QUASAR_EDITOR_H
#define QUASAR_EDITOR_H

#include "Core/ApplicationContext.hpp"
#include "Core/QuasarProject.hpp"
#include "Core/Console.hpp"
#include "EditorIncludes.hpp"

namespace Editor
{
	void init(GLFWwindow* window, Console& console);
	void loadImageIcons();
	void initECS(const quasar::ApplicationContext& appCtx);
	void deinitECS();
	void initProject(ProjectHeader* project);
	void buildDocks(ImGuiID dockID, ImGuiDockNodeFlags flags);
	void dockSpace();
	void initFonts();
	void draw(GLFWwindow* window);
	void getDrawCommands(VkCommandBuffer cmdBuffer);
	void cleanUp();

	void setViewportImages();
	void clearImageCache();

	std::mutex* getEditorMutex();

	// TODO: Move
	void drawToolbar();
	void drawRunControls(ImVec2& bSize);
	void drawMenuBar();
};

#endif //QUASAR_EDITOR_H
