#include "ImageCache.hpp"
#include "EditorSettings.hpp"
#include "GFX/Drawable/MaterialManager.hpp"
#include "backends/imgui_impl_vulkan.h"
#include <vulkan/vulkan_core.h>

std::unordered_map<int, ImTextureID> ImageCache::cache;

void ImageCache::resetCache()
{
	cache.clear();
}

bool ImageCache::getImage(int imageID, ImTextureID* pTexID)
{
	if (imageID == -1) {
		return false;
	}

	if (!cache.contains(imageID)) {
		VkImageView view = materialManager.getImageView(imageID);
		if (view == VK_NULL_HANDLE) {
			return false;
		}

		cache[imageID] = ImGui_ImplVulkan_AddTexture(uiData.sampler, view,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
	}

	*pTexID = cache[imageID];

	return true;
}
