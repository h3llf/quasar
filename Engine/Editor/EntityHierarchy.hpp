#ifndef QUASAR_ENTITY_EDITOR_H
#define QUASAR_ENTITY_EDITOR_H

#include "Core/ECS_Headers.hpp"
#include "Core/Assets/Image.hpp"
#include "EditorIncludes.hpp"

class EntityHierarchy
{
public:
	static void drawEntityHierarchy();
	static void setSelectedEntity(Entity entity);
private:

	static void addEntityMenu();
	static void entityContextMenu();
	static void addTreeNode(Entity entity, bool alwaysLeaf = false);
	static const char* getEntityIcon(Entity entity, ECS* ecs);

	static bool selectedVisited;

	static Entity rClickEntity;
	static bool shouldOpenPopup;

	static std::string searchInput;

	// Detect which tree nodes need to be opened on selection
	static std::stack<Entity> selectedParents;
};

#endif //QUASAR_ENTITY_EDITOR_H
