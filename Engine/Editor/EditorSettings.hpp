#ifndef QUASAR_EDITOR_SETTINGS_H
#define QUASAR_EDITOR_SETTINGS_H

#include <mutex>
#include "Core/ApplicationContext.hpp"
#include "Core/Types.hpp"
#include "Core/QuasarProject.hpp"
#include "Core/ECS_Headers.hpp"
#include "IconFontCppHeaders/IconsFontAwesome6.h"
#include "imgui.h"
#include "ImGuizmo.h"
#include "vulkan/vulkan.h"

namespace Editor
{
	struct IconImage
	{
		const char* fileName;
		quasar::Image* image = nullptr;
	};

	struct ToolbarItem
	{
		const char* icon;
	 	const char* toolTip;
		bool btnState;
		ImGuiKey toggleKey;
	};

	struct EditorData
	{

		quasar::ApplicationContext appCtx;

		ProjectHeader* currentProj = nullptr;
		
		enum {
			SELECTION_ECS_CLIENT = false,
			SELECTION_ECS_SERVER = true
		};
		bool ecsSelection = SELECTION_ECS_CLIENT;
		ECS* ecs;

		std::mutex editorSyncMutex;
		bool editorDrawn = false;

		Entity hiddenEntity = Entity::null; // Hidden from hierarchy window
		Entity editorCamera = Entity::null;
		Entity selectedEntity = Entity::null;

		bool profilerVisible = false;
		bool assetBrowserVisible = false;
		bool settingsVisible = false;
		bool entityHierarchyVisible = true;
		bool componentEditorVisible = true;
		bool testWindowVisible = false;
		bool projectDialogVisible = true;
		bool menuBarVisible = true;
		int projectDialogCreateMode = 0;

		bool resetDocks = true;
		float menuBarHeight = 28.0f;

		ImFont* textIconSmall;
		ImFont* textLarge;
		ImFont* iconLarge;

		int orientationNum = 0; // mode
		ImGuizmo::MODE currentGizmoMode = ImGuizmo::WORLD;
		int opNum = 1;
		ImGuizmo::OPERATION currentGizmoOp = ImGuizmo::TRANSLATE;

		// Alignment values
		float barRightExtent = 0.0f;
		float barLeftExtent = 0.0f;
		float edgeDist = 8.0f;

		float normalTxtSize = 15.0f;
		float headerTxtSize = 18.0f;
		float iconTxtSize = 20.0f;

		const float winPadding = 8.0f;
		const float framePadding = 4.0f;
		const float btnPadding = 8.0f;

		// Viewport and images
		VkSampler sampler;

		std::vector<ImTextureID> viewportImages;
		uint32 currentImg = 0;

		ImVec2 viewportExtent = ImVec2(0.0f, 0.0f);
		ImVec2 viewportPos = ImVec2(0.0f, 0.0f);

		// Project settings & scene data
		bool sceneFirstRun = true;

		std::array<IconImage, 4> iconImages = {
			IconImage {"../Assets/Editor/icon_gltf.png"},
			IconImage {"../Assets/Editor/icon_entity.png"},
			IconImage {"../Assets/Editor/icon_font.png"},
			IconImage {"../Assets/Editor/icon_audio.png"},
		};
	};

	// https://lospec.com/palette-list/endesga-36
	struct EditorColours
	{
		// bf5d20
		// e57028
		// a5501c

		// Colour layout = aabbggrr
		const uint32_t txtInput = 0xff101010;
		const uint32_t bg = 0xff202020;
		const uint32_t bgTrans = 0xaa101010;

		const uint32_t textFG = 0xffe7e0db;
		const uint32_t textGrey = 0xffaaaaaa;
		const uint32_t frameBG = 0xff101010;
		const uint32_t lighterBG = 0xff2a2a2a;
		const uint32_t header = 0xff303030;
		const uint32_t tab = 0xff303030;
		const uint32_t tabInactive = 0xff202020;
		const uint32_t tabActive = 0xff404040;
		const uint32_t childBG = 0xff202020;

		const uint32_t red = 0xff362cc4;
		const uint32_t green = 0xff4b9b50;
		const uint32_t blue = 0xffce820e;

		// Button colours
		const uint32_t button = 0xff205dbf;
		const uint32_t buttonHover = 0xff2870e5;
		const uint32_t buttonActive = 0xff1c50a5;
		const uint32_t grey = 0xff303030;
		const uint32_t redHover = 0xff3c30dd;
		const uint32_t redActive = 0xff2e25aa;

		const uint32_t modalBG = 0xaa101010;
	};
}

extern Editor::EditorData uiData;
static Editor::EditorColours uiColours {};
#endif //QUASAR_EDITOR_SETTINGS_H
