#include "Input.hpp"
#include "Utils/Timer.hpp"
#include "GFX/Renderer.hpp"
#include "Editor/Editor.hpp"
#include <GLFW/glfw3.h>

using namespace quasar;

double InputState::mousePosX = 0.0;
double InputState::mousePosY = 0.0;
double InputState::mouseDeltaX= 0.0;
double InputState::mouseDeltaY= 0.0;
double InputState::lastMouseX = 0.0;
double InputState::lastMouseY = 0.0;

bool InputState::cursorVisible = true;
bool InputState::mouseLeft = false;
bool InputState::mouseRight = false;
bool InputState::mouseMiddle = false;

std::map<int, int> InputState::keyStates;
std::map<int, bool> InputState::keyStateChanges;

static GLFWwindow* window;

void InputState::init()
{
	window = Renderer::getWindow();

	glfwGetCursorPos(window, &lastMouseX, &lastMouseY);

	// Add WASD by default
	addKey(GLFW_KEY_W);
	addKey(GLFW_KEY_A);
	addKey(GLFW_KEY_S);
	addKey(GLFW_KEY_D);

	addKey(GLFW_KEY_ESCAPE);
}

// TODO: Add mutex around poll events
void InputState::pollInputs()
{
	glfwPollEvents();

	for (auto& button : keyStates) {
		int initialState = button.second;
		button.second = glfwGetKey(window, button.first);

		// True if the key state has chaged since last being polled
		keyStateChanges[button.first] = (initialState != button.second);
	}

	glfwGetCursorPos(window, &mousePosX, &mousePosY);

	mouseDeltaX = mousePosX - lastMouseX;
	mouseDeltaY = lastMouseY - mousePosY;

	lastMouseX = mousePosX;
	lastMouseY = mousePosY;

	mouseLeft = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
	mouseRight = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
	mouseMiddle = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE);
}

void InputState::addKey(int key)
{
	keyStates.insert(std::make_pair(key, GLFW_RELEASE));
	keyStates.insert(std::make_pair(key, false));
}

void InputState::removeKey(int key)
{
	keyStates.erase(key);
}

bool InputState::toggleCursor()
{
	cursorVisible = !cursorVisible;

	if (cursorVisible)
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	else
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	return cursorVisible;
}
