#include "CPlayerController.hpp"
#include "Client/Input.hpp"
#include "Core/Components/CCamera.hpp"
#include "GLFW/glfw3.h"
#include "Utils/Math/Quaternion.hpp"
#include "Core/Physics/PhysicsController.hpp"
#include <cmath>

using namespace quasar;

void SKinematicController::init(ApplicationContext* ctx)
{
	// Register ecs functions here
	ctx->cECS->registerListener<CKinematicController>(&SKinematicController::onCreate, this, LISTENER_ON_CREATE);
	ctx->cECS->registerUpdateFunction(&SKinematicController::onUpdate, this, UPDATE_GAME_FAST);
	pCtx = ctx;

	hits.resize(256);
}

void SKinematicController::onCreate(ECS* ecs, Entity entity)
{
	CKinematicController& cFPS = ecs->get<CKinematicController>(entity);

	Entity camEntity = ecs->makeEntity<CTransform, CCamera>();
	ecs->renameEntity(camEntity, "Camera");
	ecs->setParent(camEntity, entity);
	cFPS.cameraEntity = camEntity;

	if (!ecs->has<CRigidbody>(entity)) {
		CRigidbody cR = {true};
		ecs->add(entity, cR);
	}

	if (!ecs->has<CTransform>(entity)) {
		ecs->add<CTransform>(entity);
	}

	if (!ecs->has<CColliderShape>(entity)) {
		CColliderShape cSS;
		cSS.colliderType = PHYSICS_CAPSULE_COLLIDER;
		ecs->add(entity, cSS);
	}

}

using namespace std::chrono_literals;

void SKinematicController::onUpdate(ECS *ecs, double delta)
{
	EntityView view = ecs->view<CKinematicController, CTransform, CColliderShape>();

	for (auto[entity, cCon, cT, cS] : view.components()) {
		// Check camera entity is valid/exists
		Entity camEnt = cCon.cameraEntity;
		if (!camEnt || !ecs->exists(camEnt) || !ecs->has<CCamera>(camEnt) || !ecs->has<CTransform>(camEnt)) {
			// Currently cannot be done on deserialize as children are unavalible at that stage
			findCameraEntity(ecs, entity);
			continue;
		}

		// Cursor locking
		if ((InputState::keyStates[GLFW_KEY_ESCAPE] == GLFW_PRESS) &&
				InputState::keyStateChanges[GLFW_KEY_ESCAPE]) {
			InputState::toggleCursor(); 
		}

		// Get camera/head components
		CCamera& cCam = ecs->get<CCamera>(camEnt);
		CTransform& cTCam = ecs->get<CTransform>(camEnt);
		cTCam.position.y = cCon.cameraHeight - 1.0f;

		// Apply mouse movement
		cCon.yaw += InputState::mouseDeltaX * cCon.mouseSensitivity;
		cCon.pitch += InputState::mouseDeltaY * cCon.mouseSensitivity;

		// Constrain pitch
		const float pitchBias = 0.001f;
		cCon.pitch = std::clamp(cCon.pitch , -M_PI_2f + pitchBias, M_PI_2f - pitchBias);

		vec3 yLookDir = vec3(cos(cCon.pitch), sin(cCon.pitch), 0.0f);
		vec3 xzLookDir = vec3(cos(cCon.yaw), 0.0f, sin(cCon.yaw));

		cTCam.rotation = qml::getRotationBetween(cCam.forward, yLookDir).normalize();
		cT.rotation = qml::getRotationBetween(cCam.forward, xzLookDir).normalize();

		// Get movement direction
		vec3 deltaPos = vec3(0.0f);
		if (InputState::keyStates[GLFW_KEY_W] == GLFW_PRESS)
			deltaPos += xzLookDir;
		if (InputState::keyStates[GLFW_KEY_S] == GLFW_PRESS)
			deltaPos -= xzLookDir;
		if (InputState::keyStates[GLFW_KEY_A] == GLFW_PRESS)
			deltaPos -= qml::cross(xzLookDir, cCam.up).normalize();
		if (InputState::keyStates[GLFW_KEY_D] == GLFW_PRESS)
			deltaPos += qml::cross(xzLookDir, cCam.up).normalize();

		float dist = delta;
		if (InputState::keyStates[GLFW_KEY_LEFT_CONTROL]) {
			dist *= cCon.sprintSpeed;
		} else {
			dist *= cCon.speed;
		}

		PhysicsQueryHit groundHit;
		bool grounded = isGrounded(cCon.groundAngle, groundHit, cS, cT, entity);
		cCon.isGrounded = grounded && (cCon.groundAngle <= cCon.walkAngle);

		// Reduce movement while not touching the ground
		if (!cCon.isGrounded) {
			dist *= cCon.airMovementFactor;
		}

		if (deltaPos.magnitude() != 0.0f) {
			// Project movement onto slope to move up and down smoothly
			if (cCon.isGrounded) {
				deltaPos = projOnPlane(deltaPos, groundHit.normal);
			}
			deltaPos = deltaPos.normalize() * dist;
		}

		if (!cCon.isGrounded) {
			// Increase fall speed and move down
			cCon.fallSpeed += cCon.gravity * delta;
			deltaPos.y -= cCon.fallSpeed * delta;
		} else {
			// Stop falling
			cCon.fallSpeed = 0.0f;

			if (cCon.isJumping) {
				cCon.isJumping = false;
			}
		}

		bool jumpKey = InputState::keyStates[GLFW_KEY_SPACE] == GLFW_PRESS;
			
		// Jump
		if (cCon.isGrounded && !cCon.isJumping && jumpKey) {
			cCon.fallSpeed = -cCon.jumpVelocity;
			deltaPos.y -= cCon.fallSpeed * delta;
		}

		for (uint32 i = 0; i < cCon.maxBounces; ++i) {
			if (deltaPos.magnitude() == 0.0f) {
				break;
			}

			vec3 dir = deltaPos.normalize();
			PhysicsQueryHit hit;
			float sweepDist = std::max(bias, deltaPos.magnitude());
			sweepShape(hit, cS, cT, dir, sweepDist, entity);

			if (hit.actorEntity) {
				std::stringstream ss;
				ss << hit.actorEntity;
				cCon.entityHit = ss.str();
			}

			cCon.distance = hit.hitDistance;
			cCon.moveDist = deltaPos.magnitude();

			if (hit.hitDistance== 0.0f) {
				// Push along the normal to prevent getting suck in walls
				break;
			}

			// Debug move dir
			auto& lines = InstanceManager::getDebugLines();
			if (cCon.drawDebugVectors) {
				lines.push_back(LineVertex{0xff00ffff, cT.position});
				lines.push_back(LineVertex{0xff00ffff, cT.position + dir.normalize()});
			}

			if (!hit.actorEntity || (hit.hitDistance - bias) > deltaPos.magnitude()) {
				// No collision
				cT.position += deltaPos;
				break;
			} else if (hit.hitDistance != 0.0f) {
				cT.position += dir * (hit.hitDistance - bias);
				
				// Project the direction onto the normal of the hit point to allow sliding
				vec3 projected = projOnPlane(dir, hit.normal);
				if (projected.magnitude() == 0.0f) {
					break;
				}

				projected.toNormalized();

				// Remaining movement distance
				float newDist = deltaPos.magnitude() - hit.hitDistance;

				if (cCon.limitSliding) {
					newDist *= dot(projected, dir);
				}

				deltaPos = projected * newDist;

				// Visualize slide dir
				if (cCon.drawDebugVectors) {
					lines.push_back(LineVertex{0xff00ffff, cT.position});
					lines.push_back(LineVertex{0xff00ffff, cT.position + deltaPos.normalize()});
				}
			}

			// Finished bouncing
			if (dist < bias) {
				break;
			}
		}
	}

	// TODO: The controller has issues at high frameRateS
	// TODO: REMOVE
	std::this_thread::sleep_for(1ms);
}

void SKinematicController::sweepShape(PhysicsQueryHit& hit, CColliderShape& cS, CTransform& cT, vec3 dir, 
		float dist, Entity entity)
{
	if (dir.magnitude() == 0.0f || dist == 0.0f) {
		return;
	}

	float hitDist = std::numeric_limits<float>::max();

	// Cast the collider forward to check for collisions
	hits.clear();
	hits.resize(256);
	int count = pCtx->cPhysics->sweepMulti(cS.ID, cT.position, cT.rotation, dir, dist, hits);

	for (int i = 0; i < count; ++i) {
		if (hits[i].hitDistance < hitDist && hits[i].actorEntity != entity) {
			hitDist = hits[i].hitDistance;
			hit = hits[i];

			// If the sweep is initially overlaping
			if (hits[i].initialOverlap) {
				cT.position += (hits[i].penDir * (hits[i].penDist + bias));
			}
		}
	}

}

bool SKinematicController::isGrounded(float& angle, PhysicsQueryHit& hit, quasar::CColliderShape& cS, 
		quasar::CTransform& cT, Entity entity)
{
	sweepShape(hit, cS, cT, vec3(0.0f, -1.0f, 0.0f), groundDist, entity);
	bool res = (hit.actorEntity != Entity::null);

	if (res) {
		angle = acos(qml::dot(hit.normal.normalize(), vec3(0.0f, 1.0f, 0.0f)));
	} else {
		angle = 0.0f;
	}

	return res;
}

vec3 SKinematicController::projOnPlane(vec3 dir, vec3 normal)
{
	return dir - qml::dot(dir, normal) / (dir.magnitude() * dir.magnitude()) * normal;
}

void SKinematicController::findCameraEntity(ECS* ecs, Entity root)
{
	CKinematicController& cCon = ecs->get<CKinematicController>(root);

	std::vector<Entity>& children = ecs->getChildren(root);
	for (Entity child : children) {
		if (ecs->has<CCamera>(child)) {
			cCon.cameraEntity = child;
			break;
		}
	}

	if (!cCon.cameraEntity) {
		Log::error("Deserialized CKinematicController with no camera entity attached");
	}
}

void SEditorCamController::init(ApplicationContext* ctx)
{
	uint32 callbackFn = ctx->cECS->registerUpdateFunction(update, UPDATE_EDITOR);
}

void SEditorCamController::update(ECS* ecs, double delta)
{
	EntityView view = ecs->view<CEditorCamController, CCamera, CTransform>();

	for (auto [entity, cCon, cCam, cT] : view.components()) {
		// Update camera dimensions
		cCam.width = QUASAR.windowSizeX;
		cCam.height = QUASAR.windowSizeY;

		// Cursor locking
		if ((InputState::keyStates[GLFW_KEY_TAB] == GLFW_PRESS) &&
				InputState::keyStateChanges[GLFW_KEY_TAB]) {
			InputState::toggleCursor(); 
		}

		// Mouse look active only when cursor is hidden
		if (!InputState::cursorVisible) {
			cCon.azimuth += InputState::mouseDeltaX * cCon.mouseSensitivity;
			cCon.zenith += InputState::mouseDeltaY * cCon.mouseSensitivity;
		}

		//Constrain pitch
		const float pitchBias = 0.001f;
		cCon.zenith = std::clamp(cCon.zenith, -M_PI_2f + pitchBias, M_PI_2f - pitchBias);

		vec3 lookDir;
		lookDir.x = cos(cCon.azimuth) * cos(cCon.zenith);
		lookDir.z = sin(cCon.azimuth) * cos(cCon.zenith);
		lookDir.y = sin(cCon.zenith);

		// Get the shortest arc from forward(1, 0, 0) to look dir
		cT.rotation = qml::getRotationBetween(cCam.forward, lookDir.normalize());

		// Lock movement
		if (InputState::cursorVisible) {
			return;
		}

		float dist = delta;
		if (InputState::keyStates[GLFW_KEY_LEFT_CONTROL]) {
			dist *= cCon.sprintSpeed;
		} else {
			dist *= cCon.speed;
		}

		// Movement axis
		vec3 moveDir = PlayerMovement::getMovementDir(lookDir, cCon.up);
//		moveDir = moveDir.normalize();

		cT.position += moveDir * dist;

		/*
		if (InputState::mouseLeft) {
			PhysicsQueryHit hit = physicsController.query.rayCast(cT.position, lookDir, 100.0f);

			if (hit.entity) {
				Log::out(hit.entity);
			}
		}*/
	}
}

vec3 PlayerMovement::getMovementDir(vec3 facing, vec3 up)
{
	vec3 deltaPos = vec3(0.0f);

	if (InputState::keyStates[GLFW_KEY_W] == GLFW_PRESS)
		deltaPos += facing;
	if (InputState::keyStates[GLFW_KEY_S] == GLFW_PRESS)
		deltaPos -= facing;
	if (InputState::keyStates[GLFW_KEY_A] == GLFW_PRESS)
		deltaPos -= qml::cross(facing, up).normalize();
	if (InputState::keyStates[GLFW_KEY_D] == GLFW_PRESS)
		deltaPos += qml::cross(facing, up).normalize();
	if (InputState::keyStates[GLFW_KEY_SPACE] == GLFW_PRESS)
		deltaPos += up;
	if (InputState::keyStates[GLFW_KEY_LEFT_SHIFT] == GLFW_PRESS)
		deltaPos -= up;

	return deltaPos;
}
