#include "CRenderable.hpp"
#include "Core/ApplicationContext.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "Core/Assets/BVH.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/Components/CAssetPolling.hpp"
#include "Core/ECS/CallbacksECS.hpp"
#include "Core/ECS/ECS.hpp"
#include "Core/ECS/UtilECS.hpp"
//#include <cstdlib>
//#include <stdlib.h>
#include "GFX/Renderer.hpp"

using namespace quasar;

void SMaterial::init(ApplicationContext* ctx)
{
	this->ctx = ctx;

	createFn = ctx->cECS->registerListener<CMaterialPBR>(&SMaterial::onCreate, this,
			LISTENER_ON_CREATE | LISTENER_ON_DESERIALIZE);
	destroyFn = ctx->cECS->registerListener<CMaterialPBR>(&SMaterial::onDestroy, this, 
			LISTENER_ON_DESTROY);
	assetLoadFn = ctx->cECS->registerListener<CAssetPolling>(&SMaterial::onAssetLoad, this, 
			LISTENER_ON_ASSET_LOAD);
}

void SMaterial::onCreate(ECS* ecs, Entity entity)
{
	CMaterialPBR& cM = ecs->get<CMaterialPBR>(entity);
	cM.materialProps.albedoID = -1;
	cM.materialProps.metalRoughID= -1;
	cM.materialProps.normalID = -1;
	cM.materialProps.emissiveID = -1;

	CAssetPolling& cAP = SAssetPolling::getCAssetPolling(ecs, entity);

	if (!cM.albedoImage.empty()) {
		cM.albedoHandle = ctx->assetMgr->addImageLoadTask(cM.albedoImage, Image::Albedo);
		cAP.addHandle(cM.albedoHandle);
	}
	if (!cM.metalRoughImage.empty()) {
		cM.metalRoughHandle = ctx->assetMgr->addImageLoadTask(cM.metalRoughImage, Image::MetallicRoughness);
		cAP.addHandle(cM.metalRoughHandle);
	}
	if (!cM.normalImage.empty()) {
		cM.normalHandle = ctx->assetMgr->addImageLoadTask(cM.normalImage, Image::Normal);
		cAP.addHandle(cM.normalHandle);
	}
	if (!cM.emissiveImage.empty()) {
		cM.emissiveHandle = ctx->assetMgr->addImageLoadTask(cM.emissiveImage, Image::Emissive);
		cAP.addHandle(cM.emissiveHandle);
	}
}

void SMaterial::onDestroy(ECS *ecs, Entity entity)
{

	CMaterialPBR& mat = ecs->get<CMaterialPBR>(entity);

	// Nofify image removal
	/*
	if (!mat.albedoImage.empty())
		ctx->assetMgr->notifyImageRemoval(mat.albedoImage);
	if (!mat.metalRoughImage.empty())
		ctx->assetMgr->notifyImageRemoval(mat.metalRoughImage);
	if (!mat.normalImage.empty())
		ctx->assetMgr->notifyImageRemoval(mat.normalImage);
	if (!mat.emissiveImage.empty())
		ctx->assetMgr->notifyImageRemoval(mat.emissiveImage);*/
}

void loadingCommands(AssetHandle& asset, int& imageID, ApplicationContext* ctx, CAssetPolling& cAP)
{
	if (asset.identifier.empty() || !cAP.isLoaded(asset)) {
		return;
	}
	
	Image* img = ctx->assetMgr->getImage(asset);
	imageID = img->rendererImageID;
}

void SMaterial::onAssetLoad(ECS *ecs, Entity entity)
{
	if (!ecs->has<CMaterialPBR>(entity)) {
		return;
	}

	CAssetPolling& cAP = ecs->get<CAssetPolling>(entity);
	CMaterialPBR& cMat = ecs->get<CMaterialPBR>(entity);

	loadingCommands(cMat.albedoHandle, cMat.materialProps.albedoID, ctx, cAP);
	loadingCommands(cMat.metalRoughHandle, cMat.materialProps.metalRoughID, ctx, cAP);
	loadingCommands(cMat.normalHandle, cMat.materialProps.normalID, ctx, cAP);
	loadingCommands(cMat.emissiveHandle, cMat.materialProps.emissiveID, ctx, cAP);
}

void SModelLoader::init(ApplicationContext* ctx)
{
	this->ctx = ctx;

	createFn = ctx->cECS->registerListener<CModel>(&SModelLoader::onCreate, this,
		LISTENER_ON_CREATE | LISTENER_ON_DESERIALIZE);
	assetLoadFn = ctx->cECS->registerListener<CAssetPolling>(&SModelLoader::onAssetLoad, this,
		LISTENER_ON_ASSET_LOAD);
}

void SModelLoader::createMeshEntities(ECS* ecs, Entity entity)
{
	CModel& m = ecs->get<CModel>(entity);
	Model* model = ctx->assetMgr->getModel(std::string(m.modelFolder) + m.modelName);
	m.model = model;

	if (!m.hasMeshes) {
		ECSUtil::createModelEntities(ctx->assetMgr, ecs, m.modelFolder, m.modelName, entity);
	} else {
		std::vector<Entity>& ch = ecs->getChildren(entity);
		for (Entity ent : ch) {
			if (ecs->has<CMesh>(ent)) {
				CMesh& cMesh = ecs->get<CMesh>(ent);
				Model::Mesh& mesh = model->meshes[cMesh.modelMeshIndex];
				BVHNode* node = ctx->assetMgr->getBVHNode(std::string(m.modelFolder) + 
						m.modelName, cMesh.modelMeshIndex);
				cMesh.node = node;
				cMesh.min = mesh.min;
				cMesh.max = mesh.max;

				if (cMesh.modelMeshIndex >= model->meshes.size()) {
					Log::error("Failed to update CMesh component, Submesh index out of range");
					continue;
				}
				CRenderable cRen {};
				cRen.meshID = mesh.rendererID;
				
				if (ecs->has<CRenderable>(ent)) {
					ecs->remove<CRenderable>(ent);
				}
				ecs->add(ent, cRen);
			}
		}
	}
}

void SModelLoader::onCreate(ECS* ecs, Entity entity)
{
	CModel& m = ecs->get<CModel>(entity);
	if (m.modelName[0] != '\0') {
		m.handle = ctx->assetMgr->addModelLoadTask(m.modelFolder, m.modelName);

		CAssetPolling& cAP = SAssetPolling::getCAssetPolling(ecs, entity);
		cAP.addHandle(m.handle);
	}
}

void SModelLoader::onAssetLoad(ECS *ecs, Entity entity)
{
	if (!ecs->has<CModel>(entity)) {
		return;
	}
	
	CModel& cM = ecs->get<CModel>(entity);
	CAssetPolling& cAP = ecs->get<CAssetPolling>(entity);

	if (cAP.isLoaded(cM.handle)) {
		createMeshEntities(ecs, entity);
	}
}

void SRenderable::init(ApplicationContext* ctx)
{
	this->ctx = ctx;

	updateFn = ctx->cECS->registerUpdateFunction(&SRenderable::update, this, UPDATE_ALWAYS);
	createFn = ctx->cECS->registerListener<CTransform, CRenderable>(
			&SRenderable::onCreate, this, LISTENER_ON_CREATE);
	deserializeFn = ctx->cECS->registerListener<CRenderable>(&SRenderable::onDeserialize, this, 
			LISTENER_ON_DESERIALIZE);
	destoryFn = ctx->cECS->registerListener<CRenderable, CMesh>(&SRenderable::onDestroy, this,
			LISTENER_ON_DESTROY);
}

void SRenderable::update(ECS* ecs, double delta)
{
	EntityView view = ecs->view<CTransform, CRenderable, CMesh, CMaterialPBR>();

	for (auto [entity, transform, render, mesh, mat] : view.components()) {
		InstanceManager::setInstanceTransform(render.meshID, render.instanceID, transform.ctm);
		InstanceManager::setInstanceMaterialProps(render.meshID, render.instanceID, 
				mat.materialProps, mat.materialOptions);

		if (mesh.previewBVH && mesh.node) {
			mesh.node->renderNode(transform.ctm, false);
		}
	}
}

void SRenderable::onCreate(ECS* ecs, Entity entity)
{
	CRenderable& r = ecs->get<CRenderable>(entity);
	CTransform& t = ecs->get<CTransform>(entity);

	mat4 transform = mat4::identity();
	r.instanceID = InstanceManager::addMeshInstance(r.meshID, transform);
}

void SRenderable::onDeserialize(ECS* ecs, Entity entity)
{
	ecs->get<CRenderable>(entity).instanceID = -1;
	ecs->remove<CRenderable>(entity);
}

void SRenderable::onDestroy(ECS* ecs, Entity entity)
{
	CRenderable& r = ecs->get<CRenderable>(entity);

	// Instance doesn't exist
	if (r.instanceID < 0) 
		return;

	InstanceManager::removeMeshInstance(r.meshID, r.instanceID);
}
