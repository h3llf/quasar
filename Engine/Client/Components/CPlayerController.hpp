#ifndef QUASAR_C_PLAYER_CONTROLLER_H
#define QUASAR_C_PLAYER_CONTROLLER_H

#include "Core/ApplicationContext.hpp"
#include "Core/Globals.hpp"
#include "Core/ECS/CallbacksECS.hpp"
#include "Core/ECS/ECS.hpp"
#include "Client/Input.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/Components/CRigidbody.hpp"
#include "Utils/Math/QML.hpp"
#include <GLFW/glfw3.h>

namespace quasar
{

struct CKinematicController
{
	Entity cameraEntity = Entity::null;
	float pitch = 0.0f;
	float yaw = 0.0f;
	float cameraHeight = 1.65;
	float playerHeight = 2.0f;

	float speed = 5.0f;
	float sprintSpeed = 7.0f;

	float mouseSensitivity = 0.003f;

	int maxBounces = 5;
	bool limitSliding = true;

	float gravity = 9.8f;
	float walkAngle = qml::radians(61.0f);
	float jumpAngle = qml::radians(70.0f);
	float jumpVelocity = 4.5f;
	float airMovementFactor = 1.0f;

	// Current state
	std::string entityHit = "";
	float distance = 0.0f;
	float moveDist = 0.0f;
	bool isGrounded = false;
	float groundAngle = 0.0f;
	float fallSpeed = 0.0f;
	bool isJumping = false;
	bool drawDebugVectors = false;

	BEGIN_CLASS()
	FIELD(cameraEntity, Reflect::EDITOR_READ, Reflect::SERIALIZE_DISABLE)
	FIELD(pitch, Reflect::EDITOR_READ)
	FIELD(yaw, Reflect::EDITOR_READ)
	FIELD(cameraHeight, Reflect::EDITOR_READ_WRITE)
	FIELD(playerHeight, Reflect::EDITOR_READ_WRITE)
	FIELD(speed, Reflect::EDITOR_READ_WRITE)
	FIELD(sprintSpeed, Reflect::EDITOR_READ_WRITE)
	FIELD(mouseSensitivity, Reflect::EDITOR_READ_WRITE)
	FIELD(maxBounces, Reflect::EDITOR_READ_WRITE)
	FIELD(limitSliding, Reflect::EDITOR_READ_WRITE)
	FIELD(gravity, Reflect::EDITOR_READ_WRITE)
	FIELD(walkAngle, Reflect::EDITOR_READ_WRITE)
	FIELD(jumpAngle, Reflect::EDITOR_READ_WRITE)
	FIELD(jumpVelocity, Reflect::EDITOR_READ_WRITE)
	FIELD(airMovementFactor, Reflect::EDITOR_READ_WRITE)

	FIELD(entityHit, Reflect::EDITOR_READ, Reflect::SERIALIZE_DISABLE)
	FIELD(distance, Reflect::EDITOR_READ, Reflect::SERIALIZE_DISABLE)
	FIELD(moveDist, Reflect::EDITOR_READ, Reflect::SERIALIZE_DISABLE)
	FIELD(isGrounded, Reflect::EDITOR_READ, Reflect::SERIALIZE_DISABLE)
	FIELD(groundAngle, Reflect::EDITOR_READ, Reflect::SERIALIZE_DISABLE)
	FIELD(fallSpeed, Reflect::EDITOR_READ, Reflect::SERIALIZE_DISABLE)
	FIELD(isJumping, Reflect::EDITOR_READ, Reflect::SERIALIZE_DISABLE)
	FIELD(drawDebugVectors, Reflect::EDITOR_READ_WRITE, Reflect::SERIALIZE_DISABLE)
	END_CLASS()
};

class SKinematicController
{
public:
	// Called when the library is loaded
	void init(quasar::ApplicationContext* ctx);

	void onCreate(ECS* ecs, Entity entity);
	void onDeserialize(ECS* ecs, Entity entity);
	void onUpdate(ECS* ecs, double delta);

private:
	const float bias = 0.001f;
	const float groundDist = 0.01f;

	void findCameraEntity(ECS* ecs, Entity root);

	std::vector<PhysicsQueryHit> hits;
	void sweepShape(PhysicsQueryHit& hit, quasar::CColliderShape& cS, quasar::CTransform& cT, vec3 dir, float dist, Entity entity);

	bool isGrounded(float& angle, PhysicsQueryHit& hit, quasar::CColliderShape& cS, quasar::CTransform& cT, Entity entity);

	quasar::ApplicationContext* pCtx;

	vec3 projOnPlane(vec3 dir, vec3 normal);
};

struct CEditorCamController
{
	float zenith = 0.0f;
	float azimuth = 0.0f;
	vec3 up = {0.0f, 1.0f, 0.0f};

	float mouseSensitivity = 0.003f;

	float sprintSpeed = 20.0f;
	float speed = 3.0f;
};

class SEditorCamController
{
public:
	void init(ApplicationContext* ctx);

	static void update(ECS* ecs, double delta);

	uint32 callbackFn;
};

namespace PlayerMovement
{
	// Gets 6 axis movement dir from key states
	vec3 getMovementDir(vec3 facing, vec3 up);
}

};

#endif //QUASAR_C_PLAYER_CONTROLLER_H
