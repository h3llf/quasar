#ifndef QUASAR_C_RENDERABLE_H
#define QUASAR_C_RENDERABLE_H

#include "Core/ApplicationContext.hpp"
#include "Core/Globals.hpp"
#include "Core/ECS/CallbacksECS.hpp"
#include "Core/ECS/ECS.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/Components/CModel.hpp"

namespace quasar
{

struct CMaterialPBR
{
	std::string albedoImage = "";
	std::string metalRoughImage = "";
	std::string normalImage = "";
	std::string emissiveImage = "";

	InstanceManager::MaterialProps materialProps {};
	InstanceManager::MaterialOptions materialOptions {};

	AssetHandle albedoHandle;
	AssetHandle metalRoughHandle;
	AssetHandle normalHandle;
	AssetHandle emissiveHandle;

	BEGIN_CLASS()
	FIELD(albedoImage)
	FIELD(metalRoughImage)
	FIELD(normalImage)
	FIELD(emissiveImage)
	FIELD(materialProps)
	FIELD(materialOptions)
	END_CLASS()
};

struct CRenderable
{
	int meshID = 0;
	int instanceID = -1;
	bool transparent = false;
};

class SMaterial
{
public:
	void init(ApplicationContext* ctx);

	void onCreate(ECS* ecs, Entity entity);
	void onDestroy(ECS* ecs, Entity entity);
	void onAssetLoad(ECS* ecs, Entity entity);

	uint32 createFn;
	uint32 destroyFn;
	uint32 assetLoadFn;

	ApplicationContext* ctx;
};

class SModelLoader
{
private:
	void createMeshEntities(ECS* ecs, Entity entity);
	void checkImageLoadStatus(ECS* ecs, Entity entity);

public:
	void init(ApplicationContext* ctx);

	void onCreate(ECS* ecs, Entity entity);
	void onAssetLoad(ECS* ecs, Entity entity);

	uint32 createFn;
	uint32 assetLoadFn;

	ApplicationContext* ctx;
};

class SRenderable
{
public:
	void init(ApplicationContext* ctx);

	void update(ECS* ecs, double delta);

	void onCreate(ECS* ecs, Entity entity);
	void onDeserialize(ECS* ecs, Entity entity);
	void onDestroy(ECS* ecs, Entity entity);

	uint32 updateFn;
	uint32 createFn;
	uint32 deserializeFn;
	uint32 destoryFn;

	ApplicationContext* ctx;
};

};
#endif //QUASAR_C_RENDERABLE_H
