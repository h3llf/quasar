#ifndef QUASAR_INPUT_H
#define QUASAR_INPUT_H

#include "Core/Types.hpp"
#include <GLFW/glfw3.h>
#include <map>

namespace quasar
{

class InputState
{
public:
	// A map of key codes and if they are currently pressed
	static std::map<int, int> keyStates;

	// Ture if the key has changed state since the last time it was polled
	static std::map<int, bool> keyStateChanges;

	static double mousePosX, mousePosY;
	static double mouseDeltaX, mouseDeltaY;

	static void init();
	static void pollInputs();
	static void updateCooldowns(double delta);

	static void addKey(int key);
	static void removeKey(int key); // TODO: Implement this

	// Toggles cursor visivility if currentCursorCooldown > cursorToggleCooldown
	// Returns the value is cursor visible
	static bool toggleCursor();
	static bool cursorVisible;

	static bool mouseLeft;
	static bool mouseRight;
	static bool mouseMiddle;

	// The amount of time in seconds to wait between toggleCursor calls
	static double cursorToggleCooldown;

	// TODO: Input polling lock mutex to fix imgui conflict
private:
	static double currentCursorCooldown;
	static double lastMouseX, lastMouseY;
};

};
#endif //QUASAR_INPUT_H
