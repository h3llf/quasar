#ifndef QUASAR_C_RIGIDBODY_H
#define QUASAR_C_RIGIDBODY_H

#include "Core/ApplicationContext.hpp"
#include "Core/Globals.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/Components/CModel.hpp"
#include "Core/ECS/CallbacksECS.hpp"
#include "Core/ECS/ECS.hpp"
#include "Core/Physics/PhysicsController.hpp"
#include <cmath>
#include <math.h>

namespace quasar
{

// Collisions for primitive shapes such as boxes, spheres & capsules
struct CColliderShape
{
	// Possible options are box, capsule & sphere
	PhysicsColliderType colliderType = PHYSICS_BOX_COLLIDER;

	union
	{
		// sphere or capsule
		struct
		{
			float radius;
			float halfHeight;
		};

		// Cube
		vec3 size;
	};

	int ID = -1;
	AssetHandle handle;

	CColliderShape()
	{
		size = {0.5f, 0.5f, 0.5f};
	};

	BEGIN_CLASS()
	FIELD(colliderType)
	FIELD(size)
	END_CLASS()
};

struct CRigidbody
{
	bool kinematic = false;
	float mass = 1.0f;

	vec3 linearVelocity = vec3{0.0f};
	vec3 angularVelocity = vec3{0.0f};

	int ID = -1;
	Entity* entityVal;

	// Searches the hierarchy for existing colliders when added
	// Disable this if you intend to use a large hierarchy under this entity
	bool searchColliders = true;

	// For Internal Use, Used to enable correct decomposition of the worldspace matrix
	vec3 worldScale = vec3(1.0f);
};

class SColliderShape
{
public:
	void init(ApplicationContext* ctx);

	void onCreate(ECS* ecs, Entity entity);
	void onDestroy(ECS* ecs, Entity entity);
	void onMeshLoad(ECS* ecs, Entity entity);

	uint32 createFn;
	uint32 destroyFn;
	uint32 meshLoadFn;

	static CRigidbody* findRigidbody(ECS* ecs, Entity entity);

	ApplicationContext* ctx;
};

class SRigidbody
{
public:
	void init(ApplicationContext* ctx);

	static void onCreate(ECS* ecs, Entity entity);
	static void onDestroy(ECS* ecs, Entity entity);
	static void update(ECS* ecs, double delta);

	// Gets transform from physixs simulation
	static void updateGet(ECS* ecs);

	// Sets transforms after simulation
	static void updateSet(ECS* ecs);

	uint32 createFn;
	uint32 destroyFn;

	static void addMeshColliders(ECS* ecs, Entity entity, bool convex);
	static void attachShape(CRigidbody& cR, CColliderShape& cS);
};

};
#endif //QUASAR_C_RIGIDBODY_H
