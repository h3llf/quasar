#include "CRigidbody.hpp"
#include "Core/ApplicationContext.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "Core/Components/CAssetPolling.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/Physics/PhysicsController.hpp"

using namespace quasar;

void createMeshCollider(ECS* ecs, Entity entity, ApplicationContext* ctx, CColliderShape& cC)
{
	if (!ecs->has<CMesh>(entity)) {
		Log::error("Failed to create mesh collider, no CMesh component found");
	}

	// TODO: Remove CModel components
	// Find the associated model
	Entity parent = ecs->getParent(entity);
	bool modelFound = false;
	while (parent) {
		if (ecs->has<CModel>(parent)) {
			modelFound = true;
			break;
		}

		parent = ecs->getParent(parent);
	}

	if (!modelFound) {
		Log::out("Failed to create mesh collider, CModel not found");
		return;
	}

	CModel& cModel = ecs->get<CModel>(parent);
	CMesh& cMesh = ecs->get<CMesh>(entity);

	bool isConvex = (cC.colliderType == quasar::PHYSICS_CONVEX_MESH_COLLIDER) ? true : false;
	cC.handle = ctx->assetMgr->addMeshColliderLoadTask(cModel.modelFolder, cModel.modelName, 
			cMesh.modelMeshIndex, isConvex);

	CAssetPolling& cAP = SAssetPolling::getCAssetPolling(ecs, entity);
	cAP.addHandle(cC.handle);
}

void SColliderShape::init(ApplicationContext* ctx)
{
	createFn = ctx->cECS->registerListener<CColliderShape, CTransform>
		(&SColliderShape::onCreate, this, LISTENER_ON_CREATE | LISTENER_ON_DESERIALIZE);
	destroyFn = ctx->cECS->registerListener<CColliderShape, CTransform>
		(&SColliderShape::onDestroy, this, LISTENER_ON_DESTROY);
	meshLoadFn = ctx->cECS->registerListener<CAssetPolling>
		(&SColliderShape::onMeshLoad, this, LISTENER_ON_ASSET_LOAD);

	this->ctx = ctx;
}

void SColliderShape::onCreate(ECS* ecs, Entity entity)
{
	CColliderShape& cC = ecs->get<CColliderShape>(entity);

	// Mesh colliders
	if (cC.colliderType == PHYSICS_TRIANGLE_MESH_COLLIDER || 
			cC.colliderType == PHYSICS_CONVEX_MESH_COLLIDER) 
	{
		createMeshCollider(ecs, entity, ctx, cC);
		return;
	}
	
	// Primitive colliders
	CTransform& cTransform = ecs->get<CTransform>(entity);
	vec3 size = cC.size * cTransform.scale;
	cC.ID = physicsController.addCollider(cC.colliderType, size);

	if (cC.ID == -1) {
		Log::error("Collider generation failed");
		return;
	}

	// Attach shape
	CRigidbody* cR = findRigidbody(ecs, entity);
	if (cR) {
		SRigidbody::attachShape(*cR, cC);
	}
}

void SColliderShape::onMeshLoad(ECS *ecs, Entity entity)
{
	if (!ecs->has<CColliderShape>(entity) || !ecs->has<CTransform>(entity)) {
		return;
	}

	CColliderShape& cC = ecs->get<CColliderShape>(entity);
	CAssetPolling& cAP = ecs->get<CAssetPolling>(entity);
	CTransform& cT = ecs->get<CTransform>(entity);
	if (!cAP.isLoaded(cC.handle)) {
		return;
	} else {
	}

	// Create shape from the mesh
	int meshID = ctx->assetMgr->getMeshColliderID(cC.handle);
	bool isConvex = (cC.colliderType == PHYSICS_CONVEX_MESH_COLLIDER) ? true : false;
	cC.ID = physicsController.addMeshCollider(meshID, isConvex, cT.scale, cT.position, cT.rotation);

	// Attach shape
	CRigidbody* cR = SColliderShape::findRigidbody(ecs, entity);
	if (cR) {
		SRigidbody::attachShape(*cR, cC);
	}
}

void SColliderShape::onDestroy(ECS *ecs, Entity entity)
{
	CColliderShape cC = ecs->get<CColliderShape>(entity);
	CRigidbody* cR = findRigidbody(ecs, entity);

	if (cR) {
		physicsController.detatchShape(cC.ID, cR->ID);
	}

	physicsController.removeCollider(cC.ID);
}

void SRigidbody::init(ApplicationContext* ctx)
{
	createFn = ctx->cECS->registerListener<CRigidbody, CTransform>
		(onCreate, LISTENER_ON_CREATE | LISTENER_ON_DESERIALIZE);
	destroyFn = ctx->cECS->registerListener<CRigidbody, CTransform>
		(onDestroy, LISTENER_ON_DESTROY);

	
}

// TODO: Add a function to find  parents with specific components
CRigidbody* SColliderShape::findRigidbody(ECS* ecs, Entity entity)
{
	Entity parent = entity;
	while (parent && !ecs->has<CRigidbody>(parent)) {
		parent = ecs->getParent(parent);
	}

	if (parent && ecs->has<CRigidbody>(parent)) {
		return &ecs->get<CRigidbody>(parent);
	}

	return nullptr;
}

void SRigidbody::onCreate(ECS* ecs, Entity entity)
{
	auto[cR, cT] = ecs->view<CRigidbody, CTransform>().get(entity);

	cR.entityVal = new Entity(entity); // TODO: Creating an entity like this is probably not ideal
	cR.ID = physicsController.addRigidBody(cT.position, cT.rotation, cR.mass, cR.kinematic, cR.entityVal);

	// Find colliders in the hierarchy
	if (cR.searchColliders) {
		for (Entity child : ecs->hierarchyIterator(entity)) {
			if (ecs->has<CColliderShape>(child)) {
				attachShape(cR, ecs->get<CColliderShape>(child));
			}
		}
	}
}

void SRigidbody::onDestroy(ECS *ecs, Entity entity)
{
	CRigidbody& cR = ecs->get<CRigidbody>(entity);

	if (cR.ID != -1) {
		physicsController.removeRigidBody(cR.ID);
	} else {
		Log::error("Failed to remove rigidbody");
	}
}

void SRigidbody::updateGet(ECS *ecs)
{
	EntityView view = ecs->view<CRigidbody, CTransform>();
	
	for (auto[entity, cR, cT] : view.components()) {
		vec3 worldPos;
		vec4 worldRot;
		vec3 worldScale = vec3(1.0f);
		physicsController.getTransform(worldPos, worldRot, cR.ID);
		
		Entity parent = ecs->getParent(entity);
		if (parent && ecs->has<CTransform>(parent)) {
			CTransform& cTP = ecs->get<CTransform>(parent);

			// Convert returned values to local space
			mat4 worldMat;
			qml::getTransformation(worldMat, worldPos, cR.worldScale, worldRot);
			worldMat = qml::invert(cTP.ctm) * worldMat;
			qml::decompose(worldMat, cT.position, worldScale, cT.rotation);

		} else {
			physicsController.getTransform(cT.position, cT.rotation, cR.ID);
		}

		if (!cR.kinematic)
			physicsController.getVelocity(cR.linearVelocity, cR.angularVelocity, cR.ID);
	}
}

void SRigidbody::updateSet(ECS *ecs)
{
	EntityView view = ecs->view<CRigidbody, CTransform>();
	
	for (auto[entity, cR, cT] : view.components()) {
		vec3 worldPos;
		vec4 worldRot;
		qml::decompose(cT.ctm, worldPos, cR.worldScale, worldRot);
		physicsController.setTransform(worldPos, worldRot, cR.ID);

		if (!cR.kinematic)
			physicsController.setVelocity(cR.linearVelocity, cR.angularVelocity, cR.ID);
	}
}

void SRigidbody::addMeshColliders(ECS* ecs, Entity entity, bool convex)
{
	if (!ecs->has<CModel>(entity)) {
		Log::error("Failed to add mesh colliders, no CModel component found");
		return;
	}

	CRigidbody& cR = ecs->get<CRigidbody>(entity);
	if (!cR.kinematic && !convex) {
		Log::out("Can't add triangle mesh colliders to non kinematic rigidbody");
		return;
	}

	CColliderShape cC;
	cC.colliderType = (convex) ? PHYSICS_CONVEX_MESH_COLLIDER : PHYSICS_TRIANGLE_MESH_COLLIDER;

	for (Entity child : ecs->hierarchyIterator(entity)) {
		if (ecs->has<CMesh>(child)) {
			ecs->add(child, cC);
		}
	}
}

void SRigidbody::attachShape(CRigidbody &cR, CColliderShape &cS)
{
	if (cR.ID != -1 && cS.ID != -1 && !(!cR.kinematic && cS.colliderType == PHYSICS_TRIANGLE_MESH_COLLIDER)) {
		physicsController.attachShape(cS.ID, cR.ID);
	}
}
