#include "CLighting.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/ECS/UtilECS.hpp"

using namespace quasar;

void SPointLight::init(ApplicationContext* ctx)
{
	updateFn = ctx->cECS->registerUpdateFunction(update, UPDATE_ALWAYS);
	createFn = ctx->cECS->registerListener<CTransform, CPointLight>	(onCreate, 
			LISTENER_ON_CREATE | LISTENER_ON_DESERIALIZE);
	destroyFn = ctx->cECS->registerListener<CTransform, CPointLight>(onDestroy, LISTENER_ON_DESTROY);	
}

void SPointLight::update(ECS* ecs, double delta)
{
	EntityView view = ecs->view<CPointLight, CTransform>();

	for (auto [entity, cPl, cT] : view.components()) {
		if (cPl.lightID == -1) {
			// TODO: Check why this is updating before onCreate
			continue;
		}

		vec3 pos = cT.ctm[3].xyz;
		InstanceManager::updatePointLight(cPl.lightID, pos, cPl.colour * cPl.intensity, 
				cPl.shadowMapped, cPl.distance);
	}
}

void SPointLight::onCreate(ECS* ecs, Entity entity)
{
	CPointLight& p = ecs->get<CPointLight>(entity);
	CTransform& t = ecs->get<CTransform>(entity);

	p.lightID = InstanceManager::addPointLight(t.position, p.colour * p.intensity, 
			p.shadowMapped, p.distance);
}

void SPointLight::onDestroy(ECS* ecs, Entity entity)
{
	CPointLight& p = ecs->get<CPointLight>(entity);
	InstanceManager::removePointLight(p.lightID);
}

void SDirectionalLight::init(ApplicationContext* ctx)
{
	updateFn = ctx->cECS->registerUpdateFunction(update, UPDATE_ALWAYS);
	destroyFn = ctx->cECS->registerListener<CDirectionalLight>(onDestroy, LISTENER_ON_DESTROY);
}

void SDirectionalLight::update(ECS* ecs, double delta)
{
	EntityView view = ecs->view<CDirectionalLight, CTransform>();
	
	for (auto[entity, cD, cT]: view.components()) {
		vec3 forward = qml::rotate(cD.forward, cT.rotation).normalize();
		InstanceManager::setDirectionalLight(cD.colour * cD.intensity, forward, 
				cD.shadowMapped, cD.yOffset);
	}
}

void SDirectionalLight::onDestroy(ECS* ecs, Entity entity)
{
	InstanceManager::resetDirectionalLight();
}
