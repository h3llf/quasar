#ifndef QUASAR_C_LIGHTING_H
#define QUASAR_C_LIGHTING_H

#include "Core/ApplicationContext.hpp"
#include "Core/Globals.hpp"
#include "Core/ECS/CallbacksECS.hpp"
#include "Core/ECS/ECS.hpp"
#include "CTransform.hpp"
#include "Core/Assets/InstanceManager.hpp"

namespace quasar
{

struct CPointLight
{
	qml::vec3 colour = qml::vec3(1.0f);
	float intensity = 1.0f;
	float distance = 4.0f;
	bool shadowMapped = false;
	bool visible = true;

	int lightID = -1;
};

class SPointLight
{
public:
	void init(ApplicationContext* ctx);

	static void update(ECS* ecs, double delta);
	static void onCreate(ECS* ecs, Entity entity);
	static void onDestroy(ECS* ecs, Entity entity);

	uint32 updateFn;
	uint32 createFn;
	uint32 destroyFn;
};

struct CDirectionalLight
{
	qml::vec3 colour = qml::vec3(1.0f);
	float intensity = 1.0f;
	int lightID;
	bool visible = true;

	// Shadow properties
	bool shadowMapped = true;
	float yOffset = 50.0f;

	vec3 forward = {1.0f, 0.0f, 0.0f};
};

class SDirectionalLight
{
public:
	void init(ApplicationContext* ctx);

	static void update(ECS* ecs, double delta);
	static void onDestroy(ECS* ecs, Entity entity);

	uint32 updateFn;
	uint32 destroyFn;
};

};
#endif //QUASAR_C_LIGHTING_H
