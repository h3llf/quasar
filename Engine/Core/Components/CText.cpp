#include "CText.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include "Core/Components/CAssetPolling.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/ECS/CallbacksECS.hpp"
#include "GFX/Renderer.hpp"

using namespace quasar;

void SText::init(ApplicationContext* ctx)
{
	ctx->cECS->registerUpdateFunction(&SText::onUpdate, this, UPDATE_ALWAYS);

	ctx->cECS->registerListener<CText, CTransform>(&SText::onCreate, this, LISTENER_ON_CREATE | LISTENER_ON_DESERIALIZE);
	ctx->cECS->registerListener<CText, CTransform>(&SText::onDestroy, this, LISTENER_ON_DESTROY);
	ctx->cECS->registerListener<CAssetPolling>(&SText::onLoad, this, LISTENER_ON_ASSET_LOAD);

	this->ctx = ctx;
}

void SText::onUpdate(ECS *ecs, double delta)
{
	EntityView view = ecs->view<CText, CTransform>();

	for (auto[entity, cTxt, cT] : view.components()) {
		if (cTxt.textInstanceID == -1) {
			continue;
		}

		InstanceManager::updateTextInstance(cTxt.textInstanceID, cT.ctm);
	}
}

void SText::onCreate(ECS *ecs, Entity entity)
{
	auto & cTxt = ecs->get<CText>(entity);

	if (cTxt.fontName.empty()) {
		return;
	}

	cTxt.fontAsset = ctx->assetMgr->addFontLoadTask(cTxt.fontName, cTxt.glyphQuality);
	
	auto& cAP = SAssetPolling::getCAssetPolling(ecs, entity);
	cAP.addHandle(cTxt.fontAsset);
}

void SText::onDestroy(ECS *ecs, Entity entity)
{
	auto& cTxt = ecs->get<CText>(entity);

	if (cTxt.textObject != -1) {
		InstanceManager::destroyText(cTxt.textObject);
	}

	if (cTxt.textInstanceID != -1) {
		InstanceManager::removeTextInstance(cTxt.textInstanceID);
	}
}

void SText::onLoad(ECS *ecs, Entity entity)
{
	if (!ecs->has<CText>(entity) || !ecs->has<CTransform>(entity)) {
		return;
	}

	auto& cTxt = ecs->get<CText>(entity);
	auto& cT = ecs->get<CTransform>(entity);
	auto& cAP = ecs->get<CAssetPolling>(entity);

	if (cAP.isLoaded(cTxt.fontAsset) && !cTxt.content.empty()) {
		cTxt.font = ctx->assetMgr->getFont(cTxt.fontAsset)->fontHandle;
		cTxt.textObject = Renderer::uploadText(cTxt.content, cTxt.font,
				cTxt.lineSpacing, cTxt.kerning, cTxt.maxWidth, cTxt.vertexColour);
		cTxt.textInstanceID = InstanceManager::addTextInstance(cTxt.textObject, cT.ctm);
	}
}
