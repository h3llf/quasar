#include "CAnimator.hpp"

using namespace quasar;

void SSkeleton::init(ApplicationContext* appCtx)
{
	updateFn = appCtx->cECS->registerUpdateFunction(&SSkeleton::onUpdate, this, UPDATE_ALWAYS);
	createFn = appCtx->cECS->registerListener(&SSkeleton::onCreate, this, 
			LISTENER_ON_CREATE | LISTENER_ON_DESERIALIZE);
}

void SSkeleton::onUpdate(ECS *ecs, double delta)
{
	EntityView view = ecs->view<CSkeleton>();

	for (auto[entity, cSk] : view.components()) {
		for (uint32 i = 0; i < cSk.joints.size(); ++i) {
			const JointNode& joint = cSk.joints[i];
			mat4 jointTransform  = joint.localTransform * joint.inverseBind;

			// Apply hierarchy transformation
			int ip = joint.parent;
			while (ip != -1) {
				const JointNode& parent = cSk.joints[ip];
				cSk.jointTransforms[i] = cSk.jointTransforms[ip] * jointTransform;
				ip = cSk.joints[ip].parent;
			}
			Log::out("Parents: ", ip);
		}
	}
}

void SSkeleton::onCreate(ECS *ecs, Entity entity)
{
	auto& cSk = ecs->get<CSkeleton>(entity);

	cSk.jointTransforms.resize(cSk.joints.size());
}
