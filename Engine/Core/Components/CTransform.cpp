#include "CTransform.hpp"
#include "Core/ECS/Types.hpp"
#include "Utils/Math/Transform.hpp"
#include "Utils/Timer.hpp"

using namespace quasar;

void STransform::init(ApplicationContext* ctx)
{
	updateFn = ctx->cECS->registerUpdateFunction(update, UPDATE_ALWAYS, 1000);
}

void calculateTransformHierarchy(ECS* ecs, Entity entity)
{
	std::stack<Entity> nodes;
	nodes.push(entity);

	while(nodes.size()) {
		Entity current = nodes.top();
		nodes.pop();
		const std::vector<Entity>& children = ecs->getChildren(current);

		if (!children.size()) {
			continue;
		}

		CTransform& cT1 = ecs->get<CTransform>(current);

		for (Entity child : children) {
			// Only update transforms if the hierarchy is contiguous
			if (!ecs->has<CTransform>(child)) {
				continue;
			}

			nodes.push(child);

			CTransform& cT2 = ecs->get<CTransform>(child);

			mat4 ctm = cT1.ctm * cT2.ctm;
			cT2.ctm = ctm;
		}
	}
}

void STransform::update(ECS* ecs, double delta)
{
	EntityView view = ecs->view<CTransform>();
	for (Entity entity : view) {
		CTransform& cT = ecs->get<CTransform>(entity);
		qml::getTransformation(cT.ctm, cT.position, cT.scale, cT.rotation);
	}


	EntityView view2 = ecs->view<CTransform>();
	for (Entity entity : view2) {
		// Only compute transformation hierarchy starting from the roots
		Entity parent = ecs->getParent(entity);
		if (parent && ecs->has<CTransform>(parent)) {
			continue;
		}

		calculateTransformHierarchy(ecs, entity);
	}
}

void SSineOscillation::init(ApplicationContext* ctx)
{
	updateFn = ctx->cECS->registerUpdateFunction(update, UPDATE_GAME_FAST);
}

void SSineOscillation::update(ECS* ecs, double delta)
{
	EntityView view = ecs->view<CTransform, CSineOscillation>();

	for (auto [entity, cT, cSo] : view.components()) {
		cSo.accumulator += delta * cSo.speed;
		float thisFrame = (float)sin(cSo.accumulator) * cSo.amplitude;

		if (!cSo.negative) {
			thisFrame += cSo.amplitude;
			thisFrame *= 0.5f;
		}

		float change = thisFrame - cSo.lastFrame;
		cSo.lastFrame = thisFrame;

		cT.position += cSo.dir * change;
	}
}
