#ifndef QUASAR_C_CAMERA_H
#define QUASAR_C_CAMERA_H

#include "Core/ECS/Reflection.hpp"
#include "Core/Globals.hpp"
#include "Core/ECS/ECS.hpp"
#include "Core/Camera.hpp"
#include "Core/ApplicationContext.hpp"
#include "Core/Types.hpp"
#include "GFX/RenderTypes.hpp"

namespace quasar
{

struct CCamera
{
	// Viewport ignored if drawing the the window
	std::string viewportIdentifier = "New Viewport";
	int width = 512;
	int height = 512;
	bool offscreen = false; // Enable/Disable drawing to the window
	ViewportHandle viewport;

	// Texture to use for drawing
	// By default the window is used
	ImageHandle targetImage = WINDOW_IMAGE_HANDLE;
	std::string targetImgName = "";

	mat4 view;
	mat4 proj;

	vec3 up = {0.0f, 1.0f, 0.0f};
	vec3 forward = {1.0f, 0.0f, 0.0f};
	vec3 right = {0.0f, 0.0f, 1.0f};

	int skyboxImgID = 0;
	std::string skyboxName = "";
	AssetHandle handle;
	
	Camera::CameraProperties camProps;
	int renderMask = RENDER_MASK_STANDARD;
	bool enabled = true;

	BEGIN_CLASS()
	FIELD(width)
	FIELD(height)
	FIELD(targetImage)
	FIELD(targetImgName)
	FIELD(skyboxName)
	FIELD(camProps)
	FIELD(renderMask)
	FIELD(offscreen)
	FIELD(enabled)
	END_CLASS()
};

class SCamera
{
public:
	void init(ApplicationContext* ctx);

	void onCreate(ECS* ecs, Entity entity);
	void onDestroy(ECS* ecs, Entity entity);
	void update(ECS* ecs, double delta);
	void onAssetLoad(ECS* ecs, Entity entity);

	uint32 updateFn;
	uint32 createFn;
	uint32 destroyFn;
	uint32 loadFn;

	ApplicationContext* ctx;
};

};
#endif //QUASAR_C_CAMERA_H
