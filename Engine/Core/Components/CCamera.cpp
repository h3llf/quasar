#include "CCamera.hpp"
#include "Core/ECS/CallbacksECS.hpp"
#include "Core/Types.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/Components/CAssetPolling.hpp"
#include "Core/Camera.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "GFX/Renderer.hpp"

using namespace quasar;

void SCamera::init(ApplicationContext* ctx)
{	
	this->ctx = ctx;

	// Update camera with a higher priority than renderables
	updateFn = ctx->cECS->registerUpdateFunction(&SCamera::update, this, UPDATE_ALWAYS, 10);
	createFn = ctx->cECS->registerListener<CCamera, CTransform>(&SCamera::onCreate, this, 
			LISTENER_ON_CREATE | LISTENER_ON_DESERIALIZE);

	loadFn = ctx->cECS->registerListener<CAssetPolling>(&SCamera::onAssetLoad, this, LISTENER_ON_ASSET_LOAD);
}

void SCamera::onCreate(ECS *ecs, Entity entity)
{
	CCamera& cC = ecs->get<CCamera>(entity);
	cC.skyboxImgID = -1;

	if (!cC.skyboxName.empty()) {
		CAssetPolling& cAP = SAssetPolling::getCAssetPolling(ecs, entity);
		cC.handle = ctx->assetMgr->addImageLoadTask(cC.skyboxName, Image::ImageUsage::HDR);
		cAP.addHandle(cC.handle);
	}

	if (cC.offscreen) {
		cC.viewport = Renderer::createTextureViewport(cC.width, cC.height);
	} else {
		cC.viewport = Renderer::getWindowViewport();
	}
}

void SCamera::onDestroy(ECS *ecs, Entity entity)
{
	CCamera& cC = ecs->get<CCamera>(entity);
	
	Renderer::destroyTextureViewport(cC.viewport);
}

void SCamera::onAssetLoad(ECS *ecs, Entity entity)
{
	if (!ecs->has<CCamera>(entity)) {
		return;
	}

	CCamera& cC = ecs->get<CCamera>(entity);
	CAssetPolling& cAP = ecs->get<CAssetPolling>(entity);

	if (cC.handle.identifier.empty() || !cAP.isLoaded(cC.handle)) {
		return;
	}

	Image* img = ctx->assetMgr->getImage(cC.handle);
	cC.skyboxImgID = img->rendererImageID;
}	

void SCamera::update(ECS* ecs, double delta)
{
	EntityView view = ecs->view<CCamera, CTransform>();

	for (auto[entity, cC, cT] : view.components()) {
		Camera::CameraProperties& camProps = cC.camProps;

		if (!cC.enabled) {
			continue;
		}

		vec3 pos, s;
		vec4 rot;
		qml::decompose(cT.ctm, pos, s, rot);

		vec3 forward = qml::rotate(cC.forward, rot).normalize();

		camProps.view = qml::lookAt( pos, pos + forward, cC.up);
		camProps.viewPos = pos;
		Camera::genProjection(cC.width, cC.height, &camProps);

		cC.view = camProps.view;
		cC.proj = camProps.proj;

		InstanceManager::setCameraData(camProps, cC.skyboxImgID, cC.targetImage, cC.viewport);

		// Visualize camera direction
//		auto& lines = InstanceManager::getDebugLines();
//		lines.push_back(LineVertex{0xffff0000, pos});
//		lines.push_back(LineVertex{0xffff0000, pos + forward});
	}
}
