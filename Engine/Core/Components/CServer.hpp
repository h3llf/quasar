#ifndef QUASAR_C_SERVER_H
#define QUASAR_C_SERVER_H

#include "Core/ECS/ComponentECS.hpp"
#include "Core/ECS/ECS.hpp"
#include "Core/ECS/Types.hpp"
#include "Core/Globals.hpp"
#include <bitset>

namespace quasar
{

/* CServer informs the GameServer to synchronize and entity with a remote client/server
 *
 *
 * CServer synchronizes entities between a client and server(s)
 * - If CServer is added the entity will be serialized along with any components included in submissions types
 * - Addition and removal of CServer will trigger an entity creation or deletion command on the remote client or server
 * - Parenting/unparenting an entity to a parent with CServer up the hierarchy will also trigger these commands*/
struct CServer
{
	/*
	// Include child entities
	bool includeHierarchy = true;

	// Client -> Server communication
	bool serverSubmit = true;

	// Server -> Client
	enum ClientSubmit 
	{
		CLIENT_SUBMIT_ORIGIN,
		CLIENT_SUBMIT_ALL,
		CLIENT_SUBMIT_DISABLE
	};
	ClientSubmit clientSubmit = CLIENT_SUBMIT_ALL;
	int clientOriginID = -1; // Only required for submit origin

	// Per component submission rules
	enum ComponentSubmissionFlags : uint8_t
	{ 
		SUBMIT_NONE = 0,

		// Enable submission & update on creation/removal
		SUBMIT_SERVER_ENABLE = 1 << 0,
		SUBMIT_CLIENT_ENABLE = 1 << 1,

		// Update each frame
		SUBMIT_SERVER_ALWAYS = 1 << 2,
		SUBMIT_CLIENT_ALWAYS = 1 << 3,

		// Update once and remove flag
		SUBMIT_SERVER_ONCE = 1 << 4,
		SUBMIT_CLIENT_ONCE = 1 << 5,
	};
	uint8_t submitTypes[MAX_COMPONENT_TYPES];

	template <typename T>
	void setSubmissionRule(uint8_t flags)
	{
		ComponentType type = ECSComponent<T>::type;
		submitTypes[type] = flags;
	}

	template <typename ... T>
	void setSubmissionRules(uint8_t flags)
	{
		(setSubmissionRule<T>(flags), ...);
	}

	template <typename T>
	void appendsubmissionRule(uint8_t flags)
	{
		ComponentType type = ECSComponent<T>::type;
		submitTypes[type] |= flags;
	}

	template <typename ... T>
	void appendsubmissionRules(uint8_t flags)
	{
		(appendSubmissionRule<T>(flags), ...);
	}

	template <typename T>
	void resetSubmissionType()
	{
		ComponentType type = ECSComponent<T>::type;
		submitTypes[type] = SUBMIT_NONE;
	}

	void resetSubmissionTypes();*/
};

};
#endif //QUASAR_C_SERVER_H
