#ifndef QUASAR_C_ANIMATOR_H
#define QUASAR_C_ANIMATOR_H

#include "Core/ApplicationContext.hpp"
#include "Core/ECS/Reflection.hpp"
#include <vector>

namespace quasar
{

struct JointNode
{
	mat4 inverseBind;
	mat4 localTransform;

	// Index in joints array
	int parent = -1;
};

struct CSkeleton
{
	// TODO: Look at sorting to ensure parents comse before children
	std::vector<JointNode> joints;

	// Joint transforms relative to skeleton
	std::vector<mat4> jointTransforms;

	std::string name;

	BEGIN_CLASS()
	FIELD(joints)
	FIELD(jointTransforms, Reflect::SERIALIZE_DISABLE)
	FIELD(name)
	END_CLASS()
};

class SSkeleton
{
public:
	void init(ApplicationContext* appCtx);

	void onUpdate(ECS* ecs, double delta);
	void onCreate(ECS* ecs, Entity entity);

private:
	uint32 updateFn;
	uint32 createFn;
};

};

#endif //QUASAR_C_ANIMATOR_H
