#ifndef QUASAR_C_TRANSFORM_H
#define QUASAR_C_TRANSFORM_H

#include "Core/ApplicationContext.hpp"
#include "Core/Globals.hpp"
#include "Core/ECS/CallbacksECS.hpp"
#include "Core/ECS/ECS.hpp"
#include "Core/ECS/Reflection.hpp"
#include "Utils/Math/QML.hpp"

namespace quasar
{

struct CTransform
{
	vec3 position = vec3(0.0f);
	vec3 scale = vec3(1.0f);
	quat rotation = quat(0.0f, 0.0f, 0.0f, 1.0f); // Note: Quaternions are expressed as xyzw not wxyz

	// Combined transformation matrix (Incorporates parent transformations)
	mat4 ctm;

	bool worldSpace = false;
};

class STransform
{
public:
	void init(ApplicationContext* ctx);

	static void update(ECS* ecs, double delta);
	static void updateLinked(ECS* ecs, double delta);

	// Calculates transformation matricies, should be one of the first to update
	uint32 updateFn;
};

struct CSineOscillation
{
	vec3 dir = vec3{0.0f, 1.0f, 0.0f};
	float amplitude = 1.0f;
	float speed = 1.0f;
	bool negative = false;
	float accumulator = 0.0f;

	float lastFrame = 0.0f;

	BEGIN_CLASS()
	FIELD(dir, Reflect::EDITOR_READ_WRITE)
	FIELD(amplitude, Reflect::EDITOR_READ_WRITE)
	FIELD(speed, Reflect::EDITOR_READ_WRITE)
	FIELD(negative, Reflect::EDITOR_READ_WRITE)
	END_CLASS()
};

//Produces sine motion along the dir vector
class SSineOscillation
{
public:
	void init(ApplicationContext* ctx);

	static void update(ECS* ecs, double delta);
	
	uint32 updateFn;
};

};
#endif //QUASAR_C_TRANSFORM_H
