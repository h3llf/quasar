#ifndef QUASAR_C_TEXT_H
#define QUASAR_C_TEXT_H

#include "Core/ApplicationContext.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "Core/ECS/ECS.hpp"
#include "Core/ECS/Reflection.hpp"

namespace quasar
{

struct CText
{
	std::string content;
	TextHandle textObject = -1;

	std::string fontName;
	FontHandle font = -1;
	AssetHandle fontAsset;
	int textInstanceID = -1;
	int glyphQuality = 32;

	uint32 vertexColour = 0xffffffff;

	// The maximum line width before the text wraps to a new line
	float maxWidth = 10.0f;

	// Multipliers
	float lineSpacing = 1.0f;
	float kerning = 1.0f;

	BEGIN_CLASS()
	FIELD(content)
	FIELD(fontName)
	FIELD(glyphQuality)
	FIELD(vertexColour)
	FIELD(maxWidth)
	FIELD(lineSpacing)
	FIELD(kerning)
	END_CLASS()
};

class SText
{
public:
	void init(ApplicationContext* ctx);

	void onUpdate(ECS* ecs, double delta);

	void onCreate(ECS* ecs, Entity entity);
	void onDestroy(ECS* ecs, Entity entity);
	void onLoad(ECS* ecs, Entity entity);

private:
	uint32 updateFn;
	uint32 createFn;
	uint32 destroyFn;
	uint32 loadFn;

	ApplicationContext* ctx;
};

};

#endif //QUASAR_C_TEXT_H
