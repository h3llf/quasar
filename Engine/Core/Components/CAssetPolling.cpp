#include "CAssetPolling.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "Utils/Timer.hpp"

using namespace quasar;

void CAssetPolling::addHandle(const AssetHandle &handle)
{
	if (!pollingList.contains(handle.identifier)) {
		pollingList[handle.identifier] = AssetHandle{
			.identifier = handle.identifier,
			.loadStatus = ASSET_QUEUED,
			.type = handle.type};
	}
}

AssetLoadStatus CAssetPolling::getLoadStatus(const AssetHandle& handle)
{
	if (pollingList.contains(handle.identifier)) {
		return pollingList[handle.identifier].loadStatus;
	}

	return ASSET_NOT_LOADED;
}

void SAssetPolling::init(ApplicationContext *ctx)
{
	updateFn = ctx->cECS->registerUpdateFunction(&SAssetPolling::onUpdate, this, UPDATE_ALWAYS);

	this->ctx = ctx;
}

bool isLoading(AssetLoadStatus status)
{
	return (status == ASSET_QUEUED || status == ASSET_IN_PROGRESS);
}

void SAssetPolling::onUpdate(ECS *ecs, double delta)
{
	EntityView view = ecs->view<CAssetPolling>();
	
	ctx->assetMgr->assetManagerSync.lock();

	for (auto[entity, cAP] : view.components()) {
		bool stateChange = false;
		bool currentlyLoading = false;

		for (auto& asset : cAP.pollingList) {
			bool previous = isLoading(asset.second.loadStatus);
			ctx->assetMgr->queryLoadStatusUnlocked(asset.second);
			bool current = isLoading(asset.second.loadStatus);

			stateChange |= (previous != current);
			currentlyLoading |= current;
		}

		if (stateChange && !currentlyLoading) {
			ctx->assetMgr->assetManagerSync.unlock();
			ecs->triggerListenerEvents(entity, TypeInfo<CAssetPolling>::type, LISTENER_ON_ASSET_LOAD);
			ctx->assetMgr->assetManagerSync.lock();
		}

		if (!currentlyLoading) {
			ecs->remove<CAssetPolling>(entity);
		}
	}

	ctx->assetMgr->assetManagerSync.unlock();
}

CAssetPolling& SAssetPolling::getCAssetPolling(ECS* ecs, Entity entity)
{
	if (!ecs->has<CAssetPolling>(entity)) {
		ecs->add<CAssetPolling>(entity);
	}

	return ecs->get<CAssetPolling>(entity);
}
