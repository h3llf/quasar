#ifndef QUASAR_CSCENE_H
#define QUASAR_CSCENE_H

#include "Core/ECS/ECS.hpp"
#include "Core/Globals.hpp"

namespace quasar
{

struct CSceneRoot
{
	char sceneFileName[4096] = "";
};

};
#endif //QUASAR_CSCENE_H
