#ifndef QUASAR_C_AUDIO_H
#define QUASAR_C_AUDIO_H

#include <string>
#include "Core/ApplicationContext.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "Core/Assets/Audio.hpp"
#include "Core/ECS/ECS.hpp"
#include "Core/ECS/Reflection.hpp"

namespace quasar
{

struct CAudioSource
{
	std::string filename;
	AssetHandle handle;

	bool loop = false;
	bool playOnCreate = true;
	float gain = 1.0f;
	float pitch = 1.0f;

	// Enables positional audio
	// CTransform needed for position
	bool positional = true;

	// Control parmeter and position updates
	bool shouldUpdate = true;
	bool alwaysUpdate = false;

	AudioSourceHandle audioSource = -1;

	// TODO: Move to a dedicated component
	vec3 lastPosition = vec3(0.0f);
	vec3 currentVelocity = vec3(0.0f);

	BEGIN_CLASS()
	FIELD(filename)
	FIELD(loop)
	FIELD(playOnCreate)
	FIELD(gain)
	FIELD(pitch)
	FIELD(positional)
	FIELD(shouldUpdate)
	FIELD(alwaysUpdate)
	END_CLASS()
};

class SAudioSource
{
public:
	void init(ApplicationContext* appCtx);

	void onUpdate(ECS* ecs, double delta);

	void onCreate(ECS* ecs, Entity entity);
	void onAssetLoad(ECS* ecs, Entity entity);
	void onDestroy(ECS* ecs, Entity entity);

private:
	void updateSource(ECS* ecs, Entity entity, CAudioSource& cA, float delta);

	ApplicationContext* ctx;

	uint32 updateFn;
	uint32 createFn;
	uint32 assetFn;
	uint32 destroyFn;
};

struct CAudioListener
{
	vec3 lastPosition = vec3(0.0f);
	vec3 currentVelocity = vec3(0.0f);
};

class SAudioListener
{
public:
	void init(ApplicationContext* appCtx);
	void onUpdate(ECS* ecs, double delta);

private:
	ApplicationContext* ctx;
	uint32 updateFn;
};

};

#endif //QUASAR_C_AUDIO_H
