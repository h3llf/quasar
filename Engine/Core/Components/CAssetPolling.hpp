#ifndef C_ASSET_POLLING_H
#define C_ASSET_POLLING_H

#include "Core/ApplicationContext.hpp"
#include "Core/Assets/AssetManager.hpp"
#include <unordered_map>

namespace quasar
{

// Assets are polled each tick until all assets have completed
struct CAssetPolling
{
	std::map<std::string, AssetHandle> pollingList;

	void addHandle(const AssetHandle& handle);
	AssetLoadStatus getLoadStatus(const AssetHandle& handle);

	inline bool isLoaded(const AssetHandle& handle)
	{
		return getLoadStatus(handle) == ASSET_LOADED;
	}
};

class SAssetPolling
{
public:
	void init(ApplicationContext* ctx);
	void onUpdate(ECS* ecs, double delta);

	// Returns the component for the given entity, it is created if it doesn't exist
	static CAssetPolling& getCAssetPolling(ECS* ecs, Entity entity);

private:
	ApplicationContext* ctx;
	uint32 updateFn;
};

};

#endif //C_ASSET_POLLING_H
