#ifndef QUASAR_C_MODEL_H
#define QUASAR_C_MODEL_H

#include "Core/Assets/BVH.hpp"
#include "Core/ECS/CallbacksECS.hpp"
#include "Core/ECS/ECS.hpp"
#include "Core/ECS/Reflection.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include "Core/Components/CTransform.hpp"
#include <unordered_map>

namespace quasar
{

struct CModel
{
	std::string modelFolder = "../Assets/";
	std::string modelName = "";
	AssetHandle handle;
	bool hasMeshes = false;

	Model* model = nullptr;

	BEGIN_CLASS()
	FIELD(modelFolder, Reflect::EDITOR_READ_WRITE)
	FIELD(modelName)
	FIELD(handle, Reflect::SERIALIZE_DISABLE)
	FIELD(hasMeshes)
	FIELD(model, Reflect::SERIALIZE_DISABLE)
	END_CLASS()
};


struct CMesh 
{
	//The index of this mesh within the file
	int modelMeshIndex = 0;
	bool visible = true;

	// Bounding box
	vec3 min = vec3(0.0f);
	vec3 max = vec3(0.0f);

	bool previewBVH = false;
	BVHNode* node = nullptr;
};

};
#endif //QUASAR_C_MODEL_H
