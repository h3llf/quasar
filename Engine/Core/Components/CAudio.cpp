#include "CAudio.hpp"
#include "Core/Assets/Audio.hpp"
#include "Core/Components/CAssetPolling.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/ECS/CallbacksECS.hpp"
#include "Core/Globals.hpp"

using namespace quasar;

void SAudioSource::init(ApplicationContext* appCtx)
{
	updateFn = appCtx->cECS->registerUpdateFunction(&SAudioSource::onUpdate, this, UPDATE_ALWAYS);
	createFn = appCtx->cECS->registerListener<CAudioSource>(&SAudioSource::onCreate, this, 
			LISTENER_ON_CREATE | LISTENER_ON_DESERIALIZE);
	createFn = appCtx->cECS->registerListener<CAssetPolling>(&SAudioSource::onAssetLoad, this, LISTENER_ON_ASSET_LOAD);
	createFn = appCtx->cECS->registerListener<CAudioSource>(&SAudioSource::onDestroy, this, LISTENER_ON_DESTROY);

	ctx = appCtx;
}

void SAudioSource::onUpdate(ECS *ecs, double delta)
{
	EntityView view = ecs->view<CAudioSource>();

	for (auto[entity, cA] : view.components()) {
		if (cA.audioSource == -1) {
			continue;
		}

		if (cA.alwaysUpdate || cA.shouldUpdate) {
			if (cA.shouldUpdate) {
				cA.shouldUpdate = false;
			}

			updateSource(ecs, entity, cA, delta);
		}
	}
}

void SAudioSource::onCreate(ECS *ecs, Entity entity)
{
	auto& cA = ecs->get<CAudioSource>(entity);

	if (!cA.filename.empty()) {
		cA.handle = ctx->assetMgr->addAudioLoadTask(cA.filename);

		auto& cAP = SAssetPolling::getCAssetPolling(ecs, entity);
		cAP.addHandle(cA.handle);
	}
}

void SAudioSource::onAssetLoad(ECS *ecs, Entity entity)
{
	if (!ecs->has<CAudioSource>(entity)) {
		return;
	}

	auto& cA = ecs->get<CAudioSource>(entity);
	auto& cAP = SAssetPolling::getCAssetPolling(ecs, entity);
	if (cAP.isLoaded(cA.handle)) {
		Sound* sound = ctx->assetMgr->getSound(cA.handle);
		cA.audioSource = AudioContext::createAudioSource(sound->buffer);
		
		if (cA.playOnCreate) {
			updateSource(ecs, entity, cA, 0.1f);
			AudioContext::playSound(cA.audioSource);

			// Meeds 2 updates for correct velocity
			cA.shouldUpdate = true;
		}
	}
}

void SAudioSource::onDestroy(ECS *ecs, Entity entity)
{
	auto& cA = ecs->get<CAudioSource>(entity);

	if (cA.audioSource != -1) {
		AudioContext::delteAudioSource(cA.audioSource);
	}
}

void SAudioSource::updateSource(ECS* ecs, Entity entity, CAudioSource& cA, float delta)
{
	AudioContext::updateSourceGain(cA.gain, cA.audioSource);
	AudioContext::updateSourcePitch(cA.pitch, cA.audioSource);
	AudioContext::updateSourceLooping(cA.loop, cA.audioSource);

	if (cA.positional && ecs->has<CTransform>(entity)) {
		auto& cT = ecs->get<CTransform>(entity);
		AudioContext::updateSourcePosition(cT.position, cA.audioSource);

		cA.currentVelocity = (cA.lastPosition - cT.position) / delta;
		cA.lastPosition = cT.position;

		AudioContext::updateSourcePosition(cT.position, cA.audioSource);
		AudioContext::updateSourceVelocity(cA.currentVelocity, cA.audioSource);
	}
}

void SAudioListener::init(ApplicationContext *appCtx)
{
	updateFn = appCtx->cECS->registerUpdateFunction(&SAudioListener::onUpdate, this, UPDATE_ALWAYS);
	ctx = appCtx;
}

void SAudioListener::onUpdate(ECS *ecs, double delta)
{
	EntityView view = ecs->view<CAudioListener, CTransform>();

	for (auto[entity, cAL, cT] : view.components()) {
		vec3 pos, s;
		vec4 rot;
		qml::decompose(cT.ctm, pos, s, rot);

		vec3 gForward = globalForward;
		vec3 forward = qml::rotate(gForward, rot).normalize();

		vec3 velocity = (cAL.lastPosition - cT.position) / (float)delta;
		cAL.lastPosition = cT.position;

		// TODO: Update up vector?
		AudioContext::updateListener(pos, velocity, forward, globalUp);
	}
}
