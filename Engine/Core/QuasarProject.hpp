#ifndef QUASAR_PROJECT_H
#define QUASAR_PROJECT_H

#include <string>
#include <unordered_map>
#include <filesystem>

struct ProjectClassDef
{
	std::filesystem::path relativeDir = "";
};

struct ProjectHeader
{
	// Project json
	std::string projectName = "";
	std::string description = "";
	std::string binaryName = "";
	std::string createdOn = "";
	std::unordered_map<std::string, ProjectClassDef> sourceFiles;

	// Runtime only
	bool loaded = false;
	bool shouldClose = false;
	std::filesystem::path directory = "";
	std::filesystem::path canonicalDir = "";
	bool libLoaded = false;
	bool compileFailed = false;
};

#endif //QUASAR_PROJECT_H
