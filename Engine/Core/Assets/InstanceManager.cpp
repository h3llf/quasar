#include "InstanceManager.hpp"
#include <cstddef>
#include <mutex>

using namespace quasar;

std::counting_semaphore<1> InstanceManager::renderWaitSemaphore {0};
std::atomic<bool> InstanceManager::semaphoreSignaled = true;
std::mutex InstanceManager::instanceSync;
InstanceManager::TransferDataSrc InstanceManager::transferSrc;
InstanceManager::TransferDataDst InstanceManager::transferDst[TRANSFER_DESTINATION_COUNT];

uint32 InstanceManager::transferCopyIndex = 0;
bool InstanceManager::transferComplete = false;

uint64 InstanceManager::frameCount = 0;

InstanceID InstanceManager::addMeshInstance(MeshID meshID, const qml::mat4& instance)
{
	std::lock_guard<std::mutex> lck(instanceSync);

	uint32 index = transferSrc.instances[meshID].nextIndex();
	transferSrc.instances[meshID][index] = {
		true,
		instance,
		MaterialProps{}
	};

	return index;
}

void InstanceManager::removeMeshInstance(MeshID meshID, InstanceID instanceID)
{
	std::lock_guard<std::mutex> lck(instanceSync);
	transferSrc.instances[meshID].remove(instanceID);
}

void InstanceManager::setInstanceTransform(MeshID meshID, InstanceID instanceID, const qml::mat4 &instance)
{
	std::lock_guard<std::mutex> lck(instanceSync);
	transferSrc.instances[meshID][instanceID].transform = instance;
}

void InstanceManager::setInstanceMaterialProps(MeshID meshID, InstanceID instanceID, 
		MaterialProps& props, MaterialOptions& options)
{
	std::lock_guard<std::mutex> lck(instanceSync);
	transferSrc.instances[meshID][instanceID].materialProps = props;
	transferSrc.instances[meshID][instanceID].materialOptions = options;
}

InstanceID InstanceManager::addSkinnedMeshInstance(MeshID meshID, const mat4 &instance, const std::vector<mat4> &bones)
{
	return 0;
}

void InstanceManager::setSkeletonPose(InstanceID instanceID, const std::vector<mat4> &bones)
{
};

void InstanceManager::setCameraData(Camera::CameraProperties& camProps, 
		ImageHandle skybox, ImageHandle target, ViewportHandle viewport)
{
	std::lock_guard<std::mutex> lck(instanceSync);
	
	transferSrc.cameras.push_back({camProps, skybox, target, viewport});
}

uint32 InstanceManager::addPointLight(qml::vec3 position, qml::vec3 colour, int shadow, float distance)
{
	float minLuminosity = 1.0f / (distance * distance - 0.1f);
	PointLight newLight = {position, minLuminosity, colour};
	newLight.shadowIndex = shadow - 1;

	std::lock_guard<std::mutex> lck(instanceSync);
	if (transferSrc.freePointLights.size()) {
		uint32 index = transferSrc.freePointLights.front();
		transferSrc.freePointLights.pop();

		transferSrc.pointLights[index].first = true;
		transferSrc.pointLights[index].second = newLight;
		return index;
	} else {
		transferSrc.pointLights.push_back(std::make_pair(true, newLight));
		return transferSrc.pointLights.size() - 1;
	}
}

void InstanceManager::removePointLight(uint32 index)
{
	std::lock_guard<std::mutex> lck(instanceSync);
	transferSrc.freePointLights.push(index);
	transferSrc.pointLights[index].first = false;
}

void InstanceManager::updatePointLight(uint32 index, vec3 position, vec3 colour, bool shadow, float distance)
{
	float minLuminosity = 1.0f / (distance * distance - 0.1f);
	std::lock_guard<std::mutex> lck(instanceSync);
	transferSrc.pointLights[index].second = PointLight{position, minLuminosity, colour, shadow - 1};
}

void InstanceManager::setDirectionalLight(vec3 colour, vec3 rotation, int shadow, float yOffsetMul)
{
	transferSrc.dirLight = {colour, shadow, rotation};
	transferSrc.dirLightYOffset = yOffsetMul;
}

void InstanceManager::resetDirectionalLight()
{
	transferSrc.dirLight = {vec3(0.0f), 0, vec3(0.0f)};
	transferSrc.dirLightYOffset = 0;
}

std::vector<LineVertex>& InstanceManager::getDebugLines()
{
	return transferSrc.debugLines;
}

int InstanceManager::addTextInstance(TextHandle text, const mat4& transform)
{
	std::lock_guard<std::mutex> lck(instanceSync);
	transferSrc.textTransforms.push_back(std::make_pair(text, transform));
	return transferSrc.textTransforms.size() - 1;
}

void InstanceManager::updateTextInstance(int textInstanceID, const mat4& transform)
{
	std::lock_guard<std::mutex> lck(instanceSync);
	transferSrc.textTransforms[textInstanceID].second = transform;
}

void InstanceManager::removeTextInstance(int textInstanceID)
{
	std::lock_guard<std::mutex> lck(instanceSync);
	if (transferSrc.textTransforms.size() > textInstanceID) {
		transferSrc.textTransforms[textInstanceID].first = -1;
	}
}

void InstanceManager::destroyText(TextHandle text)
{
	std::lock_guard<std::mutex> lck(instanceSync);
	transferSrc.removedText.push_back(text);
}

void InstanceManager::reserveMeshIDs(int maxNum)
{
	std::lock_guard<std::mutex> lck(instanceSync);
	if (transferSrc.instances.size() < maxNum) {
		//src
		transferSrc.instances.resize(maxNum);

		//dst
		for (uint32 i = 0; i < TRANSFER_DESTINATION_COUNT; ++i) {
			transferDst[i].instances.resize(maxNum);
		}
	}
}

void InstanceManager::copyTransferDataSrc()
{
	std::lock_guard<std::mutex> lck(instanceSync);

	//Per mesh
	for (uint32 i = 0; i < transferSrc.instances.size(); ++i) {
		size_t insCount = transferSrc.instances[i].count();
		transferDst[transferCopyIndex].instances[i].resize(insCount);

		// Copy mesh instance transforms and material props
		int dstInsIndex = 0;
		for (MeshInstance& instance : transferSrc.instances[i].elements) {
			// Only copy if the instace is in use
			if (instance.visible) {
				transferDst[transferCopyIndex].instances[i][dstInsIndex++] =
					instance;
			}
		}
	}

	//Copy lights if they exist
	uint32 lightSize = transferSrc.pointLights.size() - transferSrc.freePointLights.size();
	transferDst[transferCopyIndex].pointLights.resize(lightSize);

	int dstLightIndex = 0;
	for (std::pair<bool, PointLight>& light : transferSrc.pointLights) {
		if (light.first) {
			transferDst[transferCopyIndex].pointLights[dstLightIndex] = light.second;
			++dstLightIndex;
		}
	}

	transferDst[transferCopyIndex].dirLight  = transferSrc.dirLight;
	transferDst[transferCopyIndex].dirLightYOffset = transferSrc.dirLightYOffset;

	//Camera
	transferDst[transferCopyIndex].cameras = transferSrc.cameras;
	transferSrc.cameras.clear();

	transferComplete = true;

	// Removal instructions
	for (int id : transferSrc.removedImageIDs) {
		transferDst[transferCopyIndex].removedImageIDs.push_back(id);
	}
	transferSrc.removedImageIDs.clear();

	for (TextHandle id : transferSrc.removedText) {
		transferDst[transferCopyIndex].removedText.push_back(id);
	}
	transferSrc.removedText.clear();

	// Debug data
	transferDst[transferCopyIndex].debugLines = transferSrc.debugLines;

	transferDst[transferCopyIndex].textTransforms = transferSrc.textTransforms;

	if (!semaphoreSignaled) {
		renderWaitSemaphore.release();
		semaphoreSignaled = true;
	}
}

void InstanceManager::updateTransferDst()
{
	std::lock_guard<std::mutex> lck(instanceSync);
	if (transferComplete) {
		transferCopyIndex = (transferCopyIndex + 1) % TRANSFER_DESTINATION_COUNT;
		transferComplete = false;
	}

	frameCount++;
}

InstanceManager::TransferDataDst& InstanceManager::getTransferData()
{
	std::lock_guard<std::mutex> lck(instanceSync);
	return transferDst[ (transferCopyIndex - 1) % TRANSFER_DESTINATION_COUNT ];
}

uint64 InstanceManager::getFrameCount()
{
	std::lock_guard<std::mutex> lck(instanceSync);
	return frameCount;
}
