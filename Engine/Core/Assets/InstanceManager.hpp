#ifndef QUASAR_INSTANCEMANAGER_H
#define QUASAR_INSTANCEMANAGER_H

#include "Core/Camera.hpp"
#include "Core/Types.hpp"
#include "GFX/RenderTypes.hpp"
#include <mutex>
#include <semaphore>
#include <string>
#include <queue>

typedef uint32 SkeletalMeshID;
typedef uint32 MeshID;
typedef uint32 InstanceID;

#define TRANSFER_DESTINATION_COUNT 2

struct PointLight
{
	alignas(16) vec3 position;
	alignas(4) float minLuminosity = 0.0f;
	alignas(16) vec3 colour;
	alignas(4) int shadowIndex;
};

struct DirectionalLight
{
	alignas(16) vec3 colour = vec3(0.0f, 0.0f, 0.0f);
	alignas(4) int shadowMapped = 0;
	alignas(16) vec3 rotation = vec3(0.0f, 1.0f, 0.0f);
};

namespace quasar
{

class InstanceManager
{
public:

	struct MaterialProps
	{
		vec4 baseColour = vec4(1.0f, 1.0f, 1.0f, 1.0f);
		vec3 emissiveFactor = vec3(0.0f, 0.0f, 0.0f);
		float metallicFactor = 1.0f;
		float roughnessFactor = 1.0f;
		float normalScale = 1.0f;

		int albedoID;
		int metalRoughID;
		int normalID;
		int emissiveID;
	};

	struct MaterialOptions
	{
		bool selected = false;
		bool doubleSided = false;
	};

	struct MeshInstance
	{
		bool visible = false;
		mat4 transform;
		MaterialProps materialProps;
		MaterialOptions materialOptions;
	};

	struct Joint
	{
		mat4 transform;
	};

	struct SkeletalMeshInstance
	{
		MeshInstance mesh;
		std::vector<Joint> joints;
	};

	struct CameraInstance
	{
		Camera::CameraProperties camProps {};
		int skyboxImg;

		// Viewport index used to set the render target
		int viewportIndex = -1;
	};

	struct TransferDataSrc
	{
		//Indexed by [meshID] [instanceID]
		std::vector<quasar::IndexVector<MeshInstance>> instances;

		// Lighting data
		std::vector<std::pair<bool, PointLight>> pointLights;
		std::queue<uint32> freePointLights;
		DirectionalLight dirLight = {};
		float dirLightYOffset;

		// List of cameras to render this frame
		// Properties, skybox image, target image
		// TODO: Use struct?
		std::vector<std::tuple<Camera::CameraProperties, ImageHandle, ImageHandle, ViewportHandle>> cameras;

		// Renderer asset removal tasks 
		std::vector<ImageHandle> removedImageIDs;
		std::vector<TextHandle> removedText;

		// Debug lines
		std::vector<LineVertex> debugLines;

		// Text object transformations
		std::vector<std::pair<TextHandle, mat4>> textTransforms;
	};

	struct TransferDataDst
	{
		std::vector<std::vector<MeshInstance>> instances;

		std::vector<PointLight> pointLights;

		DirectionalLight dirLight = {};
		float dirLightYOffset;

		// Properties, skybox image, target image
		std::vector<std::tuple<Camera::CameraProperties, ImageHandle, ImageHandle, ViewportHandle>> cameras;

		// Renderer asset removal tasks 
		std::vector<ImageHandle> removedImageIDs;
		std::vector<TextHandle> removedText;

		// Debug lines
		std::vector<LineVertex> debugLines;

		// Text object transformations
		std::vector<std::pair<TextHandle, mat4>> textTransforms;
	};

	//Instanceing TODO: Hidable instances
	static InstanceID addMeshInstance(MeshID meshID, const mat4& instance);
	static void removeMeshInstance(MeshID meshID, InstanceID instanceID);
	static void setInstanceTransform(MeshID meshID, InstanceID instanceID, const qml::mat4& instance);
	static void setInstanceMaterialProps(MeshID meshID, InstanceID instanceID, 
			MaterialProps& props, MaterialOptions& options);

	// Skeletal mesh instances
	static InstanceID addSkinnedMeshInstance(MeshID meshID, const mat4& instance, const std::vector<mat4>& bones);
	static void setSkeletonPose(InstanceID instanceID, const std::vector<mat4>& bones);

	//Camera(s)
	static void setCameraData(Camera::CameraProperties& camProps, ImageHandle skybox, 
			ImageHandle target, ViewportHandle viewport);

	//Lighting
	static uint32 addPointLight(vec3 position, vec3 colour, int shadow, float distance);
	static void removePointLight(uint32 index);
	static void updatePointLight(uint32 index, vec3 position, vec3 colour, bool shadow, float distance);

	// TODO: Miltiple directional lights
	static void setDirectionalLight(vec3 colour, vec3 rotation, int shadow, float yOffsetMul);
	static void resetDirectionalLight();
	
	//Debug
	static std::vector<LineVertex>& getDebugLines();

	// Text
	static int addTextInstance(TextHandle text, const mat4& transform);
	static void updateTextInstance(int textInstanceID, const mat4& transform);
	static void removeTextInstance(int textInstanceID);
	static void destroyText(TextHandle text); // TODO: Move?

	static void reserveMeshIDs(int maxNum);
	
	//Copies tdsrc to tddst TODO: Copy only visible instances 
	//(for frustum culling and other functionality)
	static void copyTransferDataSrc();

	// Increments the transferDst index if transferSrc has been copied
	static void updateTransferDst();

	// Returns a reference to the transferDst at the previous index
	static TransferDataDst& getTransferData();

	// Instructs the renderer to unload an image
	static inline void removeImage(int imageID)
	{
		std::lock_guard<std::mutex> lck(instanceSync);
		transferSrc.removedImageIDs.push_back(imageID);
	}

	// Thread synchronization
	// Make the renderer wait for a new update on the main thread
	// This prevents the renderer effectively drawing duplicate frames & reduces latency
	static std::atomic<bool> semaphoreSignaled;
	static std::counting_semaphore<1> renderWaitSemaphore;

	static std::mutex instanceSync;

	// Increments each time transfer data is complete
	// Allows the main thread to detect when a frame has been drawn renderer side
	static uint64 getFrameCount();

private:
	static uint64 frameCount;

	/* Transfer data is triple buffered to minimize lockups between gameloop and render thread
	 * 1) TransferDataSrc is written to directly by the game loop thread
	 * 2) TransferDataSrc is copied to transferDst at transferCopyIndex 
	 * 3) Increments transfer destination index only if it has been written to, 
	 *    then returns a reference to transferDst at the nex copy index*/
	static TransferDataSrc transferSrc;
	static TransferDataDst transferDst[TRANSFER_DESTINATION_COUNT];

	// The current transfer destination index
	static uint32 transferCopyIndex;

	// Signals that new instance data is avaliable
	static bool transferComplete;

};

};

#endif //QUASAR_INSTANCEMANAGER_H
