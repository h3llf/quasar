#include "BVH.hpp"
#include "Core/Types.hpp"
#include "InstanceManager.hpp"
#include <cassert>
#include <limits>
#include <stack>
#include <queue>

using namespace quasar;

void BVHNode::renderNode(mat4& transform, bool leafOnly)
{
	std::vector<LineVertex>& lines = InstanceManager::getDebugLines();
	
	vec3 c = center;
	vec3 e = extent;

	vec3 v[8] = {
		c + vec3{-e.x, -e.y, -e.z},
		c + vec3{-e.x, -e.y, e.z},
		c + vec3{e.x, -e.y, -e.z},
		c + vec3{e.x, -e.y, e.z},
		c + vec3{-e.x, e.y, -e.z},
		c + vec3{-e.x, e.y, e.z},
		c + vec3{e.x, e.y, -e.z},
		c + vec3{e.x, e.y, e.z},
	};

	uint32 colour = (isLeaf) ? 0xff0000ff : 0xff00ff00;

	for (uint32 i = 0; i < 8; ++i) {
		v[i] = (transform * vec4(v[i], 1.0f)).xyz;
	}

	// Bottom
	lines.push_back({colour, v[0]});
	lines.push_back({colour, v[1]});
	lines.push_back({colour, v[0]});
	lines.push_back({colour, v[2]});
	lines.push_back({colour, v[1]});
	lines.push_back({colour, v[3]});
	lines.push_back({colour, v[2]});
	lines.push_back({colour, v[3]});

	// Sides
	lines.push_back({colour, v[0]});
	lines.push_back({colour, v[4]});
	lines.push_back({colour, v[1]});
	lines.push_back({colour, v[5]});
	lines.push_back({colour, v[2]});
	lines.push_back({colour, v[6]});
	lines.push_back({colour, v[3]});
	lines.push_back({colour, v[7]});

	// Top
	lines.push_back({colour, v[4]});
	lines.push_back({colour, v[5]});
	lines.push_back({colour, v[4]});
	lines.push_back({colour, v[6]});
	lines.push_back({colour, v[5]});
	lines.push_back({colour, v[7]});
	lines.push_back({colour, v[6]});
	lines.push_back({colour, v[7]});

	for (uint32 i = 0; i < children.size(); ++i) {
		children[i].renderNode(transform, leafOnly);
	}
}

void BVH::createTree(Model::Mesh* pMesh, BVHNode* pRoot)
{
	pRoot->center = (pMesh->min + pMesh->max) / 2.0f;
	pRoot->extent = (pMesh->max - pMesh->min) / 2.0f;

	assert(!(pMesh->indices.size() % 3));
	pRoot->triangles.reserve(pMesh->indices.size() / 3);
	for (uint32 i = 0; i < pMesh->indices.size(); i += 3) {
		uint32 i0 = pMesh->indices[i + 0];
		uint32 i1 = pMesh->indices[i + 1];
		uint32 i2 = pMesh->indices[i + 2];

		pRoot->triangles.push_back(Triangle { 
			{
				pMesh->vertices[i0].position,
				pMesh->vertices[i1].position,
				pMesh->vertices[i2].position
			},
			{
				pMesh->vertices[i0].normal,
				pMesh->vertices[i1].normal,
				pMesh->vertices[i2].normal
			}
		});
	}

//	processSplits(pRoot);
}

void BVH::processSplits(BVHNode* pRoot)
{
	std::stack<BVHNode*> nodes;
	nodes.push(pRoot);

	BVHNode* pNode;
	while (nodes.size()) {
		pNode = nodes.top();
		nodes.pop();

		vec3 c = pNode->center;
		vec3 e = pNode->extent;

		// Split the longest side of the box
		pNode->children.reserve(2);
		if (e.x > std::max(e.y, e.z)) {
			e.x /= 2.0f;
			pNode->children.push_back(BVHNode{c + vec3{-e.x, 0.0f, 0.0f}, e, pNode->divisionsLeft - 1});
			pNode->children.push_back(BVHNode{c + vec3{e.x, 0.0f, 0.0f}, e, pNode->divisionsLeft - 1});
		} else if (e.y > e.z) {
			e.y /= 2.0f;
			pNode->children.push_back(BVHNode{c + vec3{0.0f, -e.y, 0.0f}, e, pNode->divisionsLeft - 1});
			pNode->children.push_back(BVHNode{c + vec3{0.0f, e.y, 0.0f}, e, pNode->divisionsLeft - 1});
		} else {
			e.z /= 2.0f;
			pNode->children.push_back(BVHNode{c + vec3{0.0f, 0.0f, -e.z}, e, pNode->divisionsLeft - 1});
			pNode->children.push_back(BVHNode{c + vec3{0.0f, 0.0f, e.z}, e, pNode->divisionsLeft - 1});	
		}

		for (uint32 i = 0; i < 2; ++i) {
			BVHNode& cn = pNode->children[i];

			cn.triangles.reserve(pNode->triangles.size());
			for (uint32 j = 0; j < pNode->triangles.size(); ++j) {
				if (triangleIntersectAABB(pNode->triangles[j].pos, cn.center, cn.extent)) {
					cn.triangles.push_back(pNode->triangles[j]);
				}
			}

			if (!cn.triangles.size()) {
				pNode->children.erase(pNode->children.begin() + i);
			}
		}

		for (BVHNode& cn : pNode->children) {
			uint32 count = cn.triangles.size();
			// Further subdivide
			if (count && cn.divisionsLeft) {
				nodes.push(&cn);
			}
		}

		if (pNode->children.size()) {
			pNode->isLeaf = false;
			pNode->triangles.clear();
		}
	}
}

// TODO: Use less branching
bool BVH::triangleIntersectAABB(vec3 pos[3], vec3 center, vec3 extent)
{
	// Translate to origin
	vec3 v[3] = {
		pos[0] - center,
		pos[1] - center,
		pos[2] - center
	};

	// Calculate edges
	vec3 f[3] = {
		pos[1] - pos[0],
		pos[2] - pos[1],
		pos[0] - pos[2]
	};

	for (uint32 i = 0; i < 3; ++i) {
		vec3 a = vec3(0.0f, -f[i].z, f[i].y);
		float p[3] = {
			qml::dot(v[0], a),
			qml::dot(v[1], a),
			qml::dot(v[2], a)
		};

		float r = extent.y * abs(f[i].z) + extent.z * abs(f[i].y);
		if (std::max(-std::max({p[0], p[1], p[2]}), std::min({p[0], p[1], p[2]})) > r) {
			return false;
		}
	}

	for (uint32 i = 0; i < 3; ++i) {
		vec3 a = vec3(f[i].z, 0.0f, -f[i].x);
		float p[3] = {
			qml::dot(v[0], a),
			qml::dot(v[1], a),
			qml::dot(v[2], a)
		};

		float r = extent.x * abs(f[i].z) + extent.z * abs(f[i].x);
		if (std::max(-std::max({p[0], p[1], p[2]}), std::min({p[0], p[1], p[2]})) > r) {
			return false;
		}
	}

	for (uint32 i = 0; i < 3; ++i) {
		vec3 a = vec3(-f[i].y, f[i].x, 0.0f);
		float p[3] = {
			qml::dot(v[0], a),
			qml::dot(v[1], a),
			qml::dot(v[2], a)
		};

		float r = extent.x * abs(f[i].y) + extent.y * abs(f[i].x);
		if (std::max(-std::max({p[0], p[1], p[2]}), std::min({p[0], p[1], p[2]})) > r) {
			return false;
		}
	}

	for (uint32 i = 0; i < 3; ++i) {
		if (std::max({v[0][i], v[1][i], v[2][i]}) < -extent[i] || 
				std::min({v[0][i], v[1][i], v[2][i]}) > extent[i]) 
		{
			return false;
		}
	}

	vec3 norm = cross(f[0], f[1]);
	float dist = abs(qml::dot(norm, v[0]));

	float r = extent.x * abs(norm.x) + extent.y * abs(norm.y) + extent.z * abs(norm.z);

	if (dist > r) {
		return false;
	}

	return true;

}

// https://tavianator.com/2022/ray_box_boundary.html
bool BVH::rayBoxIntersect(vec3 bmin, vec3 bmax, vec3 origin, vec3 invRay)
{
	float tmin = 0.0f;
	float tmax = std::numeric_limits<float>::max();

	for (uint32 i = 0; i < 3; ++i) {
		float t1 = (bmin[i] - origin[i]) * invRay[i];
		float t2 = (bmax[i] - origin[i]) * invRay[i];

		tmin = std::max(tmin, std::min(t1, t2));
		tmax = std::min(tmax, std::max(t1, t2));
	}

	return tmin <= tmax;
}

const float bias = 0.000001f;
bool BVH::rayTriangleIntersect(vec3 tri[3], vec3 origin, vec3 ray, float* pDist)
{
	ray.toNormalized();
	vec3 e1 = tri[1] - tri[0];
	vec3 e2 = tri[2] - tri[1];
	vec3 h = qml::cross(ray, e2);
	float a = qml::dot(e1, h);

	if (a > -bias && a < bias)
		return false;

	float f = 1.0f / a;

	vec3 s = origin - tri[0];
	float u = qml::dot(s, h) * f;
	if (u < 0.0f || u > 1.0f) {
		return false;
	}

	vec3 q = qml::cross(s, e1);
	float v = qml::dot(ray, q) * f;
//	if (v < 0.0f || u + v > 1.0f) {
	if (v < 0.0f || u - v < 0.0f) {
		return false;
	}

	float t = qml::dot(e2, q) * f;
	if (t > bias) {
		*pDist = t;
		return true;
	}

	return false;
}

bool BVH::raycastBVH(BVHNode *root, vec3 origin, vec3 invRay, vec3 ray, float* pDist)
{
	vec3 min = root->center - root->extent;
	vec3 max = root->center + root->extent;

	if (!rayBoxIntersect(min, max, origin, invRay)) {
		return false;
	}

	for (uint32 i = 0; i < root->children.size(); ++i) {
		if (raycastBVH(&root->children[i], origin, invRay, ray, pDist)) {
			return true;
		}
	}

	bool hit = false;
	float hitDist = std::numeric_limits<float>::max();
	for (uint32 i = 0; i < root->triangles.size(); ++i) {
		float dist = std::numeric_limits<float>::max();
		if (rayTriangleIntersect(root->triangles[i].pos, origin, ray, &dist)) {
			if (dist > 0.0f && dist < hitDist) {
				hitDist = dist;
				hit = true;
			}
		}
	}

	if (hit) {
		*pDist = hitDist;
		return true;
	}

	return false;
}

