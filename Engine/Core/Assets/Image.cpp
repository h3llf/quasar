#include "Image.hpp"
#include "GFX/Renderer.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <algorithm>
#include <filesystem>
#include <stdlib.h>
#include <immintrin.h>
#include <emmintrin.h>

using namespace quasar;

Image::Image() { }

Image::~Image() {};

void Image::loadImage(std::string fileName)
{
	//replace back slashes
	std::replace(fileName.begin(), fileName.end(), '\\', '/');

	// Check that the image exists
	if (std::filesystem::exists(fileName)) {
		pixels = stbi_load(fileName.c_str(), &width, &height, 
				&channels, STBI_rgb_alpha);
	} else {
		Log::error("Failed to load image at: ", fileName);

		generateDefault();
	}

	this->fileName = fileName;
}

void Image::loadImage16f(std::string fileName)
{
	//replace back slashes
	std::replace(fileName.begin(), fileName.end(), '\\', '/');

	// Check that the image exists
	if (std::filesystem::exists(fileName)) {
		float* px = stbi_loadf(fileName.c_str(), &width, &height, 
				&channels, STBI_rgb_alpha);
		uint32 pixelCount = width * height * 4;
		pixels = new uint16_t[pixelCount + 8];

		// TODO: Check alignment
		// Convert image data to half precision for the GPU format
		for (uint32 i = 0; i < pixelCount; i += 8) {
			const __m256 input = _mm256_loadu_ps(&px[i]);
			__m128i res = _mm256_cvtps_ph(input, 0);
			void* dst = &reinterpret_cast<uint16_t*>(pixels)[i];
			_mm_storeu_si128(reinterpret_cast<__m128i*>(dst), res);
		}
	} else {
		Log::error("Failed to load image at: ", fileName);
	}

	this->fileName = fileName;
}

void Image::generateDefault()
{
	// Create a missing texture image
	const int blankSize = 128;
	const int freq = 8;

	width = blankSize;
	height = blankSize;
	channels = 4;
	unsigned char* newPx = new unsigned char[blankSize * blankSize * 4];
	pixels = newPx;

	for (uint32 i = 0; i < blankSize; ++i) {
		uint32 v = i * freq / blankSize;
		for (uint32 j = 0; j < blankSize * 4; j += 4) {
			uint32 h = j / 4 * freq / blankSize;

			if ((v + h) % 2) {
				newPx[(i * blankSize * 4) + j + 0] = 255;
				newPx[(i * blankSize * 4) + j + 1] = 20;
				newPx[(i * blankSize * 4) + j + 2] = 147;
				newPx[(i * blankSize * 4) + j + 3] = 255;
			} else {
				newPx[(i * blankSize * 4) + j + 0] = 0;
				newPx[(i * blankSize * 4) + j + 1] = 0;
				newPx[(i * blankSize * 4) + j + 2] = 0;
				newPx[(i * blankSize * 4) + j + 3] = 255;
			}
		}
	}
}

void Image::freePixels()
{
	stbi_image_free(pixels);
}

void Image::freePixels16f()
{
	delete[] reinterpret_cast<uint16_t*>(pixels);
}
