#include "Font.hpp"
#include "Core/Assets/Image.hpp"
#include "GFX/RenderTypes.hpp"
#include "GFX/Renderer.hpp"
#include "Utils/Log.hpp"
#include "msdf-atlas-gen/msdf-atlas-gen.h"

using namespace quasar;
using namespace msdf_atlas;

void submitAtlas(msdfgen::BitmapConstRef<unsigned char, 4> pixelData, ImageHandle& atlasImage)
{
	const unsigned char* pixels = pixelData(0, 0);

	Image tmpImg {};
	tmpImg.width = pixelData.width;
	tmpImg.height = pixelData.height;
	tmpImg.pixels = (void*)pixels;
	tmpImg.imageUsage = Image::Albedo;
	atlasImage = Renderer::uploadImage(&tmpImg);
}

bool TextFont::loadFont(const std::string &path, int minGlyphSize)
{
	std::vector<msdf_atlas::GlyphGeometry> glyphGeom;

	msdfgen::FreetypeHandle* ft = msdfgen::initializeFreetype();
	if (!ft) {
		Log::error("Failed to intitialize font loader");
		return false;
	}

	msdfgen::FontHandle* font = msdfgen::loadFont(ft, path.c_str());
	if (!font) {
		msdfgen::deinitializeFreetype(ft);
		return false;
	}

	FontGeometry fontGeometry(&glyphGeom);
	fontGeometry.loadCharset(font, 1.0, Charset::ASCII);
	lineHeight = fontGeometry.getMetrics().lineHeight;
	kerning = fontGeometry.getKerning();

	const double maxCornerAngle = 3.0;
	for (GlyphGeometry &glyph : glyphGeom) {
		glyph.edgeColoring(&msdfgen::edgeColoringInkTrap, maxCornerAngle, 0);
	}

	TightAtlasPacker packer;
	packer.setDimensionsConstraint(TightAtlasPacker::DimensionsConstraint::POWER_OF_TWO_SQUARE);
	packer.setMinimumScale(static_cast<float>(minGlyphSize));
	packer.setPixelRange(2.0);
	packer.setMiterLimit(4.0);
	packer.pack(glyphGeom.data(), glyphGeom.size());
	packer.getDimensions(width, height);
	glyphScale = packer.getScale();

	if (width == 0 || height == 0) {
		msdfgen::destroyFont(font);
		msdfgen::deinitializeFreetype(ft);
		return false;
	}

	ImmediateAtlasGenerator<float, 4, &mtsdfGenerator, BitmapAtlasStorage<byte, 4>> generator(width, height);

	GeneratorAttributes attributes;
	generator.setAttributes(attributes);
	generator.setThreadCount(2);
	generator.generate(glyphGeom.data(), glyphGeom.size());

	submitAtlas(generator.atlasStorage(), atlasImage);

	glyphIndices.resize(256, -1);
	glyphs.reserve(glyphGeom.size());
	for (GlyphGeometry& glyph : glyphGeom) {
		if (glyph.getCodepoint() > glyphIndices.size()) {
			Log::error("Glyph index out of range. Index: ", glyph.getCodepoint(), " Size: ", glyphIndices.size());
			continue;
		}

		int glyphIndex = glyph.getIndex();
		glyphIndices[glyph.getCodepoint()] = glyphIndex;
		GlyphDescription desc;
		desc.character = glyph.getCodepoint();
		desc.advance = glyph.getAdvance();
		desc.scale = glyph.getBoxScale();
		glyph.getBoxRect(desc.x, desc.y, desc.w, desc.h);
		desc.tx = glyph.getBoxTranslate().x;
		desc.ty = glyph.getBoxTranslate().y;
	
		if (glyphs.size() <= glyphIndex) {
			glyphs.resize(glyphIndex + 1);
		}
		glyphs[glyphIndex] = desc;
	}

	msdfgen::destroyFont(font);
	msdfgen::deinitializeFreetype(ft);

	fontHandle = Renderer::uploadFont(this);

	return true;
}
