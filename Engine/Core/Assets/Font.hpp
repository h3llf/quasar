#ifndef QUASAR_FONT_H
#define QUASAR_FONT_H

#include "GFX/RenderTypes.hpp"
#include <vector>
#include <string>
#include <map>

namespace quasar
{

class TextFont
{
public:
	struct GlyphDescription
	{
		char32_t character;

		// Rect
		int x, y, w, h;

		// Offset of the next glyph
		float advance;

		// Glyph scale
		float scale;

		// Translation in glyph units
		float tx, ty;
	};

	std::vector<GlyphDescription> glyphs;
	ImageHandle atlasImage;
	FontHandle fontHandle;

	bool loadFont(const std::string& path, int minGlyphSize);

	// Maps utf-8 values to glyph indices
	// TODO: Full unicode support (currently limited to 256 range)
	std::vector<int> glyphIndices;
	std::map<std::pair<int, int>, double> kerning;

	double lineHeight;
	double glyphScale;
	int width = 0;
	int height = 0;
};

};

#endif //QUASAR_FONT_H
