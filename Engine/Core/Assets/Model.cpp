#include "Model.hpp"
#include "Utils/Math/Transform.hpp"
#include <cstddef>
#include <thread>
#include <simple_glTF/simple_glTF.hpp>
#include <algorithm>
#include <fstream>

#ifndef _HEADLESS
#include "GFX/Renderer.hpp"
#endif //_HEADLESS

using namespace quasar;

Model::Model() {}
Model::~Model() {}

Model::Model(const std::string& path, const std::string& filename)
{
	loadGLTF(path, filename);
}

qml::mat4 ident = qml::mat4::identity();

bool Model::loadGLTF(const std::string& path, const std::string& filename)
{
	modelPath = path;
	modelName = filename;

	if (!gltf.readModelInformation(path, filename))
		return false;

	gltf.setVertexLayout(offsetof(Vertex, position),
			offsetof(Vertex, normal),
			offsetof(Vertex, tangent),
			offsetof(Vertex, uv));

	meshCount = gltf.primitives.size();
	
	// Retrieve material info
	defaultMaterials.resize(gltf.materials.size());
	for (uint32 i = 0; i < defaultMaterials.size(); ++i) {
		simple_glTF::Material& mat = gltf.materials[i];

		if (mat.albedoIndex > -1) {
			if (mat.albedoIndex < gltf.textureNames.size()) {
				defaultMaterials[i].textureNames.push_back(
						std::make_pair(Albedo,
							path + gltf.textureNames[mat.albedoIndex]));
			} else {
				defaultMaterials[i].textureNames.push_back(
						std::make_pair(Albedo, "MissingTexture"));
			}
		}
		if (mat.PBRIndex > -1) {
			if (mat.PBRIndex < gltf.textureNames.size()) {
				defaultMaterials[i].textureNames.push_back(
						std::make_pair(MetallicRoughness, 
							path + gltf.textureNames[mat.PBRIndex]));
			} else {
				defaultMaterials[i].textureNames.push_back(
						std::make_pair(MetallicRoughness, "MissingTexture"));
			}
		}
		if (mat.normalIndex > -1) {
			if (mat.normalIndex < gltf.textureNames.size()) {
				defaultMaterials[i].textureNames.push_back(
						std::make_pair(Normal, 
							path + gltf.textureNames[mat.normalIndex]));
			} else {
				defaultMaterials[i].textureNames.push_back(
						std::make_pair(Normal, "MissingTexture"));
			}
		}
		if (mat.emissiveIndex > -1) {
			if (mat.emissiveIndex < gltf.textureNames.size()) {
				defaultMaterials[i].textureNames.push_back(
						std::make_pair(Emissive, 
							path + gltf.textureNames[mat.emissiveIndex]));
			} else {
				defaultMaterials[i].textureNames.push_back(
						std::make_pair(Emissive, "MissingTexture"));
			}
		}

		defaultMaterials[i].albedoColour = *reinterpret_cast<vec4*>(mat.baseColourFactor);
		defaultMaterials[i].emissiveFactor = *reinterpret_cast<vec3*>(mat.emissiveFactor);
		defaultMaterials[i].metallic = mat.metallicFactor;
		defaultMaterials[i].roughness = mat.roughnessFactor;
		defaultMaterials[i].normalScale = mat.normalScale;
		defaultMaterials[i].doubleSided = mat.doubleSided;
	}

	// Get geometry info
	meshes.resize(gltf.primitives.size());
	for (uint32 i = 0; i < gltf.primitives.size(); ++i) {
		meshes[i] = Mesh();
		meshes[i].hasTangents = (gltf.primitives[i].tangent != -1);
		simple_glTF::Primitive p = gltf.primitives[i];
		gltf.setMeshIndices(&p, &meshes[i].indices);
		gltf.setMeshVertices(&p, &meshes[i].vertices);
		gltf.setMeshBounds(&p, &meshes[i].min[0], &meshes[i].max[0]);
		calculateTangents(&meshes[i]);
		meshes[i].materialIndex = p.materialIndex;

		// Load skeleton data is avaliable
		meshes[i].rigged = gltf.setVertexJoints(&p, &meshes[i].vertexJoints);

#ifndef _HEADLESS
		meshes[i].rendererID = Renderer::uploadMesh(&meshes[i]);
#endif //_HEADLESS
	}

	// Get skinning info
	skins.resize(gltf.skins.size());
	for (const simple_glTF::Skin& s : gltf.skins) {
		Skin newSkin {};
		
		// Get inverse bind matrices
		newSkin.inverseBindPose.resize(s.nodeJoint.size() * 4);
		gltf.setInverseBindMatrices(&s, newSkin.inverseBindPose[0].dataArray);

		// Indices into scene nodes
		newSkin.skinName = s.name;
		for (uint32 i = 0; i < s.nodeJoint.size(); ++i) {
			newSkin.indices.push_back(s.nodeJoint[i]);
		}
	}

	return true;
}

void Model::calculateTangents(Mesh *mesh)
{
	// TODO: Compute tangent space handedness
	if (!mesh->hasTangents) {
		for (uint32 i = 0; i < mesh->indices.size(); i += 3) {
			uint32 in[3];
			in[0] = mesh->indices[i];
			in[1] = mesh->indices[i + 1];
			in[2] = mesh->indices[i + 2];

			qml::vec3 edge1 = mesh->vertices[in[1]].position - mesh->vertices[in[0]].position;
			qml::vec3 edge2 = mesh->vertices[in[2]].position - mesh->vertices[in[0]].position;
			qml::vec2 deltaUV1 = mesh->vertices[in[1]].uv - mesh->vertices[in[0]].uv;
			qml::vec2 deltaUV2 = mesh->vertices[in[2]].uv - mesh->vertices[in[0]].uv;

			float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);
			qml::vec4 tangent = {0.0f, 0.0f, 0.0f, 1.0f};
			tangent = vec4(f * (deltaUV2.y * edge1 - deltaUV1.y * edge2), 1.0f);

			mesh->vertices[in[0]].tangent += tangent;
			mesh->vertices[in[1]].tangent += tangent;
			mesh->vertices[in[2]].tangent += tangent;
		}
	}

	for (Vertex& vertex : mesh->vertices) {
		vertex.tangent.toNormalized();
	}
}
