#ifndef QUASAR_BVH_H
#define QUASAR_BVH_H

#include "Core/Types.hpp"
#include "Model.hpp"
#include <vector>

namespace quasar
{

struct Triangle
{
	vec3 pos[3];
	vec3 norm[3];
};

class BVHNode
{
public:
	// AABB
	vec3 center;
	vec3 extent;
	int divisionsLeft = 5;

	std::vector<BVHNode> children;
	std::vector<Triangle> triangles;

	// Defuaults to a leaf until split
	bool isLeaf = true;

	void renderNode(mat4& transform, bool leafOnly);
};

class BVH
{
public:
	// Generates a BVH tree from a given models mesh
	static void createTree(Model::Mesh* pMesh, BVHNode* pRoot);

	static bool rayBoxIntersect(vec3 bmin, vec3 bmax, vec3 origin, vec3 invRay);
	static bool rayTriangleIntersect(vec3 triangle[3], vec3 origin, vec3 ray, float* pDist);
	static bool raycastBVH(BVHNode* root, vec3 origin, vec3 invRay, vec3 ray, float* pDist);

	static bool triangleIntersectAABB(vec3 pos[3], vec3 center, vec3 extent);
private:
	static void processSplits(BVHNode* pRoot);
};

};

#endif //QUASAR_BVH_H
