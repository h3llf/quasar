#include "AssetQueue.hpp"

using namespace quasar;

void AssetQueue::stopQueue()
{
	running = false;

	taskLock.lock();
	cv.notify_all();
	taskLock.unlock();
}

TaskType AssetQueue::getNextTask()
{
	TaskType ret;
	ret.path.clear();

	std::unique_lock<std::mutex> lck(taskLock);
	cv.wait(lck, [this]() {
			return !fileLoadList.empty() || !running;
		});

	if (running) {
		ret = fileLoadList.front();
		fileLoadList.pop();
	}

	return ret;
}

void AssetQueue::addNewTask(const TaskType& task)
{
	std::lock_guard<std::mutex> lck(taskLock);
	fileLoadList.push(task);
	cv.notify_one();	
}
