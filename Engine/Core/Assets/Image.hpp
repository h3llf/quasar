#ifndef QUASAR_IMAGE_H
#define QUASAR_IMAGE_H

#include "Core/Types.hpp"
#include "GFX/RenderTypes.hpp"
#include <string>
#include <unordered_map>

namespace quasar
{

class Image
{
public:
	Image();
	~Image();

	int useCount = 0;
	int width = 0;
	int height = 0;
	int channels = 0;
	std::string fileName = "";
	ImageHandle rendererImageID;
	void* pixels = nullptr;

	//material types not yet used by any shaders will be ignored
	enum ImageUsage 
	{
		Albedo,
		MetallicRoughness,
		Emissive,
		Normal,
		HDR,
		Blank
	} imageUsage;

	void loadImage(std::string fileName);
	void freePixels();
	void generateDefault();

	void loadImage16f(std::string fileName);
	void freePixels16f();
private:
};

};
#endif //QUASAR_IMAGE_H
