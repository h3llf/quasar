#ifndef QUASAR_AUDIO_H
#define QUASAR_AUDIO_H

#include "Core/Assets/AssetQueue.hpp"
#include "Utils/Math/Vector.hpp"
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <string>
#include <AL/al.h>
#include <AL/alc.h>
#include <thread>

// TODO: Make a generic task queue container to share with the asset manager

namespace quasar
{

typedef int AudioSourceHandle;

class Sound;

class AudioContext
{
public:
	static void initContext();
	static void deinitContext();

	static void uploadAudioData(Sound* sound);
	static AudioSourceHandle createAudioSource(const Sound& sound);
	static AudioSourceHandle createAudioSource(ALuint buffer);
	static void playSound(AudioSourceHandle handle);
	static void stopSound(AudioSourceHandle handle);
	static void delteAudioSource(AudioSourceHandle handle);

	// Update source parametes
	static void updateSourcePitch(const float value, AudioSourceHandle handle);
	static void updateSourceGain(const float value, AudioSourceHandle handle);
	static void updateSourcePosition(const qml::vec3 value, AudioSourceHandle handle);
	static void updateSourceVelocity(const qml::vec3 value, AudioSourceHandle handle);
	static void updateSourceLooping(const bool value, AudioSourceHandle handle);

	static void updateListener(const qml::vec3 pos, const qml::vec3 vel, 
			const qml::vec3 front, const qml::vec3 up);

	static std::mutex audioLock;
private:
	enum TaskType
	{
		PLAY_SOUND,
		STOP_SOUND,
		DELETE_SOURCE,
		UPDATE_SRC_PARAMETER_3F,
		UPDATE_SRC_PARAMETER_F,
		UPDATE_SRC_PARAMETER_I,
		UPDATE_LIST_PARAMETER,
		UPDATE_LIST_PARAMETER_2,
		INVALID_TASK
	};

	struct Task
	{
		TaskType type = INVALID_TASK;
		AudioSourceHandle handle = -1;
		ALuint paramType = 0;

		float vf[6] = {0.0f};
		int vi = 0;
	};

	static void startAudioThread();
	static std::thread audioThread;

	static ALCdevice* device;
	static ALCcontext* context;

	static std::vector<ALuint> sources;
	static std::queue<AudioSourceHandle> freeSources;
	static std::atomic<bool> running;

	// Task queue management
	static void addTask(const Task& task);
	static Task getTask();

	static std::mutex queueLock;
	static std::queue<Task> taskQueue;
	static std::condition_variable cv;

};

class Sound
{
public:
	bool loadWaveform(const std::string& filename);
	bool loadVorbis(const std::string& filename);

	uint32_t sampleRate;
	int samplesPerChannel;
	int bitDepth;
	bool isMono = false;
	ALuint buffer;
};

};
#endif // QUASAR_AUDIO_H
