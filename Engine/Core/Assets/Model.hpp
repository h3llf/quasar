#ifndef QUASAR_MODEL_H
#define QUASAR_MODEL_H

#include "Core/Types.hpp"
#include "Image.hpp"
#include "simple_glTF/simple_glTF.hpp"
#include <vector>
#include <string>

#include <mutex>

namespace quasar
{

class Model
{
public:
	enum ImageUsage 
	{
		Albedo,
		MetallicRoughness,
		Emissive,
		Normal
	};

	struct MaterialInfo
	{
		std::vector<std::pair<ImageUsage, std::string>> textureNames;

		qml::vec4 albedoColour = vec4(1.0f);
		qml::vec3 emissiveFactor = vec3(0.0f);
		float normalScale = 1.0f;
		float metallic = 0.0f;
		float roughness = 0.0f;

		bool doubleSided = false;
	};

	/*
	// Skeleton
	struct Joint
	{
		std::string name;
		int parent = -1;
//		mat4 localPose;
		mat4 inverseBindPose;
	};
	
	struct Skeleton
	{
		std::string name;
		std::vector<Joint> joints;
//		mat4 globalPose;
	};*/

	struct Skin
	{
		std::string skinName;

		// Joints
		std::vector<int> indices;
		std::vector<mat4> inverseBindPose;
	};

	// Amation
	struct Keyframe
	{
		float time;
		std::vector<mat4> boneTransforms;
	};

	struct Animation
	{
		std::string name;
		float duration;
		std::vector<Keyframe> keyFrames;
	};

	// Joint weights
	// Limited to 4 bones per vertex
	struct JointVertex
	{
		// Index of joint array in skin
		qml::vectorType<uint16_t, 4> jointIDs = {};
		vec4 weights;
	};

	struct Mesh
	{
		bool hasTangents = false;
		std::vector<Vertex> vertices;
		std::vector<JointVertex> vertexJoints;
		std::vector<uint32> indices;

		vec3 min;
		vec3 max;

		int materialIndex;
		int rendererID = -1;

		bool rigged = false;
		int animationIndex = -1;
		int skeletonIndex = -1;
	};

	Model(const std::string& path, const std::string& filename);
	Model();
	~Model();
	
	bool loadGLTF(const std::string& path, const std::string& fileName);
	void loadMaterial(const std::string& path, simple_glTF::Material& m, simple_glTF& gltf, uint32 index);
	void calculateTangents(Mesh* mesh);

	std::vector<MaterialInfo> defaultMaterials;
	std::vector<Mesh> meshes;
	std::vector<Animation> animations;
	std::vector<Skin> skins;
	int meshCount = 0;
	int modelID = 0;

	simple_glTF gltf;

	std::string modelPath = "";
	std::string modelName = "";

	//TODO: Reference count will be needed to determine if model is safe to unload
	
	// TODO: Basic glTF scene information including mesh instances and lights
};

};

#endif //QUASAR_MODEL_H
