#ifndef QUASAR_ASSETMANAGER_H
#define QUASAR_ASSETMANAGER_H

#include "InstanceManager.hpp"
#include "AssetQueue.hpp"
#include "BVH.hpp"
#include "Model.hpp"
#include "Image.hpp"
#include "Font.hpp"
#include "Audio.hpp"
#include "Core/Types.hpp"
#include <queue>
#include <string>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <thread>
#include <unordered_map>

namespace quasar
{

enum AssetLoadStatus
{
	ASSET_NOT_LOADED, ASSET_QUEUED, ASSET_IN_PROGRESS, ASSET_LOADED, ASSET_LOAD_FAILED
};


// Handle for polling and retrieval of assets
struct AssetHandle
{
	std::string identifier = "";
	AssetLoadStatus loadStatus = ASSET_NOT_LOADED;
	AssetType type = ASSET_TYPE_INVALID_ASSET;
};

class AssetManager
{
public:
	// Starts asset loader threads
	void start(uint32 threadCount);
	void stop();

	// Adds loader/creation tasks to be executed by the loader threads
	AssetHandle addModelLoadTask(const std::string& folder, const std::string& filename);
	AssetHandle addImageLoadTask(std::string path, Image::ImageUsage usage);
	AssetHandle addBlankImageTask(const std::string& identifier, int width, int height);
	AssetHandle addFontLoadTask(const std::string& filename, int minGlyphSize = 32);
	AssetHandle addAudioLoadTask(const std::string& filename);

	// Immediate creation (no task)
	ViewportHandle addViewport(const std::string& identifier, int width, int height);
	void removeViewport(const std::string& identifier);
	void removeViewport(ViewportHandle viewport);

	// Commandline versions
	void addBlankImageTaskCmd(const std::string identifier, int width, int height);
	void addViewportcmd(const std::string identifier, int width, int height);

	// Generates a collider from a model file, Folder and filename should be the same as the origninal model
	// The model will be loaded automatically if needed.
	AssetHandle addMeshColliderLoadTask(const std::string& folder, const std::string& filename, 
			int meshIndex, bool isConvex);

	//Returns Model* once the load task is complete
	Model* getModel(const std::string& path);
	BVHNode* getBVHNode(const std::string& path, int index);

	Image* getImage(const AssetHandle& asset);
	Image* getImage(const std::string& identifier);
	TextFont* getFont(const AssetHandle& asset);
	TextFont* getFont(const std::string& identifier);
	ViewportHandle getViewport(const std::string& identifier);
	Sound* getSound(const AssetHandle& asset);
	Sound* getSound(const std::string& identifier);

	int getMeshColliderID(const AssetHandle& AssetHandle);

	// Updates load status within the handle
	void queryLoadStatus(AssetHandle& asset);

	// Querys load status without locking any mutexes
	void queryLoadStatusUnlocked(AssetHandle& asset);

	// Returns all loaded image names and rendererIDs
	void getImageList(std::vector<std::pair<std::string, ImageHandle>>* pImages);
	void getModelList(std::vector<std::pair<std::string, int>>* pModels);
	void getFontList(std::vector<std::pair<std::string, FontHandle>>* pFonts);
	void getViewports(std::vector<std::pair<std::string, ViewportHandle>>* pViewports);
	void getAudioList(std::vector<std::string>* pSounds);

	std::mutex assetManagerSync;

private:
	void updateLoadStatus(AssetHandle& handle, AssetType type);

	static void loaderLoop(AssetManager* assetManager, AssetQueue* queue);

	std::unordered_map<std::string, Model*> models;
	std::unordered_map<std::string, std::vector<BVHNode>> BVHTrees;
	std::unordered_map<std::string, Image> images;
	std::unordered_map<std::string, TextFont> fonts;
	std::unordered_map<std::string, int> meshColliderIDs;
	std::unordered_map<std::string, ViewportHandle> viewports;
	std::unordered_map<std::string, Sound> audioFiles;

	// TODO: Rename from file
	std::vector<std::unordered_map<std::string, AssetLoadStatus>> fileLoadStatus;

	void loadModelInternal(const std::string& path, const std::string& filename);
	void loadImageInternal(const std::string& path);
	void loadTriangleMeshInternal(const std::string& path, int meshIndex);
	void loadConvexMeshInternal(const std::string& path, int meshIndex);
	int generateColliderInternal(const std::string& path, int meshIndex, bool isConvex);
	void loadFont(const std::string& path, int minGlyphSize);
	void loadAudioFile(const std::string& path);

	std::vector<std::thread> threadPool;
	std::atomic<bool> managerRunning;

	AssetQueue assetQueue;
};

};

#endif //QUASAR_ASSETMANAGER_H

