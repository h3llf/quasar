#include "Audio.hpp"
#include <AudioFile.h>
#include <stb_vorbis.c>
#include <filesystem>
#include <thread>
#include "AL/al.h"
#include "AL/alc.h"
#include "AL/alext.h"
#include "Utils/Log.hpp"

using namespace std::chrono_literals;

using namespace quasar;
namespace fs = std::filesystem;

ALCdevice* AudioContext::device = nullptr;
ALCcontext* AudioContext::context = nullptr;
std::thread AudioContext::audioThread;
std::vector<ALuint> AudioContext::sources;
std::queue<AudioSourceHandle> AudioContext::freeSources;
std::atomic<bool> AudioContext::running = true;
std::mutex AudioContext::audioLock;

std::mutex AudioContext::queueLock;
std::queue<AudioContext::Task> AudioContext::taskQueue;
std::condition_variable AudioContext::cv;

void AudioContext::initContext()
{
	device = alcOpenDevice(NULL);
	if (device == NULL) {
		Log::error("Failed to open default OpenAL audio device");
		return;
	}

	context = alcCreateContext(device, NULL);
	if (context == NULL) {
		Log::error("Failed to create OpenAL context");
		return;
	}

	if (!alcMakeContextCurrent(context)) {
		Log::error("Failed to set OpenAL context");
		return;
	}

	const ALCchar* deviceList = alcGetString(NULL, ALC_ALL_DEVICES_SPECIFIER);
	if (deviceList == NULL) {
		Log::error("No audio devices connected");
		return;
	}

	std::vector<std::string> deviceNames;
	const ALCchar* device = deviceList;
	uint32 count = 0;
	while (*device != '\0') {
		deviceNames.push_back(std::string(device));
		device += strlen(device) + 1;
	}

	running = true;

	// Default listner
	ALfloat lp[] = { 0.0f, 0.0f, 0.0f };
	ALfloat lv[] = { 0.0f, 0.0f, 0.0f };
	ALfloat lr[] = { 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f };
	alListenerfv(AL_POSITION, lp);
	alListenerfv(AL_VELOCITY, lv);
	alListenerfv(AL_ORIENTATION, lr);

	alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);

	audioThread = std::thread(startAudioThread);
}

void AudioContext::deinitContext()
{
	if (running) {
		running = false;

		queueLock.lock();
		cv.notify_all();
		queueLock.unlock();

		audioThread.join();
	}
}

AudioSourceHandle AudioContext::createAudioSource(const Sound& sound)
{
	return createAudioSource(sound.buffer);
}

AudioSourceHandle AudioContext::createAudioSource(ALuint buffer)
{
	std::lock_guard<std::mutex> lck(audioLock);
	ALuint source;
	alGenSources(1, &source);
	alSourcef(source, AL_PITCH, 1.0f);
	alSourcef(source, AL_GAIN, 1.0f);
	alSource3f(source, AL_POSITION, 0.0f, 0.0f, 0.0f);
	alSource3f(source, AL_VELOCITY, 0.0f, 0.0f, 0.0f);
	alSource3f(source, AL_ORIENTATION, -1.0f, 0.0f, 0.0f);
	alSourcei(source, AL_LOOPING, AL_FALSE);
	alSourcei(source, AL_BUFFER, buffer);

	AudioSourceHandle handle = -1;
	if (freeSources.size()) {
		handle = freeSources.front();
		freeSources.pop();
		sources[handle] = source;
	} else {
		handle = sources.size();
		sources.push_back(source);
	}

	return handle;
}

void AudioContext::playSound(AudioSourceHandle handle)
{
	Task task = {
		.type = PLAY_SOUND,
		.handle = handle,
	};
	addTask(task);
}

void AudioContext::stopSound(AudioSourceHandle handle)
{
	Task task = {
		.type = STOP_SOUND,
		.handle = handle
	};
	addTask(task);
}

void AudioContext::delteAudioSource(AudioSourceHandle handle)
{
	Task task = {
		.type = DELETE_SOURCE,
		.handle = handle,
	};
	addTask(task);
}

void AudioContext::updateSourcePitch(const float value, AudioSourceHandle handle)
{
	Task task = {
		.type = UPDATE_SRC_PARAMETER_F,
		.handle = handle,
		.paramType = AL_PITCH,
	};
	task.vf[0] = value;
	addTask(task);
}

void AudioContext::updateSourceGain(const float value, AudioSourceHandle handle)
{
	Task task = {
		.type = UPDATE_SRC_PARAMETER_F,
		.handle = handle,
		.paramType = AL_GAIN,
	};
	task.vf[0] = value;
	addTask(task);
}

void AudioContext::updateSourcePosition(const qml::vec3 value, AudioSourceHandle handle)
{
	Task task = {
		.type = UPDATE_SRC_PARAMETER_3F,
		.handle = handle,
		.paramType = AL_POSITION,
		.vf = {value.data[0], value.data[1], -value.data[2]},
	};
	addTask(task);
}

void AudioContext::updateSourceVelocity(const qml::vec3 value, AudioSourceHandle handle)
{
	Task task = {
		.type = UPDATE_SRC_PARAMETER_3F,
		.handle = handle,
		.paramType = AL_VELOCITY,
		.vf = {value.data[0], value.data[1], -value.data[2]},
	};
	addTask(task);
}

void AudioContext::updateSourceLooping(const bool value, AudioSourceHandle handle)
{
	Task task = {
		.type = UPDATE_SRC_PARAMETER_I,
		.handle = handle,
		.paramType = AL_LOOPING,
		.vi = value
	};
	addTask(task);
}

void AudioContext::updateListener(const qml::vec3 pos, const qml::vec3 vel, 
		const qml::vec3 front, const qml::vec3 up)
{
	// TODO: Use a single task operation
	Task task = {
		.type = UPDATE_LIST_PARAMETER,
	};

	task.paramType = AL_POSITION;
	task.vf[0] = pos.data[0];
	task.vf[1] = pos.data[1];
	task.vf[2] = -pos.data[2];
	addTask(task);

	task.paramType = AL_VELOCITY;
	task.vf[0] = vel.data[0];
	task.vf[1] = vel.data[1];
	task.vf[2] = -vel.data[2];
	addTask(task);

	// Font.z needs to be reversed
	task.type = UPDATE_LIST_PARAMETER_2;
	task.paramType = AL_ORIENTATION;
	task.vf[0] = up.data[0];
	task.vf[1] = up.data[1];
	task.vf[2] = up.data[2];
	task.vf[3] = front.data[0];
	task.vf[4] = front.data[1];
	task.vf[5] = -front.data[2];
	addTask(task);
}

void AudioContext::startAudioThread()
{
	while (running) {
		Task task = getTask();

		std::lock_guard<std::mutex> lck(audioLock);

		ALuint source = 0;
		if (task.handle >= 0 && task.handle < sources.size()) {
			source = sources[task.handle];
		}

		switch (task.type) {
		case PLAY_SOUND:
			alSourcePlay(source);
			break;
		case STOP_SOUND:
			alSourceStop(source);
			break;
		case DELETE_SOURCE:
			alSourceStop(source);
			sources[task.handle] = -1;
			freeSources.push(task.handle);
			break;
		case UPDATE_SRC_PARAMETER_3F:
			alSourcefv(source, task.paramType, task.vf);
			break;
		case UPDATE_SRC_PARAMETER_F:
			alSourcef(source, task.paramType, *task.vf);
			break;
		case UPDATE_SRC_PARAMETER_I:
			alSourcei(source, task.paramType, task.vi);
			break;
		case UPDATE_LIST_PARAMETER:
			alListenerfv(task.paramType, task.vf);
			break;
		case UPDATE_LIST_PARAMETER_2:
			alListenerfv(task.paramType, task.vf);
			break;
		default:
			break;
		}
	}
}

void AudioContext::addTask(const Task& task)
{
	std::lock_guard<std::mutex> lck(queueLock);
	taskQueue.push(task);
	cv.notify_one();
}

AudioContext::Task AudioContext::getTask()
{
	std::unique_lock<std::mutex> lck(queueLock);
	if (taskQueue.empty()) {
		cv.wait(lck, []() {
				return !taskQueue.empty() || !running;
			});
	}

	Task task;
	if (running) {
		task = taskQueue.front();
		taskQueue.pop();
	}

	return task;
}

bool Sound::loadWaveform(const std::string &filename)
{
	if (!fs::exists(filename)) {
		return false;
	}

	AudioFile<float> audioFile;
	if (!audioFile.load(filename)) {
		return false;
	}

	isMono = audioFile.isMono();
	sampleRate = audioFile.getSampleRate();
	samplesPerChannel = audioFile.getNumSamplesPerChannel();
	bitDepth = audioFile.getBitDepth();
	isMono = (audioFile.getNumChannels() == 1) ? true : false;

	// Upload audio data
	AudioContext::audioLock.lock();
	alGenBuffers(1, &buffer);

	std::vector<float> chBuffer2;

	ALuint format;
	void* data;
	isMono = true; // TODO: Enable sterio
	if (isMono) {
		format = AL_FORMAT_MONO_FLOAT32;
		data = audioFile.samples[0].data();
	} else {
		format = AL_FORMAT_STEREO_FLOAT32;

		// Channels need to be combined into a single array
		chBuffer2.reserve(samplesPerChannel * 2);
		for (uint32 i = 0; i < samplesPerChannel; ++i) {
			chBuffer2.push_back(audioFile.samples[0][i]);
			chBuffer2.push_back(audioFile.samples[1][i]);
		}
		data = chBuffer2.data();
	}

	int dataSize = samplesPerChannel * sizeof(float) * ((isMono) ? 1 : 2);
	alBufferData(buffer, format, data, dataSize, sampleRate);
	AudioContext::audioLock.unlock();

	ALenum error;
	while ((error = alGetError())) {
		Log::error("[OpenAL] ", error);
	}

	return true;
}

// TODO: Vorbis file support
bool Sound::loadVorbis(const std::string &filename)
{
	if (!fs::exists(filename)) {
		return false;
	}

	return true;
}
