#ifndef QUASAR_ASSET_QUEUE
#define QUASAR_ASSET_QUEUE

#include <mutex>
#include <condition_variable>
#include <queue>
#include <atomic>

namespace quasar
{

enum AssetType
{
	ASSET_TYPE_MODEL,
	ASSET_TYPE_IMAGE,
	ASSET_TYPE_TRIANGLE_MESH_COLLIDER,
	ASSET_TYPE_CONVEX_MESH_COLLIDER,
	ASSET_TYPE_FONT,
	ASSET_TYPE_AUDIO,
	ASSET_TYPE_INVALID_ASSET
};

struct TaskType
{
	std::string path;
	std::string filename;
	AssetType assetType = ASSET_TYPE_INVALID_ASSET;
	int index;
};

class AssetQueue
{
public:
	void stopQueue();

	TaskType getNextTask();
	void addNewTask(const TaskType& task);

	std::queue<TaskType> fileLoadList;
private:
	std::mutex taskLock;
	std::condition_variable cv;
	std::atomic<bool> running = true;
};

}

#endif //QUASAR_ASSET_QUEUE
