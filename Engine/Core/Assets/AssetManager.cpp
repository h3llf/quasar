#include "AssetManager.hpp"
#include "Core/Assets/Font.hpp"
#include "Core/Physics/PhysicsController.hpp"
#include "InstanceManager.hpp"
#include "Utils/Timer.hpp"
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <unordered_map>
#include <utility>

// Asset uploads
#ifndef _HEADLESS
#include "GFX/Renderer.hpp"
#endif //_HEADLESS

using namespace quasar;
using namespace std::chrono_literals;

void AssetManager::start(uint32 threadCount)
{
	if (fileLoadStatus.size() < ASSET_TYPE_INVALID_ASSET) {
		fileLoadStatus.resize(ASSET_TYPE_INVALID_ASSET);
	};

	managerRunning = true;

	for (uint32 i = 0; i < threadCount; ++i) {
		threadPool.push_back(std::thread(loaderLoop, this, &assetQueue));
	}
}

void AssetManager::stop()
{
	managerRunning = false;

	assetQueue.stopQueue();

	for (uint32 i = 0; i < threadPool.size(); ++i) {
		threadPool[i].join();
	}
}

void AssetManager::loaderLoop(AssetManager* assetManager, AssetQueue* queue)
{
	while (assetManager->managerRunning) {
		TaskType nextTask = queue->getNextTask();
		if (!nextTask.path.empty()) {
			
			switch (nextTask.assetType) {
			case ASSET_TYPE_MODEL:
				assetManager->loadModelInternal(nextTask.path, nextTask.filename);
				break;
			case ASSET_TYPE_IMAGE:
				assetManager->loadImageInternal(nextTask.path + nextTask.filename);
				break;
			case ASSET_TYPE_TRIANGLE_MESH_COLLIDER: {
				assetManager->loadTriangleMeshInternal(nextTask.path, nextTask.index);
				break;
			}
			case ASSET_TYPE_CONVEX_MESH_COLLIDER:
				assetManager->loadConvexMeshInternal(nextTask.path, nextTask.index);
				break;
			case ASSET_TYPE_FONT:
				assetManager->loadFont(nextTask.path, nextTask.index);
				break;
			case ASSET_TYPE_AUDIO:
				assetManager->loadAudioFile(nextTask.path);
				break;
			default:
				
				break;
			}
		}
	 }
}

void AssetManager::updateLoadStatus(AssetHandle& handle, AssetType type)
{
	if (!fileLoadStatus[type].contains(handle.identifier)) {
		fileLoadStatus[type][handle.identifier] = ASSET_NOT_LOADED;
	} else {
		handle.loadStatus = fileLoadStatus[type][handle.identifier];
	}
}

AssetHandle AssetManager::addModelLoadTask(const std::string& folder, const std::string& filename)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);

	AssetHandle handle = {
		.identifier = (folder + filename),
		.loadStatus = ASSET_NOT_LOADED,
		.type = ASSET_TYPE_MODEL
	};

	updateLoadStatus(handle, ASSET_TYPE_MODEL);

	if (handle.loadStatus == ASSET_NOT_LOADED) {
		models[handle.identifier] = nullptr;
		TaskType task = {folder, filename, ASSET_TYPE_MODEL};
		assetQueue.addNewTask(task);
		fileLoadStatus[ASSET_TYPE_MODEL][handle.identifier] = ASSET_QUEUED;
		handle.loadStatus = ASSET_QUEUED;
	}

	return handle;
}

AssetHandle AssetManager::addImageLoadTask(std::string path, Image::ImageUsage usage)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);

	AssetHandle handle = {
		.identifier = path,
		.loadStatus = ASSET_NOT_LOADED,
		.type = ASSET_TYPE_IMAGE
	};

	updateLoadStatus(handle, ASSET_TYPE_IMAGE);

	if (handle.loadStatus == ASSET_NOT_LOADED) {
		Image newImage;
		newImage.imageUsage = usage;

		images[path] = newImage;
		TaskType task = {path, "", ASSET_TYPE_IMAGE};
		assetQueue.addNewTask(task);
		fileLoadStatus[ASSET_TYPE_IMAGE][handle.identifier] = ASSET_QUEUED;
		handle.loadStatus = ASSET_QUEUED;
	}

	return handle;
}

void AssetManager::addBlankImageTaskCmd(const std::string identifier, int width, int height)
{
	addBlankImageTask(identifier, width, height);
}

AssetHandle AssetManager::addBlankImageTask(const std::string& identifier, int width, int height)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);

	AssetHandle handle = {
		.identifier = identifier,
		.loadStatus = ASSET_NOT_LOADED,
		.type = ASSET_TYPE_IMAGE 
	};

	updateLoadStatus(handle, ASSET_TYPE_IMAGE);

	if (handle.loadStatus == ASSET_NOT_LOADED) {
		Image newImage;
		newImage.imageUsage = Image::Blank;
		newImage.width = width;
		newImage.height = height;

		images[handle.identifier] = newImage;
		TaskType task = {handle.identifier, "", ASSET_TYPE_IMAGE};
		assetQueue.addNewTask(task);
		fileLoadStatus[ASSET_TYPE_IMAGE][handle.identifier] = ASSET_QUEUED;
		handle.loadStatus = ASSET_QUEUED;
	}

	return handle;
}

AssetHandle AssetManager::addFontLoadTask(const std::string &filename, int minGlyphSize)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);

	AssetHandle handle = {
		.identifier = filename,
		.loadStatus = ASSET_NOT_LOADED,
		.type = ASSET_TYPE_FONT
	};

	updateLoadStatus(handle, ASSET_TYPE_FONT);

	if (handle.loadStatus == ASSET_NOT_LOADED) {
		TaskType task = {handle.identifier, "", ASSET_TYPE_FONT, minGlyphSize};
		assetQueue.addNewTask(task);
		fileLoadStatus[ASSET_TYPE_FONT][handle.identifier] = ASSET_QUEUED;
		handle.loadStatus = ASSET_QUEUED;
	}

	return handle;
}

AssetHandle AssetManager::addMeshColliderLoadTask(const std::string& folder, const std::string& filename, 
		int meshIndex, bool isConvex)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);
	const std::string isConvStr = (isConvex) ? ".convex" : ".triangle";
	std::string key = folder + filename + std::to_string(meshIndex) + isConvStr;

	AssetType type = (isConvex) ? ASSET_TYPE_CONVEX_MESH_COLLIDER : ASSET_TYPE_TRIANGLE_MESH_COLLIDER;

	AssetHandle handle = {
		.identifier = key,
		.loadStatus = ASSET_NOT_LOADED,
		.type = type
	};

	updateLoadStatus(handle, type);

	if (handle.loadStatus == ASSET_NOT_LOADED) {
		meshColliderIDs[key] = -1;
		TaskType task = {folder + filename, "", type, meshIndex};
		assetQueue.addNewTask(task);
		fileLoadStatus[type][handle.identifier] = ASSET_QUEUED;
		handle.loadStatus = ASSET_QUEUED;
	}

	return handle;
}

AssetHandle AssetManager::addAudioLoadTask(const std::string &filename)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);

	AssetHandle handle = {
		.identifier = filename,
		.loadStatus = ASSET_NOT_LOADED,
		.type = ASSET_TYPE_AUDIO
	};

	updateLoadStatus(handle, ASSET_TYPE_AUDIO);

	if (handle.loadStatus == ASSET_NOT_LOADED) {
		TaskType task = {filename, "", ASSET_TYPE_AUDIO};
		assetQueue.addNewTask(task);
		fileLoadStatus[ASSET_TYPE_AUDIO][handle.identifier] = ASSET_QUEUED;
		handle.loadStatus = ASSET_QUEUED;
	}

	return handle;
}

void AssetManager::addViewportcmd(const std::string identifier, int width, int height)
{
	addViewport(identifier, width, height);
}

ViewportHandle AssetManager::addViewport(const std::string &identifier, int width, int height)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);
	if (viewports.contains(identifier)) {
		return viewports[identifier]; // May not be the best approach
	}

	ViewportHandle handle = Renderer::createTextureViewport(width, height);
	viewports[identifier] = handle;
	return handle;
}

void AssetManager::removeViewport(const std::string& identifier)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);
	if (viewports.contains(identifier)) {
		removeViewport(viewports.at(identifier));
	}
}

void AssetManager::removeViewport(ViewportHandle viewport)
{
	Renderer::destroyTextureViewport(viewport);
}

Model* AssetManager::getModel(const std::string& path)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);

	if (!models.contains(path))
		return nullptr;

	return(models[path]);
}

BVHNode* AssetManager::getBVHNode(const std::string &path, int index)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);

	if (!BVHTrees.contains(path) || BVHTrees[path].size() <= index)
		return nullptr;

	return &BVHTrees[path][index];

}

Image* AssetManager::getImage(const AssetHandle& handle)
{
	return getImage(handle.identifier);
}

Image* AssetManager::getImage(const std::string& identifier)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);

	if (!images.contains(identifier))
		return nullptr;

	Image* img = &images.at(identifier);
	img->useCount++;
	return img;
}

TextFont* AssetManager::getFont(const AssetHandle &asset)
{
	return getFont(asset.identifier);
}

TextFont* AssetManager::getFont(const std::string& identifier)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);

	if (!fonts.contains(identifier)) {
		return nullptr;
	}

	return &fonts[identifier];
}

Sound* AssetManager::getSound(const AssetHandle& handle)
{
	return getSound(handle.identifier);
}

Sound* AssetManager::getSound(const std::string& identifier)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);
	if (audioFiles.contains(identifier)) {
		return &audioFiles[identifier];
	}

	return nullptr;
}

int AssetManager::getMeshColliderID(const AssetHandle& handle)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);
	if (meshColliderIDs.contains(handle.identifier)) {
		return meshColliderIDs.at(handle.identifier);
	}

	return -1;
}


void AssetManager::getImageList(std::vector<std::pair<std::string, int>>* pImages)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);
	for (const auto& asset : fileLoadStatus[ASSET_TYPE_IMAGE]) {
		if (asset.second == ASSET_LOADED) {
			Image* img = &images.at(asset.first);
			pImages->push_back(std::make_pair(asset.first, img->rendererImageID));
		}
	}
}

void AssetManager::getModelList(std::vector<std::pair<std::string, int>>* pModels)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);
	for (const auto& asset : fileLoadStatus[ASSET_TYPE_MODEL]) {
		if (asset.second == ASSET_LOADED) {
			pModels->push_back(std::make_pair(asset.first, models[asset.first]->modelID));
		}
	}
}

void AssetManager::getFontList(std::vector<std::pair<std::string, FontHandle>> *pFonts)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);
	for (const auto& asset : fileLoadStatus[ASSET_TYPE_FONT]) {
		if (asset.second == ASSET_LOADED) {
			pFonts->push_back(std::make_pair(asset.first, fonts[asset.first].fontHandle));
		}
	}
}

void AssetManager::getAudioList(std::vector<std::string>* pSounds)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);
	for (const auto& asset : fileLoadStatus[ASSET_TYPE_AUDIO]) {
		if (asset.second == ASSET_LOADED) {
			pSounds->push_back(asset.first);
		}
	}
}

void AssetManager::queryLoadStatus(AssetHandle &asset)
{
	std::lock_guard<std::mutex> lck(assetManagerSync);

	const auto& statusList = fileLoadStatus[asset.type];

	if (statusList.contains(asset.identifier)) {
		asset.loadStatus = statusList.at(asset.identifier);
	}
}

void AssetManager::queryLoadStatusUnlocked(AssetHandle &asset)
{
	const auto& statusList = fileLoadStatus[asset.type];

	if (statusList.contains(asset.identifier)) {
		asset.loadStatus = statusList.at(asset.identifier);
	}
}

void AssetManager::loadModelInternal(const std::string& path, const std::string& filename)
{
#ifdef _DEBUG
	Timer::StopStartTimer timer;
	timer.start();
#endif

	std::string fullPath = path + filename;

	{
		std::lock_guard<std::mutex> lck1(assetManagerSync);
		AssetLoadStatus loadStatus = fileLoadStatus[ASSET_TYPE_MODEL][fullPath];

		// Cancel if previously loaded by the collider generator
		if (loadStatus >= ASSET_IN_PROGRESS) {
			return;
		}

		fileLoadStatus[ASSET_TYPE_MODEL][fullPath] = ASSET_IN_PROGRESS;
	}

	Model* model = new Model();
	model->loadGLTF(path, filename);

	if (!model->meshCount) {
		std::lock_guard<std::mutex> lck1(assetManagerSync);
		fileLoadStatus[ASSET_TYPE_MODEL][fullPath] = ASSET_LOAD_FAILED;
		Log::out("Failed to load model ", fullPath);
		return;
	}

	int maxID = 0;
	for (const Model::Mesh& mesh : model->meshes) {
		//Get larget meshID
		maxID = std::max(maxID, mesh.rendererID);
	}

	//Reserve mesh IDs for instance manager
	InstanceManager::reserveMeshIDs(maxID + 1);

	BVHTrees[fullPath].resize(model->meshCount);
	for (uint32 i = 0; i < model->meshCount; ++i) {
		BVH::createTree(&model->meshes[i], &BVHTrees[fullPath][i]);
	}

	//Update model status
	{
		std::lock_guard<std::mutex> lck2(assetManagerSync);
		fileLoadStatus[ASSET_TYPE_MODEL][fullPath] = ASSET_LOADED;
		models[fullPath] = model;
	}

#ifdef _DEBUG
	Log::out("Loaded: ", path, filename, " in ", timer.stop() / 1000, "ms");
#endif
}

void AssetManager::loadImageInternal(const std::string& path)
{
#ifndef _HEADLESS
#ifdef _DEBUG
//	Timer::StopStartTimer timer;
//	timer.start();
#endif // _DEBUG
	assetManagerSync.lock();
	Image image = images[path];
	assetManagerSync.unlock();

	if (image.imageUsage == Image::HDR) {
		image.loadImage16f(path);
	} else if (image.imageUsage != Image::Blank) {
		image.loadImage(path);
	}


	image.rendererImageID = Renderer::uploadImage(&image);

	if (image.imageUsage != Image::Blank) {

		if (image.imageUsage == Image::HDR) {
			image.freePixels16f();
		} else {
			image.freePixels();
		}
	}

	// Update image
	assetManagerSync.lock();
	images[path] = image;
	fileLoadStatus[ASSET_TYPE_IMAGE][path] = ASSET_LOADED;
	assetManagerSync.unlock();

#ifdef _DEBUG
//	Log::out("Loaded: ", path, " in ", timer.stop() / 1000, "ms");
#endif // _DEBUG
#endif //_HEADLESS
}

void AssetManager::loadTriangleMeshInternal(const std::string& path, int meshIndex)
{
	// Try to load cached collider
	int colliderID = physicsController.meshes.loadTriangleMesh(path, meshIndex);

	// Regenerate if cached file is missing
	if (colliderID == -1) {
		colliderID = generateColliderInternal(path, meshIndex, false);
	}

	// TODO: Retain handle key?
	
	std::string key = path + std::to_string(meshIndex) + ".triangle";
	assetManagerSync.lock();
	AssetLoadStatus result = (colliderID == -1) ? ASSET_LOAD_FAILED : ASSET_LOADED;
	fileLoadStatus[ASSET_TYPE_TRIANGLE_MESH_COLLIDER][key] = result;
	meshColliderIDs[key] = colliderID;
	assetManagerSync.unlock();
}

void AssetManager::loadConvexMeshInternal(const std::string& path, int meshIndex)
{
	// Try to load cached collider
	int colliderID = physicsController.meshes.loadConvexMesh(path, meshIndex);

	// Regenerate if cached file is missing
	if (colliderID == -1) {
		colliderID = generateColliderInternal(path, meshIndex, true);
	}

	std::lock_guard<std::mutex> lck2(assetManagerSync);
	std::string key = path + std::to_string(meshIndex) + ".convex";
	assetManagerSync.lock();
	AssetLoadStatus result = (colliderID == -1) ? ASSET_LOAD_FAILED : ASSET_LOADED;
	fileLoadStatus[ASSET_TYPE_CONVEX_MESH_COLLIDER][key] = result;
	meshColliderIDs[key] = colliderID;
	assetManagerSync.unlock();
}

int AssetManager::generateColliderInternal(const std::string& path, int meshIndex, bool isConvex)
{
	AssetHandle modelHandle = {
		.identifier = path,
		.loadStatus = ASSET_NOT_LOADED,
		.type = ASSET_TYPE_MODEL
	};

	queryLoadStatus(modelHandle);

	// Take over model loading if it hasn't been started
	if (modelHandle.loadStatus == ASSET_NOT_LOADED || modelHandle.loadStatus == ASSET_QUEUED) {
		std::string tmpFileName = path.substr(path.find_last_of('.'), std::string::npos);
		std::string tmpFolder = path.substr(0, path.find_last_of('.'));
		loadModelInternal(tmpFolder, tmpFileName);
	}

	// Query model load status
	while (true) {
		queryLoadStatus(modelHandle);

		// Loading already started, wait for it to finish
		if (modelHandle.loadStatus == ASSET_IN_PROGRESS) {
			std::this_thread::sleep_for(1ms);
		} else {
			break;
		}
	}

	// Generate collider
	if (modelHandle.loadStatus == ASSET_LOADED) {
		assetManagerSync.lock();
		Model* model = models[path];
		Model::Mesh& mesh = model->meshes[meshIndex]; // TODO: Check thread safety
		assetManagerSync.unlock();

		if (isConvex) {
			return physicsController.meshes.createConvexMesh(&mesh.vertices, true, &path, meshIndex);
		} else {
			return physicsController.meshes.createTriangleMesh(&mesh.vertices, &mesh.indices, 
					true, &path, meshIndex);
		}
	}

	return -1;
}

void AssetManager::loadFont(const std::string& path, int minGlyphSize)
{
	TextFont newFont;
	bool result = newFont.loadFont(path, minGlyphSize);
	
	assetManagerSync.lock();
	if (!result) {
		Log::error("Failed to load font at: ", path);
	} else {
		fonts[path] = newFont;
	}

	fileLoadStatus[ASSET_TYPE_FONT][path] = (result) ? ASSET_LOADED : ASSET_LOAD_FAILED;
	assetManagerSync.unlock();
}

void AssetManager::loadAudioFile(const std::string& path)
{
	// Detect file type
	std::string ext;
	if (path.find_last_of('.') != std::string::npos) {
		ext = path.substr(path.find_last_of('.'));
	}

	Sound audioFile;

	bool res = false;
	if (ext == ".wav") {
		res = audioFile.loadWaveform(path);
	} else if (ext == ".ogg") {
		res = audioFile.loadVorbis(path);
	}

	assetManagerSync.lock();
	fileLoadStatus[ASSET_TYPE_AUDIO][path] = (res) ? ASSET_LOADED : ASSET_LOAD_FAILED;
	if (res) {
		audioFiles[path] = audioFile;
	} else {
		Log::error("Failed to load audio file at: ", path);
	}
	assetManagerSync.unlock();
}

