#ifndef QUASAR_CONSOLE_H
#define QUASAR_CONSOLE_H

#include <ios>
#include <iostream>
#include <sstream>
#include <streambuf>
#include <unordered_map>
#include <map>
#include <thread>
#include <mutex>
#include <queue>
#include <functional>
#include <typeinfo>
#include <typeindex>
#include <json.hpp>

#if defined __linux__
	#include <curses.h>
#elif defined _WIN32

#endif

// Console commands based on: https://github.com/kevinmkchin/noclip
class Console : public std::streambuf
{
private:
	template <typename T, typename A>
	void bindCVarInternal(std::string name, void* dataPtr, bool persistent)
	{
		// Variable name already bound
		if (cVars.find(name) != cVars.end())
			return;

		CVar cvar;
		cvar.valuePtr = static_cast<void*>(dataPtr);
		cvar.persistent = persistent;
		cvar.type = typeid(T).name();

		cvar.setFunc = [this, dataPtr](std::string params[]) 
		{
			std::string* p = params;
			*(A*)dataPtr = this->convertParam<T>(&p);
		};

		cvar.printFunc = [dataPtr]()
		{
			std::stringstream buffer;
			buffer << *(A*)dataPtr;
			return buffer.str();
		};

		// Load persistent values from the config file
		if (persistent && fileEntries.contains(name)) {
			cvar.setFunc(&fileEntries[name]);
		}

		cVars.insert(std::pair<std::string, CVar>(name, cvar));
	}

public:
	typedef std::function<void(std::string[])> FunctionType;
	typedef std::function<std::string()> PrintFunctionType;

	void init(std::string fileName);
	void deinit();
	void saveCVarState();
	void parseCVarFile();

	template <typename T>
	void bindCVar(std::string name, T* dataPtr, bool persistent = false)
	{
		bindCVarInternal<T, T>(name, dataPtr, persistent);
	};

	template <typename T>
	void bindCVar(std::string name, std::atomic<T>* dataPtr, bool persistent = false)
	{
		bindCVarInternal<T, std::atomic<T>>(name, dataPtr, persistent);
	};

	template <typename T>
	T* getCVar(std::string name)
	{
		if (cVars.find(name) != cVars.end()) {
			return static_cast<T*>(cVars[name].valuePtr);
		}

		return nullptr;
	}

	// Converts the string to type T using a string stream
	template <class T>
	static T convertParam(std::string** param)
	{
		std::string* p = *param;
		*param -= 1;

		// Return strings directly to preserve whitespace
		if constexpr(std::is_same<std::string, T>::value) {
			return *p;
		}

		T buffer;
		std::stringstream paramStream(*p);
		paramStream >> buffer;

		return buffer;
	}

	// Static functions
	template <typename ...T>
	void bindCommand(std::string name, void(*func)(T...))
	{
		const size_t argCount = sizeof...(T);

		cmdCommands[name].argCount = argCount;
		cmdCommands[name].func = [func, argCount](std::string params[])
		{
			std::string* p = &params[argCount - 1];
			func(convertParam<T>(&p)...);
		};
	}

	// Called from object
	template <typename O, typename ...T>
	void bindCommand(std::string name, void(O::*func)(T...), O* o)
	{
		const size_t argCount = sizeof...(T);

		cmdCommands[name].argCount = argCount;
		cmdCommands[name].func = [func, o, argCount](std::string params[])
		{
			std::string* p = &params[argCount - 1];
			(o->*func)(convertParam<T>(&p)...);
		};
	}

	void updateCommands();

	int underflow() override
	{
		std::cout << std::endl;
		return 0;
	}

	int overflow(int ch) override
	{
		std::cout << char(ch);
		return ch;
	}

private:
	struct CVar
	{
		void* valuePtr = nullptr;
		bool persistent = false;
		std::string type;
		FunctionType setFunc;
		PrintFunctionType printFunc;
	};

	struct Command
	{
		size_t argCount;
		FunctionType func;
	};

	std::string filePath;

	std::queue<std::string> commandQueue;
	std::map<std::string, CVar> cVars;
	std::unordered_map<std::string, Command> cmdCommands;

	std::unordered_map<std::string, std::string> fileEntries;

	void processCommand(std::string& cmd);
	void pollInput();

	std::thread inputPollingThread;
	std::mutex inputMutex;
	std::atomic<bool> isRunning = true;

	// Built in console functions
	void setVal(std::string valueName, std::string params[]);
	void printVal(std::string valueName);
	void list(std::string type);

	int row = 0;
	int column = 0;
};

#endif //QUASAR_CONSOLE_H
