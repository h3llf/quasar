#include "Camera.hpp"
#include "Utils/Math/MatrixOps.hpp"

void Camera::genProjection(int width, int height, CameraProperties* pCamProps)
{
	float aspect = (float)width / (float)height;
	pCamProps->proj = qml::perspective(pCamProps->fov, aspect, pCamProps->near, pCamProps->far);
//	pCamProps->proj = qml::ortho(-3.0f, 3.0f, -3.0f, 3.0f, 1000.0f, 0.01f);
}
