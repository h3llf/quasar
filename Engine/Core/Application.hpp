#ifndef QUASAR_APPLICATION_H
#define QUASAR_APPLICATION_H

#include "ApplicationContext.hpp"
#include "Assets/InstanceManager.hpp"
#include "Physics/PhysicsController.hpp"
#include "QuasarProject.hpp"
#include "GLFW/glfw3.h"
#include "Types.hpp"
#include "Console.hpp"
#include "ECS_Headers.hpp"
#include <filesystem>

namespace quasar
{

class QuasarApp
{
public:
	void init();

	// Adds basic test lights to the scene
	void testLighting();

	void startGameLoop();
	void startServerThread();
	void cleanup();

	ECS clientECS;
	ECS serverECS;

	AssetManager assetManager;

private:
	ApplicationContext appContext;

	// Automatically load/Reload the game library as specified by the project
	ComponentType highestID;
	void* libHandle = nullptr;
	std::filesystem::file_time_type lastModified {};
	void checkGameLib();
	bool loadGameLib(const char* path);
	void unloadGameLib();
	void loadEntities();
	void unloadEntities();
	void deleteEntities();

	void initSystems();

	void serverLoopInternal();
	std::thread serverThread;

	ProjectHeader currentProject = {};

	static inline double cTime = 0.0;
	static void frameRateC(uint64 time);

	static inline double sTime = 0.0;
	static void frameRateS(uint64 time);

	// Default systems
	// TODO: Find a better way of manageing these
#ifndef _HEADLESS
	SMaterial sMaterial;
	SEditorCamController sEditorCamController;
	SKinematicController sKinematicController;
	SModelLoader sModelLoader;
	SRenderable sRenderable;
	SText sText;
#endif
	SAssetPolling sAssetPolling;
	SPointLight sPointLight;
	SDirectionalLight sDirectionalLight;
	SSineOscillation sSine;
	STransform sTransform;
	SCamera sCamera;
	SColliderShape sColliderShape;
	SRigidbody sRigidbody;
	SAudioSource sAudioSource;
	SAudioListener sAudioListener;
	SSkeleton sSkeleton;

	template<typename T>
	void constructSystem(T& system)
	{
		system.init(&appContext);
	}

	// Add profiler threads & tasks for the gameloop & server
	uint32 prof;
	uint32 profNet;
	uint32 profECS;
	uint32 profPhysx;
	uint32 profEditor;
	uint32 profRenderData;

	uint32 profServ;
	uint32 profServNet;
	uint32 profServECS;
	uint32 profServPhysx;

	void addProfilerTasks();

	std::ostream* consoleStream;
	Console console;
	// Binds several miscellaneous varaibles and commands to the console
	void bindCVars();

	GLFWwindow* window;
};

};
#endif //QUASAR_APPLICATION_H
