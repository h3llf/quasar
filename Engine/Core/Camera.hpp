#ifndef QUASAR_CAMERA_H
#define QUASAR_CAMERA_H

#include "Types.hpp"

namespace Camera
{
	struct CameraProperties
	{
		mat4 view;
		mat4 proj;
		mat4 invProj;
		vec3 ambientColour = vec3(220.0f/255.0f, 229.0f/255.0f, 1.0f) * 0.2f;
		float exposure = 1.0f;
		vec3 viewPos;
		float gamma = 2.2f;
		float fov = qml::radians(100.0f);
		float near = 0.1f;
		float far = 1000.0f;

		// Used internally by shaders to scale NDC to an area of sampled images
		float xScale = 1.0f;
		float yScale = 1.0f;

		// TODO: Use a better method of satisfying alignement requirements
		vec3 tmp = 0.0f;
	};

	void genProjection(int width, int height, CameraProperties* pCamProps);
};

#endif //QUASAR_CAMERA_H
