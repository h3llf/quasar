#include "Application.hpp"
#include "Core/Assets/Audio.hpp"
#include "Core/Camera.hpp"
#include "Core/ECS/Reflection.hpp"
#include "Platform/ApplicationLaunch.hpp"
#include "Utils/Timer.hpp"
#include "Globals.hpp"
#include <type_traits>
#include <filesystem>

#ifdef __linux__
#include <dlfcn.h>
#endif //__linux__

#ifdef _WIN32
#include <Windows.h>
#endif //_WIN32

#ifndef _HEADLESS
#include "Client/Input.hpp"
#include "GFX/Renderer.hpp"
#include "Editor/Editor.hpp"
#include "Editor/Profiler.hpp"
#define PROFILE(threadNum, taskNum) Profiler::ScopedMeasurement psm(threadNum, taskNum);
#else
// Disable profiler calls for headless mode
#define PROFILE(threadNum, taskNum)
#endif //_HEADLESS

using namespace quasar;
using namespace std::chrono_literals;

QuasarGlobals QGlobals;

std::mutex serverEditorLock;

void QuasarApp::init()
{
	// Init console & logger
#ifndef _HEADLESS
	console.init("quasar");
#else
	console.init("quasar_headless");
#endif
	consoleStream = new std::ostream(&console);
	Log::init(consoleStream);
	bindCVars();
	
	namespace fs = std::filesystem;

	// Create engine assets folder
	if (!fs::exists("./EngineAssets")) {
		fs::create_symlink("../Assets", "./EngineAssets");
	}
	
	assetManager.start(6); // TODO: Automatically detect thread count
	AudioContext::initContext();

	physicsController.initPhysX();

	appContext = {
		.assetMgr = &assetManager,
		.cECS = &clientECS,
		.cPhysics = &physicsController,
		.client = nullptr,
		.sECS = &serverECS,
		.sPhysics = nullptr,
		.server = nullptr,
	};

#ifndef _HEADLESS
	// Renderer init
	Renderer::init();
	window = Renderer::getWindow();
	glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);

#ifndef _DISABLE_EDITOR
	Editor::init(GFX.window, console);
	Editor::setViewportImages();
	Editor::initFonts();
#endif //_DISABLE_EDITOR



	Renderer::startThread();
	InputState::init();
#endif

	initSystems();

#ifndef _HEADLESS
	Editor::initECS(appContext);
	Editor::loadImageIcons();
	Editor::initProject(&currentProject);
#endif

	addProfilerTasks();
}

void QuasarApp::startGameLoop()
{
	// TODO: Move
	Timer::ExecuteEvery queryTick(0.05f);
	Timer::ExecuteEvery libLoad(1.0f);
	Timer::ExecuteEvery physicsTick(1.0f / 120.0f, 0.0f, 0.5f);

	console.bindCVar("client_physics_tick", &physicsTick.rate);
	console.bindCommand("blank_image", &AssetManager::addBlankImageTaskCmd, &assetManager);

	while (QUASAR.applicationRunning) {
		// TODO: ECS thread safety & remove this lock
		Timer::ScopedTimer timeTest(frameRateC);
		{
			PROFILE(prof, profNet)
			serverEditorLock.lock();
#ifndef _HEADLESS
//			client.listen();
//			client.update();
#endif //_HEADLESS
		}

		{
			// Update the physics simulation
			PROFILE(prof, profPhysx)
			if (physicsTick.shouldTick(cTime)) {
				// Ignore large spikes (caused by editor) to maintain stability
				// TODO: Find a better solution to this
				if (QGlobals.updateGame) {
					SRigidbody::updateSet(&clientECS);
					physicsController.updateSimulation(physicsTick.rate);
					SRigidbody::updateGet(&clientECS);
				}
				physicsController.getDebugData();
			}
		}

		// Main update
		{
			PROFILE(prof, profECS)
			int updateLayer = UPDATE_NONE;
			if (QGlobals.updateGame)
				updateLayer |= UPDATE_GAME_FAST;
			else
				updateLayer |= UPDATE_EDITOR;


			clientECS.update(cTime, updateLayer);

			// Thread safe asset polling
			if (queryTick.shouldTick(cTime)) {
				clientECS.update(cTime, UPDATE_ASSET_POLLING);
			}

			// TODO: Add an other category to the profiler
			if (libLoad.shouldTick(cTime)) {
				checkGameLib();
			}
		}

#ifndef _HEADLESS
		{
			PROFILE(prof, profEditor)
			int width, height;
			glfwGetWindowSize(window, &width, &height);
			QUASAR.windowSizeX = width;
			QUASAR.windowSizeY = height;
			QUASAR.applicationRunning = !glfwWindowShouldClose(window);
			InputState::pollInputs();

#ifndef _DISABLE_EDITOR
			if (QGlobals.drawEditor) {
				Editor::draw(window);
			}
       		}
#endif //_DISABLE_EDITOR
      		{
			PROFILE(prof, profRenderData);
			serverEditorLock.unlock();
			InstanceManager::copyTransferDataSrc();
		} 
#endif //_HEADLESS
	}
}

void QuasarApp::startServerThread()
{
	serverThread = std::thread(&QuasarApp::serverLoopInternal, this);
}

void QuasarApp::serverLoopInternal()
{
	float physicsTickRate = 1.0f / 120.0f;
	float physicsAccumulator = 0.0f;
	console.bindCVar("server_physics_tick", &physicsTickRate);

	float queryTickRate = 0.05f;
	float queryAccumulator = 0.0f;

	float cVarSaveRate = 1.0f;
	float cVarSaveAccumulator = 0.0f;

	while (QUASAR.applicationRunning) {
		Timer::ScopedTimer timeTest(frameRateS);
		{
			PROFILE(profServ, profServNet);
			std::this_thread::sleep_for(10ms);
			//serverEditorLock.lock();
			//server.listen();
			//server.update();
		}

		//TODO: Server side physics
		{
			PROFILE(profServ, profServPhysx);

			// TODO: Profile separately
			console.updateCommands();
			cVarSaveAccumulator += sTime;
			if (cVarSaveAccumulator > cVarSaveRate) {
				console.saveCVarState();
				cVarSaveAccumulator = 0.0f;
			}
		}

		// ECS update
		{
			PROFILE(profServ, profServECS)
			int updateLayer = UPDATE_NONE;
			if (QGlobals.updateGame)
				updateLayer |= UPDATE_GAME_FAST;

			//serverECS.update(sTime, updateLayer);

			// Thread safe asset polling
			queryAccumulator += sTime;
			if (queryAccumulator > queryTickRate) {
				//serverECS.update(sTime, UPDATE_ASSET_POLLING);
				queryAccumulator = 0.0f;
			}
			//serverEditorLock.unlock();
		}
	}
}

void QuasarApp::cleanup()
{
#ifndef _HEADLESS
	//client.shutdown();
#endif //_HEADLESS
	//server.shutdown();

	AudioContext::deinitContext();

	assetManager.stop();

#ifndef _HEADLESS
	Renderer::cleanUp();
#endif //_HEADLESS
       
	serverThread.join();

	// The socket needs some time to close
//	std::this_thread::sleep_for(10ms);
}

void QuasarApp::checkGameLib()
{
	namespace fs = std::filesystem;

	// Unload the existing dll when a new project is made/opened
	if (currentProject.shouldClose) {
		currentProject.shouldClose = false;
		if (libHandle) {
			deleteEntities();
			unloadGameLib();
#ifndef _HEADLESS
			Editor::initECS(appContext);
#endif //_HEADLESS
			lastModified = {};
		}
	}

	if (!currentProject.loaded)
		return;

#ifdef __linux__
	fs::path libPath = currentProject.directory;
	libPath += ("/build/lib" + currentProject.binaryName + ".so");
	fs::path dstPath("./lib" + currentProject.binaryName + ".so");
#elif _WIN32
	fs::path libPath = currentProject.directory;
	libPath += ("build/" + currentProject.binaryName + ".dll");
	fs::path dstPath("./" + currentProject.binaryName + ".dll");
#endif
	
	// Check that library exists
	if (!fs::exists(libPath)) {
		return;
	}

	fs::file_time_type fTime = fs::last_write_time(libPath);

	if (fTime != lastModified) {
		Editor::deinitECS();
		if (libHandle) {
			unloadEntities();
			unloadGameLib();
		}		

		// Copy lib
		std::error_code error;
		fs::copy(libPath, dstPath, fs::copy_options::overwrite_existing, error);
		if (error) {
			Log::error(error.message());
			return;
		}

		// TODO: Shouldn't run on first load?
		Editor::initECS(appContext);
		if (loadGameLib(dstPath.string().c_str())) {
			loadEntities();
			lastModified = fTime;
		}
	}
}

std::vector<std::vector<uint8_t>> clientEntityBuffer;
std::vector<std::vector<uint8_t>> serverEntityBuffer;

typedef void (*GameInitFn)(ApplicationContext& appContext);
bool QuasarApp::loadGameLib(const char* path)
{
	// Component IDs are assigned sequentially so this is the highest built in component ID
	highestID = ComponentRegistry::componentSizes.size() - 1;
	libHandle = nullptr;

// TODO: Move to Platform
#ifdef __linux__
	libHandle = dlopen(path, RTLD_LAZY);
	if (!libHandle) {
		return false;
	}

	GameInitFn gameInit = nullptr;
	try {
		gameInit = (GameInitFn)dlsym(libHandle, "init");
	} catch(const std::exception& ex) {
		std::cout << "Error occurred: " << ex.what() << std::endl;
	}


	if (!gameInit) {
		Log::error("Failed to load shared libray symbols");
		return false;
	} else {
		gameInit(appContext);
	}
#elif _WIN32
	libHandle = LoadLibraryA(path);
	if (!libHandle) {
		Log::error("Failed to load dynamic library");
		return false;
	}

	GameInitFn gameInit = nullptr;
	gameInit = (GameInitFn)GetProcAddress((HMODULE)libHandle, "init");
	if (!gameInit) {
		Log::error("Failed to get proc address");
		return false;
	} else {
		gameInit(appContext);
	}
#endif //_WIN32

	currentProject.libLoaded = true;
	return true;
}

void QuasarApp::unloadGameLib()
{
	clientECS.context.componentPools.clear();
	serverECS.context.componentPools.clear();

	// Unregister components initialized after lib load
	ComponentRegistry::unregisterComponentTypeAbove(highestID);

	// Reset system state
	clientECS.clearCallbacks();
	serverECS.clearCallbacks();

	// Reload editor and core systems
	initSystems();
//	client.registerSystems();
//	server.registerSystems();

// TODO: Move to platform
#ifdef __linux__
	dlclose(libHandle);
#endif //__linux

#ifdef _WIN32
	FreeLibrary((HMODULE)libHandle);
#endif //_WIN32
       
	currentProject.libLoaded = false;
	libHandle = nullptr;
}


void QuasarApp::loadEntities()
{
	for (uint32 i = 0; i < clientEntityBuffer.size(); ++i) {
		clientECS.readEntity(clientEntityBuffer[i].size(), clientEntityBuffer[i].data());
	}

	for (uint32 i = 0; i < serverEntityBuffer.size(); ++i) {
		serverECS.readEntity(serverEntityBuffer[i].size(), serverEntityBuffer[i].data());
	}

	clientEntityBuffer.resize(0);
	serverEntityBuffer.resize(0);
}

void QuasarApp::unloadEntities()
{
	Log::out("Unload entities ", clientECS.context.entities.size());

	// Serialize all root level entities in a buffer to be reloaded
	for (ContextECS::EntityEntry& entry : clientECS.context.entities) {
		if (entry.entity && !clientECS.getParent(entry.entity)) {
			size_t entitySize = clientECS.getEntitySize(entry.entity, true, true);
			clientEntityBuffer.push_back(std::vector<uint8_t>(entitySize));
			clientECS.writeEntity(entry.entity, clientEntityBuffer.back().data(), true, true);
			clientECS.destroyEntity(entry.entity);
		}
	}

	for (ContextECS::EntityEntry& entry : serverECS.context.entities) {
		if (entry.entity && !serverECS.getParent(entry.entity)) {
			size_t entitySize = serverECS.getEntitySize(entry.entity, true, true);
			serverEntityBuffer.push_back(std::vector<uint8_t>(entitySize));
			serverECS.writeEntity(entry.entity, serverEntityBuffer.back().data(), true, true);
			serverECS.destroyEntity(entry.entity);
		}
	}
}

void QuasarApp::deleteEntities()
{
	clientEntityBuffer.resize(0);
	serverEntityBuffer.resize(0);

	// Serialize all root level entities in a buffer to be reloaded
	for (ContextECS::EntityEntry& entry : clientECS.context.entities) {
		if (entry.entity && !clientECS.getParent(entry.entity)) {
			clientECS.destroyEntity(entry.entity);
		}
	}

	for (ContextECS::EntityEntry& entry : serverECS.context.entities) {
		if (entry.entity && !serverECS.getParent(entry.entity)) {
			serverECS.destroyEntity(entry.entity);
		}
	}
}

void QuasarApp::initSystems()
{
#ifndef _HEADLESS
	constructSystem(sMaterial);
	constructSystem(sEditorCamController);
	constructSystem(sKinematicController);
	constructSystem(sModelLoader);
	constructSystem(sRenderable);
	constructSystem(sText);
#endif
	constructSystem(sPointLight);
	constructSystem(sDirectionalLight);
	constructSystem(sSine);
	constructSystem(sTransform);
	constructSystem(sCamera);
	constructSystem(sColliderShape);
	constructSystem(sRigidbody);
	constructSystem(sAssetPolling);
	constructSystem(sAudioSource);
	constructSystem(sAudioListener);
	constructSystem(sSkeleton);
}

void QuasarApp::frameRateC(uint64 time)
{
	cTime = static_cast<double>(time) / 1e6;
}

void QuasarApp::frameRateS(uint64 time)
{
	sTime = static_cast<double>(time) / 1e6;
}

void QuasarApp::testLighting()
{
	CTransform cTransform {vec3(0.0f, 2.0f, 0.0f), vec3(0.4f)};
	CSineOscillation cSine {vec3{1.0f, 0.0f, 0.0f}, 8.0f, 0.5f, true};
	CPointLight cPoint = {vec3(0.1f, 0.5f, 1.0f)};
	cPoint.intensity = 40.0f;
	cPoint.shadowMapped = true;
	cPoint.distance = 20.0f;
	CModel cM {"../Assets/Primitive/", "sphere.gltf"};
	cM.hasMeshes = true;
	Entity light = clientECS.makeEntity(cPoint, cTransform, cSine, cM);
	clientECS.renameEntity(light, "Blue light");

	cSine.speed *= -1.0f;
	cPoint.colour = vec3(1.0f, 3.0f/5.0f, 0.1f);
	Entity light2 = clientECS.makeEntity(cPoint, cTransform, cSine, cM);
	clientECS.renameEntity(light2, "Orange light");

	CTransform mT {};
	CMesh mM {};
	CMaterialPBR mMP {};
	mMP.materialProps.emissiveFactor = vec3(5, 15, 55);
	Entity mesh = clientECS.makeEntity(mT, mM, mMP);
	clientECS.setParent(mesh, light);

	mMP.materialProps.emissiveFactor = vec3(55, 15, 5);
	Entity mesh2 = clientECS.makeEntity(mT, mM, mMP);
	clientECS.setParent(mesh2, light2);
}

void QuasarApp::addProfilerTasks()
{
#ifndef _HEADLESS
	prof = Profiler::addThread("Client", "tps");
	profNet = Profiler::addTask(prof, "Network Listen");
	profECS = Profiler::addTask(prof, "ECS Update");
	profPhysx = Profiler::addTask(prof, "Physics");
	profEditor = Profiler::addTask(prof, "Draw Editor");
	profRenderData = Profiler::addTask(prof, "Render Data Transfer");

	profServ = Profiler::addThread("Server", "tps");
	profServNet = Profiler::addTask(profServ, "Network Listen");
	profServECS = Profiler::addTask(profServ, "ECS Update");
	profServPhysx = Profiler::addTask(profServ, "Physics");
#endif //_HEADLESS
}

void QuasarApp::bindCVars()
{
	console.bindCVar("update_game", &QGlobals.updateGame);
	console.bindCVar("update_editor", &QGlobals.updateGame);
	console.bindCVar("enable_server", &QGlobals.enableServer, true);
	console.bindCVar("enable_collider_vis", &QGlobals.colliderVisualization, true);
	console.bindCVar("default_address", &QGlobals.connectionIP, true);
	console.bindCVar("default_port", &QGlobals.connectionPort, true);
	ECSUtil::addEntityCVars(console);

#ifndef _HEADLESS
	Renderer::initCVars(console);
#endif

	console.bindCommand("test_lighting", &QuasarApp::testLighting, this);

}
