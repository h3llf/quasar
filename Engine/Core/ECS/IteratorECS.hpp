#ifndef QUASAR_ITERATOR_ECS_H
#define QUASAR_ITERATOR_ECS_H

#include "ComponentECS.hpp"
#include "ContextECS.hpp"
#include "Core/ECS/Types.hpp"
#include <bitset>
#include <cstdint>
#include <stack>
#include <tuple>

// TODO: Look at an excliude condition callback  e.g. bool exclude(Entity) to exclude sub hierarchies
class HierarchyIterator
{
public:
	ContextECS& context;
	Entity current;
	std::stack<Entity> children;

	HierarchyIterator(Entity rootEntity, ContextECS& contextECS) : 
		context(contextECS), current(rootEntity)
	{
		children.push(rootEntity);
	}

	HierarchyIterator begin()
	{
		operator++();
		return *this;
	}

	HierarchyIterator end()
	{
		current = Entity::null;
		return *this;
	}

	bool operator!=(const HierarchyIterator& other) const
	{
		return current != other.current;
	}

	HierarchyIterator operator++()
	{
		if (!children.size()) {
			current = Entity::null;
			return *this;
		}

		current = children.top();
		children.pop();

		ContextECS::EntityEntry& entry = context.entities[current.index];
		for (int i = entry.children.size() - 1; i >= 0; --i) {
			children.push(entry.children[i]);
		}

		return *this;
	}

	Entity operator*()
	{
		return current;
	}

private:
};

namespace IteratorInternal
{
	template <typename ... C>
	static std::tuple<C& ...> getComponents(Entity entity, ContextECS& context)
	{
		return std::tie(reinterpret_cast<Component<C>*>(
					context.getComponent(entity, TypeInfo<C>::type))->data ...);
	}

	template <typename ... C>
	static std::tuple<Entity, C& ...> getAll(Entity entity, ContextECS& context)
	{
		return std::tuple_cat(std::make_tuple(entity), std::tie(reinterpret_cast<Component<C>*>(
				context.getComponent(entity, TypeInfo<C>::type))->data ...));
	}
};

template <typename ... C>
class EntityComponentIterator
{
public:
	EntityComponentIterator(int back, int front, ComponentType* types, ComponentPool* pool, 
			ContextECS& context) : back(back), front(front), pool(pool), context(context)
	{
		for (uint32 i = 0; i < compNum; ++i) {
			compTypes[i] = types[i];
		}
	};

	int back = 0;
	int front = 0;

	EntityComponentIterator begin()
	{
		return *this;
	}

	EntityComponentIterator end()
	{
		back = front;
		return *this;
	}

	EntityComponentIterator operator++()
	{
		while ((--back > front) && !componentCheck(back));
		return *this;
	}

	bool operator!=(const EntityComponentIterator& other) const
	{
		return back != other.back;
	}

	std::tuple<Entity, C& ...> operator*()
	{
		return IteratorInternal::getAll<C ...>(pool->at(back)->entity, context);
	}

private:
	static const uint32 compNum = sizeof...(C);
	ComponentType compTypes[compNum];
	ComponentPool* pool;
	ContextECS& context;

	bool componentCheck(int index)
	{
		Entity entity = pool->at(index)->entity;

		for (uint32 i = 0; i < compNum; ++i) {
			ComponentType type = compTypes[i];

			if (type == pool->componentType)
				continue;

			if (!context.componentPools[type]->entityHasComponent(entity))
				return false;
		}

		return true;
	};


};

template <typename ... C>
class EntityView
{
public:
	ComponentType leastCommon;

	int back = 0;
	int front = 0;

	EntityView(ContextECS& context) : context(context)
	{
		// Create component type array and bitmask
		(addComponentType<C>(), ...);

		// Check all pools are present
		for (uint32 i = 0; i < compNum; ++i) {
			ComponentType type = compTypes[i];
			
			if (context.componentPools.size() <= type || !context.componentPools[type]) {
				compCount = 0; // Disable iterator
				return;
			}
		}

		compCount = compNum;
		leastCommon = findLeastCommon();
		pool = context.getComponentPool(leastCommon);

		// Starting point
		findLastValidEntity();

		// End
		findFirstValidEntity();
	}

	EntityView begin()
	{
		return *this;
	}

	EntityView end()
	{
		back = front;
		return *this;
	}

	EntityComponentIterator<C ...> components()
	{
		return EntityComponentIterator<C ...>(back, front, compTypes, pool, context);
	}

	std::tuple<C& ...> get(Entity entity)
	{
		return IteratorInternal::getComponents<C ...>(entity, context);
	}

	template<typename ... T>
	std::tuple<T& ...> get(Entity entity)
	{
		return IteratorInternal::getComponents<T ...>(entity, context);
	}

	EntityView operator++()
	{
		do {
			back--;
		} while ((back > front) && !componentCheck(back));

		return *this;
	}

	bool operator!=(const EntityView& other) const
	{
		return back != other.back;
	}

	Entity operator*()
	{
		return pool->at(back)->entity;
	}

private:
	static const uint32 compNum = sizeof...(C);
	uint32 compCount = 0;
	ComponentType compTypes[compNum];
	ContextECS& context;
	ComponentPool* pool = nullptr;

	uint32 compIndex = 0;
	template <typename T>
	void addComponentType()
	{
		compTypes[compIndex++] = TypeInfo<T>::type;
	}
	
	ComponentType findLeastCommon()
	{
		ComponentType leastCommonComp = 0;
		uint32 leastCommonSize = UINT32_MAX;
	
		for (uint32 i = 0; i < compCount; ++i) {
			uint32 size = context.getComponentPool(compTypes[i])->compCount;

			if (size < leastCommonSize) {
				leastCommonSize = size;
				leastCommonComp = compTypes[i];
			}
		}

		return leastCommonComp;
	}

	bool componentCheck(int index)
	{
		Entity entity = pool->at(index)->entity;

		for (uint32 i = 0; i < compCount; ++i) {
			ComponentType type = compTypes[i];

			if (type == pool->componentType)
				continue;

			if (!context.componentPools[type]->entityHasComponent(entity))
				return false;
		}

		return true;
	};

	void findLastValidEntity()
	{
		back = pool->compCount - 1;
		while ((back != -1) && !componentCheck(back)) {
			back--;
		}	
	}

	void findFirstValidEntity()
	{
		while (1) {
			if (front!= pool->compCount) {
				if (componentCheck(front)) {
					// Offset to before index for != comparison
					front--;
					break;			
				}
				front++;
			} else {
				// None found
				front = -1;
				break;
			}
		}
	}
};

#endif //QUASAR_ITERATOR_ECS_H
