#ifndef QUASAR_CONTEXT_ECS
#define QUASAR_CONTEXT_ECS

#include <queue>
#include <unordered_map>
#include "ComponentECS.hpp"
#include "CallbacksECS.hpp"
#include "Core/ECS/Types.hpp"
#include "Core/Types.hpp"

struct ContextECS
{
	struct EntityEntry
	{
		Entity entity = Entity::null;
		uint32 compIndex = 0;
		uint32 compCount = 0;
		uint32 reservedSpace = 0;

		Entity parent = Entity::null;
		std::vector<Entity> children;
		std::string name;
	};

	struct FreeSpaceEntry
	{
		uint32 compIndex;
		uint32 reservedSpace;
	};


	uint32 nextEntity = 0;
	uint32 nextEntityID = 0;
	std::vector<EntityEntry> entities;

	std::queue<Entity> freeEntities;

	std::vector<ComponentPool*> componentPools;

	CallbacksECS listeners;

	inline ComponentPool* getComponentPool(ComponentType type)
	{
		// TODO: Setup component pools ahead of time
		if (type >= componentPools.size()) {
			componentPools.resize(type + 1);
		}

		if (!componentPools[type]) {
			componentPools.at(type) = new ComponentPool(ComponentRegistry::componentSizes[type], 
					type, entities.size());
		}

		return componentPools.at(type);
	}

	inline ComponentBase* getComponent(Entity entity, ComponentType type)
	{
		ComponentPool* pool = getComponentPool(type);
		return pool->atEntity(entity.index);
	}

	~ContextECS()
	{
		for (uint32 i = 0; i < componentPools.size(); ++i) {
			delete componentPools.at(i);
		}
	}

};

#endif //QUASAR_CONTEXT_ECS
