#ifndef QUASAR_ECS_H
#define QUASAR_ECS_H

#include "Core/ECS/CallbacksECS.hpp"
#include "Core/ECS/ComponentECS.hpp"
#include "Core/ECS/Types.hpp"
#include "Core/Types.hpp"
#include "ContextECS.hpp"
#include "IteratorECS.hpp"

class ECS
{
public:
	// Component functions
	// Add default initialized component(s) to an entity
	template<typename ... C>
	void add(Entity entity)
	{
		(addComponentInternal(entity, TypeInfo<C>::type, nullptr), ...);
	}
	
	// Add components to an entity
	template<typename ... C>
	void add(Entity entity, C& ... components)
	{
		(addComponentInternal(entity, TypeInfo<C>::type, &components), ...);
	}

	// Adds a single component 
	void addComponentInternal(Entity entity, ComponentType type, void* component);

	template<typename C>
	inline void remove(Entity entity)
	{
		remove(entity, TypeInfo<C>::type);
	}

	void remove(Entity entity, ComponentType type);

	template<typename C>
	C& get(Entity entity)
	{
		ComponentType type = TypeInfo<C>::type;
		ComponentBase* comp = context.getComponent(entity, type);

		if (comp)
			return reinterpret_cast<Component<C>*>(comp)->data;

		Log::error("Get component failed: ", typeid(C).name());

		static C defaultComponent;
		return defaultComponent;
	}

	template<typename C>
	inline bool has(Entity entity)
	{
		ComponentType type = TypeInfo<C>::type;
		return has(entity, type);
	}

	inline bool has(Entity entity, ComponentType type)
	{
		// TODO: Avoid needing so many checks
		ComponentPool* pool = context.getComponentPool(type);
		return pool->entityHasComponent(entity);
	}

	// Refreshes a component by calling the onDestroy events followed by onCreate 
	// Note: This might misbehave with some components
	template<typename C>
	void refresh(Entity entity)
	{
		refresh(entity, TypeInfo<C>::type);
	}
	void refresh(Entity entity, ComponentType type);

	// Entities
	template<typename ... C>
	Entity makeEntity(C& ... components)
	{
		const uint32 compCount = (uint32)sizeof...(C);
		Entity entity = makeEntityInternal(compCount, Entity::null);
		add(entity, components...);
		return entity;
	}

	template<typename ... C>
	Entity makeEntity()
	{
		const uint32 compCount = (uint32)sizeof...(C);
		Entity entity = makeEntityInternal(compCount, Entity::null);
		add<C...>(entity);
		return entity;
	}

	void destroyEntity(Entity entity);
	void renameEntity(Entity entity, const std::string& name);
	std::string getEntityName(Entity entity);

	inline bool exists(Entity entity)
	{
		return entity && (entity.index < context.entities.size()) && 
			(context.entities[entity.index].entity == entity);
	}

	// Entity hierarchy
	void setParent(Entity entity, Entity parent, bool suppressCallbacks = false);

	inline Entity getParent(Entity entity)
	{
		return context.entities[entity.index].parent;
	}

	inline void removeParent(Entity entity)
	{
		setParent(entity, Entity::null);
	}

	inline std::vector<Entity>& getChildren(Entity entity)
	{
		return context.entities[entity.index].children;
	}

	// Update functions
	inline void update(double delta, int layerFlags)
	{
		context.listeners.updateFunctions(this, delta, layerFlags);
	}

	// Registers a callback function to be called by ECS::update()
	// Functions with a higher priority value are called first
	inline uint32 registerUpdateFunction(CallbacksECS::UpdateFn fn, int layerFlags, int priority = 0)
	{
		return context.listeners.registerUpdateFn(fn, layerFlags, priority);
	}

	template <typename O>
	uint32 registerUpdateFunction(void(O::*updateFn)(ECS*, double), O* o, int layerFlags, int priority = 0)
	{
		return context.listeners.registerUpdateFn(updateFn, o, layerFlags, priority);
	}

	// Event listeners
	template<typename ... C>
	uint32 registerListener(CallbacksECS::ListenerFn fn, int listnerFlags)
	{
		int typeIndex = 0;
		ComponentType types[MAX_TYPES_PER_CALLBACK];
		(setComponentTypes<C>(types, typeIndex), ...);
		return context.listeners.registerListenerFn(fn, types, typeIndex, listnerFlags);
	}

	template<typename ... C, typename O>
	uint32 registerListener(void(O::*fn)(ECS*, Entity), O* o, int listnerFlags)
	{
		int typeIndex = 0;
		ComponentType types[MAX_TYPES_PER_CALLBACK];
		(setComponentTypes<C>(types, typeIndex), ...);
		return context.listeners.registerListenerFn(fn, o, types, typeIndex, listnerFlags);
	}

	template<typename O>
	uint32 registerGlobalListener(void(O::*fn)(ECS*, Entity, ComponentType), O* o, int listenerFlags)
	{
		return context.listeners.registerGlobalListenerFn(fn, o, listenerFlags);
	}

	template<typename O>
	uint32 registerReparentListener(void(O::*fn)(ECS* ecs, Entity entity, Entity parent, Entity old), O* o)
	{
		return context.listeners.registerReparentFn(fn, o);
	}

	inline uint32 registerReparentListener(CallbacksECS::ReparentFn fn)
	{
		return context.listeners.registerReparentFn(fn);
	}

	void triggerListenerEvents(Entity entity, ComponentType comp, ListenerEventFlags listener);
	void triggerReparentEvents(Entity entity, Entity parent, Entity previous);

	inline void removeListner(uint32 callbackID)
	{
		context.listeners.removeListenerFn(callbackID);
	}

	// view iterates over entities with the specified component types
	template<typename ... C>
	EntityView<C ...> view()
	{
		return EntityView<C ...>(context);
	}

	// Hierarchy iterator
	HierarchyIterator hierarchyIterator(Entity rootEntity)
	{
		return HierarchyIterator(rootEntity, context);
	}

	// Serialization
	uint32 getEntitySize(Entity entity, bool preserveHeirearchy = true, bool includeReflect = false);
	uint32 getComponentSize(Entity entity, ComponentType type, bool includeReflect = false);

	bool writeEntity(Entity entity, uint8_t* dataDst, bool preserveHeirearchy = true, 
			bool includeReflect = false);
	bool writeComponent(Entity entity, ComponentType type, uint8_t* dataDst, bool includeReflect = false);

	// Reads serialized entity data from dataSrc, calls the specified Listener command
	// Optional:
	// 	entityMaps maps incoming entity indexes to local entities to correctly preserve hierarchy
	// 	map delta tracks changes to the entity map, the pair contains the incoming and local entity
	Entity readEntity(uint32 size, uint8_t* dataSrc, 
			std::unordered_map<uint32, Entity>* entityMaps = nullptr,
			std::vector<std::pair<Entity, Entity>>* mapDelta = nullptr);
	void readComponent(uint32 size, Entity dstEntity, uint8_t* dataSrc);

	// ECS State reset
	void clearCallbacks();
	
	ContextECS context;

private:
	Entity makeEntityInternal(uint32 compCount, Entity parent);

	void removeComponentInternal(Entity entity, ComponentType type);

	template<typename C>
	void setComponentTypes(ComponentType* types, int& index)
	{
		types[index++] = TypeInfo<C>::type;
	}
};

#endif //QUASAR_ECS_H
