#include "Types.hpp"

const Entity Entity::null = Entity{0, UINT32_MAX};

std::ostream& operator<<(std::ostream& o, const Entity& entity)
{
	o << "Entity {Index " << entity.index << ", ID " << entity.ID << "}";
	return o;
}
