#include "ECS.hpp"
#include "ComponentECS.hpp"
#include "Core/ECS/CallbacksECS.hpp"
#include "Core/ECS/Types.hpp"
#include "SerializeECS.hpp"

void ECS::addComponentInternal(Entity entity, ComponentType type, void* component)
{
	ComponentPool* pool = context.getComponentPool(type);

	// Entity doesn't exist or already attached
	if (!entity || (pool->entityHasComponent(entity))) {
		Log::error("Failed to add duplicate component type of type: ", 
				ComponentRegistry::componentNames.at(type));
		return;
	}

	// Allocate a new component to pool
	AllocateFunction alloc = ComponentRegistry::allocateFunctions[type];
	ComponentBase* comp = alloc(pool, entity, component);

	ContextECS::EntityEntry& entry = context.entities[entity.index];
	entry.compCount++;

	triggerListenerEvents(entity, type, LISTENER_ON_CREATE);
}

uint32 ECS::getEntitySize(Entity entity, bool preserveHeirearchy, bool includeReflect)
{
	return Serialization::getEntitySize(this, entity, preserveHeirearchy, includeReflect);
}

uint32 ECS::getComponentSize(Entity entity, ComponentType type, bool includeReflect)
{
	return Serialization::getComponentSize(this, entity, type, includeReflect);
}

// TODO: Move most of the implementaion to Serialization
bool ECS::writeEntity(Entity entity, uint8_t* dataDst, bool preserveHeirearchy, bool includeReflect)
{
	return Serialization::writeEntity(this, entity, dataDst, preserveHeirearchy, includeReflect);
}

bool ECS::writeComponent(Entity entity, ComponentType type, uint8_t* dataDst, bool includeReflect)
{
	return Serialization::writeComponent(this, entity, dataDst, type, includeReflect);
}

Entity ECS::readEntity(uint32 size, uint8_t* dataSrc, std::unordered_map<uint32, Entity>* entityMaps,
		std::vector<std::pair<Entity, Entity>>* mapDelta)
{
	return Serialization::readEntity(this, size, dataSrc, entityMaps, mapDelta);
}

void ECS::readComponent(uint32 size, Entity dstEntity, uint8_t* dataSrc)
{
	Serialization::readComponent(this, size, dstEntity, dataSrc);
}

// TODO: Accept an entity name parameter here
Entity ECS::makeEntityInternal(uint32 compCount, Entity parent)
{
	ContextECS::EntityEntry* entry = nullptr;

	// Reuse free entity index
	if (context.freeEntities.size()) {
		uint32 index = context.freeEntities.front().index;
		entry = &context.entities[index];

		entry->entity.index = index;
		context.freeEntities.pop();
	} else {
		context.entities.push_back(ContextECS::EntityEntry{});
		entry = &context.entities.back();

		entry->entity.index = context.nextEntity++;
	}
	entry->entity.ID = context.nextEntityID++;
	entry->name = std::string("Entity [") + std::to_string(entry->entity.index) + ']';
	entry->compCount = 0; // Components not added yet

	setParent(entry->entity, parent);

	for (uint32 type = 0; type < context.componentPools.size(); ++type) {
		ComponentPool* pool = context.componentPools[type];

		if (!pool)
			continue;

		pool->reserveEntityIndices(entry->entity.index);
	}

	return entry->entity;
}

void ECS::destroyEntity(Entity entity)
{
	if (context.entities[entity.index].entity.ID != entity.ID) {
		Log::error("Failed to remove entity with mismatched ID");
		return;
	}

	ContextECS::EntityEntry& entry = context.entities[entity.index];

	// Recursively destroy children
	while (context.entities[entity.index].children.size()) {
		destroyEntity(context.entities[entity.index].children.back());
	}

	context.freeEntities.push(entity);

	// Destroy all components
	// TODO: Avoid having to check all pools (Type list on entry)
	for (ComponentPool* compPool : context.componentPools) {
		if (!compPool)
			continue;

		if (entry.compCount == 0)
			break;

		if (compPool->entityHasComponent(entity)) {
			removeComponentInternal(entity, compPool->componentType);
		}
	}

	// Update hierarchy
	removeParent(entity);

	// Reset entry
	context.entities[entity.index] = ContextECS::EntityEntry{};
	context.entities[entity.index].children = std::vector<Entity>{};
}

void ECS::renameEntity(Entity entity, const std::string& name)
{
	context.entities[entity.index].name = name;
}

std::string ECS::getEntityName(Entity entity)
{
	return context.entities[entity.index].name;
}

void ECS::setParent(Entity entity, Entity parent, bool supressCallbacks)
{	
	Entity prev = context.entities[entity.index].parent;

	if (prev) {
		std::vector<Entity>& ch = context.entities[prev.index].children;

		for(int i = 0; i < ch.size(); ++i) {
			if (ch[i].index == entity.index) {
				ch[i] = ch.back();
				ch.resize(ch.size() - 1);
				break;
			}
		}
	}

	context.entities[entity.index].parent = parent;

	if (parent) {
		if (context.entities.size() <= parent.index) {
			Log::error("Can't reparent entitiy to non-existent parent", parent.index);
			return;
		}

		context.entities[parent.index].children.push_back(entity);
	}

	// Call listener
	if (!supressCallbacks) {
		triggerReparentEvents(entity, parent, prev);
	}
}

void ECS::triggerListenerEvents(Entity entity, ComponentType comp, ListenerEventFlags listener)
{
	// Component specific
	// TODO: More efficient lookup
	for (CallbacksECS::Callback& callback : context.listeners.callbacks) {
		if ((callback.callbackFn) && (callback.listenerFlags & listener)) {
			int matchedTypes = 0;
			bool shouldTrigger = false;
			for (int i = 0; i < callback.typeCount; ++i) {
	
				if (has(entity, callback.types[i]) || callback.types[i] == comp)
					matchedTypes++;

				// Trigger if the last/first to be added/removed
				if (callback.types[i] == comp)
					shouldTrigger = true;
			}
			
			if (shouldTrigger && matchedTypes == callback.typeCount) {
				callback.callbackFn(this, entity);
			}
		}
	}

	// Global
	for (CallbacksECS::GlobalCallback& callback : context.listeners.globalCallbacks) {
		if (callback.callbackFn && (callback.listenerFlags & listener)) {
			callback.callbackFn(this, entity, comp);
		}
	}
}

void ECS::triggerReparentEvents(Entity entity, Entity parent, Entity previous)
{
	for (CallbacksECS::ReparentFn& callback : context.listeners.reparentCallbacks) {
		if (callback) {
			callback(this, entity, parent, previous);
		}
	}
}

void ECS::remove(Entity entity, ComponentType type)
{
	removeComponentInternal(entity, type);
}

void ECS::removeComponentInternal(Entity entity, ComponentType type)
{
	triggerListenerEvents(entity, type, LISTENER_ON_DESTROY);

	ComponentPool* compPool = context.getComponentPool(type);
	compPool->removeComponent(entity);

	ContextECS::EntityEntry& entry = context.entities[entity.index];
	entry.compCount--;
}

void ECS::refresh(Entity entity, ComponentType type)
{
	triggerListenerEvents(entity, type, LISTENER_ON_DESTROY);
	triggerListenerEvents(entity, type, LISTENER_ON_CREATE);
}

void ECS::clearCallbacks()
{
	context.listeners.updateCallbacks.clear();
	context.listeners.callbacks.clear();
	context.listeners.globalCallbacks.clear();
	context.listeners.reparentCallbacks.clear();

	context.listeners.updateFreeSpace = {};
	context.listeners.callbackFreeSpace = {};
	context.listeners.globalCallbackFreeSpace = {};
	context.listeners.reparentFreespace = {};
}
