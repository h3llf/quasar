#ifndef QUASAR_TYPES_ECS_H
#define QUASAR_TYPES_ECS_H

#include "Core/ECS/Reflection.hpp"
#include "Core/Types.hpp"
#include "Utils/WindowsExportDLL.hpp"
#include <ostream>


// Forward declaration of ecs class for use as a 
// function parameter
class ECS;

#ifndef MAX_TYPES_PER_CALLBACK
#define MAX_TYPES_PER_CALLBACK 64u
#endif

#ifndef DEFAULT_POOL_SIZE
#define DEFAULT_POOL_SIZE 1024u 
#endif

struct DLLEXP Entity
{
	Entity() {};
	Entity(uint32 entityIndex, uint32 entityID) : index(entityIndex), ID(entityID) {};

	uint32 index;
	uint32 ID;

	bool operator==(const Entity&) const = default;
	bool operator!=(const Entity&) const = default;

	explicit operator bool() const
	{
		return (*this != Entity::null);
	}

	static const Entity null;

};

std::ostream& operator<<(std::ostream& o, const Entity& entity);

struct ComponentBase;
class ComponentPool;

typedef uint32 ComponentType;
typedef void (*DestroyFunction)(ComponentBase* baseComponent);
typedef ComponentBase* (*AllocateFunction)(ComponentPool* pool, Entity entity, void* component);
typedef ComponentBase* (*DeserializeFunction)(ComponentPool* pool, Entity entity, ComponentBase* component, 
		Reflect::FieldList* fields);
typedef size_t (*SerializeFunction)(uint8_t* dataDst, ComponentBase* component);
typedef void (*MoveFunction)(ComponentBase* dst, ComponentBase* src);
typedef size_t (*SizeFunction)(ComponentBase* component);

#endif // QUASAR_TYPES_ECS_H
