#include "SerializeECS.hpp"
#include "Core/ECS/CallbacksECS.hpp"
#include "Core/ECS/ComponentECS.hpp"
#include "Core/ECS/ContextECS.hpp"
#include "Core/ECS/ECS.hpp"
#include "Core/ECS/Types.hpp"
#include <cstring>
#include <limits>
#include <utility>
#include <unordered_set>

uint32 Serialization::getEntitySize(ECS* ecs, Entity root, bool preserveHeirearchy, bool includeReflect)
{
	uint32 size = sizeof(Serialization::SerializationHeader);

	std::unordered_set<ComponentType> componentTypes;

	if (includeReflect)
		componentTypes.reserve(ComponentRegistry::hashLookup.size());

	for (Entity entity : ecs->hierarchyIterator(root)) {
		ContextECS::EntityEntry& entry = ecs->context.entities[entity.index];
		size += sizeof(EntityHeader);
		size += ecs->getEntityName(entry.entity).size();

		for (uint32 type = 0; type < ecs->context.componentPools.size(); ++type) {
			ComponentPool* pool = ecs->context.componentPools[type];
			if (!pool)
				continue;

			if (!ecs->has(entity, type))
				continue;

			// TODO: Do this once per component type
			if (includeReflect) {
				componentTypes.insert(type);
			}

			size += sizeof(ComponentHeader);

			ComponentBase* comp = pool->atEntity(entity.index);
			size += ComponentRegistry::serializedSizeFunctions[type](comp);
		}

		if (!preserveHeirearchy)
			break;
	}

	for (ComponentType t : componentTypes) {
		size += getFieldListSize(t);
	}

	return size;
}

uint32 Serialization::getComponentSize(ECS* ecs, Entity entity, ComponentType type, bool includeReflect)
{
	uint32 size = sizeof(ComponentHeader);
	ComponentPool* pool = ecs->context.componentPools[type];
	if (!pool)
		return 0;

	if (!ecs->has(entity, type))
		return 0;

	ComponentBase* comp = pool->atEntity(entity.index);
	size += ComponentRegistry::serializedSizeFunctions[type](comp);
	return size;
}

bool Serialization::writeEntity(ECS* ecs, Entity root, uint8_t* dataDst, bool preserveHeirearchy, 
		bool includeReflect)
{
	SerializationHeader* serializationData = new(dataDst) SerializationHeader;
	uint32 offset = sizeof(SerializationHeader);
	serializationData->hirearchhyPreserved = preserveHeirearchy;

	uint32 entityCount = 0;

	std::unordered_set<ComponentType> componentTypes;

	// Write entity and component data
	for (Entity currentEntity : ecs->hierarchyIterator(root)) {
		ContextECS::EntityEntry& entry = ecs->context.entities[currentEntity.index];
		uint32 compCount = entry.compCount;

		// Entity header information
		EntityHeader* entityHeader = new(dataDst + offset) EntityHeader();
		offset += sizeof(EntityHeader);
		entityHeader->entity = currentEntity;

		// Entity name
		std::string entityName = ecs->getEntityName(currentEntity);
		entityHeader->nameSize = entityName.size();
		memcpy(dataDst + offset, entityName.data(), entityHeader->nameSize);
		offset += entityHeader->nameSize;

		// TODO: Store a list of components per entity to avoid checking all of them
		// For each component
		for (uint32 type = 0; type < ecs->context.componentPools.size(); ++type) {
			if (!ecs->has(currentEntity, type))
				continue;

			bool shouldReflect = false;
			if (!componentTypes.contains(type)) {
				componentTypes.insert(type);
				shouldReflect = includeReflect;
			}

			offset += writeComponentInternal(ecs, currentEntity, dataDst + offset, 
					type, shouldReflect);
		}

		entityHeader->nextEntityOffset = offset;

		// Set parent component
//		if (preserveHeirearchy && currentEntity != root) {
			entityHeader->parentEntity = ecs->getParent(currentEntity);
//		} else {
//			entityHeader->parentEntity = Entity::null;
//		}

		entityCount++;
	}

	serializationData->entityCount = entityCount;

	return true;
}

Entity Serialization::readEntity(ECS* ecs, uint32 size, uint8_t* dataSrc,
		std::unordered_map<uint32, Entity>* entityMaps,
		std::vector<std::pair<Entity, Entity>>* mapDelta)
{
	// Read header data
	SerializationHeader* serializationHeader = reinterpret_cast<SerializationHeader*>(dataSrc);
	uint32 offset = sizeof(SerializationHeader);
	
	std::unordered_map<ComponentType, Reflect::FieldList> reflectionCache;
	std::unordered_map<uint32, Entity> entityMapBuffer;
	std::vector<Entity> entityList(serializationHeader->entityCount);

	// No entity mappings provided
	if (!entityMaps) {
		entityMaps = &entityMapBuffer;
	}

	// Pre allocate storage for map delta
	if (mapDelta) {
		mapDelta->reserve(serializationHeader->entityCount);
	}

	// Obtain entity information
	for (uint32 i = 0; i < serializationHeader->entityCount; ++i) {
		// Entity header
		EntityHeader* entityHeader = reinterpret_cast<EntityHeader*>(dataSrc + offset);

		Entity newEntity = ecs->makeEntity();
		entityList[i] = newEntity;

		// Map old Entity onto new one
		entityMaps->operator[](entityHeader->entity.index) = newEntity;
		if (mapDelta) {
			mapDelta->push_back(std::make_pair(entityHeader->entity, newEntity));
		}

		offset = entityHeader->nextEntityOffset;
	}

	offset = sizeof(SerializationHeader);

	// Deserialize entities
	for (uint32 i = 0; i < serializationHeader->entityCount; ++i) {
		assert(offset < size && "Entity offset out of range");

		// Entity header
		EntityHeader* entityHeader = reinterpret_cast<EntityHeader*>(dataSrc + offset);
		offset += sizeof(EntityHeader);

		Entity currentEnt = entityList[i];

		char* stringData = reinterpret_cast<char*>(dataSrc + offset);
		std::string entityName(stringData, entityHeader->nameSize);
		ecs->renameEntity(currentEnt, entityName);
		offset += entityHeader->nameSize;

		// Update parent entity
		Entity newParent = Entity::null;
		if (serializationHeader->hirearchhyPreserved && entityHeader->parentEntity &&
				entityMaps->contains(entityHeader->parentEntity.index)) 
		{
				newParent = entityMaps->at(entityHeader->parentEntity.index);
		}	
		ecs->setParent(currentEnt, newParent, true);

		ContextECS::EntityEntry& entry = ecs->context.entities[currentEnt.index];

		// Deserialize components
		while(offset < entityHeader->nextEntityOffset) {
			bool result;
			offset += readComponentInternal(ecs, currentEnt, dataSrc + offset, 
					&result, &reflectionCache);

			// Failed
			if (!result) {
				// Delete current entities and abort
//				for (uint32 j = 0; j <= i; ++j) {
//					ecs->destroyEntity(entityList[j]);
//					return Entity::null;
//				}
			} else {
				entry.compCount++;
			}
		}

		// Delay reparent events until components are avaliable
		ecs->triggerReparentEvents(currentEnt, newParent, Entity::null);

	}

	// Return the root entity
	return entityList[0];
}

bool Serialization::writeComponent(ECS* ecs, Entity entity, uint8_t* dataDst, ComponentType type, 
		bool includeReflect)
{
	if (!ecs->has(entity, type))
		return 0;

	size_t size = writeComponentInternal(ecs, entity, dataDst, type, includeReflect);
	return (size);
}

bool Serialization::readComponent(ECS* ecs, uint32 size, Entity entity, uint8_t* dataSrc)
{
	// TODO: Check header size against input size
	bool result;
	readComponentInternal(ecs, entity, dataSrc, &result, nullptr);
	return result;
}

bool Serialization::getCompAttribs(ComponentHeader* header, ComponentType* type, uint32* size)
{
	// Validate & get attribs
	if (!ComponentRegistry::typeLookup.contains(header->nameHash)) {
		Log::error("Failed to read component, typename hash not found");
		return false;
	}
			
	*type = ComponentRegistry::typeLookup[header->nameHash];
	*size = ComponentRegistry::componentSizes[*type];

	if (*size != header->size) {
		Log::error("Failed to read component: ", ComponentRegistry::componentNames[*type], 
				", incorrect size in header");
		return false;
	}

	return true;
}

size_t Serialization::writeComponentInternal(ECS* ecs, Entity entity, uint8_t* dataDst, 
		ComponentType type, bool includeReflect)
{
	size_t offset = 0;
	ComponentPool* pool = ecs->context.componentPools[type];
	ComponentHeader* header = reinterpret_cast<ComponentHeader*>(dataDst + offset);
	header->nameHash = ComponentRegistry::hashLookup[type];
	offset += sizeof(ComponentHeader);

	// Write reflection data
	size_t refSize = 0;
	if (includeReflect) {
		// Zero if fields not avaliable
		refSize = writeFieldListData(type, dataDst + offset);
	}
	//Log::out("Relsize: ", refSize, " ref: ", includeReflect);
	header->hasReflectionData = (refSize != 0);
	offset += refSize;

	// Component data
	// TODO: Check for reflection data or trivially copyable
	ComponentBase* comp = pool->atEntity(entity.index);
	SerializeFunction serializeFn = ComponentRegistry::serializeFunctions[type];
	size_t size = serializeFn(dataDst + offset, comp);
	header->size = size;
	offset += size;

	return offset;
}

size_t Serialization::readComponentInternal(ECS* ecs, Entity entity, uint8_t* dataSrc, bool* result, 
		std::unordered_map<ComponentType, Reflect::FieldList>* reflectionCache)
{
	size_t offset = 0;
	ComponentHeader* header = reinterpret_cast<ComponentHeader*>(dataSrc + offset);
	offset += sizeof(ComponentHeader);

	size_t hash = header->nameHash;
	if (!ComponentRegistry::typeLookup.contains(header->nameHash))
	{
		Log::error("Failed to load unrecognized component type");
		*result = false;

		// Get fd size
		if (header->hasReflectionData) {
			offset += Reflect::FieldList().readFieldListData(dataSrc + offset);
		}

		return offset + header->size;
	}

	ComponentType type = ComponentRegistry::typeLookup[hash];
	DeserializeFunction deserializeFn = ComponentRegistry::deserializeFunctions[type];

	// Read reflection information
	Reflect::FieldList fieldList;
	if (header->hasReflectionData) {
		offset += readFieldListData(type, dataSrc + offset, fieldList);
		if (reflectionCache) {
			reflectionCache->insert(std::make_pair(type, fieldList));
		}
	} else if (reflectionCache && reflectionCache->contains(type)) {
		fieldList = reflectionCache->at(type); // TODO: Avoid copy
	}

	// Replace existing (TODO: Might not want this to be the default)
	if (ecs->has(entity, type)) {
		ecs->remove(entity, type);
	}

	ComponentPool* pool = ecs->context.getComponentPool(type);
	ComponentBase* comp = reinterpret_cast<ComponentBase*>(dataSrc + offset);
	deserializeFn(pool, entity, comp, &fieldList);
	ecs->triggerListenerEvents(entity, type, LISTENER_ON_DESERIALIZE);

	offset += header->size;
	*result = true;

	return offset;
}

size_t Serialization::getFieldListSize(ComponentType type)
{
	if (!ComponentRegistry::reflectionData[type]) {
		return 0;
	}

	Reflect::FieldList& fl = *ComponentRegistry::reflectionData[type];
	return fl.getFieldListSize();
}

size_t Serialization::writeFieldListData(ComponentType type, uint8_t* dataDst)
{
	if (!ComponentRegistry::reflectionData[type]) {
		return 0;
	}

	Reflect::FieldList& fl = *ComponentRegistry::reflectionData[type];
	return fl.writeFieldListData(dataDst);
}

size_t Serialization::readFieldListData(ComponentType type, uint8_t* dataSrc, Reflect::FieldList& dstList)
{
	if (!ComponentRegistry::reflectionData[type]) {
		return 0;
	}

//	Reflect::FieldList& fl = *ComponentRegistry::reflectionData[type];
//	return fl.readFieldListData(dataSrc);
	return dstList.readFieldListData(dataSrc);
}
