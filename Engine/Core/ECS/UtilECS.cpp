#include "UtilECS.hpp"
#include "Core/Assets/AssetManager.hpp"
#include "Core/Assets/BVH.hpp"
#include "Core/ECS/ComponentECS.hpp"
#include "Core/ECS/ECS.hpp"
#include "Core/ECS/SerializeECS.hpp"
#include "Core/ECS/Types.hpp"
#include "Core/Components/CModel.hpp"
#include "Client/Components/CRenderable.hpp"
#include "GFX/Renderer.hpp"
#include "Utils/Math/Transform.hpp"
#include "Utils/Timer.hpp"
#include "simple_glTF/simple_glTF.hpp"
#include <cstdio>
#include <filesystem>
#include <ios>

using namespace quasar;

// TODO; Move some parts to separeate function to avoid code duplication
void ECSUtil::writeComponentDisk(ECS* ecs, std::string filename, Entity entity, ComponentType type, 
		bool overwrite)
{
	addFileExtension(filename, ".component");

	if (std::filesystem::exists(filename)) {
		if (overwrite) {
			std::filesystem::remove(filename);
		} else {
			Log::error("Failed to write component to: ", filename, ", file already exists.");
			return;
		}
	}

	uint32 size = ecs->getComponentSize(entity, type, true);
	if (size == 0) {
		Log::error("Failed to write component of size 0");
		return;
	}

	uint8_t* dataDst = new uint8_t[size];

	if (!ecs->writeComponent(entity, type, dataDst, true)) {
		Log::error("Failed to write component");
		delete [] dataDst;
		return;
	}

	std::fstream file(filename, std::ios::binary | std::ios::app);

	if (!file.is_open()) {
		Log::error("Failed to write component to: ", filename);
		file.close();
		return;
	}

	file.write(reinterpret_cast<char*>(dataDst), size);
	file.close();

	delete [] dataDst;
}

void ECSUtil::readComponentDisk(ECS* ecs, Entity entity, const std::string& filename)
{
	std::fstream file(filename, std::ios::in | std::ios::binary);

	if (!file.is_open()) {
		Log::error("Failed to load component at ", filename);
		file.close();
		return;
	}
	
	file.seekg(0, std::ios::end);
	uint32 size = file.tellg();
	file.seekg(0, std::ios::beg);

	uint8_t* data = new uint8_t[size];

	file.read(reinterpret_cast<char*>(data), size);

	ecs->readComponent(size, entity, data);

	delete [] data;
}

bool ECSUtil::writeEntityDisk(ECS* ecs, std::string filename, Entity entity, bool overwrite)
{
	if (filename.empty()) {
		Log::error("Failed to write entity, filename empty");
	}

	addFileExtension(filename, ".entity");

	if (std::filesystem::exists(filename)) {
		if (overwrite) {
			std::filesystem::remove(filename);
		} else {
			Log::error("Failed to write entity to: ", filename, ", file already exists.");
			return false;
		}
	}

	std::fstream file(filename, std::ios::binary | std::ios::app);
	if (!file.is_open()) {
		Log::error("Failed to write entity to: ", filename);
		file.close();
		return false;
	}

	uint32 size = ecs->getEntitySize(entity, true, true);
	uint8_t* dataDst = new uint8_t[size];

	if (!ecs->writeEntity(entity, dataDst, true, true)) {
		Log::error("Failed to write entity");
		delete [] dataDst;
		return false;
	}

	file.write(reinterpret_cast<char*>(dataDst), size);
	file.close();

	delete [] dataDst;
	return true;
}

void ECSUtil::addFileExtension(std::string& filename, const std::string& fileExt)
{
	// Add file extension if none exists
	if (filename.size() < fileExt.size() ||
		filename.substr(filename.size() - fileExt.size(),
		fileExt.size()) != fileExt)
	{
		filename += fileExt;
	}
}

// TODO: Reenable console commands

void ECSUtil::writeEntityDiskConsole(std::string filename, uint32 entIndex, uint32 entID, bool overwrite)
{
//	writeEntityDisk(&clientECS, filename, Entity{entIndex, entID}, overwrite);
}

Entity ECSUtil::readEntityDisk(ECS* ecs, const std::string& filename)
{
#ifdef _DEBUG
	Timer::StopStartTimer timer;
	timer.start();
#endif
	Entity entity = {0, 0};
	
	std::fstream file(filename, std::ios::in | std::ios::binary);

	if (!file.is_open()) {
		Log::error("Failed to load entity at ", filename);
		file.close();
		return entity;
	}
	
	file.seekg(0, std::ios::end);
	uint32 size = file.tellg();
	file.seekg(0, std::ios::beg);

	uint8_t* data = new uint8_t[size];

	file.read(reinterpret_cast<char*>(data), size);

	entity = ecs->readEntity(size, data);

	delete [] data;

#ifdef _DEBUG
	Log::out("Loaded: ", filename, " in ", timer.stop() / 1000, "ms");
#endif
	return entity;
}

void ECSUtil::readEntityDiskConsole(std::string filename)
{
//	Entity tmpEnt = readEntityDisk(&clientECS, filename);
//	Log::out("Loaded Entity(", tmpEnt.index, ", ", tmpEnt.ID, ')');
}

void ECSUtil::destroyEntityConsole(uint32 entIndex, uint32 entID)
{
//	clientECS.destroyEntity(Entity{entIndex, entID});
}

void ECSUtil::addEntityCVars(Console& console)
{
	console.bindCommand("load", ECSUtil::readEntityDiskConsole);
	console.bindCommand("write", ECSUtil::writeEntityDiskConsole);
	console.bindCommand("destroy", ECSUtil::destroyEntityConsole);
}

void ECSUtil::createModelEntities(AssetManager* assetMgr, ECS* ecs, const std::string& path, 
		const std::string& filename, Entity root)
{
	Model* model = assetMgr->getModel(path + filename);

	std::vector<Entity> entities(model->gltf.sceneNodes.size());
	for (int i = 0; i < model->gltf.sceneNodes.size(); ++i) {
			entities[i] = ecs->makeEntity();
	}

	if (!root || !ecs->has<CModel>(root)) {
		CModel cM;
		cM.modelFolder = path;
		cM.modelName = filename;

		if (!root) {
			root = ecs->makeEntity(cM);
			ecs->renameEntity(root, filename);
		} else {
			ecs->add(root, cM);
		}
	}
	ecs->get<CModel>(root).hasMeshes = true;

	for (int i = 0; i < model->gltf.sceneNodes.size(); ++i) {
		auto& node = model->gltf.sceneNodes[i];

		if (node.parent != -1) {
			ecs->setParent(entities[i], entities[node.parent]);
		} else {
			ecs->setParent(entities[i], root);
		}

		if (!node.name.empty()) {
			ecs->renameEntity(entities[i], node.name);
		}

		CTransform cT = createNodeTransform(node);
		ecs->add(entities[i], cT);

		// TODO: Combined primitives
		if (node.type == simple_glTF::NODE_MESH || node.type == simple_glTF::NODE_SKINNED_MESH) {
			std::vector<int>& primitives = model->gltf.meshPrimitives[node.meshIndex];
			setupMeshEntity(assetMgr, ecs, model, path + filename, primitives[0], entities[i]);

			// Overflow primitives
			// TODO: Allow multiple primitives per mesh and remove this
			for (int i = 1; i < primitives.size(); ++i) {
				Entity overflow = ecs->makeEntity(cT);
				if (node.parent != -1) {
					ecs->setParent(overflow, entities[node.parent]);
				} else {
					ecs->setParent(overflow, root);
				}

				if (!node.name.empty()) {
					ecs->renameEntity(overflow, node.name);
				}

				setupMeshEntity(assetMgr, ecs, model, path + filename, primitives[i], overflow);
			}
		}
	}
}

CTransform ECSUtil::createNodeTransform(simple_glTF::Node &node)
{
	CTransform cT;
	if (node.usesMatrix) {
		mat4 matrix = *reinterpret_cast<mat4*>(node.matrix);
		qml::decompose(matrix, cT.position, cT.scale, cT.rotation);
	} else {
		cT.position = vec3 {
			node.px,
			node.py,
			node.pz
		};

		cT.scale = vec3 {
			node.sx,
			node.sy,
			node.sz
		};

		cT.rotation = quat {
			node.rx,
			node.ry,
			node.rz,
			node.rw
		};
	}

	return cT;
}

void ECSUtil::setupMeshEntity(AssetManager* assetMgr, ECS* ecs, quasar::Model* model, 
		const std::string& path, int primitive, Entity entity)
{
	Model::Mesh& mesh = model->meshes[primitive];
	BVHNode* node = assetMgr->getBVHNode(path, primitive);

	CMesh cM = {primitive, true, mesh.min, mesh.max, false, node};

	CRenderable cR = {
		.meshID = mesh.rendererID
	};

	ecs->add(entity, cM, cR);

	setupDefaultMaterial(ecs, model, mesh, entity);
}

void ECSUtil::setupDefaultMaterial(ECS* ecs, quasar::Model* model, quasar::Model::Mesh& mesh, Entity entity)
{
	CMaterialPBR cMaterial {};

	Model::MaterialInfo& mat = model->defaultMaterials[mesh.materialIndex];

	// Copy default texture names
	for (auto& texture : mat.textureNames) {
		if (texture.first == Model::Albedo)
			cMaterial.albedoImage = texture.second;
		else if (texture.first == Model::MetallicRoughness)
			cMaterial.metalRoughImage = texture.second;
		else if (texture.first == Model::Normal) 
			cMaterial.normalImage = texture.second;
		else if (texture.first == Model::Emissive)
			cMaterial.emissiveImage = texture.second;
	}

	cMaterial.materialProps = {
		mat.albedoColour,
		mat.emissiveFactor,
		mat.metallic,
		mat.roughness,
		mat.normalScale,
		-1, -1, -1, -1
	};

	cMaterial.materialOptions = {
		false,
		mat.doubleSided
	};

	ecs->add(entity, cMaterial);
}
