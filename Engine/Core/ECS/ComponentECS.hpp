#ifndef QUASAR_COMPONENT_ECS_H
#define QUASAR_COMPONENT_ECS_H

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <math.h>
#include <memory>
#include <unordered_map>
#include <bitset>
#include <vector>
#include <shared_mutex>
#include "GFX/RendererData.hpp"
#include "Utils/Timer.hpp"
#include "Utils/WindowsExportDLL.hpp"
#include "Reflection.hpp"

#include "Types.hpp"

template<typename C>
struct TypeInfo;

template<typename C>
struct Component;

struct ComponentBase
{
	struct Entity entity;
};

template<typename C>
struct Component
{
	ComponentBase base;
	C data {};

	Component(Entity entity) : base({entity}) {};

	Component(Entity entity, C* component) : base({entity}), data(*component) {};

	Component(ComponentBase* component)
	{
		Component<C>* comp = component;

		base = comp->base;
		data = comp->data;
	}
};



struct ComponentRegistry
{
	static inline std::shared_mutex readWriteLock;

	template<typename C>
	static  ComponentType registerComponentType()
	{
		readWriteLock.lock();

		uint64 nameHash = Reflect::getHashFromTypename<C>();

		// Workaround for MSVC not sharing static template members accross DLLs if they are implemented in a header
		if (typeLookup.contains(nameHash)) {
			return typeLookup.at(nameHash);
		}

		componentSizes.push_back(sizeof(Component<C>));
		componentNames.push_back(Reflect::getTypename<C>());
		destroyFunctions.push_back(TypeInfo<C>::destroyComponent);
		allocateFunctions.push_back(TypeInfo<C>::allocateComponent);
		deserializeFunctions.push_back(TypeInfo<C>::deserializeComponent);
		serializeFunctions.push_back(TypeInfo<C>::serializeComponent);
		moveFunctions.push_back(TypeInfo<C>::moveComponent);
		serializedSizeFunctions.push_back(TypeInfo<C>::getSerializedSize);

		if constexpr (Reflect::HasReflectionData<C>) {
			reflectionData.push_back(std::make_shared<Reflect::FieldList>(C().getReflectionData()));
		} else {
			reflectionData.push_back(nullptr);
		}

		ComponentType type;
		type = hashLookup.size();
		hashLookup.push_back(nameHash);
		typeLookup[nameHash] = type;

		readWriteLock.unlock();

		return type;
	}

	// Unregisters all components with ID's higher than start
	static void unregisterComponentTypeAbove(ComponentType start);

	static inline std::unordered_map<uint64, ComponentType> typeLookup;
	static inline std::vector<uint64> hashLookup;
	static inline std::vector<uint32> componentSizes;
	static inline std::vector<std::string> componentNames;
	static inline std::vector<DestroyFunction> destroyFunctions;
	static inline std::vector<AllocateFunction> allocateFunctions;
	static inline std::vector<DeserializeFunction> deserializeFunctions;
	static inline std::vector<SerializeFunction> serializeFunctions;
	static inline std::vector<MoveFunction> moveFunctions;
	static inline std::vector<SizeFunction> serializedSizeFunctions;
	static inline std::vector<std::shared_ptr<Reflect::FieldList>> reflectionData;
};

class ComponentPool
{
public:
	std::vector<uint8_t> data;
	std::vector<uint32> sparseArray;

	ComponentType componentType;
	uint32 componentSize;
	uint32 compCount = 0;

	ComponentPool(uint32 size, ComponentType type, uint32 entityCount) :
		componentType(type), componentSize(size)
	{
		data.resize(DEFAULT_POOL_SIZE * componentSize);
	}

	void reserve(uint32 index)
	{
		if (index * componentSize >= data.size()) {
			data.resize(data.size() + DEFAULT_POOL_SIZE * componentSize);
		}
	}

	void reserveEntityIndices(uint32 entityIndex)
	{
		if (sparseArray.size() <= entityIndex) {
			sparseArray.resize(entityIndex + DEFAULT_POOL_SIZE, UINT32_MAX);
		}
	}

	ComponentBase* operator[](uint32 index)
	{
		return reinterpret_cast<ComponentBase*>(&data.at(index * componentSize));
	}

	ComponentBase* at(uint32 index)
	{
		return operator[](index);
	}

	ComponentBase* atEntity(uint32 entityIndex)
	{
		uint32 index = sparseArray[entityIndex];

		if (index != UINT32_MAX) {
			return operator[](index);
		} else {
			return nullptr;
		}
	}

	uint32 getNextIndex()
	{
		compCount++;
		return compCount - 1;
	}

	void removeComponent(Entity entity)
	{
		if (compCount == 0) 
			return;

		/// TODO: Define uint32max as null index
		uint32 index = sparseArray[entity.index];
		sparseArray[entity.index] = UINT32_MAX;

		ComponentBase* component = operator[](index);

		compCount--;

		DestroyFunction dstFunc = ComponentRegistry::destroyFunctions[componentType];
		if (!dstFunc) 
			return;

		if (compCount == index)  {
			// Destroy if this is the last component
			dstFunc(component);
		} else {
			ComponentBase* endComp = operator[](compCount);

			//Move last component to the empty slot
			MoveFunction moveFunc = ComponentRegistry::moveFunctions[componentType];
			moveFunc(component, endComp);
			dstFunc(endComp);

			// Update index
			sparseArray[endComp->entity.index] = index;
		}

	}

	bool entityHasComponent(Entity entity)
	{
		return (sparseArray.size() > entity.index) && (sparseArray.at(entity.index) != UINT32_MAX);
	}
};

template<Reflect::HasReflectionData T>
static size_t getComponentSize(T* compData)
{
	ComponentType type = TypeInfo<T>::type;
	return ComponentRegistry::reflectionData[type]->getClassSize(compData) + sizeof(ComponentBase);
}

template<typename T>
static size_t getComponentSize(T* compData)
{
	return sizeof(Component<T>);
}

template<Reflect::HasReflectionData T>
static T readComponent(Component<T>* component, Reflect::FieldList* fields)
{
	T buffer;
	ComponentType type = TypeInfo<T>::type;

	if (fields) {
		ComponentRegistry::reflectionData[type]->deserializeClass(&buffer, 
			reinterpret_cast<uint8_t*>(&component->data), fields);
	} else {
		ComponentRegistry::reflectionData[type]->deserializeClass(&buffer, 
			reinterpret_cast<uint8_t*>(&component->data));
	}

	return buffer;
}

template<typename T>
static T readComponent(Component<T>* component, Reflect::FieldList*)
{
	return component->data;
}

template<Reflect::HasReflectionData T>
static size_t writeComponent(uint8_t* dataDst, T* compData, ComponentBase* base)
{
	memcpy(dataDst, base, sizeof(ComponentBase));
	size_t offset = sizeof(ComponentBase);

	ComponentType type = TypeInfo<T>::type;
	offset += ComponentRegistry::reflectionData[type]->serializeClass(compData, dataDst + offset);

	return offset;
}

template<typename T>
static size_t writeComponent(uint8_t* dataDst, T* compData, ComponentBase* base)
{
	memcpy(dataDst, base, sizeof(ComponentBase));
	size_t offset = sizeof(ComponentBase);
	memcpy(dataDst + offset, compData, sizeof(T));
	return offset += sizeof(T);
}

template <typename C>
struct TypeInfo
{
	static const uint64 nameHash;
	static const uint32 size;
	static const ComponentType type;

	static void destroyComponent(ComponentBase* baseComponent)
	{
		Component<C>* comp = reinterpret_cast<Component<C>*>(baseComponent);
		comp->~Component<C>();
	}

	static ComponentBase* allocateComponent(ComponentPool* pool, Entity entity, void* component)
	{
		uint32 index = pool->getNextIndex(); 
		pool->reserve(index);
		pool->reserveEntityIndices(entity.index);

		pool->sparseArray[entity.index] = index;

		Component<C>* newComp;
		if (component)
			newComp = new(pool->at(index)) Component<C>(entity, reinterpret_cast<C*>(component));
		else
			newComp = new(pool->at(index)) Component<C>(entity);

		return reinterpret_cast<ComponentBase*>(newComp);
	}

	static ComponentBase* deserializeComponent(ComponentPool* pool, Entity entity, 
			ComponentBase* baseComponent, Reflect::FieldList* fields)
	{
		Component<C>* component = reinterpret_cast<Component<C>*>(baseComponent);
		C compData = readComponent(component, fields);
		return allocateComponent(pool, entity, &compData);
	}

	static void moveComponent(ComponentBase* dst, ComponentBase* src)
	{
		*reinterpret_cast<Component<C>*>(dst) = std::move(*reinterpret_cast<Component<C>*>(src));
	}

	static size_t serializeComponent(uint8_t* dataDst, ComponentBase* baseComponent)
	{
		Component<C>* component = reinterpret_cast<Component<C>*>(baseComponent);
		return writeComponent<C>(dataDst, &component->data, &component->base);
	}

	static size_t getSerializedSize(ComponentBase* baseComponent)
	{
		Component<C>* component = reinterpret_cast<Component<C>*>(baseComponent);
		return getComponentSize<C>(&component->data);
	}

	TypeInfo(const TypeInfo&) = delete;
	void operator=(const TypeInfo&) = delete;
};

template <typename C>
const uint64 TypeInfo<C>::nameHash = Reflect::getHashFromTypename<C>();

template <typename C>
const uint32 TypeInfo<C>::size = sizeof(Component<C>);

template <typename C>
const ComponentType TypeInfo<C>::type = ComponentRegistry::registerComponentType<C>();

#endif //QUASAR_COMPONENT_ECS_H
