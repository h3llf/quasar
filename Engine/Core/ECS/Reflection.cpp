#include "Reflection.hpp"
#include "Utils/Timer.hpp"
#include <cassert>

using namespace Reflect;

FieldList::FieldList(std::initializer_list<Field> fields) : fieldData(fields)
{
}

size_t FieldList::getFieldListSize()
{
	size_t size = sizeof(Header);

	for (const Field& fd : fieldData) {
		if (fd.serializeOptions & SERIALIZE_DISABLE)
			continue;

		size += getFieldSize(fd);
	}

	return size;
}

size_t FieldList::getFieldSize(const Field& fd)
{
	size_t size = 0;
	size += fd.fieldName.size()+ 1;
	size += sizeof(fd.offset);

	size += getFieldValSize(fd.valueType);

	size += sizeof(fd.serializeOptions);
	size += sizeof(fd.editorOptions);

	return size;
}

size_t FieldList::getFieldValSize(const Field::FieldVal& val)
{
	size_t size = 0;

	size += sizeof(val.triviallyCopyable);
	size += sizeof(val.typeHash);
	size += sizeof(val.typeSize);

	return size;
}

size_t FieldList::writeFieldListData(uint8_t* data)
{
	Header* header = new(data) Header;

	size_t offset = sizeof(Header);

	for (const Field& fd : fieldData) {
		if (fd.serializeOptions & SERIALIZE_DISABLE) {
			continue;
		}

		offset += writeFieldListField(data + offset, fd);
		header->fieldCount++;
	}

	header->size = offset;
	return offset;
}

size_t FieldList::writeFieldListField(uint8_t* output, const Field& fd)
{
	size_t offset = 0;

	strncpy(reinterpret_cast<char*>(output), fd.fieldName.c_str(), fd.fieldName.size() + 1);
	offset += fd.fieldName.size() + 1;
	memcpy(output + offset, &fd.offset, sizeof(fd.offset));
	offset += sizeof(fd.offset);

	offset += writeFieldListVal(output + offset, fd.valueType);

	memcpy(output + offset, &fd.serializeOptions, sizeof(fd.serializeOptions));
	offset += sizeof(fd.serializeOptions);
	memcpy(output + offset, &fd.editorOptions, sizeof(fd.editorOptions));
	offset += sizeof(fd.editorOptions);

	return offset;
}

size_t FieldList::writeFieldListVal(uint8_t* output, const Field::FieldVal& val)
{
	size_t offset = 0;

	memcpy(output + offset, &val.triviallyCopyable, sizeof(val.triviallyCopyable));
	offset += sizeof(val.triviallyCopyable);
	memcpy(output + offset, &val.typeHash, sizeof(val.typeHash));
	offset += sizeof(val.typeHash);
	memcpy(output + offset, &val.typeSize, sizeof(val.typeSize));
	offset += sizeof(val.typeSize);

	return offset;
}

size_t FieldList::readFieldListData(uint8_t *input)
{
	fieldData.clear();

	Header* header = reinterpret_cast<Header*>(input);
	size_t offset = sizeof(Header);

	fieldData.resize(header->fieldCount);

	for (Field& fd : fieldData) {
		if (fd.serializeOptions & SERIALIZE_DISABLE) {
			continue;
		}

		offset += readFieldListField(input + offset, fd);

		assert(offset <= header->size && "Failed to read field list data: incorrect header size");
	}

	return offset;
}

size_t FieldList::readFieldListField(uint8_t* input, Field& fd)
{
	size_t offset = 0;

	fd.fieldName = std::string(reinterpret_cast<char*>(input));
	offset += strlen(reinterpret_cast<char*>(input)) + 1;
	fd.offset = *reinterpret_cast<uint32*>(input + offset);
	offset += sizeof(fd.offset);

	offset += readFieldListVal(input + offset, fd.valueType);

	fd.serializeOptions = *reinterpret_cast<SerializeOptions*>(input + offset);
	offset += sizeof(fd.serializeOptions);
	fd.editorOptions = *reinterpret_cast<EditorOptions*>(input + offset);
	offset += sizeof(fd.editorOptions);

	return offset;
}

size_t FieldList::readFieldListVal(uint8_t* input, Field::FieldVal& val)
{
	size_t offset = 0;
	val.triviallyCopyable = *reinterpret_cast<bool*>(input + offset);
	offset += sizeof(val.triviallyCopyable);
	val.typeHash= *reinterpret_cast<size_t*>(input + offset);
	offset += sizeof(val.typeHash);
	val.typeSize= *reinterpret_cast<uint32*>(input + offset);
	offset += sizeof(val.typeSize);

	return offset;
}

size_t FieldList::getClassSize(void* srcClass)
{
	size_t size = 0;

	for (Field field : fieldData) {
		if (field.serializeOptions & SERIALIZE_DISABLE)
			continue;

		size += sizeof(FieldHeader);
		size += getValSize((char*)srcClass + field.offset, field.valueType);
	}

	return size;
}

size_t FieldList::getValSize(void* srcVal, const Field::FieldVal& val)
{
	if (val.triviallyCopyable) {
		return val.typeSize;
	}

	if (val.containerSizeFn) {
		return val.containerSizeFn(srcVal);
	}

	return 0;
}

size_t FieldList::serializeClass(void* src, uint8_t* output)
{
	size_t offset = 0;

	for (Field field : fieldData) {
		if (field.serializeOptions & SERIALIZE_DISABLE)
			continue;

		FieldHeader* fieldHeader = reinterpret_cast<FieldHeader*>(output + offset);
		offset += sizeof(FieldHeader);

		if (field.valueType.triviallyCopyable) {
			memcpy(output + offset, (char*)src + field.offset, field.valueType.typeSize);
			offset += field.valueType.typeSize;
		} else if (field.valueType.serializeFunction) {
			offset += field.valueType.serializeFunction((char*)src + field.offset, output + offset);
		}
		
		// Record the next field's offset
		fieldHeader->offset = offset;
	}

	return offset;
}

void FieldList::deserializeClass(void *dst, uint8_t* input)
{
	size_t offset = 0;
	for (Field field : fieldData) {
		if (field.serializeOptions & SERIALIZE_DISABLE)
			continue;

		FieldHeader* fieldHeader = reinterpret_cast<FieldHeader*>(input + offset);
		offset += sizeof(FieldHeader);

		if (field.valueType.triviallyCopyable) {
			memcpy((char*)dst + field.offset, input + offset, field.valueType.typeSize);
		} else if (field.valueType.serializeFunction) {
			field.valueType.deserializeFunction((char*)dst + field.offset, input + offset);
		}

		offset = fieldHeader->offset;
	}
}

void FieldList::deserializeClass(void *dst, uint8_t* input, FieldList* inputFields)
{
	size_t offset = 0;
	for (uint32 i = 0; i < inputFields->fieldData.size(); ++i) {
		Field& srcField = inputFields->fieldData[i];
		Field* dstField = nullptr;

		if (srcField.serializeOptions & SERIALIZE_DISABLE)
			continue;

		FieldHeader* fieldHeader = reinterpret_cast<FieldHeader*>(input + offset);
		offset += sizeof(FieldHeader);

		if (fieldData[i] == srcField) {
			dstField = &fieldData[i];
		} else {
			// Search for moved fields
			for (uint32 j = 0; j < fieldData.size(); ++j) {
				if (fieldData[j] == srcField) {
					dstField = &fieldData[j];
					break;
				}
			}
		}

		if (dstField && !(dstField->serializeOptions & SERIALIZE_DISABLE)) {
			if (dstField->valueType.triviallyCopyable) {
				memcpy((char*)dst + dstField->offset, input + offset, dstField->valueType.typeSize);
			} else if (dstField->valueType.serializeFunction) {
				dstField->valueType.deserializeFunction((char*)dst + dstField->offset, input + offset);
			}		
		} else {
			Log::out("[Warning] Failed to remap field \"", srcField.fieldName, 
					"\" during deserialization");
		}

		offset = fieldHeader->offset;
	}
}

size_t FieldList::count()
{
	return fieldData.size();
}

Field& FieldList::operator[](int i)
{
	return fieldData[i];
}
