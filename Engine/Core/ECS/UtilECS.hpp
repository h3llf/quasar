#ifndef QUASAR_UTIL_ECS
#define QUASAR_UTIL_ECS 

#include "Core/Assets/AssetManager.hpp"
#include "Core/Assets/Model.hpp"
#include "Core/Console.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/ECS/Types.hpp"
#include "simple_glTF/simple_glTF.hpp"

#include <fstream>
#include <iostream>
#include <string>

class ECSUtil
{
public:
	static void writeComponentDisk(ECS* ecs, std::string filename, Entity entity, 
			ComponentType type, bool overwrite = false);
	static void readComponentDisk(ECS* ecs, Entity entity, const std::string& filename);

	static bool writeEntityDisk(ECS* ecs, std::string filename, Entity entity, bool overwrite = false);
	static Entity readEntityDisk(ECS* ecs, const std::string& filename);

	// TODO: Specify ecs!
	static void readEntityDiskConsole(std::string filename);
	static void writeEntityDiskConsole(std::string filename, uint32 entIndex, uint32 entID, bool overwrite);
	static void destroyEntityConsole(uint32 entIndex, uint32 entID);

	static void addEntityCVars(Console& console);

	static void addFileExtension(std::string& filename, const std::string& fileExt);

	static void createModelEntities(quasar::AssetManager* assetMgr, ECS* ecs, const std::string& patH, 
			const std::string& filename, Entity root = Entity::null);

	static void setupMeshEntity(quasar::AssetManager* assetMgr, ECS* ecs, quasar::Model* model, 
			const std::string& path, int primitive, Entity entity);

	static quasar::CTransform createNodeTransform(simple_glTF::Node& node);
	static void setupDefaultMaterial(ECS* ecs, quasar::Model* model, quasar::Model::Mesh& mesh, Entity entity);
};

#endif //QUASAR_UTIL_ECS
