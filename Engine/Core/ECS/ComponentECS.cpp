#include "ComponentECS.hpp"
#include "Core/ECS/Types.hpp"
#include <cstddef>

void ComponentRegistry::unregisterComponentTypeAbove(ComponentType type)
{
	readWriteLock.lock();

	if (componentSizes.size() > type + 1) {
		Log::out("Unregister starting from: ", componentNames[type + 1]);
	}

	for (int i = type + 1; i < hashLookup.size(); ++i) {
		typeLookup.erase(hashLookup[i]);
	}
	
	hashLookup.resize(type + 1);
	componentSizes.resize(type + 1);
	componentNames.resize(type + 1);
	destroyFunctions.resize(type + 1);
	allocateFunctions.resize(type + 1);
	deserializeFunctions.resize(type + 1);
	serializeFunctions.resize(type + 1);
	moveFunctions.resize(type + 1);
	serializedSizeFunctions.resize(type + 1);
	reflectionData.resize(type + 1);

	readWriteLock.unlock();
}
