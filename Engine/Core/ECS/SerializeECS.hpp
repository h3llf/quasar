#ifndef QUASAR_SERIALIZATION_H
#define QUASAR_SERIALIZATION_H

#include "Core/ECS/CallbacksECS.hpp"
#include "Types.hpp"
#include <unordered_map>

class Serialization
{
public:
	struct ComponentHeader
	{
		uint32 size;
		uint64 nameHash;

		// Enabled once per component per entity (first usage)
		bool hasReflectionData;

		uint8_t reserved[16];
	};

	struct EntityHeader
	{
		Entity entity = Entity::null;
		Entity parentEntity = Entity::null;
		uint32 nextEntityOffset = 0;
		uint8_t reserved[16];

		// Size of the name string after this header
		uint32 nameSize = 0;
	};
	
	struct SerializationHeader
	{
		uint32 version = 0;
		uint32 entityCount = 0;
		bool hirearchhyPreserved = true;
		uint8_t reserved[64];
	};

	static uint32 getEntitySize(ECS* ecs, Entity root, bool preserveHeirearchy, bool includeReflect);
	static uint32 getComponentSize(ECS* ecs, Entity entity, ComponentType type, bool includeReflect);

	// Writes a hierarchy of entities starting with root to the dataDst array
	// Note: dataDst should be allocated ahead of time using the size from getEntitySize()
	static bool writeEntity(ECS* ecs, Entity root, uint8_t* dataDst, bool preserveHeirearchy, 
			bool includeReflect);

	// Loads a hierarchy of entities & components from the dataSrc array and returns the root entity
	static Entity readEntity(ECS* ecs, uint32 size, uint8_t* dataSrc, 
			std::unordered_map<uint32, Entity>* entityMaps,
			std::vector<std::pair<Entity, Entity>>* mapDelta);

	// Writes component data into dataDst
	static bool writeComponent(ECS* ecs, Entity srcEntity, uint8_t* dataDst, ComponentType type, 
			bool includeReflect);

	// Loads a component from dataSrc and adds it to entity
	static bool readComponent(ECS* ecs, uint32 size, Entity dstEntity, uint8_t* dataSrc);

	static bool getCompAttribs(ComponentHeader* header, ComponentType* type, uint32* size);

private:
	static size_t writeComponentInternal(ECS* ecs, Entity entity, uint8_t* dataDst, ComponentType type, 
			bool includeReflect);
	static size_t readComponentInternal(ECS* ecs, Entity entity, uint8_t* dataSrc, bool* result, 
			std::unordered_map<ComponentType, Reflect::FieldList>* reflectionCache);

	static size_t getFieldListSize(ComponentType type);
	static size_t writeFieldListData(ComponentType type, uint8_t* dataDst);
	static size_t readFieldListData(ComponentType type, uint8_t* dataSrc, Reflect::FieldList& dstList);
};

#endif //QUASAR_SERIALIZATION_H
