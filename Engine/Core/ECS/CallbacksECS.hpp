#ifndef QUASAR_CALLBACKS_ECS_H
#define QUASAR_CALLBACKS_ECS_H

#include "Core/Types.hpp"
#include "ComponentECS.hpp"
#include "Types.hpp"
#include <bitset>
#include <functional>
#include <queue>

enum ListenerEventFlags
{
	LISTENER_NONE = 0,
	LISTENER_ON_CREATE = 1 << 0,
	LISTENER_ON_DESTROY = 1 << 1,
	LISTENER_ON_DESERIALIZE = 1 << 2,
	LISTENER_ON_ASSET_LOAD = 1 << 3,
};

enum UpdateLayer
{
	UPDATE_NONE = 0,
	UPDATE_EDITOR = 1 << 0,
	UPDATE_GAME_FAST = 1 << 1,
	UPDATE_ALWAYS = UPDATE_EDITOR | UPDATE_GAME_FAST,
	UPDATE_ASSET_POLLING = 1 << 3
};

class CallbacksECS 
{
public:
	typedef std::function<void(ECS* ecs, double delta)> UpdateFn;
	typedef std::function<void(ECS* ecs, Entity entity)> ListenerFn;
	typedef std::function<void(ECS* ecs, Entity entity, ComponentType type)> GlobalListenerFn;
	typedef std::function<void(ECS* ecs, Entity entity, Entity parent, Entity oldParent)> ReparentFn;
//	typedef std::function<void(ECS* ecs, Entity entity, std::string name)> RenameFn;

	struct Update
	{
		UpdateFn callback;
		int layerFlags;
		int priority;
	};

	struct Callback
	{
		ListenerFn callbackFn;
		uint32 ID;
		int listenerFlags;
		int typeCount;
		ComponentType types[MAX_TYPES_PER_CALLBACK];
	};

	struct GlobalCallback
	{
		GlobalListenerFn callbackFn;
		uint32 ID;
		int listenerFlags;
	};

	void updateFunctions(ECS* ecs, double delta, int layerFlags);
	template <typename O>
	uint32 registerUpdateFn(void(O::*updateFn)(ECS*, double), O* o, int layerFlags, int priority)
	{
		UpdateFn newFn = [updateFn, o](ECS* ecs, double delta)
			{
				(o->*updateFn)(ecs, delta);
			};

		return registerUpdateFn(newFn, layerFlags, priority);
	}
	uint32 registerUpdateFn(UpdateFn fn, int layerFlags, int priority);
	void removeUpdateFn(uint32 callbackID);
	std::vector<Update> updateCallbacks;
	std::queue<uint32> updateFreeSpace;

	template <typename O>
	uint32 registerListenerFn(void(O::*listFn)(ECS*, Entity), O* o, 
			ComponentType* types, int typeCount, int listenerEvent)
	{
		ListenerFn newFn = [listFn, o](ECS* ecs, Entity entity)
			{
				(o->*listFn)(ecs, entity);
			};
		
		return registerListenerFn(newFn, types, typeCount, listenerEvent);

	}
	uint32 registerListenerFn(ListenerFn fn, ComponentType* types, int typeCount, int listenerEvent);
	void removeListenerFn(uint32 callbackID);
	std::vector<Callback> callbacks;
	std::queue<uint32> callbackFreeSpace;

	template <typename O>
	uint32 registerGlobalListenerFn(void(O::*listFn)(ECS*, Entity, ComponentType), O* o, int listenerEvent)
	{
		GlobalListenerFn newFn = [listFn, o](ECS* ecs, Entity entity, ComponentType type)
			{
				(o->*listFn)(ecs, entity, type);
			};
		
		return registerGlobalListenerFn(newFn, listenerEvent);
	}
	uint32 registerGlobalListenerFn(GlobalListenerFn fn, int listenerEvent);
	void removeGlobalListenerFn(uint32 callbackID);
	std::vector<GlobalCallback> globalCallbacks;
	std::queue<uint32> globalCallbackFreeSpace;

	template <typename O>
	uint32 registerReparentFn(void(O::*reparentFn)(ECS* ecs, Entity entity, Entity parent, Entity old), O* o)
	{
		ReparentFn newFn = [reparentFn, o](ECS* ecs, Entity entity, Entity parent, Entity old)
			{
				(o->*reparentFn)(ecs, entity, parent, old);
			};

		return registerReparentFn(newFn);
	}
	uint32  registerReparentFn(ReparentFn fn);
	void removeReparentFn(uint32 callbackID);
	std::vector<ReparentFn> reparentCallbacks;
	std::queue<uint32> reparentFreespace;
private:
	// TODO: Avoid needing an additional ordering list
	
	void configureUpdateOrder();
	std::vector<int> updateCallbackOrder;
};

#endif //QUASAR_CALLBACKS_ECS_H
