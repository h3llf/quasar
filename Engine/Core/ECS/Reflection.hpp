#ifndef QUASAR_REFLECTION_H
#define QUASAR_REFLECTION_H

#include <cstddef>
#include <initializer_list>
#include <type_traits>
#include <vector>
#include <array>
#include <unordered_map>
#include <map>
#include <string>
#include <iostream>
#include <regex>
#include "Core/Types.hpp"

// Get human readable typenames on GCC and Clang
#ifdef __GNUC__
	#include <cxxabi.h>
#endif

#define REFLECTION_DATA static const FieldData reflectedTypes;

#define BEGIN_CLASS()\
	Reflect::FieldList getReflectionData()\
		{\
		using T = std::remove_reference<decltype(*this)>::type;\
		return Reflect::FieldList {

#define END_CLASS()\
		};\
	};

#define FIELD(FIELD_NAME, ...)\
		Reflect::createField(#FIELD_NAME, offsetof(T, FIELD_NAME), FIELD_NAME __VA_OPT__(,) __VA_ARGS__),

namespace Reflect
{

// Returns a compiler agnostic typename for C
// This function has not been tested extensively and might not always produce consistent names
template <typename C>
static const std::string getTypename()
{
	std::string typeidName = typeid(C).name();

	// Convert to human readable string if using GCC,
	// MSVC provides human readable strings
#ifdef __GNUC__
	size_t length = 4096;
	char* result = new char[length];
	int status;
	abi::__cxa_demangle(typeidName.c_str(), result, &length, &status);
	typeidName = result;
#endif

	// Remove prefixs to make naming consistent with GCC
#ifdef _MSC_VER
	const char* reg[] = {
		"struct",
		"class",
		"enum"
	};

	for (uint32_t i = 0; i < 3; ++i) {
		typeidName = regex_replace(typeidName, std::regex(reg[i]), "");
	}
#else
	typeidName = regex_replace(typeidName, std::regex("__cxx\\d\\d::"), "");
#endif

	typeidName = regex_replace(typeidName, std::regex("(\\d)ul"), "$1");
	typeidName = regex_replace(typeidName, std::regex(" "), "");

	if (typeidName[0] == ' ') {
		typeidName = typeidName.substr(1, typeidName.npos);
	}

	return typeidName;
}

// Generates a unique type identifier for C
// FNV-1a hash www.isthe.com/chongo/tech/comp/fnv/
template <typename C>
static const uint64_t getHashFromTypename()
{
	std::string name = getTypename<C>();

	uint64_t constexpr fnv_prime = 1099511628211ULL;
	uint64_t constexpr fnv_offset_basis = 14695981039346656037ULL;

	uint64_t hash = fnv_offset_basis;
    
	for(char c : name) {
		hash ^= c;
		hash *= fnv_prime;
	}

	return hash;
}

enum SerializeOptions
{
	SERIALIZE_DEFAULT = 0,
	SERIALIZE_DISABLE = 1 << 0,
};

enum EditorOptions
{
	EDITOR_NONE = 0,
	EDITOR_READ = 1 << 0,
	EDITOR_WRITE = 1 << 1,
	EDITOR_READ_WRITE = ((1 << 0) | (1 << 1)),
};

struct SerializeContainerHeader
{
	uint32 version = 0;
	uint32 elementCount = 0;
	uint8_t reserved[32];
};

// Check insert function parameters
template<typename T, typename P>
concept IsSequenceContainer = requires(T t, P p) {
	t.emplace(t.begin(), p);
};

template<typename T, typename P>
concept IsAssociativeContainer = requires(T t, P p) {
	t.emplace(p);
};

// TODO: Needs a proper iterator check not just begin & end functions
// TODO: Add a check for the emplace function
template<typename T>
concept IsContainer = (IsSequenceContainer<T, typename T::value_type> || 
	IsAssociativeContainer<T, typename T::value_type>) && 
	requires(T t) {
		t.begin();
		t.end();
		t.size();
	};

typedef size_t(*GetSizeFunction)(const void* src);
typedef size_t(*SerializeFunction)(const void* src, uint8_t* output);
typedef size_t(*DeserializeFunction)(void* dst, uint8_t* input);

struct Field
{
	struct FieldVal
	{
		bool triviallyCopyable;

		std::string typeName = "";
		size_t typeHash = 0;
		uint32 typeSize = 0;

		// Get the current size of this field
		template<typename T>
		static size_t getSizeFunction(const void* src)
		{
			return getSize<T>(src);
		}

		template<IsContainer T>
		static size_t getSize(const void* src)
		{
			const T* container = reinterpret_cast<const T*>(src);
			if (container->size()) {
				using V = typename T::value_type;

				return container->size() * getSize<V>(&(*container->begin())) + 
					sizeof(SerializeContainerHeader);
			} else {
				return sizeof(SerializeContainerHeader);
			}

			return 0;
		}

		template<typename T>
		static size_t getSize(const void* src)
		{
			if (std::is_trivially_copyable_v<T>) {
				return sizeof(T);
			} else if (std::is_same<T, std::string>::value) {
				const std::string* str = reinterpret_cast<const std::string*>(src);
				return str->size() + 1;
			} else {
				return 0;
			}
		}

		GetSizeFunction containerSizeFn = nullptr;

		// Write field data to a byte array
		template<typename T>
		static size_t getSerializeFunction(const void* src, uint8_t* output)
		{
			return serializeField<T>(src, output);
		}

		template<IsContainer T>
		static size_t serializeField(const void* src, uint8_t* output)
		{
			const T* container = reinterpret_cast<const T*>(src);

			SerializeContainerHeader* header = reinterpret_cast<SerializeContainerHeader*>(output);
			header->elementCount = container->size();
			size_t offset = sizeof(SerializeContainerHeader);

			if (!container->size())
				return 0;

			using V = typename T::value_type;
			for (const auto& element : *container) {
				offset += serializeField<V>(&element, output + offset);
			}

			return offset;
		}

		template<typename T>
		static size_t serializeField(const void* src, uint8_t* output)
		{
			if (std::is_trivially_copyable_v<T>) {
				memcpy(output, src, sizeof(T));
				return sizeof(T);
			} else if (std::is_same<T, std::string>::value) {
				const std::string* str = reinterpret_cast<const std::string*>(src);
				size_t strLen = str->size() + 1;
				memcpy(output, str->c_str(), strLen);
				return strLen;
			} else {
				return 0;
			}
		}

		SerializeFunction serializeFunction = nullptr;

		template<typename T, typename P> requires IsSequenceContainer<T, P>
		static void containerEmplace(T* container, const P& value)
		{
			container->emplace(container->end(), value);
		}

		template<typename T, typename P> requires IsAssociativeContainer<T, P>
		static void containerEmplace(T* container, const P& value)
		{
			container->emplace(value);
		}

		template<typename T>
		static size_t getDeserializeFunction(void* dst, uint8_t* input)
		{
			return deserializeField<T>(dst, input);
		}

		template<IsContainer T>
		static size_t deserializeField(void* dst, uint8_t* input)
		{
			SerializeContainerHeader* header = reinterpret_cast<SerializeContainerHeader*>(input);
			size_t offset = sizeof(SerializeContainerHeader);

			T* container = reinterpret_cast<T*>(dst);

			using V = typename T::value_type;
			for (uint32 i = 0; i < header->elementCount; ++i) {
				V value;
				offset += deserializeField<V>(&value, input + offset);
				containerEmplace(container, value);
			}

			return offset;
		}

		template<typename T>
		static size_t deserializeField(void* dst, uint8_t* input)
		{
			if (std::is_trivially_copyable_v<T>) {
				memcpy(dst, input, sizeof(T));
				return sizeof(T);
			} else if (std::is_same<T, std::string>::value) {
				// TODO: Validate string & size
				std::string* str = reinterpret_cast<std::string*>(dst);
				*str = std::string(reinterpret_cast<char*>(input));
				return str->size() + 1;
			} else {
				Log::error("Failed to deserialize non trivially copyable field.");
				return 0;
			}

		}
		DeserializeFunction deserializeFunction = nullptr;
	};

	std::string fieldName;
	uint32 offset;

	FieldVal valueType = {};

	int serializeOptions = SERIALIZE_DEFAULT;
	void setFlag(SerializeOptions o) 
	{
		serializeOptions |= o;
	}

	int editorOptions = EDITOR_NONE;
	void setFlag(EditorOptions o) 
	{
		editorOptions |= o;
	}

	// Throw a compiler error if an invliad flag is used
	template<typename T>
	void setFlag(T options) 
	{
		static_assert(sizeof(T) == std::size_t(-1),
				"Option parameter passed on field reflection is not a valid enum type");
	}

	bool operator==(const Field& v) const
	{
		return (fieldName == v.fieldName && valueType.typeHash == v.valueType.typeHash);
	}
};

template<typename T>
void determineTypeInfo(const T& v, Field::FieldVal& field)
{
	field = {};
	field.triviallyCopyable = std::is_trivially_copyable_v<T>;
	field.typeName = getTypename<T>();
	field.typeHash = getHashFromTypename<T>();
	field.typeSize = sizeof(T);

	if (!field.triviallyCopyable) {
		void* dont;
		field.containerSizeFn = Field::FieldVal::getSizeFunction<T>;
		field.serializeFunction = Field::FieldVal::getSerializeFunction<T>;
		field.deserializeFunction = Field::FieldVal::getDeserializeFunction<T>;
	}
}

template<typename T, typename ... Enums>
Field createField(const std::string& fieldName, uint32 offset, const T& field, Enums ... options)
{
	Field fieldData = {
		.fieldName = fieldName,
		.offset = offset,
	};

	determineTypeInfo(field, fieldData.valueType);

	(fieldData.setFlag(options), ...);

	return fieldData;
}

class FieldList
{
public:
	FieldList() = default;

	FieldList(std::initializer_list<Field> fields);

	size_t count();
	Field& operator[](int i);;
	std::vector<Field> fieldData;
	
	// Get the serialized size of this field list
	size_t getFieldListSize();

	// Serialize the field list as binary data
	size_t writeFieldListData(uint8_t* output);

	// Construct this field list from binary data
	size_t readFieldListData(uint8_t* input);

	// TODO: Move class & field serialization elsewhere?
	// Get the size of srcClass as binary data
	size_t getClassSize(void* srcClass);

	// Copy the data of the class src into the output buffer
	size_t serializeClass(void* src, uint8_t* output);

	// Copy the input data into a class
	// Field names of srcFD can be remapped to this field list if provided
	void deserializeClass(void* dst, uint8_t* output);
	void deserializeClass(void* dst, uint8_t* output, FieldList* srcFieldList);

private:
	struct Header
	{
		uint32 size = 0;
		uint32 fieldCount = 0;
	};

	// Field list read/write
	size_t writeFieldListField(uint8_t* output, const Field& fd);
	size_t readFieldListField(uint8_t* input, Field& fd);
	size_t writeFieldListVal(uint8_t* output, const Field::FieldVal& val);
	size_t readFieldListVal(uint8_t* input, Field::FieldVal& val);
	size_t getFieldSize(const Field& fd);
	size_t getFieldValSize(const Field::FieldVal& val);

	// Class serialization
	struct FieldHeader
	{
		// Offset in bytes of the next field
		size_t offset;
	};
	
	size_t getValSize(void* srcVal, const Field::FieldVal& val);
};

//void serializeClass(void* srcClass, uint8_t* output, FieldList& fields);
//void deserializeClass(void* dstClass, uint8_t* input, FieldList& fields, FieldList* inputFields = nullptr);

//void serializeField(void* srcField, uint8_t output, Field& fd);

template<typename T>
concept HasReflectionData = requires(T t) {
	{t.getReflectionData()} -> std::same_as<Reflect::FieldList>;
};


};

#endif //QUASAR_REFLECTION_H
