#include "CallbacksECS.hpp"
#include "Core/ECS/ComponentECS.hpp"
#include "Core/ECS/Types.hpp"
#include <utility>
#include <algorithm>

// TODO: Make a new container class to handle the vector and queue together
void CallbacksECS::updateFunctions(ECS* ecs, double delta, int layerFlags)
{
	for (int index : updateCallbackOrder) {
		if (updateCallbacks[index].callback && (updateCallbacks[index].layerFlags & layerFlags)) {
			updateCallbacks[index].callback(ecs, delta);
		}
	}
}

uint32 CallbacksECS::registerUpdateFn(UpdateFn fn, int layerFlags, int priority)
{
	uint32 index = 0;

	Update callback = Update{fn, layerFlags, priority};

	if (updateFreeSpace.size()) {
		index = updateFreeSpace.front();
		updateFreeSpace.pop();
		updateCallbacks[index] = callback;
	} else {
		index = updateCallbacks.size();
		updateCallbacks.push_back(callback);
	}

	configureUpdateOrder();
	return index;
}

void CallbacksECS::removeUpdateFn(uint32 callbackID)
{
	updateFreeSpace.push(callbackID);
	updateCallbacks[callbackID] = Update{nullptr, UPDATE_NONE, 0};
	configureUpdateOrder();
}

void CallbacksECS::configureUpdateOrder()
{
	updateCallbackOrder.clear();
	for (int i = 0; i < updateCallbacks.size(); ++i) {
		if (updateCallbacks[i].callback == nullptr) {
			continue;
		}

		updateCallbackOrder.push_back(i);
	}

	std::vector<Update>& updateCallbacksRef = updateCallbacks;
	std::sort(updateCallbackOrder.begin(), updateCallbackOrder.end(),
			[updateCallbacksRef](int a, int b)
			{ return updateCallbacksRef[a].priority > updateCallbacksRef[b].priority;});
}

uint32 CallbacksECS::registerListenerFn(ListenerFn fn, 
		ComponentType* types, int typeCount, int listenerFlags)
{
	uint32 index = 0;

	if (callbackFreeSpace.size()) {
		index = callbackFreeSpace.front();
		callbackFreeSpace.pop();
	} else {
		index = callbacks.size();
		callbacks.resize(index + 1);
	}

	callbacks[index] = Callback{fn, index, listenerFlags, typeCount};

	for (int i = 0; i < typeCount; ++i) {
		callbacks[index].types[i] = types[i];
	}

	return index;
}

void CallbacksECS::removeListenerFn(uint32 callbackID)
{
	callbackFreeSpace.push(callbackID);
	callbacks[callbackID].listenerFlags = LISTENER_NONE;
	callbacks[callbackID].callbackFn = nullptr;

}

uint32 CallbacksECS::registerGlobalListenerFn(GlobalListenerFn fn, int listenerEvent)
{
		uint32 index = 0;

		if (globalCallbackFreeSpace.size()) {
			index = globalCallbackFreeSpace.front();
			globalCallbackFreeSpace.pop();
		} else {
			index = globalCallbacks.size();
			globalCallbacks.resize(index + 1);
		}

		globalCallbacks[index] = GlobalCallback{fn, index, listenerEvent};

		return index;
}

void CallbacksECS::removeGlobalListenerFn(uint32 callbackID)
{
	globalCallbackFreeSpace.push(callbackID);
	globalCallbacks[callbackID].listenerFlags = LISTENER_NONE;
	globalCallbacks[callbackID].callbackFn = nullptr;

}

uint32 CallbacksECS::registerReparentFn(ReparentFn fn)
{
	uint32 index = 0;

	if (reparentFreespace.size()) {
		index = reparentFreespace.front();
		reparentFreespace.pop();
	} else {
		index = reparentCallbacks.size();
		reparentCallbacks.resize(index + 1);
	}

	reparentCallbacks[index] = fn;
	return index;
}

void CallbacksECS::removeReparentFn(uint32 callbackID)
{
	reparentFreespace.push(callbackID);
	reparentCallbacks[callbackID] = nullptr;
}
