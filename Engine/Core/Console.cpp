#include "Console.hpp"
#include <string>
#include <typeindex>
#include <filesystem>
#include <algorithm>
#include <fstream>
#include "Utils/Math/Vector.hpp"

void Console::init(std::string fileName)
{
	filePath = fileName + ".conf";

	parseCVarFile();

	inputPollingThread = std::thread{&Console::pollInput, this};
	inputPollingThread.detach();

	bindCommand("print", &Console::printVal, this);
	bindCommand("list", &Console::list, this);
	bindCommand("ls", &Console::list, this);
}

void Console::deinit()
{
	isRunning = false;
	inputPollingThread.join();
}

void Console::saveCVarState()
{
	std::ofstream cvarFile(filePath);

	for (std::pair<std::string, CVar> cVar : cVars) {
		if (cVar.second.persistent) {
			fileEntries[cVar.first] = cVar.second.printFunc();
		}
	}

	for (std::pair<std::string, std::string> entry : fileEntries) {
		cvarFile << entry.first << ": " << entry.second << '\n';
	}
}

void Console::parseCVarFile()
{
	if (std::filesystem::exists(filePath)) {
		std::ifstream cvarFile(filePath);

		if (cvarFile.is_open()) {
			std::string line;

			while (std::getline(cvarFile, line)) {
				if (line.empty())
					continue;

				std::string value;
				std::string res[2] = {""};
				std::stringstream ss(line);
				int i = 0;
				while (std::getline(ss, value, ':') && i < 2) {
					// Remove leading and trailing spaces
					int leading = value.find_first_not_of(' ');
					int trailing = value.find_last_not_of(' ');
					if (trailing == value.npos)
						trailing = value.size();

					res[i] = value.substr(leading, trailing - leading + 1);
					i++;
				}
				std::cout << std::endl;

				fileEntries[res[0]] = res[1];
			}
		}
	}
}

void Console::updateCommands()
{
	std::lock_guard<std::mutex> lck(inputMutex);
	while (commandQueue.size()) {
		processCommand(commandQueue.front());
		commandQueue.pop();
	}
}

void Console::processCommand(std::string& cmd)
{
	if (cmd.empty()) return;

	// Itemise inputs
	size_t prevChar = 0;
	size_t currentChar = 0;
	unsigned int argCount = 0;

	char containerChar = 0;

	std::vector<std::string> cmdArgs;

	while ((!cmd.empty()) && (argCount < 256)) {
		// Quotes
		if (cmd.at(0) == '\"' || cmd.at(0) == '\'')
			containerChar = cmd.at(0);

		if (cmd.at(0) == '(')
			containerChar = ')';

		// Remove bracket/quotation
		if (containerChar) {
			cmd.erase(0, 1);
		};

		// Remove leading whitespace
		cmd.erase(0, cmd.find_first_not_of(' '));

		if (!containerChar) {
			currentChar = cmd.find_first_of(' ');
		} else {
			currentChar = cmd.find_first_of(containerChar);
		}
		containerChar = 0;

		cmdArgs.push_back(cmd.substr(0, currentChar));
		++argCount;

		if (currentChar == std::string::npos) break;
		
		cmd.erase(0, currentChar + 1);
	}

	if ((cmdArgs[0] == "set") && (cmdArgs.size() == 3)) {
		setVal(cmdArgs[1], &cmdArgs[2]);
		return;
	}

	// Execute matching commands
	if (cmdCommands.contains(cmdArgs[0])) {
		Command& com = cmdCommands.at(cmdArgs[0]);

		if (com.argCount != cmdArgs.size() - 1) {
			std::cout << "Could not execute " << cmdArgs[0] << " command: Got " << 
				cmdArgs.size()-1 << " arguments but expected " << com.argCount << std::endl;
			return;
		}

		if (cmdArgs.size() > 1) {
			cmdCommands[cmdArgs[0]].func(&cmdArgs[1]);
		}
		else {
			cmdCommands[cmdArgs[0]].func(nullptr);
		}
		
		return;
	}

	std::cout << "Unrecognized command." << std::endl;
}

void Console::pollInput()
{
	while(isRunning) {
		std::string input;
		std::getline(std::cin, input);

		std::lock_guard<std::mutex> lck(inputMutex);
		commandQueue.push(input);
	}
}

void Console::setVal(std::string valueName, std::string params[])
{
	if (cVars.find(valueName) != cVars.end()) {
		cVars.at(valueName).setFunc(params);
	}
	else {
		std::cout << "Variable name " << valueName << " not found" << std::endl;
	}
}

void Console::printVal(std::string valueName)
{
	if (cVars.find(valueName) != cVars.end()) {
		CVar* cvar = &cVars.at(valueName);
		
		std::cout << valueName << " = ";
		std::cout << cvar->printFunc() << std::endl;
	}
	else {
		std::cout << "Variable name " << valueName << " not found" << std::endl;
	}
}

void Console::list(std::string type)
{
	std::transform(type.begin(), type.end(), type.begin(), ::tolower);

	if (type == "cmd") {
		std::cout << "\n[" << cmdCommands.size() << "] commands bound.\n";
		for (std::pair<std::string, Command> cmd : cmdCommands) {
			std::cout << cmd.first << "	Args=" << cmd.second.argCount << '\n';
		}
	} else if (type == "var") {
		std::cout << "\n[" << cVars.size() << "] variables bound.\n";
		for (std::pair<std::string, CVar> cvar : cVars) {
			std::cout << cvar.second.type << ' ' << cvar.first <<
				" = " << cvar.second.printFunc() << '\n';
		}
	} else {
		std::cout << "Invalid argument: " << type << '\n' << 
			"usage: [list / ls] [cmd / var] " << '\n';
	}

	std::cout << '\n';
}
