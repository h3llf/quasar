#include "MeshColliders.hpp"
#include "Utils/Log.hpp"
#include <sstream>
#include <fstream>
#include <filesystem>

using namespace physx;
using namespace quasar;

void MeshColliders::init(physx::PxPhysics* physics)
{
	pxPhysics = physics;
}

bool MeshColliders::readMeshFile(std::vector<physx::PxU8>* pData, const std::string& path, 
		int meshIndex, bool isConvex)
{
	size_t pathHash = std::hash<std::string>{}(path);
	std::stringstream outputDir ;
	outputDir << "ColliderCache/" << pathHash << '_' << meshIndex << 
		((isConvex) ? ".convex_mesh" : ".triangle_mesh");

	std::fstream fileIn(outputDir.str(), std::ios::in | std::ios::binary);

	if (!fileIn) {
		return false;
	}

	fileIn.seekg(0, std::ios::end);
	size_t size = fileIn.tellg();
	fileIn.seekg(0, std::ios::beg);

	pData->resize(size);
	fileIn.read(reinterpret_cast<char*>(pData->data()), size);

	return true;
}

bool MeshColliders::writeMeshFile(physx::PxDefaultMemoryOutputStream* pStream, const std::string& path, 
			int meshIndex, bool isConvex)
{
	size_t pathHash = std::hash<std::string>{}(path);
	std::stringstream outputDir ;
	outputDir << "ColliderCache/" << pathHash << '_' << meshIndex << ".triangle_mesh";

	if (!std::filesystem::exists("ColliderCache")) {
		std::filesystem::create_directories("ColliderCache");
	}

	std::fstream fileOut(outputDir.str(), std::ios::out | std::ios::binary);
	if (!fileOut) {
		Log::error("Failed to write collider cache file: ", outputDir.str());
		return false;
	} else {
		fileOut.write(reinterpret_cast<char*>(pStream->getData()),  pStream->getSize());
	}

	return true;
}


int MeshColliders::loadTriangleMesh(const std::string& path, int meshIndex)
{
	std::vector<PxU8> data;
	if (!readMeshFile(&data, path, meshIndex, false)) {
		return -1;
	}

	PxDefaultMemoryInputData readBuffer(data.data(), data.size());
	PxTriangleMesh* tri = pxPhysics->createTriangleMesh(readBuffer);	

	if (!tri) {
		Log::error("Failed to create triangle mesh collider from file");
		return -1;
	}

	std::lock_guard<std::mutex> lck(colliderGenSync);
	return triangleMeshes.add(tri);
}

int MeshColliders::loadConvexMesh(const std::string& path, int meshIndex)
{
	std::vector<PxU8> data;
	if (!readMeshFile(&data, path, meshIndex, true)) {
		return -1;
	}

	PxDefaultMemoryInputData readBuffer(data.data(), data.size());
	PxConvexMesh* convex= pxPhysics->createConvexMesh(readBuffer);

	if (!convex) {
		Log::error("Failed to create convex mesh collider from file");
		return -1;
	}

	std::lock_guard<std::mutex> lck(colliderGenSync);
	return convexMeshes.add(convex);
}

int MeshColliders::createTriangleMesh(std::vector<Vertex>* vertices, std::vector<uint32>* indices, 
		bool cache, const std::string* path, int meshIndex)
{

	PxTriangleMeshDesc meshDesc;
	meshDesc.points.count = vertices->size();
	meshDesc.points.stride = sizeof(Vertex);
	meshDesc.points.data = vertices->data();

	meshDesc.triangles.count = indices->size() / 3;
	meshDesc.triangles.stride = sizeof(uint32) * 3;
	meshDesc.triangles.data = indices->data();

	PxCookingParams params(pxPhysics->getTolerancesScale());

	PxTriangleMesh* tri = nullptr;

	if (cache == false) {
		// Directly cook into physics
		tri = PxCreateTriangleMesh(params, meshDesc, pxPhysics->getPhysicsInsertionCallback());
	} else {
		PxDefaultMemoryOutputStream writeBuffer;
		if (!PxCookTriangleMesh(params, meshDesc, writeBuffer)) {
			Log::out("Triangle mesh cooking failed");
			return -1;
		}

		// Store triangle mesh to disk
		writeMeshFile(&writeBuffer, *path, meshIndex, false);

		PxDefaultMemoryInputData readBuffer(writeBuffer.getData(), writeBuffer.getSize());
		tri = pxPhysics->createTriangleMesh(readBuffer);
	}

	if (!tri) {
		Log::error("Failed to create triangle mesh from model: ", *path);
		return -1;
	}

	std::lock_guard<std::mutex> lck(colliderGenSync);
	return triangleMeshes.add(tri);
}

int MeshColliders::createConvexMesh(std::vector<Vertex>* vertices, 
		bool cache, const std::string* path, int meshIndex)
{
	PxConvexMeshDesc meshDesc;
	meshDesc.points.count = vertices->size();
	meshDesc.points.stride = sizeof(Vertex);
	meshDesc.points.data = vertices->data();
	meshDesc.flags = PxConvexFlag::eCOMPUTE_CONVEX;

	PxCookingParams params(pxPhysics->getTolerancesScale());

	PxConvexMesh* convex;

	// Directly cook into physics
	if (cache == false) {
		convex = PxCreateConvexMesh(params, meshDesc, pxPhysics->getPhysicsInsertionCallback());
	} else {
		PxDefaultMemoryOutputStream writeBuffer;
		if (!PxCookConvexMesh(params, meshDesc, writeBuffer)) {
			Log::out("Convex mesh cooking failed");
			return -1;
		}

		// Store triangle mesh to disk
		writeMeshFile(&writeBuffer, *path, meshIndex, false);

		PxDefaultMemoryInputData readBuffer(writeBuffer.getData(), writeBuffer.getSize());
		convex = pxPhysics->createConvexMesh(readBuffer);
	}

	if (!convex) {
		Log::error("Failed to create convex mesh from model: ", *path);
		return -1;
	}

	std::lock_guard<std::mutex> lck(colliderGenSync);
	return convexMeshes.add(convex);
}

