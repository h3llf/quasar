#ifndef QUASAR_MESH_COLLIDERS_H
#define QUASAR_MESH_COLLIDERS_H

#include <PxPhysicsAPI.h>
#include "Core/Types.hpp"
#include <string>

namespace quasar
{

class MeshColliders
{
public:
	void init(physx::PxPhysics* physics);

	void setPhysicsScale(physx::PxPhysics* pxPhysics);

	bool readMeshFile(std::vector<physx::PxU8>* pData, const std::string& path, 
			int meshIndex, bool isConvex);

	bool writeMeshFile(physx::PxDefaultMemoryOutputStream* pStream, const std::string& path, 
			int meshIndex, bool isConvex);

	// Attempts to load a mesh collider using the specified path as an identifier
	// (Doesn't load from this path)
	// Returns -1 if not found
	int loadTriangleMesh(const std::string& path, int meshIndex);
	int loadConvexMesh(const std::string& path, int meshIndex);

	// Cooks a mesh collider 
	// If cacheing is enabled the result is stored in the cache using the path as an identifier
	int createTriangleMesh(std::vector<Vertex>* vertices, std::vector<uint32>* indices, bool cache = false,
			const std::string* path = nullptr, int meshIndex = 0);
	int createConvexMesh(std::vector<Vertex>* vertices, bool cache = false,
			const std::string* path = nullptr, int meshIndex = 0);

	quasar::IndexVector<physx::PxConvexMesh*> convexMeshes;
	quasar::IndexVector<physx::PxTriangleMesh*> triangleMeshes;

private:

	physx::PxPhysics* pxPhysics;

	std::mutex colliderGenSync;
};

};
#endif //QUASAR_MESH_COLLIDERS_H
