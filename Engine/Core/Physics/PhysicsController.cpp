#include "PhysicsController.hpp"
#include "Core/Components/CTransform.hpp"
#include "Core/Assets/InstanceManager.hpp"
#include "Core/Types.hpp"
#include "PxPhysics.h"
#include "Utils/Timer.hpp"
#include "common/PxRenderBuffer.h"
#include <filesystem>
#include <mutex>

using namespace physx;
using namespace quasar;

PhysicsController physicsController;

physx::PxDefaultAllocator PhysicsController::defaultAllocator;
PhysicsController::ErrorCallback PhysicsController::defaultError;

const char* PhysicsController::physicsColliderNames[5] = {
	"Box",
	"Sphere",
	"Capsule",
	"Triangle mesh",
	"Convex mesh"
};

void PhysicsController::initPhysX()
{
	pxFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, defaultAllocator, defaultError);
	if(!pxFoundation)
		Log::fatal("PhysX PxFoundation creation failed");
	
	PxTolerancesScale physicsScale = PxTolerancesScale();
	physicsScale.length = 1.0f;

	pxPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *pxFoundation, physicsScale);
	if (!pxPhysics)
		Log::fatal("PhysX PxPhysics creation failed");

	if (!PxInitExtensions(*pxPhysics, nullptr))
		Log::fatal("PhysX couldn't enable extensions");

	// TODO: Consider using multiple scenes
	PxSceneDesc sceneDesc(pxPhysics->getTolerancesScale());
	pxCpuDispatcher = PxDefaultCpuDispatcherCreate(4);
	if (!pxCpuDispatcher)
		Log::fatal("PhysX failed to create CPU dispatcher");
	sceneDesc.cpuDispatcher = pxCpuDispatcher;
	sceneDesc.filterShader = PxDefaultSimulationFilterShader;
	sceneDesc.flags = sceneDesc.flags | PxSceneFlag::eENABLE_ACTIVE_ACTORS;

	pxScene = pxPhysics->createScene(sceneDesc);

	pxScene->setGravity(PxVec3(0.0f, -9.8f, 0.0f));

	if (!pxScene)
		Log::fatal("Failed to create physx scene");

	meshes.init(pxPhysics);
	query.init(pxScene);
	Log::out("PhysX initialized");
}

// TODO: enable multithreadding
void PhysicsController::updateSimulation(double delta)
{
	pxScene->simulate(delta);
	pxScene->fetchResults(true);
}

void PhysicsController::getDebugData()
{
	// Disable visualization parms to improve physx performance
	if (debugVisEnabled != QGlobals.colliderVisualization) {
		if (!debugVisEnabled) {
			pxScene->setVisualizationParameter(PxVisualizationParameter::eSCALE, 1.0f);
			pxScene->setVisualizationParameter(PxVisualizationParameter::eCOLLISION_SHAPES, 1.0f);
			pxScene->setVisualizationParameter(PxVisualizationParameter::eBODY_LIN_VELOCITY, 1.0f);
		} else {
			pxScene->setVisualizationParameter(PxVisualizationParameter::eSCALE, 0.0f);
			pxScene->setVisualizationParameter(PxVisualizationParameter::eCOLLISION_SHAPES, 0.0f);
			pxScene->setVisualizationParameter(PxVisualizationParameter::eBODY_LIN_VELOCITY, 0.0f);
		}

		debugVisEnabled = QGlobals.colliderVisualization;
	}

	std::lock_guard<std::mutex> lck(InstanceManager::instanceSync);
	std::vector<LineVertex>& lineDst = InstanceManager::getDebugLines();
	if (!debugVisEnabled) {
		lineDst.resize(0);
		return;
	}

	const PxRenderBuffer& rb = pxScene->getRenderBuffer();
	lineDst.resize(rb.getNbLines() * 2);

	for (PxU32 i = 0; i < rb.getNbLines(); ++i) {
		const PxDebugLine& line = rb.getLines()[i];
		lineDst[i * 2].colour = line.color0;
		lineDst[i * 2].position = ptoq(line.pos0);
		lineDst[i * 2 + 1].colour = line.color1;
		lineDst[i * 2 + 1].position = ptoq(line.pos1);
	}
}

int PhysicsController::addMeshCollider(int meshColliderID, bool isConvex, vec3 scale, vec3 position, vec4 rotation)
{
	int i = colliders.nextIndex();

	PxMeshScale meshScale(qtop(scale));
	PxMaterial* material = pxPhysics->createMaterial(0.5f, 0.5f, 0.1f);

	if (isConvex) {
		PxConvexMesh* convex = meshes.convexMeshes[meshColliderID];
		colliders[i].shape = pxPhysics->createShape(PxConvexMeshGeometry(convex, meshScale), *material);
	} else {
		PxTriangleMesh* triMesh = meshes.triangleMeshes[meshColliderID];
		colliders[i].shape = pxPhysics->createShape(PxTriangleMeshGeometry(triMesh, meshScale), *material);	
	}

	PxTransform pxT {qtop(position), qtop(rotation)};
	colliders[i].shape->setLocalPose(pxT);

	if (!colliders[i].shape)
		Log::error("Failed to create triangle mesh collider");

	return i;
}

int PhysicsController::addCollider(PhysicsColliderType type, vec3 dimension)
{
	int i = colliders.nextIndex();
	colliders[i].colliderType = type;
	PxMaterial* material = pxPhysics->createMaterial(0.5f, 0.5f, 0.1f);

	switch (type){
	case PHYSICS_BOX_COLLIDER: {
		PxVec3 boxDim = qtop(dimension);
		colliders[i].shape = pxPhysics->createShape(PxBoxGeometry(boxDim), *material, true);
		break;
	} 
	case PHYSICS_SPHERE_COLLIDER: {
		colliders[i].shape = pxPhysics->createShape(PxSphereGeometry(dimension.x), *material, true);
		break;
	}
	case PHYSICS_CAPSULE_COLLIDER: {
		// Rotate to stand upright
		PxTransform relativeP(PxQuat(PxHalfPi, PxVec3(0.0f, 0.0f, 1.0f)));
		colliders[i].shape = pxPhysics->createShape(PxCapsuleGeometry(dimension.x, dimension.y), 
				*material, true);
		colliders[i].shape->setLocalPose(relativeP);
		break;
	}
	default:
		break;
	}

	if (!colliders[i].shape) {
		Log::error("Invalid collider shape: ", (*(vec3*)&dimension).str());
		removeCollider(i);
		return -1;
	}

	return i;
}

void PhysicsController::removeCollider(int colliderID)
{
	if (colliderID >= colliders.size()) {
		return;
	}
	
	if (colliders[colliderID].shape && colliders[colliderID].shape->isReleasable()) {
		colliders[colliderID].shape->release();
	} else {
		Log::error("Faield to release collider, ", colliderID);
		return;
	}

	colliders.remove(colliderID);
}

bool PhysicsController::attachShape(int colliderID, int rigidBodyID)
{
	if (colliderID >= colliders.size() || !colliders[colliderID].shape) {
		Log::error("Attach failed, collider not found ", colliderID);
		return false;
	}

	if (rigidBodyID >= rigidbodies.size() || !rigidbodies[rigidBodyID]) {
		Log::error("Attach failed, rigidbody not found ", rigidBodyID);
		return false;
	}

	return rigidbodies[rigidBodyID]->attachShape(*colliders[colliderID].shape);
}

void PhysicsController::detatchShape(int colliderID, int rigidBodyID)
{
	if (colliderID >= colliders.size() || !colliders[colliderID].shape) {
		Log::error("Detach failed, collider not found ", colliderID);
		return;
	}

	if (rigidBodyID >= rigidbodies.size() || !rigidbodies[rigidBodyID]) {
		Log::error("Detatch failed, rigidbody not found ", rigidBodyID);
		return;
	}

	rigidbodies[rigidBodyID]->detachShape(*colliders[colliderID].shape);
}

int PhysicsController::addRigidBody(vec3& pos, vec4& quat, float mass, bool kinematic, Entity* entity)
{
	int i = rigidbodies.nextIndex();

	PxTransform transform = PxTransform(qtop(pos), qtop(quat));
	rigidbodies[i] = pxPhysics->createRigidDynamic(transform);

	if (kinematic)
		rigidbodies[i]->setRigidBodyFlags(PxRigidBodyFlag::eKINEMATIC);

	rigidbodies[i]->setMass(mass);
	rigidbodies[i]->userData = entity;
	pxScene->addActor(*rigidbodies[i]);
	
	return i;
}

void PhysicsController::removeRigidBody(int rigidbodyID)
{
	if (rigidbodyID >= rigidbodies.size())
	{
		Log::error("Rigidbody ID out of array bounds");
		return;
	}

	if (rigidbodies[rigidbodyID] && rigidbodies[rigidbodyID]->isReleasable()) {
		pxScene->removeActor(*rigidbodies[rigidbodyID], true);
		rigidbodies[rigidbodyID]->release();
	} else {
		Log::error("Failed to release rigidbody, ", rigidbodyID);
		return;
	}

	rigidbodies.remove(rigidbodyID);
}

void PhysicsController::getTransform(vec3& pos, vec4& quat, int rigidbodyID)
{
	if (rigidbodyID > -1 && rigidbodyID <= rigidbodies.size()) {
		if (!rigidbodies[rigidbodyID]->isSleeping()) {
			PxTransform t = rigidbodies[rigidbodyID]->getGlobalPose();
			pos = ptoq(t.p);
			quat = ptoq(t.q);
		}
	}
}

void PhysicsController::setTransform(vec3& pos, vec4& quat, int rigidbodyID)
{
	if (rigidbodyID > -1 && rigidbodyID <= rigidbodies.size()) {
		PxTransform t = {qtop(pos), qtop(quat)};
		rigidbodies[rigidbodyID]->setGlobalPose(t, true);
	}
}

void PhysicsController::getVelocity(vec3& linearVel, vec3& angularVel, int rigidbodyID)
{
	if (rigidbodyID > -1 && rigidbodyID <= rigidbodies.size()) {
		if (!rigidbodies[rigidbodyID]->isSleeping()) {
			linearVel = ptoq(rigidbodies[rigidbodyID]->getLinearVelocity());
			angularVel = ptoq(rigidbodies[rigidbodyID]->getAngularVelocity());
		}
	}
}

void PhysicsController::setVelocity(vec3& linearVel, vec3& angularVel, int rigidbodyID)
{
	if (rigidbodyID > -1 && rigidbodyID <= rigidbodies.size()) {
		rigidbodies[rigidbodyID]->setLinearVelocity(qtop(linearVel));
		rigidbodies[rigidbodyID]->setAngularVelocity(qtop(angularVel));
	}
}
