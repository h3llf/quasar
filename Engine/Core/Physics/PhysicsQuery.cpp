#include "PhysicsQuery.hpp"

using namespace quasar;

void PhysicsQuery::init(PxScene* scene)
{
	pxScene = scene;
}

// TODO: Add entity information to shapes
PhysicsQueryHit PhysicsQuery::rayCast(vec3 origin, vec3 dir, float distance)
{
	PxRaycastBuffer hit;
	PxVec3 pxOrigin = qtop(origin);
	PxVec3 pxDir = qtop(dir);
	bool status = pxScene->raycast(pxOrigin, pxDir, distance, hit);

	PhysicsQueryHit qHit = {Entity::null};
	if (status) {
		PxVec3 pxPos = hit.block.position;
		PxVec3 pxNorm = hit.block.normal;

		qHit.actorEntity = *reinterpret_cast<Entity*>(hit.block.actor->userData);
//		qHit.shapeEntity = *reinterpret_cast<Entity*>(hit.block.shape->userData);
		qHit.position = ptoq(pxPos);
		qHit.normal = ptoq(pxNorm);
	}

	return qHit;
}

PhysicsQueryHit PhysicsQuery::sweep(const PxShape *shape, const PxTransform& pose, vec3 dir, float distance)
{
	PxSweepBuffer hit;
	const PxGeometry& geometry = shape->getGeometry();
	const PxHitFlags flags = PxHitFlag::ePOSITION | PxHitFlag::eNORMAL;
	PxVec3 pxDir = qtop(dir);
	bool status = pxScene->sweep(geometry, pose, pxDir, distance, hit, flags);

	PhysicsQueryHit qHit;
	if (status) {
		getSweepResult(hit.block, geometry, pose, qHit);
	}

	return qHit;
}

int PhysicsQuery::sweepMulti(const PxShape *shape, const PxTransform &pose, vec3 dir, float distance, 
		std::vector<PhysicsQueryHit> &touches)
{
	if (touches.size() == 0)
		return -1;

	PxSweepHit hitBuffer[touches.size() - 1];
	PxSweepBuffer buf(hitBuffer, touches.size() - 1);

	const PxGeometry& geometry = shape->getGeometry();
	const PxHitFlags flags = PxHitFlag::eMESH_MULTIPLE | PxHitFlag::ePOSITION | PxHitFlag::eNORMAL | PxHitFlag::ePRECISE_SWEEP;
	PxVec3 pxDir = qtop(dir);

	PxTransform shapePose = pose;//shape->getLocalPose();
	shapePose.q *= shape->getLocalPose().q;
	bool status = pxScene->sweep(geometry, shapePose, pxDir, distance, buf, flags);

	int index = 0;
	if (status) {
		if (buf.hasBlock) {
			getSweepResult(buf.block, geometry, shapePose, touches[index++]);
		}

		for (uint32 i = 0; i < buf.nbTouches; ++i) {
			getSweepResult(buf.touches[i], geometry, shapePose, touches[index++]);
		}
	}

	return index;
}

void PhysicsQuery::getSweepResult(const PxSweepHit& pxHit, const PxGeometry& geometry, const PxTransform& pose, 
		PhysicsQueryHit& qHit)
{
	qHit = {Entity::null};
	PxVec3 pxPos = pxHit.position;
	PxVec3 pxNorm = pxHit.normal;

	qHit.actorEntity = *reinterpret_cast<Entity*>(pxHit.actor->userData);
	qHit.position = ptoq(pxPos);
	qHit.normal = ptoq(pxNorm);
	qHit.hitDistance = pxHit.distance;
	qHit.initialOverlap = pxHit.hadInitialOverlap();

	if (qHit.initialOverlap) {
		PxVec3 penDir;
		PxReal penDist;
		bool res = PxGeometryQuery::computePenetration(penDir, penDist, geometry, pose, 
				pxHit.shape->getGeometry(), pxHit.actor->getGlobalPose());
		if (res) {
			qHit.penDir = ptoq(penDir);
			qHit.penDist = penDist;
		}
	}

	pxHit.shape->getGeometry();
	pxHit.actor->getGlobalPose();
}
