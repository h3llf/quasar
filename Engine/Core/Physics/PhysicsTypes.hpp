#ifndef QUASAR_PHYSICS_TYPES_H
#define QUASAR_PHYSICS_TYPES_H

#include "Core/Types.hpp"
#include "PxPhysicsAPI.h"

namespace quasar
{
	// Convert betweem QML and PhysX math types
	static inline physx::PxVec3 qtop(const qml::vec3& vec)
	{
		return *reinterpret_cast<const physx::PxVec3*>(&vec);
	}
	static inline qml::vec3 ptoq(const physx::PxVec3& vec)
	{
		return *reinterpret_cast<const qml::vec3*>(&vec);
	}
	static inline physx::PxQuat qtop(const qml::vec4& vec)
	{
		return *reinterpret_cast<const physx::PxQuat*>(&vec);
	}
	static inline qml::vec4 ptoq(const physx::PxQuat& vec)
	{
		return *reinterpret_cast<const qml::vec4*>(&vec);
	}
};

#endif //QUASAR_PHYSICS_TYPES_H
