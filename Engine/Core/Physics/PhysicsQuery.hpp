#ifndef QUASAR_PHYSICS_QUERY_H
#define QUASAR_PHYSICS_QUERY_H

#include "Core/ECS/ECS.hpp"
#include "PhysicsTypes.hpp"
#include <PxPhysicsAPI.h>

using namespace physx;

struct PhysicsQueryHit
{
	// Owner of a rigidbody/actor
	Entity actorEntity = Entity::null;

	// Owner of a collider
	Entity shapeEntity = Entity::null;

	vec3 position;
	vec3 normal;

	// Distance from the shape origin to the hit point
	float hitDistance;

	// If initial overlap, record penetration direction and depth
	bool initialOverlap = false;
	vec3 penDir = vec3(0.0f);
	float penDist = 0.0f;
};

class PhysicsQuery
{
public:
	void init(PxScene* scene);

	PhysicsQueryHit rayCast(vec3 origin, vec3 dir, float distance);

	// Returns the first blocking hit
	PhysicsQueryHit sweep(const PxShape* shape, const PxTransform& pose, vec3 dir, float distance);

	// Returns the first blocking hit and any touching hits along the path
	int sweepMulti(const PxShape* shape, const PxTransform& pose, vec3 dir, float distance, 
			std::vector<PhysicsQueryHit>& touches);

private:
	void getSweepResult(const PxSweepHit& pxHit, const PxGeometry& geometry, const PxTransform& pose,
			PhysicsQueryHit& qHit);

	PxScene* pxScene;
};

#endif //QUASAR_PHYSICS_QUERY_H
