#ifndef QUASAR_PHYSICS_CONTROLLER_H
#define QUASAR_PHYSICS_CONTROLLER_H

#include "Utils/Math/Vector.hpp"
#define PX_PHYSX_STATIC_LIB
#include <PxPhysicsAPI.h>

#include "Core/Types.hpp"
#include "Core/ECS/ECS.hpp"
#include "MeshColliders.hpp"
#include "PhysicsQuery.hpp"
#include "PhysicsTypes.hpp"

namespace quasar
{

enum PhysicsColliderType
{
	PHYSICS_BOX_COLLIDER,
	PHYSICS_SPHERE_COLLIDER,
	PHYSICS_CAPSULE_COLLIDER,
	PHYSICS_TRIANGLE_MESH_COLLIDER,
	PHYSICS_CONVEX_MESH_COLLIDER
};


class PhysicsController
{
public:
	static const char* physicsColliderNames[5];

	// TODO: Add entity information to shapes

	void initPhysX();
	void updateSimulation(double delta);

	void getDebugData();

	int addMeshCollider(int meshColliderID, bool isConvex, vec3 scale, vec3 position, vec4 rotation);
	int addCollider(PhysicsColliderType type, vec3 dimension);
	void removeCollider(int colliderID);

	bool attachShape(int colliderID, int rigidBodyID);
	void detatchShape(int colliderID, int rigidBodyID);

	// TODO: Configurable density
	int addRigidBody(vec3& pos, vec4& quat, float mass, bool kinematic, Entity* entity);
	void removeRigidBody(int rigidbodyID);

	void getTransform(vec3& pos, vec4& quat, int rigidbodyID);
	void setTransform(vec3& pos, vec4& quat, int rigidbodyID);

	void getVelocity(vec3& linearVel, vec3& angularVel,int rigidbodyID);
	void setVelocity(vec3& linearVel, vec3& angularVel,int rigidbodyID);

	PhysicsQueryHit rayCast(vec3 origin, vec3 dir, float distance)
	{
		return query.rayCast(origin, dir, distance);
	}

	PhysicsQueryHit sweep(int colliderID, vec3 origin, quat rotation, vec3 dir, float distance)
	{
		PxTransform transform = {qtop(origin), qtop(rotation)};

		return query.sweep(colliders[colliderID].shape, transform, dir, distance);
	}

	int sweepMulti(int colliderID, vec3 origin, quat rotation, vec3 dir, float distance,
			std::vector<PhysicsQueryHit> &touches)
	{
		PxTransform transform = {qtop(origin), qtop(rotation)};
		return query.sweepMulti(colliders[colliderID].shape, transform, dir, distance, touches);
	}

	MeshColliders meshes;
	PhysicsQuery query;

private:
	class ErrorCallback : public physx::PxErrorCallback
	{
	public:
		void reportError(physx::PxErrorCode::Enum code, const char* message, 
				const char* file, int line)
		{
			Log::out(message);
		}
	};

	static ErrorCallback defaultError;
	static physx::PxDefaultAllocator defaultAllocator;

	struct Collider
	{
		PhysicsColliderType colliderType = PHYSICS_BOX_COLLIDER;
		physx::PxShape* shape = nullptr;
	};

	quasar::IndexVector<Collider> colliders;
	quasar::IndexVector<physx::PxRigidDynamic*> rigidbodies;

	physx::PxFoundation* pxFoundation;
	physx::PxPhysics* pxPhysics;
	physx::PxCpuDispatcher* pxCpuDispatcher;
	physx::PxScene* pxScene;

	bool debugVisEnabled = false;
};

};

extern quasar::PhysicsController physicsController;
#endif //PHYSICS_CONTROLLER_H
