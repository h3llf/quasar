#ifndef QUASAR_ECS_HEADERS_H
#define QUASAR_ECS_HEADERS_H

#include "ECS/ECS.hpp"
#include "ECS/UtilECS.hpp"

#include "Components/CLighting.hpp"
#include "Components/CTransform.hpp"
#include "Components/CCamera.hpp"
#include "Components/CRigidbody.hpp"
#include "Components/CModel.hpp"
#include "Components/CServer.hpp"
#include "Components/CAssetPolling.hpp"
#include "Components/CText.hpp"
#include "Components/CAudio.hpp"
#include "Components/CAnimator.hpp"

#ifndef _HEADLESS
#include "Client/Components/CRenderable.hpp"
#include "Client/Components/CPlayerController.hpp"
#endif

#endif //QUASAR_ECS_HEADERS_H
