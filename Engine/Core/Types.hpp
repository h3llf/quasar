#ifndef QUASAR_TYPES_H
#define QUASAR_TYPES_H

#include <cstdint>
#include <vector>
#include <array>
#include <string.h>
#include <algorithm>
#include <mutex>
#include <atomic>
#include <cstring>
#include <queue>
#include "Utils/Log.hpp"
#include "Utils/Math/QML.hpp"

typedef int64_t int64;
typedef int16_t int16;
typedef unsigned char uint8;
typedef uint64_t uint64;
typedef uint32_t uint32;
typedef uint16_t uint16;

namespace quasar
{

/* Stores elements that need to be indexd in an stl vector 
 * Resizing and index reuse is handled automatically */
template <typename T>
class IndexVector
{
public:
	IndexVector() : nullVal(T {})
	{

	}

	IndexVector(T nullValue) : nullVal(nullValue)
	{
		
	}

	T& operator[](int index)
	{
		return elements.at(index);
	}

	T& at(int index)
	{
		return elements.at(index);
	}

	int add(T& val)
	{
		int index;

		if (freeElements.size()) {
			index = freeElements.front();
			freeElements.pop();
			elements.at(index) = val;
		} else {
			index = elements.size();
			elements.push_back(val);
		}

		return index;
	}

	int nextIndex()
	{
		return add(nullVal);
	}

	void remove(int index)
	{
		freeElements.push(index);
		elements[index] = nullVal;
	}

	size_t size()
	{
		return elements.size();
	}

	size_t count()
	{
		return elements.size() - freeElements.size();
	}

	T nullVal;

	std::vector<T> elements;
	std::queue<int> freeElements;
};

struct QuasarProperties
{
	const char* appName = "Quasar";
	uint32 version[3] = {0, 1, 0}; /*Major, Minor, Patch*/
	std::atomic<int> windowSizeX = 1600;
	std::atomic<int> windowSizeY = 900;
	std::atomic<bool> applicationRunning = true;
};
static QuasarProperties QUASAR;

struct Vertex 
{
	vec3 position;
	vec3 normal;
	vec4 tangent;
	vec2 uv;
};

struct LineVertex
{
	uint32 colour;
	vec3 position;
};

// The render mask specifies which objects should be drawn by each camera
enum RenderMask : uint32_t
{
	RENDER_MASK_NONE = 0,
	RENDER_MASK_EDITOR = 1 << 0,
	RENDER_MASK_GAME = 1 << 1,
	RENDER_MASK_STANDARD = (1 << 0) | (1 << 1),
	RENDER_MASK_EDITOR_THUMBNAIL_GEN = 1 << 15,
	RENDER_MASK_ALWAYS = 0xffffffff,
};

};

#endif //QUASAR_TYPES_H
