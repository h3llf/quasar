#ifndef QUASAR_APPLICATION_CONTEXT_H
#define QUASAR_APPLICATION_CONTEXT_H

#include "ECS/ECS.hpp"
#include "Assets/AssetManager.hpp"
#include "Physics/PhysicsController.hpp"

namespace quasar
{

struct ApplicationContext
{	
	// Shared
	AssetManager* assetMgr = nullptr;
//	InstanceManager* instanceMgr = nullptr;

	// CLient side
	ECS* cECS = nullptr;
	PhysicsController* cPhysics = nullptr;
	void* client = nullptr;

	// Server size
	ECS* sECS = nullptr;
	PhysicsController* sPhysics = nullptr;
	void* server = nullptr;

};

};

#endif //QUASAR_APPLICATION_CONTEXT_H
