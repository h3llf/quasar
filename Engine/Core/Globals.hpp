#ifndef QUASAR_GLOBALS_H
#define QUASAR_GLOBALS_H

#include <atomic>
#include "Utils/Math/Vector.hpp"

struct QuasarGlobals
{
	std::atomic<bool> updateEditor = true;
	std::atomic<bool> updateGame = false;

	std::atomic<bool> drawEditor = true;
	
	std::atomic<bool> colliderVisualization = true;
	bool enableServer = true;
	std::string connectionIP = "0.0.0.0";
//	std::string connectionIP = "127.0.0.1";
	uint16_t connectionPort = 17017;
};

static inline const qml::vec3 globalUp = {0.0f, 1.0f, 0.0f};
static inline const qml::vec3 globalForward = {1.0f, 0.0f, 0.0f};
static inline const qml::vec3 globalRight = {0.0f, 0.0f, 1.0f};

extern QuasarGlobals QGlobals;

#endif //QUASAR_GLOBALS_H
