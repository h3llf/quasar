#version 450
#define TILE_SIZE 8

#define DOWNSAMPLE_FIRST 0
#define DOWNSAMPLE 1
#define UPSAMPLE_FINAL 2
#define UPSAMPLE 3

layout (set = 0, binding = 0) uniform sampler2D inputImg;
layout (set = 1, binding = 0, r11f_g11f_b10f) uniform image2D outputMip;


// Based on the slides from http://advances.realtimerendering.com/s2014/#_NEXT_GENERATION_POST
layout (push_constant) uniform Constants{
	vec2 texelSize;
	ivec2 outputResolution;
	int mode;
	float sampleScale;
	float intensity;
};

float toLuma(vec3 col)
{
	return dot(col, vec3(0.2126f, 0.7152f, 0.0722f));
}

float karisAverage(vec3 colour)
{
	float luma = toLuma(colour) * 0.25f;
	return 1.0 / (1.0 + luma);
}

vec2 uv;

// 5x5 filter
// A - B - C
// - D - E -
// F - G - H
// - I - J -
// K - L - M
vec3 downsample5x5(bool first)
{
	vec2 tx = texelSize;

	vec3 A = texture(inputImg, uv + tx * vec2(-2.0, 2.0)).rgb;
	vec3 B = texture(inputImg, uv + tx * vec2(0.0, 2.0)).rgb;
	vec3 C = texture(inputImg, uv + tx * vec2(2.0, 2.0)).rgb;

	vec3 D = texture(inputImg, uv + tx * vec2(-1.0, 1.0)).rgb;
	vec3 E = texture(inputImg, uv + tx * vec2(1.0, 1.0)).rgb;

	vec3 F = texture(inputImg, uv + tx * vec2(-2.0, 0.0)).rgb;
	vec3 G = texture(inputImg, uv + tx * vec2(0.0, 0.0)).rgb;
	vec3 H = texture(inputImg, uv + tx * vec2(2.0, 0.0)).rgb;

	vec3 I = texture(inputImg, uv + tx * vec2(-1.0, -1.0)).rgb;
	vec3 J = texture(inputImg, uv + tx * vec2(1.0, -1.0)).rgb;

	vec3 K = texture(inputImg, uv + tx * vec2(-2.0, -2.0)).rgb;
	vec3 L = texture(inputImg, uv + tx * vec2(0.0, -2.0)).rgb;
	vec3 M = texture(inputImg, uv + tx * vec2(2.0, -2.0)).rgb;

	// Apply colour coded weights
	vec3 accumulator = vec3(0.0);
	vec3 r = (D + E + I + J) * 0.5 * 0.25; // Red
	vec3 y = (A + B + F + G) * 0.125 * 0.25; // Yellow
	vec3 g = (B + C + G + H) * 0.125 * 0.25; // Green
	vec3 b = (G + H + L + M) * 0.125 * 0.25; // Blue
	vec3 p = (F + G + K + L) * 0.125 * 0.25; // Purple
	
	if (first) {
		r *= karisAverage(r);
		y *= karisAverage(y);
		g *= karisAverage(g);
		b *= karisAverage(b);
		p *= karisAverage(p);
	}

	accumulator = r + y + g + b + p;

	if (first) {
		accumulator = max(accumulator, 0.0001);
		accumulator = min(accumulator, 1500.0);
	}

	return accumulator;
}

// 3z3 tent filter
//        1 2 1
// 1/16 * 2 4 2
//        1 2 1
vec3 upsample3x3()
{
	vec2 tx = texelSize;

	vec3 accumulator = vec3(0.0);
	accumulator += texture(inputImg, uv + tx * vec2(-1.0, 1.0)).rgb;
	accumulator += texture(inputImg, uv + tx * vec2(0.0, 1.0)).rgb * 2.0;
	accumulator += texture(inputImg, uv + tx * vec2(1.0, 1.0)).rgb;
	accumulator += texture(inputImg, uv + tx * vec2(-1.0, 0.0)).rgb * 2.0;
	accumulator += texture(inputImg, uv + tx * vec2(0.0, 0.0)).rgb * 4.0;
	accumulator += texture(inputImg, uv + tx * vec2(1.0, 0.0)).rgb * 2.0;
	accumulator += texture(inputImg, uv + tx * vec2(-1.0, -1.0)).rgb;
	accumulator += texture(inputImg, uv + tx * vec2(0.0, -1.0)).rgb * 2.0;
	accumulator += texture(inputImg, uv + tx * vec2(1.0, -1.0)).rgb;

	return accumulator / 16.0;
}

layout (local_size_x = TILE_SIZE, local_size_y = TILE_SIZE) in;
void main()
{
//	if (gl_GlobalInvocationID.x >= outputResolution.x || gl_GlobalInvocationID.y >= outputResolution.y) {
//		return;
//	}

	uv = vec2(gl_GlobalInvocationID.xy) / outputResolution;
	uv += (1.0 / vec2(outputResolution)) * 0.5;

	vec3 result;
	if (mode == DOWNSAMPLE_FIRST) {
		result = downsample5x5(true);
	} else if (mode == DOWNSAMPLE) {
		result = downsample5x5(false);
	} else {
		vec3 original = imageLoad(outputMip, ivec2(gl_GlobalInvocationID.xy)).rgb;
		if (mode == UPSAMPLE) {
			result = original + upsample3x3();
		} else if (mode == UPSAMPLE_FINAL) {
			result = upsample3x3();
			result = mix(original, result, intensity);
		}
	}

	imageStore(outputMip, ivec2(gl_GlobalInvocationID.xy), vec4(result, 1.0));
}
