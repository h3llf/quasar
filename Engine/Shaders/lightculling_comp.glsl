#version 450

#define TILE_SIZE 16
#define TILE_SIZE_2 TILE_SIZE * TILE_SIZE
#define MAX_LIGHTS_PER_TILE 128

#include <SharedTypes.glsl>
#include <PostProcessCommon.glsl>

layout (set = 0, binding = 0) uniform readonly CameraProps
{
	CamProps camProps;
};


struct PointLight {
	vec3 position;
	float minLuminosity;
	vec3 colour;
	int shadowID;
};

layout (set = 1, binding = 0) readonly buffer PointLightBuffer {
	uint lightCount;
	PointLight pointLights[4096];
};

layout (set = 2, binding = 0) uniform sampler2D depthImage;

struct Frustum {
	vec4 planes[4];
};

layout (set = 3, binding = 0) readonly buffer frustumBuffer {
	Frustum frustums[];
};

struct LightCullingResult{
	uint numLights;
	uint indexList[MAX_LIGHTS_PER_TILE];
};

// TODO: transparent light culling resources
layout (set = 3, binding = 1) writeonly buffer TileLightVisibilites {
	LightCullingResult cullingResult[];
};

layout (set = 3, binding = 2) readonly uniform TileSizeDescription {
	int groupCountX;
	int groupCountY;
	int screenWidth;
	int screenHeight;
};

// TODO: Cone checks for spot lights

bool sphereInsidePlane(vec3 c, float r, vec3 n)
{
	return dot(n, c) < -r;
}

bool sphereInsideFrustum(vec3 c, float r, Frustum frustum, float near, float far)
{
	bool result = true;

	//Depth test
	if (c.z - r > near || c.z + r < far) {
		result = false;
	}

	//Frustum plane tests
	for (int i = 0; i < 4; ++i) {
		if (sphereInsidePlane(c, r, frustum.planes[i].xyz)) {
			result = false;
		}
	}

	return result;
}

shared uint numLightsInTile;
shared uint minIntDepth;
shared uint maxIntDepth;

layout (local_size_x = TILE_SIZE, local_size_y = TILE_SIZE) in;
void main()
{
	//TODO: Precalculate
	mat4 invProj = inverse(camProps.proj);
	uint index = gl_WorkGroupID.x + gl_WorkGroupID.y * gl_NumWorkGroups.x;

	if (gl_LocalInvocationIndex == 0) {
		numLightsInTile = 0;
		minIntDepth = 0xffffffff;
		maxIntDepth = 0;	
	}

	barrier();

	//Depth bounds check
	vec2 texCoord = gl_GlobalInvocationID.xy;
	texCoord /= vec2(screenWidth, screenHeight);
	
	float sampledDepth = texture(depthImage, texCoord * vec2(camProps.xScale, camProps.yScale)).r;
	
	// Get viewspace positon from depth
	float aspect = float(screenWidth) / float(screenHeight);
	float tanFov = tan(camProps.fov / 2.0);
	vec3 viewDepth = getViewPos(texCoord, sampledDepth, aspect, tanFov, camProps.near, camProps.far);

	//Floating point atomic operations are not supported on most of implementations
	uint depthInt = floatBitsToUint(-viewDepth.z);
	atomicMin(minIntDepth, depthInt);
	atomicMax(maxIntDepth, depthInt);
	
	barrier();

	float minDepth = -uintBitsToFloat(minIntDepth);
	float maxDepth = -uintBitsToFloat(maxIntDepth);

	float differerence = minDepth - maxDepth;
	minDepth += differerence;
	maxDepth -= differerence;

	Frustum currentFrustum = frustums[index];

	barrier();

	for (uint i = gl_LocalInvocationIndex; i < lightCount && numLightsInTile < MAX_LIGHTS_PER_TILE; i += TILE_SIZE_2) {
		vec3 center = (camProps.view * vec4(pointLights[i].position, 1.0)).xyz;
		float radius = sqrt(1.0f / pointLights[i].minLuminosity + 0.1f); // TODO: Precompute/store radius in point light
		if (sphereInsideFrustum(center, radius, currentFrustum, minDepth, maxDepth)) {

			uint lcount = atomicAdd(numLightsInTile, 1);
			if (lcount >= MAX_LIGHTS_PER_TILE) break;

			cullingResult[index].indexList[lcount] = i;
		}
	}

	barrier();

	if (gl_LocalInvocationIndex == 0)
		cullingResult[index].numLights = numLightsInTile;
}
