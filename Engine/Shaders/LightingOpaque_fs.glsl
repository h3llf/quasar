#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : require
#define PI 3.1415
#define TILE_SIZE 16
#define MAX_LIGHTS_PER_TILE 128

layout (constant_id = 0) const bool ENABLE_GTAO = true;
layout (constant_id = 1) const bool ENABLE_GTAO_ONLY = true;
layout (constant_id = 2) const bool ENABLE_LIGHT_HEATMAP = true;
layout (constant_id = 3) const bool ENABLE_WHITEWORLD = true;
layout (constant_id = 4) const bool ENABLE_SHOW_CASCADE_SPLITS = true;
layout (constant_id = 5) const uint CASCADE_COUNT = 5;
layout (constant_id = 6) const int NUM_PCF_SAMPLES = 12;

#include <LightingBase_fs.glsl>
#include <Visualization.glsl>

// Opaque geometry light culling
struct LightCullingResult{
	uint numLights;
	uint indexList[MAX_LIGHTS_PER_TILE];
};

layout (set = 4, binding = 1) readonly buffer TileLightVisibilites {
	LightCullingResult cullingResult[];
};

layout (set = 4, binding = 2) readonly uniform TileSizeDescription {
	int groupCountX;
	int groupCountY;
	int screenWidth;
	int screenHeight;
};

layout (location = 0) out vec4 outColour;
layout (location = 1) out vec4 gBuffers;

void main()
{
	vec3 texColour;
	if (albedoImg != -1) {
		texColour = texture(textureSamplers[albedoImg], texCoords).rgb;
	} else {
		texColour = vec3(1.0);
	}
	texColour *= baseColour.rgb;
	
	vec3 normalColour ;
	if (normImg != -1) {
		normalColour = texture(textureSamplers[normImg], texCoords).rgb;
	} else {
		normalColour = vec3(0.5, 0.5, 1.0);
	}

	vec3 emissiveColour;
	if (emissiveImg != -1) {
		emissiveColour = texture(textureSamplers[emissiveImg], texCoords).rgb;
	} else {
		emissiveColour = vec3(1.0);
	}
	emissiveColour *= emissiveFactor;

	vec2 PBRmat;
	if (pbrImg != -1) {
		PBRmat = texture(textureSamplers[pbrImg], texCoords).gb;
	} else {
		PBRmat = vec2(1.0);
	}
	PBRmat.r *= roughnessFactor;
	PBRmat.g *= metallicFactor;

#ifdef __Debug
	if (ENABLE_WHITEWORLD) {
		texColour = vec3(1.0f, 1.0f, 1.0f);
	}
#endif

	vec3 normal = getNormalVector(normalColour);
	vec3 V = normalize(inCameraProps.viewPos - fragPos);

	gBuffers = vec4(normal, PBRmat.x);

	// Find the light list that corrisponds to this tile
	ivec2 tileID = ivec2(gl_FragCoord.xy / TILE_SIZE);
	uint tileIndex = tileID.x + tileID.y * groupCountX;

	// Point light shadow calculations
	vec3 accumulator = vec3(0.0);
	for (uint i = 0; i < cullingResult[tileIndex].numLights; ++i) {
		uint lightIndex = cullingResult[tileIndex].indexList[i];
		PointLight pLight = pointLights[lightIndex];

		vec3 lightColour = pointLightCalculation(pLight.position, pLight.colour, texColour, normal, 
				PBRmat, V, fragPos, pLight.minLuminosity);

		if (pointLights[lightIndex].shadowIndex > -1) {
			lightColour *= omniShadowCalculation(pointLights[lightIndex],inCameraProps.viewPos,
					fragPos, omniShadow);
		}

		accumulator += lightColour;
	}

	// Dir light shading and shadows
	accumulator += dirLightCalculations(texColour, PBRmat, normal, V);

	// Ambient lighting + AO
	float aoFactor = 1.0;
	if (ENABLE_GTAO) {
		aoFactor = texture(ambientOcclusion, screenCoords * inCameraProps.ndcScale).r;
	}
	accumulator += texColour * inCameraProps.ambientColour * aoFactor;

	// Emissive
	accumulator += emissiveColour;

#ifdef __Debug
	// Light heatmap
	if (ENABLE_LIGHT_HEATMAP) {
		ivec2 tileGap = ivec2(mod(ivec2(gl_FragCoord.xy), ivec2(TILE_SIZE)));
		if (tileGap.x != 0 && tileGap.y != 0) {
			float count = float(cullingResult[tileIndex].numLights);
//			accumulator += debugHeatMap(count, float(MAX_LIGHTS_PER_TILE)) / 2.0;
			accumulator += debugHeatMap(count, 96) / 2.0;
		}
	}

	if (ENABLE_GTAO_ONLY) {
		accumulator = vec3(aoFactor);
	}
#endif

	outColour = vec4(accumulator, 1.0);
}
