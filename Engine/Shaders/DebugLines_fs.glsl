#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec4 colour;

//const vec4 colour = vec4(235.0/255.0, 120.0/255.0, 50.0/255.0, 1.0);
layout (location = 0) out vec4 outColour;

void main()
{
	outColour = colour;
}
