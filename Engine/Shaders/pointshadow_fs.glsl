#version 450

layout (location = 0) in vec4 inPos;
layout (location = 1) in float radius;
layout (location = 2) in vec3 inLightPos;
layout (location = 3) in vec2 texCoords;

// Texture array
layout (set = 1, binding = 0) uniform sampler2D textureSamplers[16384];

layout (push_constant) uniform MaterialProps
{
	layout (offset = 64)
	uint albedoImg;
};
const float alphaBias = 0.1;


void main() 
{
	if (texture(textureSamplers[albedoImg], texCoords).a < alphaBias) {
		discard;
	}

	vec3 lightVec = inPos.xyz - inLightPos;
	gl_FragDepth = length(lightVec) / radius;
}
