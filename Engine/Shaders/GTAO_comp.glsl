#version 450
#define TILE_SIZE 16

#include <SharedTypes.glsl>
#include <PostProcessCommon.glsl>

const float PI = 3.141592654;
const float HALF_PI = 1.570796327;

// Presets provided as push constants
layout (constant_id = 0) const float SLICE_COUNT = 9.0; // 1 -> 9
layout (constant_id = 1) const float STEP_COUNT = 3.0; // 2 -> 3
layout (constant_id = 2) const float OCCLUSION_RANGE = 2.0;
layout (constant_id = 3) const float FALLOFF_RANGE = 1.0;
layout (constant_id = 4) const float DISTRIBUTION_POWER = 2.0;
layout (constant_id = 5) const float THICKNESS_MIX = 0.2;

layout (set = 0, binding = 0) uniform sampler2D depthImage;
layout (set = 1, binding = 0, r16f) uniform writeonly image2D aoResult;
//layout (set = 1, binding = 0, rgba32f) uniform writeonly image2D aoResult;
layout (set = 2, binding = 0) uniform readonly CameraProps
{
	CamProps camProps;
};

layout (push_constant) uniform Constants
{
	int width;
	int height;
};

vec2 texelSize;
ivec2 screenSize;
float a;
float tanFov;
float n;
float f;

float sampleDepth(vec2 uv)
{
	uv *= vec2(camProps.xScale, camProps.yScale);
	return texture(depthImage, uv).r;
}

float nrand( vec2 n )
{
	return fract(sin(dot(n.xy, vec2(12.9898, 78.233)))* 43758.5453);
} 

// https://wickedengine.net/2019/09/22/improved-normal-reconstruction-from-depth/
vec3 getDepthNormal(vec3 centerPos, vec2 centerCoord)
{
	const vec2 cross_idx[4] = {
		centerCoord + vec2(texelSize.x, 0.0),
		centerCoord + vec2(-texelSize.x, 0.0),
		centerCoord + vec2(0.0, texelSize.y),
		centerCoord + vec2(0.0, -texelSize.y)
	};

	vec3 pos[4] = {
		getViewPos(cross_idx[0], sampleDepth(cross_idx[0]).r, a, tanFov, n, f),
		getViewPos(cross_idx[1], sampleDepth(cross_idx[1]).r, a, tanFov, n, f),
		getViewPos(cross_idx[2], sampleDepth(cross_idx[2]).r, a, tanFov, n, f),
		getViewPos(cross_idx[3], sampleDepth(cross_idx[3]).r, a, tanFov, n, f)
	};

	uint bestH = abs(pos[0].z - centerPos.z) < abs(pos[1].z - centerPos.z) ? 0 : 1;
	uint bestV = abs(pos[2].z - centerPos.z) < abs(pos[3].z - centerPos.z) ? 2 : 3;

	vec3 p1 = vec3(0.0);
	vec3 p2 = vec3(0.0);

	if (bestH == 0 && bestV == 3) {
		p1 = pos[0];
		p2 = pos[3];
	} else if (bestH == 0 && bestV == 2) {
		p1 = pos[2];
		p2 = pos[0];
	} else if (bestH == 1 && bestV == 3) {
		p1 = pos[3];
		p2 = pos[1];
	} else if (bestH == 1 && bestV == 2) {
		p1 = pos[1];
		p2 = pos[2];
	}

	vec3 normal = normalize(cross(p2 - centerPos, p1 - centerPos));

	return normal;
}

// Eberly2014 GPGPU Programming for Games and Science
float fastAcos(float x)
{
    float res = -0.156583 * abs(x) + HALF_PI;
    res *= sqrt(1.0 - abs(x));
    return x >= 0 ? res : PI - res;
}

// Based on the paper https://www.activision.com/cdn/research/Practical_Real_Time_Strategies_for_Accurate_Indirect_Occlusion_NEW%20VERSION_COLOR.pdf
layout (local_size_x = TILE_SIZE, local_size_y = TILE_SIZE) in;
void main()
{
	screenSize = ivec2(width, height);
	texelSize = vec2(1.0 / screenSize.x, 1.0 / screenSize.y);
	a = float(screenSize.x) / float(screenSize.y);
	tanFov = tan(camProps.fov / 2.0);
	n = camProps.near;
	f = camProps.far;

	vec2 centerTexCoord = vec2(float(gl_GlobalInvocationID.x) / screenSize.x,
			float(gl_GlobalInvocationID.y) / screenSize.y);
	float depthVal = sampleDepth(centerTexCoord).r;

	// Get center position in view space position from depth
	vec3 centerPos = getViewPos(centerTexCoord, depthVal, a, tanFov, n, f);
	vec3 viewV = normalize(-centerPos);

	// Get normal from the depth image
	vec3 normal = getDepthNormal(centerPos, centerTexCoord);

//	vec3 resultTest;
	float visibility = 0;

	// TODO: Change scale to match falloff range
	vec2 screenScale = vec2(a, 1.0) * 0.02 * (OCCLUSION_RANGE + FALLOFF_RANGE) / -centerPos.z;

	// Algorithm 1 from the paper
	float sliceNoise = nrand(centerTexCoord);
	for (uint i = 0; i < SLICE_COUNT; ++i) {
		
		float slice = (i + sliceNoise) / SLICE_COUNT;
		float phi = PI * slice;

		vec2 omega = vec2(cos(phi), sin(phi));

		vec3 dirV = vec3(omega, 0);
		vec3 orthoDirV = dirV - (dot(dirV, viewV) * viewV);
		vec3 axisV = cross(dirV, viewV);
		vec3 projNormalV = normal;// - axisV * dot(normal, axisV);

		float sgnN = sign(dot(orthoDirV, projNormalV));
		float cosN = clamp(dot(projNormalV, viewV) / length(projNormalV) ,0, 1);
		float hn = sgnN * acos(cosN);
		
		float cHorizonCos0 = -1;
		float cHorizonCos1 = -1;

		for (int j = 0; j < STEP_COUNT; ++j) {
			// TODO: Step noise
			float s = (j + 1) / (STEP_COUNT + 1);
			s = pow(s, DISTRIBUTION_POWER);
			vec2 sampleOffset = screenScale * (s + texelSize * 1.1);

			sampleOffset *= vec2(omega.x, omega.y);

			// Don't sample the same pixel
			vec2 sTexCoord0 = centerTexCoord - sampleOffset;
			float sDepthVal0 = sampleDepth(sTexCoord0).r;
			vec3 sPosV0 = getViewPos(sTexCoord0, sDepthVal0, a, tanFov, n, f);
			float sPosDist0 = length(sPosV0 - centerPos);
			vec3 sHorizonV0 = normalize(sPosV0 - centerPos);

			float weight0 = clamp((OCCLUSION_RANGE - sPosDist0) / FALLOFF_RANGE, 0.0, 1.0);
			float shc0 = dot(sHorizonV0, viewV);
			cHorizonCos0 = max(cHorizonCos0, mix(-1.0, shc0, weight0));

			cHorizonCos0 = mix(cHorizonCos0, shc0, weight0 * THICKNESS_MIX);

			// Side 1
			vec2 sTexCoord1 = centerTexCoord + sampleOffset;
			float sDepthVal1 = sampleDepth(sTexCoord1).r;
			vec3 sPosV1 = getViewPos(sTexCoord1, sDepthVal1, a, tanFov, n, f);
			float sPosDist1 = length(sPosV1 - centerPos);
			vec3 sHorizonV1 = normalize(sPosV1 - centerPos);

			float weight1 = clamp((OCCLUSION_RANGE - sPosDist1) / FALLOFF_RANGE, 0.0, 1.0);
			float shc1 = dot(sHorizonV1, viewV);
			cHorizonCos1 = max(cHorizonCos1, mix(-1.0, shc1, weight1));

			cHorizonCos1 = mix(cHorizonCos1, shc1, weight1 * THICKNESS_MIX);
		}

		float h0 = hn + clamp(-acos(cHorizonCos0) - hn, -HALF_PI, HALF_PI);
		float h1 = hn + clamp(acos(cHorizonCos1) - hn, -HALF_PI, HALF_PI);

		float arc = length(projNormalV) * 0.25 * (
				(cosN + 2.0 * h0 * sin(hn) - cos(2.0 * h0 - hn)) +
				(cosN + 2.0 * h1 * sin(hn) - cos(2.0 * h1 - hn)));

		visibility += arc;
	}
	visibility /= SLICE_COUNT;

	imageStore(aoResult, ivec2(gl_GlobalInvocationID.xy), vec4(visibility, 0.0, 0.0, 1.0));
}
