#version 450
#extension GL_ARB_separate_shader_objects : enable

#include <SharedTypes.glsl>

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec4 inTangent;
layout (location = 3) in vec2 inUv;

layout (location = 0) out vec3 fragPos;
layout (location = 1) out vec2 texCoords;
layout (location = 2) out vec3 fragViewPos;

// Interpolation fix: https://stackoverflow.com/questions/49262877/opengl-ndc-and-coordinates
layout (location = 3) noperspective out vec2 screenCoords;

out gl_PerVertex 
{
	vec4 gl_Position;
};

layout (set = 0, binding = 0) uniform readonly CameraProps
{
	CamProps camProps;
};

layout (push_constant) uniform constants
{
	mat4 transform;
} pushConst;

layout (location = 4) out OutCameraProps 
{
	vec3 viewPos;
	vec3 ambientColour;
	float exposure;
	float gamma;
	float near;
	float far;
	vec2 ndcScale;
} outCameraProps;

layout (location = 11) out mat3 TBN;

void main() 
{
	vec4 fragPosBuffer = pushConst.transform * vec4(inPosition, 1.0f);
	fragPos = vec3(fragPosBuffer);
	fragViewPos = vec3(camProps.view * fragPosBuffer);
	gl_Position = (camProps.proj * camProps.view) * pushConst.transform * vec4(inPosition, 1.0f);
	screenCoords = (vec2(gl_Position.x, gl_Position.y) / gl_Position.w) * 0.5f + 0.5f;

	texCoords = inUv;

	// Compose TBN matrix for normal mapping
	mat3 mv = transpose(inverse(mat3(pushConst.transform)));
	vec3 T = normalize(mv * inTangent.xyz);
	vec3 N = normalize(mv * inNormal);
	vec3 B = normalize(mv * cross(inNormal, inTangent.xyz) * inTangent.w);
	T = normalize(T - dot(T, N) * N);
	TBN = mat3(T, B, N);

	// Pass camera variables
	outCameraProps.viewPos = camProps.viewPos;
	outCameraProps.ambientColour = camProps.ambientColour;
	outCameraProps.exposure = camProps.exposure;
	outCameraProps.gamma = camProps.gamma;
	outCameraProps.near = camProps.near;
	outCameraProps.far = camProps.far;

	// Scale sampler for full screen textures to match render area
	outCameraProps.ndcScale = vec2(camProps.xScale, camProps.yScale);
}


