#version 450
#extension GL_ARB_separate_shader_objects : enable

#include <SharedTypes.glsl>

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec2 inUv;

layout(location = 0) out vec2 texCoords;

layout (set = 0, binding = 0) uniform readonly CameraProps
{
	CamProps camProps;
}; 

layout (push_constant) uniform constants
{
	mat4 transform;
} pushConst;

void main()
{
	texCoords = inUv;
	gl_Position = camProps.proj * camProps.view * pushConst.transform * vec4(inPosition, 1.0);
}
