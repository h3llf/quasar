#version 450
#define TILE_SIZE 16

layout (set = 0, binding = 0, rgba16f) uniform image2D colourImage;
layout (set = 0, binding = 1) uniform usampler2D stencilImage;
layout (set = 0, binding = 2, rg16f) uniform image2D uvBuffer;

layout (push_constant) uniform Constants
{
	vec4 colour;
	int currentSample;
	int numSamples;	
	int width;
	int height;
	float radius;
	float fadeout;
	vec2 ndcScale;
};

void testPixel(vec2 uvColour, ivec2 iuv)
{
	if ((iuv.x >= width) || (iuv.x < 0) || (iuv.y >= height) || (iuv.y < 0))
		return;
	
	vec2 dstVal = imageLoad(uvBuffer, iuv).rg;

	if (dstVal.x == 0) {
		// Write immediately
		imageStore(uvBuffer, iuv, vec4(uvColour, 0.0f, 0.0f));
	} else {
		float prevDist = length(iuv - dstVal * vec2(width, height));
		float currentDist = length(iuv - uvColour * vec2(width, height));
		if (currentDist < prevDist) {
			imageStore(uvBuffer, iuv, vec4(uvColour, 0.0f, 0.0f));
		}
	}
}

// TODO: Customizable outline effects

layout (local_size_x = TILE_SIZE, local_size_y = TILE_SIZE) in;
void main()
{
	if ((gl_GlobalInvocationID.x >= width) || (gl_GlobalInvocationID.y >= height))
		return;

	ivec2 iuv = ivec2(gl_GlobalInvocationID.xy);
	vec2 uv = gl_GlobalInvocationID.xy;
	uv /= vec2(width, height);

	if (currentSample == 0) {
		// Set UV values on stencil positions
		uint stencil = texture(stencilImage, uv * ndcScale).r;
		if (stencil != 0) {
			imageStore(uvBuffer, iuv, vec4(uv.x, uv.y, 0.0f, 0.0f));
		} else {
			imageStore(uvBuffer, iuv, vec4(-10.0f, 0.0f, 0.0f, 0.0f));
		}
	} else if (currentSample <= numSamples) {
		

		ivec2 kernel[8] = {
			ivec2(-1, 1),
			ivec2(0, 1),
			ivec2(1, 1),
			ivec2(-1, 0),
			ivec2(1, 0),
			ivec2(-1, -1),
			ivec2(0, -1),
			ivec2(1, -1)
		};

		vec2 thisPx = imageLoad(uvBuffer, iuv).rg;
		if (thisPx.x != 0 && thisPx.y != 0) {
			int currentOffset = int(pow(2, numSamples - currentSample));
			for (int k = 0; k < 8; ++k) {
				testPixel(thisPx, kernel[k] * currentOffset + iuv);
			}
		}
	} else {
		uint stencil = texture(stencilImage, uv * ndcScale).r;
		if (stencil != 0) {
			return;
		}

		float aspect = float(width) / height;

		vec2 diff = uv - imageLoad(uvBuffer, iuv).rg;
		diff.x *= aspect;
		float dist = length(diff);

		if (dist < radius) {
			barrier();
			imageStore(colourImage, iuv, colour);
		} else if (dist < radius + fadeout && stencil == 0.0) {
			float weight = dist - radius;
			weight = max(0.0, weight);
			weight /= fadeout;

			vec4 prevColour = imageLoad(colourImage, iuv);
			vec4 outputColour = mix(colour, prevColour, weight);
			imageStore(colourImage, iuv, outputColour);
		}
//		else {
//			imageStore(colourImage, iuv, imageLoad(uvBuffer, iuv));
//		}
	}
}
