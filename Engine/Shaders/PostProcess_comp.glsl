#version 450
#define TILE_SIZE 16

#include <SharedTypes.glsl>

layout (set = 0, binding = 0, rgba16f) uniform image2D shadingImg;

/*
layout (set = 0, binding = 1) uniform readonly Options
{
	int width;
	int height;
} options;*/

layout (set = 1, binding = 0) uniform readonly CameraProps
{
	CamProps camProps;
};

layout (push_constant) uniform Options
{
	int width;
	int height;
} options;

// https://knarkowicz.wordpress.com/2016/01/06/aces-filmic-tone-mapping-curve/
vec3 ACESFilm(vec3 x)
{
	float a = 2.51f;
	float b = 0.03f;
	float c = 2.43f;
	float d = 0.59f;
	float e = 0.14f;
	return clamp((x*(a*x+b))/(x*(c*x+d)+e), 0.0, 1.0);
}

layout (local_size_x = TILE_SIZE, local_size_y = TILE_SIZE) in;
void main()
{
	ivec2 iuv = ivec2(gl_GlobalInvocationID.xy);
	vec2 uv = vec2(float(iuv.x) / options.width, float(iuv.y) / options.height);

	// TODO: Tone mapping
	vec3 colour = imageLoad(shadingImg, iuv).rgb;//texture(inputImg, uv).rgb;

	// Exposure & Tone mapping
//	colour = vec3(1.0) - exp(-colour * camProps.exposure);
	colour *= camProps.exposure;
	colour = ACESFilm(colour);

	// Gamma correction
	colour = pow(colour, vec3(1.0 / camProps.gamma));

	imageStore(shadingImg, iuv, vec4(colour, 1.0));
}
