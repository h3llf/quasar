#version 450

#include <SharedTypes.glsl>

layout (location = 0) in vec3 position;

layout (location = 0) out vec3 vertexPos;

layout (set = 0, binding = 0) uniform readonly CameraProps
{
	CamProps camProps;
};

void main()
{
	// Remove position data so the skybox follows the camera
	mat4 view = camProps.view;
	view[3][0] = 0;
	view[3][1] = 0;
	view[3][2] = 0;

	vertexPos = position;

	// Transform to far plane
	gl_Position = camProps.proj * view * vec4(position * camProps.far * 0.5, 1.0);
}
