#version 450
#define TILE_SIZE 16

#include <SharedTypes.glsl>
#include <PostProcessCommon.glsl>

// Buffers and depth
layout (set = 0, binding = 0, rg16f) uniform image2D resultImage;
layout (set = 0, binding = 1, r11f_g11f_b10f) uniform image2D shadingImage;
layout (set = 0, binding = 2) uniform sampler2D prevShadingImage;
layout (set = 0, binding = 3) uniform Settings
{
	int width;
	int height;
};

layout (local_size_x = TILE_SIZE, local_size_y = TILE_SIZE) in;
void main()
{
	vec2 uv = imageLoad(resultImage, ivec2(gl_GlobalInvocationID.xy)).xy;
	
	if (uv == vec2(0.0)) {
		return;
	}

	vec2 prevUV = vec2(gl_GlobalInvocationID.xy) / vec2(width, height);
	vec4 prevColour = imageLoad(shadingImage, ivec2(gl_GlobalInvocationID.xy));

	vec4 reflectColour = texture(prevShadingImage, uv) + prevColour;

	imageStore(shadingImage, ivec2(gl_GlobalInvocationID.xy), prevColour + reflectColour);
	
}
