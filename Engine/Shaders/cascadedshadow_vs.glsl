#version 450
#extension GL_EXT_multiview : enable

// TODO: Use specialization constant for this value
#define CASCADE_COUNT 4

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec2 inUv;

layout(location = 0) out vec2 texCoords;

layout (binding = 0) uniform UBO
{
	mat4 lightVP[CASCADE_COUNT];
	vec4 cascadeSplits;
} ubo;

layout (push_constant) uniform constants
{
	mat4 transform;
} pushConst;

void main()
{
	gl_Position = ubo.lightVP[gl_ViewIndex] * pushConst.transform * vec4(inPosition, 1.0);
}
