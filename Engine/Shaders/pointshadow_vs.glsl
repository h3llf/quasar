#version 450
#extension GL_EXT_multiview : enable

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec2 inUv;

layout (location = 0) out vec4 outPos;
layout (location = 1) out float radius;
layout (location = 2) out vec3 outLightPos;
layout(location = 3) out vec2 texCoords;

layout (binding = 0) uniform UBO 
{
	mat4 depthVP[6];
	vec3 lightPos;
	float radius;
} ubo;

layout (push_constant) uniform constants
{
	mat4 transform;
} pushConst;
 
void main()
{
	texCoords = inUv;
	outPos = pushConst.transform * vec4(inPosition, 1.0);
	outLightPos = ubo.lightPos;
	radius = ubo.radius;
	gl_Position = ubo.depthVP[gl_ViewIndex] * outPos;
}
