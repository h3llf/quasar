#version 450

layout (location = 0) in vec2 texCoord;
layout (location = 1) in vec4 colour;

layout (location = 0) out vec4 outColour;

// Texture array
layout (set = 0, binding = 0) uniform sampler2D textureSamplers[16384];

layout (push_constant) uniform PushConstantData{
	layout (offset = 64)
	int texID;
};

float median(float r, float g, float b) {
    return max(min(r, g), min(max(r, g), b));
}

void main()
{
	vec3 s = texture(textureSamplers[texID], texCoord).rgb;
	ivec2 sz = textureSize(textureSamplers[texID], 0);
	float dx = dFdx(texCoord.x) * sz.x;
	float dy = dFdy(texCoord.y) * sz.y;
	float toPixels = 8.0 * inversesqrt( dx * dx + dy * dy );
	float sigDist = median(s.r, s.g, s.b) - 0.5;
	float opacity = clamp(sigDist * toPixels + 0.5, 0.0, 1.0);

	outColour = vec4(colour.rgb, opacity * colour.a);
}
