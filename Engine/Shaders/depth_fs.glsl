#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec2 texCoords;

// Texture array
layout (set = 1, binding = 0) uniform sampler2D textureSamplers[16384];

layout (push_constant) uniform MaterialProps
{
	layout (offset = 64)
	int albedoImg;
};
const float alphaBias = 0.1;

void main()
{
	if (albedoImg != -1 && texture(textureSamplers[albedoImg], texCoords).a < alphaBias) {
		discard;
	}
}
