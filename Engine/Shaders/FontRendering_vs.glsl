#version 450

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec4 colour;

layout (location = 0) out vec2 outTexCoord;
layout (location = 1) out vec4 outColour;

layout (push_constant) uniform PushConstantData{
	mat4 MVP;
};

void main()
{
	outTexCoord = texCoord;
	outColour = colour;

	gl_Position = MVP * vec4(position, 1.0);
}
