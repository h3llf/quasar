import argparse
import platform
import os

command = ""

# Platform specific commands
if (platform.system() == "Linux"):
    # Compiler should be installed as part of a repo packae
    command = "/bin/glslc";
elif (platform.system() == "Windows"):
    vulkan_path = os.getenv('VULKAN_SDK');
    if (vulkan_path == None):
        print ("Error: the VULKAN_SDK environment needs to be set to the vulkan SDK installation directory");
        os._exit(1);
    command = vulkan_path + "\\Bin\\glslc.exe"
elif (platform.system() == "Darwin"):
    print("Platform not yet supported");

parser = argparse.ArgumentParser();
parser.add_argument("-t", "--type", type=str, required=True);
parser.add_argument("-n", "--names", type=str, required=True, nargs='+');
args = parser.parse_args();

shaderFlags = "-I ./include/";

# Select compiler flags
# Debug symbols and preprocessor directive
if (args.type == "Release"):
    shaderFlags += " -D=__Release";
else:
    shaderFlags += " -D=__Debug -g -O0";

shaderStages = {("vert", "vs"), ("frag", "fs"), ("comp", "comp")}

# TODO: Maybe switch to pyshaderc bindings rather than system commands https://github.com/realitix/pyshaderc

for var in args.names:
    for stage in shaderStages:
        # Find shader files matching the format: name_type.glsl
        # Compile if shader stage is present
        fullName = var + "_" + stage[1];

        if (os.path.exists(fullName + ".glsl")):
            compileCmd = command; # glslc
            compileCmd += " -fshader-stage=" + stage[0]; # Set type
            compileCmd += " " + shaderFlags + " "; # Flags
            compileCmd += fullName + ".glsl "; # Input
            compileCmd += "-o " + fullName + ".spv"; # Output
            print(compileCmd);
            os.system(compileCmd);
