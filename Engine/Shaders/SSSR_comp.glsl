#version 450
#define TILE_SIZE 16

//#extension GL_EXT_debug_printf : enable

#include <SharedTypes.glsl>
#include <PostProcessCommon.glsl>

// Buffers and depth
layout (set = 0, binding = 0) uniform sampler2D depthImage;
layout (set = 0, binding = 1) uniform sampler2D gBuffers;
layout (set = 0, binding = 2, rg16f) uniform image2D resultImage;
layout (set = 0, binding = 3) uniform Settings
{
	int width;
	int height;
};

layout (set = 1, binding = 0) uniform readonly CameraProps
{
	CamProps camProps;
};

const float step = 0.1;
const int numSteps = 70;
const int numBinarySteps = 5;

float aspect;
float tanFov;

vec3 viewToScreen(vec3 viewOrigin)
{
	vec4 projected = camProps.proj * vec4(viewOrigin, 1.0);
	projected.xyz /= projected.w;
	projected.xy = projected.xy / 2.0 + 0.5;
	return projected.xyz;
}

vec3 binarySearch(vec3 dir, vec3 pos)
{
//	dir *= 0.5;
//	pos -= dir;

	for (int i = 0; i < numBinarySteps; ++i) {
		float depth = texture(depthImage, pos.xy).r;

		/*
		float dDepth = depth - pos.z;
	
		dir *= 0.5;
		if (dDepth > 0.0) {
			pos += dir;
		} else {
			pos -= dir;
		}*/

		if (pos.z < depth) {
			pos += dir;
		}

		dir *= 0.5;
		pos -= dir;
	}

	return pos;
}

vec3 rayMarch(vec3 dir, vec3 origin)
{
	dir.z *= -1;
	dir /= numSteps;
	vec3 pos = origin;

	for (int i = 0; i < numSteps; ++i) {
		pos += dir;
		
		if (pos.x > 1 || pos.x < 0 || pos.y > 1 || pos.y < 0) {
			return vec3(0.0);
		}

		float depth = texture(depthImage, pos.xy).r;
		float dDepth = depth - pos.z;

		/*
		if (gl_GlobalInvocationID.xy == ivec2(width / 2, height / 2)) {
			ivec2 hitCoord = ivec2(pos.xy * vec2(width, height));
			imageStore(shadedImage, hitCoord, vec4(22.0, 99.0, 22.0, 1.0));
			debugPrintfEXT("pos %v3f d %f dir %v3f\n", pos, depth, dir);
		}*/

		if (dDepth <= 0) {
			vec3 posBin = binarySearch(dir, pos);
			return posBin;

//			ivec2 hitCoord = ivec2(posBin.xy * vec2(width, height));
//			return imageLoad(shadedImage, hitCoord).rgb;
		}
	}

	return vec3(0.0);
}

vec3 result = vec3(0.0);
layout (local_size_x = TILE_SIZE, local_size_y = TILE_SIZE) in;
void main()
{
	aspect = float(width) / float(height);
	tanFov = tan(camProps.fov / 2.0);

	vec2 uv = vec2(gl_GlobalInvocationID.xy) / vec2(width, height);
	float depth = texture(depthImage, uv).r;
	vec4 gColour  = texture(gBuffers, uv);

	if (gColour.a > 0.01) {
		imageStore(resultImage, ivec2(gl_GlobalInvocationID.xy), vec4(0.0));
		return;
	}

	vec3 normal = gColour.xyz;
	vec3 viewNormal = (camProps.view * vec4(normal, 0.0)).xyz;

	// Screenspace test
//	vec3 screenOrigin = vec3(uv, depth);
	vec3 viewOrigin = getViewPos(uv, depth, aspect, tanFov, camProps.near, camProps.far);
	viewOrigin.y *= -1;
	vec3 viewDir = reflect(normalize(viewOrigin), normalize(viewNormal));
	vec3 viewDirNorm = (viewOrigin + viewDir);

	vec3 screenOrigin = viewToScreen(viewOrigin);
	vec3 screenDir = viewToScreen(viewDirNorm) - screenOrigin;

	// Scale to a constant size in screenspace
	screenDir = normalize(screenDir);

	screenOrigin.z = 1.0 - screenOrigin.z;
	result = rayMarch(screenDir, screenOrigin);

//	if (result != vec3(0.0)) {
//		vec4 prev = imageLoad(shadedImage, ivec2(gl_GlobalInvocationID.xy));
//		prev.xyz += result;
		imageStore(resultImage, ivec2(gl_GlobalInvocationID.xy), vec4(result, 1.0));
//	}
}
