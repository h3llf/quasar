#version 450

layout (location = 0) in vec3 vertexPos;

layout (location = 0) out vec4 outColour;
layout (location = 1) out vec4 gBuffers;

// Texture array
layout (set = 1, binding = 0) uniform sampler2D textureSamplers[16384];

layout (push_constant) uniform PushConstantData{
	int textureIndex;
};

const vec2 invAtan = vec2(0.1591, 0.3183);
vec2 sampleEquirectangular(vec3 v)
{
	vec2 uv = vec2(atan(v.z, v.x), -asin(v.y));
	uv *= invAtan;
	uv += 0.5;
	return uv;
}

void main()
{
	vec2 uv = sampleEquirectangular(normalize(vertexPos));
	vec3 colour = vec3(0.0);

	if (textureIndex != -1) {
		colour = texture(textureSamplers[textureIndex], uv).rgb;
	}

	outColour = vec4(colour, 1.0);
	gBuffers = vec4(0.0, 0.0, 0.0, 1.0);
}
