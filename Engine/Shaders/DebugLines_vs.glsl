#version 450
#extension GL_ARB_separate_shader_objects : enable

#include <SharedTypes.glsl>

layout (location = 0) in vec4 inColour;
layout (location = 1) in vec3 inPosition;

layout (location = 0) out vec4 outColour;

layout (set = 0, binding = 0) uniform readonly CameraProps
{
	CamProps camProps;
};

void main()
{
	outColour = inColour;
	gl_Position = camProps.proj * camProps.view * vec4(inPosition, 1.0);
}
