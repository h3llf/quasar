#ifndef LIGHTING_BASE_FS_GLSL
#define LIGHTING_BASE_FS_GLSL

#include <LightingPBR.glsl>
#include <Shadows.glsl>
#include <Visualization.glsl>

layout (location = 0) in vec3 fragPos;
layout (location = 1) in vec2 texCoords;
layout (location = 2) in vec3 fragViewPos;
layout (location = 3) noperspective in vec2 screenCoords;
layout (location = 4) in InCameraProps {
	vec3 viewPos;
	vec3 ambientColour;
	float exposure;
	float gamma;
	float near;
	float far;
	vec2 ndcScale;
} inCameraProps;
layout (location = 11) in mat3 TBN;

layout (set = 1, binding = 0) readonly buffer PointLightBuffer {
	uint lightCount;
	PointLight pointLights[4096];
};
layout (set = 1, binding = 1) readonly uniform DirectionalLightBuffer {
	DirectionalLight directionalLight;
};

// Texture array
layout (set = 2, binding = 0) uniform sampler2D textureSamplers[16384];

// Material properties
// 128 bytes total
layout (push_constant) uniform MaterialProps
{
	// Offset of mat4 push const in vertex shader stage
	layout (offset = 64)
	vec4 baseColour;
	vec3 emissiveFactor;
	float metallicFactor;
	float roughnessFactor;
	float normalScale;
	int albedoImg;
	int pbrImg;
	int normImg;
	int emissiveImg;
};

layout (set = 3, binding = 0) uniform samplerCubeArray omniShadow;
layout (set = 3, binding = 1) uniform sampler2DArray cascadedShadow;
layout (set = 3, binding = 2) uniform CascadeShadowVP {
	mat4 lightVP[8];
	vec4 cascadeSplits[8]; // TODO: Avoid wasting space
} cascadeShadowVP;
layout (set = 3, binding = 3) uniform sampler2D ambientOcclusion;

vec3 getNormalVector(vec3 normalColour)
{
	vec3 normal = normalColour;//texture(textureSamplers[normImg], texCoords).rgb;
	normal = normal * 2.0 - 1.0;
	normal.xy *= normalScale;
	normal = TBN * normalize(normal);
	return normalize(normal);
}

const mat4 biasMat = mat4( 
	0.5, 0.0, 0.0, 0.0,
	0.0, 0.5, 0.0, 0.0,
	0.0, 0.0, 1.0, 0.0,
	0.5, 0.5, 0.0, 1.0 
);

vec3 dirLightCalculations(vec3 texColour, vec2 PBRmat, vec3 normal, vec3 V)
{
	// Directional light shadow
	float cascadeShadowResult = 1.0;
	uint cascadeIndex = 0;

	for (uint i = 0; i < CASCADE_COUNT; ++i) {
		if (fragViewPos.z < cascadeShadowVP.cascadeSplits[i].x) {
			cascadeIndex++;
		}
	}

	float blendDist = abs(fragViewPos.z - cascadeShadowVP.cascadeSplits[cascadeIndex].x);

	const float cascadeBlendDist = 2.0;
	if (directionalLight.shadowMapped > 0) {
		vec4 shadowCoord = biasMat * cascadeShadowVP.lightVP[cascadeIndex] * vec4(fragPos, 1.0);
		cascadeShadowResult = cascadedShadowCalculation(shadowCoord / shadowCoord.w, cascadeIndex,
				fragPos, cascadedShadow);

		// Blend seams
		if (blendDist < cascadeBlendDist && cascadeIndex < CASCADE_COUNT - 1) {
			vec4 shadowCoord2 = biasMat * cascadeShadowVP.lightVP[cascadeIndex + 1] * vec4(fragPos, 1.0);
			float shadowResult2 = cascadedShadowCalculation(shadowCoord2 / shadowCoord2.w, cascadeIndex + 1,
				fragPos, cascadedShadow);

			float blendFactor = 1.0 - (blendDist / cascadeBlendDist);
			cascadeShadowResult = mix(cascadeShadowResult, shadowResult2, blendFactor);
		}
	}

	// Directional light
	vec3 lightDir = directionalLight.direction;
	vec3 shadowResult = lightingCalculation(lightDir, directionalLight.colour, texColour, normal, PBRmat, V);
	shadowResult *= cascadeShadowResult;

#ifdef __Debug
	if (ENABLE_SHOW_CASCADE_SPLITS) {
		shadowResult += cascadeIndexDisplay(cascadeIndex);
	}
#endif

	return shadowResult;
}

#endif //LIGHTING_BASE_FS_GLSL
