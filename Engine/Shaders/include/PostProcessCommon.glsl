float linearizeDepth(float depth,float near,float far)
{
	return near * far / (far + depth * (near - far));
}

// https://stackoverflow.com/questions/11277501/how-to-recover-view-space-position-given-view-space-depth-value-and-ndc-xy
vec3 getViewPos(vec2 xy_ndc, float depth, float aspect, float tanHalfFov, float n, float f)
{
//	vec2 xy = vec2(2.0 * xy_ndc.x - 1.0, 2.0 * (1.0 - xy_ndc.y) - 1.0);
	vec2 xy = xy_ndc * 2.0 - 1.0;
	float d = linearizeDepth(depth, n, f);

	vec3 viewPos;
	viewPos.x = d * xy.x * aspect * tanHalfFov;
	viewPos.y = d * xy.y * tanHalfFov;
	viewPos.z = -d;

	return viewPos;
}

