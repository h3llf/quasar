struct CamProps
{
	mat4 view;
	mat4 proj;
	mat4 invProj;
	vec3 ambientColour;
	float exposure;
	vec3 viewPos;
	float gamma;
	float fov;
	float near;
	float far;
	float xScale;
	float yScale;
};
