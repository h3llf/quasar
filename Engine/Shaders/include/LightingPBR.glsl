#ifndef LIGHTING_PBR_GLSL
#define LIGHTING_PBR_GLSL

struct PointLight {
	vec3 position;
	float minLuminosity;
	vec3 colour;
	int shadowIndex;
};

struct DirectionalLight {
	vec3 colour;
	int shadowMapped;
	vec3 direction;
};

//Ratio between specular and diffuse reflection
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
	return F0 + (1.0 - F0) * pow(max(1.0 - cosTheta, 0.0), 5.0);
}

float DistributionGGX(vec3 N, vec3 H, float roughness)
{
	float a      = roughness*roughness;
	float a2     = a*a;
	float NdotH  = max(dot(N, H), 0.0);
	float NdotH2 = NdotH*NdotH;

	float num   = a2;
	float denom = (NdotH2 * (a2 - 1.0) + 1.0);
	denom = PI * denom * denom;
	
	return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
	float r = (roughness + 1.0);
	float k = (r*r) / 8.0;

	float num   = NdotV;
	float denom = NdotV * (1.0 - k) + k;

	return num / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
	float NdotV = max(dot(N, V), 0.0);
	float NdotL = max(dot(N, L), 0.0);
	float ggx2  = GeometrySchlickGGX(NdotV, roughness);
	float ggx1  = GeometrySchlickGGX(NdotL, roughness);
	
	return ggx1 * ggx2;
}

vec3 lightingCalculation(vec3 dir, vec3 colour, vec3 texColour, vec3 normal, vec2 PBRmat, vec3 viewDir)
{
	vec3 H = normalize(viewDir + dir);

	vec3 F0 = vec3(0.04);
	F0 = mix(F0, texColour.rgb, PBRmat.y);
	vec3 F = fresnelSchlick(max(dot(H, viewDir), 0.0), F0);
	
	float NDF = DistributionGGX(normal, H, PBRmat.x);
	float G = GeometrySmith(normal, viewDir, dir, PBRmat.x);

	vec3 numerator = NDF * G * F;
	float denominator = 4.0 * max(dot(normal, viewDir), 0.0) * max(dot(normal, dir), 0.0);
	vec3 specular = numerator / max(denominator, 0.001);

	vec3 KS = F;
	vec3 KD = vec3(1.0) - KS;
	KD *= 1.0 - PBRmat.y;
	float NdotL = max(dot(normal, dir), 0.0);

	return (KD * texColour / PI + specular) * NdotL * colour;
}

vec3 pointLightCalculation(vec3 position, vec3 colour, vec3 texColour, vec3 normal, vec2 PBRmat, 
		vec3 viewDir, vec3 fragPos, float minLuminosity)
{
	vec3 dir = normalize(position - fragPos);

	float distance = length(position - fragPos);
	float attenuation = 1.0 / (distance * distance) - minLuminosity;
	attenuation = max(0.0, attenuation);
	
	return lightingCalculation(dir, colour, texColour, normal, PBRmat, viewDir) * attenuation;
}

#endif //LIGHTING_PBR_GLSL
