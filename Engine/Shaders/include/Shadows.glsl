#ifndef SHADOWS_GLSL
#define SHADOWS_GLSL

#include <LightingPBR.glsl>
#include <Random.glsl>

float omniShadowCalculation(PointLight pLight, vec3 viewPos, vec3 fragPos,
		samplerCubeArray omniShadow)
{
	vec3 fragToLight = fragPos - pLight.position;
	fragToLight.z = -fragToLight.z; //TODO: Investigate why Z coords need to be flipped
	float currentDepth = length(fragToLight);
	float bias = 0.05;

	// TODO: Replace this
	// Calculate radius
	float radius = sqrt(1.0 / pLight.minLuminosity + 0.1);

	//PCF + random noise
	float viewDistance = length(viewPos - fragPos);
	float diskRadius = (1.0 + (viewDistance / radius)) / 25.0;

	float shadow = 0.0;

	//Limit tile size to because of floating point inaccuracy
	const float tSize = 64.0;
	float seed = float(mod(gl_FragCoord.x, tSize) + mod(gl_FragCoord.y, tSize) * tSize) * NUM_PCF_SAMPLES;
	
	for (int i = 0; i < NUM_PCF_SAMPLES; ++i) {
		vec3 sampleOffset = qrand3(seed + i);

 		vec3 offsetVector = normalize(fragToLight + sampleOffset * diskRadius);

		float closestDepth = texture(omniShadow, vec4(offsetVector, pLight.shadowIndex)).r;

		closestDepth *= radius;
		if (currentDepth - bias > closestDepth)
			shadow += 1.0;
	}

	shadow /= float(NUM_PCF_SAMPLES);

	//No PCF
	/*
	float closestDepth = texture(omniShadow, fragToLight).r;
	closestDepth *= farPlane;
	float shadow = currentDepth - bias > closestDepth ? 1.0 : 0.0;*/

	return 1.0 - shadow;
}

float cascadedShadowCalculation(vec4 shadowCoord, uint cascadeIndex, vec3 fragPos, 
		sampler2DArray shadowTex)
{
	float shadow = 0.0;
	float bias = 0.0;

	// PCF
	float radius = 3.0;
	float texelSize = 1.0f / textureSize(shadowTex, 0).x;
	radius *= texelSize;

	const float tSize = 64.0;
	float seed = interleavedGradientNoise(gl_FragCoord.xy);
	float theta = seed * 2.0 * PI;
	mat2 rotation = mat2(cos(theta), -sin(theta), sin(theta), cos(theta));

	for (int i = 0; i < NUM_PCF_SAMPLES; ++i) {
		vec2 sampleOffset = rotation * poissonDisk[i] * radius;
	
		float dist = 1.0f - texture(shadowTex,
				vec3(shadowCoord.st + sampleOffset, cascadeIndex)).r;

		if (shadowCoord.w > 0 && dist > shadowCoord.z + bias) {
			shadow += 1.0;
		}

		// Early out
//		if (i == 4 && (shadow == 0.0 || shadow == 5.0)) {
//			return 1.0 - (shadow / 5.0);
//		}
	}
	shadow /= NUM_PCF_SAMPLES;

	return 1.0 - shadow;
}

	// No PCF
	/*
	if ((shadowCoord.z > -1.0) && (shadowCoord.z < 1.0)) {
		float dist = 1.0f - texture(shadowTex, vec3(shadowCoord.st, cascadeIndex)).r;

		if (shadowCoord.w > 0 && dist > shadowCoord.z + bias) {
			shadow += 1.0;
		}
	}*/



#endif //SHADOWS_GLSL
