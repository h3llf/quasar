#ifndef QUASAR_FILE_DIALOG_H 
#define QUASAR_FILE_DIALOG_H

#include <string>
#include <vector>

class SystemDialog
{
public:
	static std::string getFile(const char* filters, bool saveMode, const char* path = nullptr);
	static std::vector<std::string> getFileMulti(const char* filters, const char* path = nullptr);
	static std::string getDirectory(const char* path = nullptr);
};

#endif //QUASAR_FILE_DIALOG_H
