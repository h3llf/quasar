#include "Platform/FileDialog.hpp"
#include <windows.h>
#include <shobjidl.h>
#include <iostream>
#include <filesystem>
#include <codecvt>

namespace fs = std::filesystem;

std::string convertResult(std::wstring input)
{
	// Convert to relative path & convert to regular string
	std::string result = fs::relative(fs::path(input)).string();
	
	// For compatability between Windows and Linux
	std::replace(result.begin(), result.end(), '\\', '/');
	return result;
}

void setFilters(const char* filters, IFileDialog* pFileDialog)
{
	std::wstring filter;

	// Convert filter to wchar
	std::wstring_convert<std::codecvt_utf8<wchar_t>> u8toW;
	filter = u8toW.from_bytes(filters);

	// Change separator to ';'
	std::replace(filter.begin(), filter.end(), ' ', ';');

	COMDLG_FILTERSPEC spec[1] = {L"", filter.c_str()};
	pFileDialog->SetFileTypes(1, spec);
}

std::string SystemDialog::getFile(const char* filters, bool saveMode, const char* path)
{
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | 
		COINIT_DISABLE_OLE1DDE);

	if (!SUCCEEDED(hr))
		return "";

	IFileDialog* pFileDialog;

	// Create the FileOpenDialog object.
	if (saveMode) {
		hr = CoCreateInstance(CLSID_FileSaveDialog, NULL, CLSCTX_ALL,
			IID_IFileSaveDialog, reinterpret_cast<void**>(&pFileDialog));
	} else {
		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL,
			IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileDialog));
	}

	// Convert filters & apply
	if (filters && SUCCEEDED(hr)) {
		setFilters(filters, pFileDialog);
	}

	// Show the Open dialog box
	if (SUCCEEDED(hr)) {
		hr = pFileDialog->Show(NULL);
	}

	// Get the file name from the dialog box
	IShellItem* pItem = nullptr;
	if (SUCCEEDED(hr)) {
		hr = pFileDialog->GetResult(&pItem);
	}

	PWSTR pszFilePath;
	if (SUCCEEDED(hr)) {
		hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);
	}

	if (pItem) {
		pItem->Release();
	}
	
	pFileDialog->Release();
	CoUninitialize();

	if (SUCCEEDED(hr)) {
		std::wstring wres = pszFilePath;
		return convertResult(wres);
	}

	return "";
}

std::vector<std::string> SystemDialog::getFileMulti(const char* filters, const char* path)
{
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | 
		COINIT_DISABLE_OLE1DDE);

	if (!SUCCEEDED(hr))
		return std::vector<std::string>();

	//ileOpenDialog *pFileDialog;
	IFileOpenDialog* pFileDialog;

	// Create the FileOpenDialog object.
	hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL,
		IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileDialog));

	// Specify multiselect.
	if (SUCCEEDED(hr)) {
		DWORD dwOptions;
		hr = pFileDialog->GetOptions(&dwOptions);

		if (SUCCEEDED(hr))
		{
			hr = pFileDialog->SetOptions(dwOptions | FOS_ALLOWMULTISELECT);
		}
	}

	// Convert filters & apply
	if (filters && SUCCEEDED(hr)) {
		setFilters(filters, pFileDialog);
	}

	// Show the Open dialog box.
	if (SUCCEEDED(hr)) {
		hr = pFileDialog->Show(NULL);
	}

	// Get the file name array from the dialog box.
	IShellItemArray *pItemArray = nullptr;
	if (SUCCEEDED(hr)) {
		hr = pFileDialog->GetResults(&pItemArray);
	}

	// Get each file name
	std::vector<std::string> filenames;
	if (SUCCEEDED(hr)) {
		DWORD itemCount;
		pItemArray->GetCount(&itemCount);
		IShellItem* pItem;

		for (DWORD i = 0; i < itemCount; ++i) {
			hr = pItemArray->GetItemAt(i, &pItem);
			
			PWSTR pszFilePath;
			if (SUCCEEDED(hr)) {
				hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);
			}

			if (SUCCEEDED(hr)) {
				std::wstring wres = pszFilePath;
				filenames.push_back(convertResult(wres));
			}
		}
	}

	if (pItemArray) {
		pItemArray->Release();
	}

	pFileDialog->Release();
	CoUninitialize();

	return filenames;
}

std::string SystemDialog::getDirectory(const char* path)
{
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | 
		COINIT_DISABLE_OLE1DDE);

	if (!SUCCEEDED(hr))
		return "";

	IFileDialog* pFileDialog;

	// Create the FileOpenDialog object
	hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL,
		IID_IFileOpenDialog, reinterpret_cast<void**>(&pFileDialog));

	// Specify directory selectin
	if (SUCCEEDED(hr)) {
		DWORD dwOptions;
		hr = pFileDialog->GetOptions(&dwOptions);

		if (SUCCEEDED(hr))
		{
			hr = pFileDialog->SetOptions(dwOptions | FOS_PICKFOLDERS);
		}
	}

	// Show the Open dialog box
	if (SUCCEEDED(hr)) {
		hr = pFileDialog->Show(NULL);
	}

	// Get the file name from the dialog box
	IShellItem* pItem = nullptr;
	if (SUCCEEDED(hr)) {
		hr = pFileDialog->GetResult(&pItem);
	}

	PWSTR pszFilePath;
	if (SUCCEEDED(hr)) {
		hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);
	}

	if (pItem) {
		pItem->Release();
	}

	pFileDialog->Release();
	CoUninitialize();

	if (SUCCEEDED(hr)) {
		std::wstring wres = pszFilePath;
		return convertResult(wres);
	}

	return "";
}
