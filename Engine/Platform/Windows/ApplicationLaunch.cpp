#include "Platform/ApplicationLaunch.hpp"
#include "Utils/Log.hpp"
#include <filesystem>
#include <Windows.h>

namespace fs = std::filesystem;
bool LaunchApplication::openFolder(const std::string& path)
{
	fs::path fspath(path);
	if (!fs::exists(fspath)) {
		Log::error("Failed to open directory, path not found");
		return false;
	}

	CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	ShellExecute(NULL, "open", fs::canonical(fspath).string().c_str(), NULL, NULL, SW_SHOWDEFAULT);
	return true;
}
