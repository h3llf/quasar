#ifndef QUASAR_APPLICATION_LAUNCH_H
#define QUASAR_APPLICATION_LAUNCH_H

#include <string>

class LaunchApplication
{
public:
	// Opens a folder using the default file browser application
	static bool openFolder(const std::string& path);
};

#endif //QUASAR_APPLICATION_LAUNCH_H