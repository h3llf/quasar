#include "Platform/ApplicationLaunch.hpp"
#include "Utils/Log.hpp"
#include <filesystem>

namespace fs = std::filesystem;
bool LaunchApplication::openFolder(const std::string& path)
{
	fs::path fspath(path);
	if (!fs::exists(fspath)) {
		Log::error("Failed to open directory, path not found");
		return false;
	}

	std::string command = "xdg-open \"" + fspath.string() + "\"&";
	system(command.c_str());

	return true;
}
