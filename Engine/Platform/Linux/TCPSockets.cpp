#include "TCPSockets.hpp"
#include <cstring>
#include <fcntl.h>
#include <errno.h>
#include <utility>

bool ServerConnection::startServerTCP(uint16_t port)
{
	struct sockaddr_in address;
	int addrlen = sizeof(address);
	
	memset(&address, 0, addrlen);

	socketfd = socket(AF_INET, SOCK_STREAM, 0);

	if (socketfd == -1) {
		Log::error("TCP socket creation failed ", strerror(errno));
		return false;
	}

	// Make socket non blocking
	fcntl(socketfd, F_SETFL, O_NONBLOCK);
	const int enable = 1;
	if (setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) {
		Log::error("Failed to set socket SO_REUSEADDR");
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port); // Sets byte order

	if (bind(socketfd, (struct sockaddr*) &address, addrlen) < 0) {
		Log::error("Failed to bind TCP socket ", strerror(errno));
	}

	if (listen(socketfd, 8) < 0) {
		Log::error("Listen failed", strerror(errno));
		return false;
	}

	Log::out("Created TCP socket");

	return true;
}

void CommonConnection::readMessageTCP(NetworkReceiveType& received)
{
	struct pollfd clientPoll;
	memset(&clientPoll, 0, sizeof(pollfd));
	clientPoll.events = POLLIN;
	clientPoll.fd = received.socketfd;

	// TODO: Check for the possibility of a infinate loop if read doesn't clear an event/error
	int pollRes;
	while ((pollRes = poll(&clientPoll, 1, 0)) == 1) {
		if (clientPoll.revents & POLLIN) {

			// Allocate memory
			if (received.data.size() <= received.dataSizes.size()) {
				received.data.resize(received.dataSizes.size() + 1);
			}

			std::vector<uint8_t>& dst = received.data.at(received.dataSizes.size());

			// Get message details
			MessageHeader header;
			if (read(received.socketfd, &header, sizeof(MessageHeader)) == sizeof(MessageHeader)) {
				dst.resize(header.size);
			} else {
				return;
			}

			// Read until the full message is received
			int readSize = 0;
			while (readSize < header.size) {
				readSize += read(received.socketfd, dst.data(), header.size - readSize);

				if (readSize == -1) {
					Log::error("Failed to read from socket ", strerror(errno));
					return;
				}
			}
			received.dataSizes.push(readSize);
		}
	}
}

void ServerConnection::receiveTCP()
{
	// Check for new connections
	int newSocketfd = accept(socketfd, (struct sockaddr*) &address, (socklen_t*) &addrlen);

	if (newSocketfd > -1) {
		// Get associated ip address
		struct sockaddr_in tmpAddr;
		getsockname(newSocketfd, (struct sockaddr*) &tmpAddr, (socklen_t*) &addrlen);
		char clientAddress[INET6_ADDRSTRLEN];
		strcpy(clientAddress, inet_ntoa(tmpAddr.sin_addr));

		uint16_t clientPort = htons(tmpAddr.sin_port);

		Log::out("Client connected on: ", clientAddress, ":", clientPort);
		
		socketList.push_back(NetworkReceiveType{newSocketfd});
	}

	// Poll clients for data
	for (NetworkReceiveType& receiveData : socketList) {
		CommonConnection::readMessageTCP(receiveData);
	}
}

void ClientConnection::receiveTCP()
{
	if (serverData.socketfd != -1) {
		CommonConnection::readMessageTCP(serverData);
	}
}

bool CommonConnection::sendMessageTCP(int socket, void *data, size_t size)
{
	if (socket == -1) {
		Log::error("Failed to send TCP message, invalid socket");
		return false;
	}

	MessageHeader header = {size};
	if (write(socket, &header, sizeof(MessageHeader)) != sizeof(MessageHeader)) {
		Log::error("Error sending message header");
		return false;
	}

	int writeSize = write(socket, data, size);
	return true;
}

void ServerConnection::shutdown()
{
	for (NetworkReceiveType& receiveData : socketList) {
		close(receiveData.socketfd);
	}

	close(socketfd);
}

bool ClientConnection::connectTCP(const char* serverIP, uint16_t port)
{
	serverData.socketfd = socket(AF_INET, SOCK_STREAM, 0);
	if (serverData.socketfd == -1) {
		Log::out("Cannot connect to server, socket creation failed", strerror(errno));
		return false;
	}

	memset(&serverAddress, 0, addrlen);

	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(port);

	//if (inet_pton(AF_INET, "192.168.1.182", &serverAddress.sin_addr) < 1) {
	if (inet_pton(AF_INET, serverIP, &serverAddress.sin_addr) < 1) {
		Log::error("Connection failed, invalid ip address", strerror(errno));
		return false;
	}

	if (connect(serverData.socketfd, (struct sockaddr*) &serverAddress, addrlen) < 0) {
		Log::error("TCP Connection failed ", strerror(errno));
		return false;
	}

	Log::out("Succesfully established TCP connection");

	return true;
}

void ClientConnection::shutdown()
{
	close(serverData.socketfd);
}
