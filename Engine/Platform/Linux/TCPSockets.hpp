#ifndef QUASAR_TCP_SOCKETS_H
#define QUASAR_TCP_SOCKETS_H

#include "Core/Types.hpp"
#include <queue>

#ifdef __linux__
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <poll.h>
#endif //__linux__

typedef int SocketType;
#define SocketInvalid -1

struct NetworkReceiveType
{
	int socketfd = -1;
	std::vector<std::vector<uint8_t>> data;
	std::queue<int> dataSizes;
};

class CommonConnection
{
public:
	static bool sendMessageTCP(int socket, void* data, size_t size);

	static void readMessageTCP(NetworkReceiveType& received);

private:
	struct MessageHeader
	{
		size_t size;
	};
};

class ServerConnection
{
public:
	bool startServerTCP(uint16_t port);
	void receiveTCP();

	void shutdown();


	int socketfd; 
	struct sockaddr_in address;
	size_t addrlen = sizeof(address);

	std::vector<NetworkReceiveType> socketList;
	std::queue<uint32> freeClients;

private:
};

class ClientConnection 
{
public:
	bool connectTCP(const char* serverIP, uint16_t port);
	void receiveTCP();

	void shutdown();

	struct sockaddr_in serverAddress;
	size_t addrlen = sizeof(serverAddress);

	NetworkReceiveType serverData;
};

#endif //QUASAR_TCP_SOCKETS_H
