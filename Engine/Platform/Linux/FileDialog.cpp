#include "Platform/FileDialog.hpp"

#include <iostream>
#include <filesystem>
#include <sstream>

namespace fs = std::filesystem;

char directoryBuffer[16384];

bool checkProgramInternal(const std::string& name) 
{
	const std::string cmd = "which " + name;
	int ret = system(cmd.c_str());
	int res = WEXITSTATUS(ret);

	return !res;
}

std::string getFileCmdInternal(const char* path, const char* filters, bool saveMode, bool multiple, bool directory)
{
	if (!checkProgramInternal("zenity")) {
		std::cout << "Failed to open file dialog. Make sure that zenity is installed" << std::endl;
		return "";
	}

	std::string command = "zenity --file-selection --filename=";

	if (!path) {
		command += fs::current_path().string() + '/';
	} else {
		command = command + path + '/';
	}

	if (filters)
		command = command + " --file-filter=\"" + filters + "\"";

	if (multiple)
		command += " --multiple";

	if (saveMode)
		command += " --save";

	if (directory)
		command += " --directory";

	return command;
}

std::string executeCommand(const std::string& command)
{
	directoryBuffer[0] = '\0';

	FILE* f = popen(command.c_str(), "r");
	fgets(directoryBuffer, sizeof(directoryBuffer), f);

	// fgets sets a data link escape character if no string is found
	if (directoryBuffer[0] == 0x10) {
		return "";
	}

	std::string fileRet = directoryBuffer;

	// Remove newline char
	if (!fileRet.empty() && fileRet[fileRet.length() - 1] == '\n') {
		fileRet.erase(fileRet.length() - 1);
	}

	return fileRet;
}

std::vector<std::string> splitPaths(const std::string& input, const std::string& splitStr)
{
	std::vector<std::string> pathBuf;

	size_t pos = std::string::npos;
	size_t start = 0;

	while (true) {
		pos = input.find(splitStr, start);

		std::string currentFile = input.substr(start, pos - start);
		pathBuf.push_back(currentFile);

		start = pos + 1;
		if (pos == std::string::npos)
			break;
	}

	return pathBuf;
}

std::string SystemDialog::getFile(const char *filters, bool saveMode, const char* path)
{
	std::string command = getFileCmdInternal(path, filters, saveMode, false, false);
	std::string fileRet = executeCommand(command);

	if (fileRet.empty()) {
		return std::string();
	}

	// Convert to relative path
	return fs::relative(fs::path(fileRet)).string();
}

std::vector<std::string> SystemDialog::getFileMulti(const char* filters, const char* path)
{
	std::string command = getFileCmdInternal(path, filters, false, true, false);
	std::string result = executeCommand(command);

	if (result.empty()) {
		return std::vector<std::string>();
	}

	std::vector<std::string> fileRet = splitPaths(result, "|");

	// Convert to relative paths
	for (std::string& file : fileRet) {
		file = fs::relative(fs::path(file)).string();
	}

	return fileRet;	
}

std::string SystemDialog::getDirectory(const char* path)
{
	std::string command = getFileCmdInternal(path, nullptr, false, false, true);
	std::string fileRet = executeCommand(command);
	return fs::relative(fs::path(fileRet)).string();
}
