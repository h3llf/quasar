# Quasar Engine

### About

Quasar is a work in progress 3D game engine made using Vulkan and C++20. It currently supports Windows and Linux.



### Disclaimer

Quasar is still in the prototyping phase and breaking changes are pushed regularly.
Many important features are missing and as such the engine shouldn't be considered ready for use.

### Short-term goals

-	**Testing:** Unit tests for core engine features (e.g. ECS & Reflection)
-	**Animation:** Skeletal animation with blending and IK
-	**Networking:** Authoritative servers with an easy to use API
-	**Render graph:** User programmable render pipelines & custom shaders
-	**Large worlds:** Floating origin with semi automatic chunking & multiple render layers
-	**2D Renderer:** Sprite & 2D font rendering with a focus on game UI
-	**Particle system:** Compute-based GPU particle systems

### Documentation

Documentation and example projects will be added once the engine is more feature complete.

### 3rd Party libraries
-	[PhysX 5.1](https://github.com/NVIDIA-Omniverse/PhysX)
-	[Vulkan memory allocator](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator)
-	[imgui](https://github.com/ocornut/imgui)
-	[ImGuizmo](https://github.com/CedricGuillemet/ImGuizmo)
-	[ImPlot](https://github.com/epezent/implot)
-	[json.hpp](https://github.com/nlohmann/json/blob/develop/single_include/nlohmann/json.hpp)
-	[stb libraries](https://github.com/nothings/stb)
-	[AudioFile](https://github.com/adamstark/AudioFile)
-	[GLFW](https://github.com/glfw/glfw)
-	[IconFontCppHeaders](https://github.com/juliettef/IconFontCppHeaders)
-	[msdf-atlas-gen](https://github.com/Chlumsky/msdf-atlas-gen)
-	[OpenAL Soft](https://github.com/kcat/openal-soft)

## Acknowledgements

-	[SaschaWillems vulkan samples](https://github.com/SaschaWillems/Vulkan)
-	https://vulkan-tutorial.com/
-	https://vkguide.dev/
-	https://learnopengl.com/
-	[Jump flood outline rendering](https://bgolus.medium.com/the-quest-for-very-wide-outlines-ba82ed442cd9)
-	Forward+ rendering:
	-	https://www.3dgep.com/forward-plus/#Forward
	-	https://github.com/WindyDarian/Vulkan-Forward-Plus-Renderer
	-	https://github.com/zimengyang/ForwardPlus_Vulkan
	-	https://github.com/bcrusco/Forward-Plus-Renderer
-	Ambient occlusion:
	-	https://www.activision.com/cdn/research/Practical_Real_Time_Strategies_for_Accurate_Indirect_Occlusion_NEW%20VERSION_COLOR.pdf
	-	https://github.com/GameTechDev/XeGTAO
-	Bloom
	-	http://advances.realtimerendering.com/s2014/#_NEXT_GENERATION_POST
