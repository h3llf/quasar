#ifndef SIMPLE_GLTF_H
#define SIMPLE_GLTF_H

#include <cstddef>
#include <cstdint>
#include <cstring>
#include <string>
#include <iostream>
#include <vector>
#include "json.hpp"

#include "Utils/Timer.hpp"

class simple_glTF
{
public:
	struct Material
	{
		int albedoIndex = -1;
		int PBRIndex = -1;
		int normalIndex = -1;
		int emissiveIndex = -1;

		float baseColourFactor[4] = {1.0f, 1.0f, 1.0f, 1.0f};
		float emissiveFactor[3] = {0.0f, 0.0f, 0.0f};
		float normalScale = 1.0;
		float metallicFactor = 1.0f;
		float roughnessFactor = 1.0f;

		bool doubleSided = false;
	};

	struct Primitive
	{
		int materialIndex = -1;

		// Accessor indices
		int position = -1;
		int normal = -1;
		int tangent = -1;
		int texCoord = -1;
		int indices = -1;

		// Optional
		int joints = -1;
		int weights = -1;

		// Morph targets
		std::vector<int> targets;
	};

	enum AnimationPath
	{
		PATH_TRANSLATION,
		PATH_ROTATION,
		PATH_SCALE,
		PATH_WEIGHTS
	};

	enum AnimationInterpolation
	{
		LINEAR,
		STEP,
		CUBICSPLINE
	};

	// Could be private
	struct Animation
	{
		std::string name = "unnamed animation";

		struct Channel
		{
			int sampler = -1;
			int targetNode = -1; // Joint node
			AnimationPath path = PATH_WEIGHTS;
		};
		std::vector<Channel> channels;

		struct Sampler
		{
			// Input and output accessors
			int input = -1; // Keyframe timesteps
			int output = -1; // Keyframe output vals
			AnimationInterpolation interpolation = LINEAR;
		};
		std::vector<Sampler> samplers;
	};
	
	struct Skin
	{
		// Node indices
		std::vector<int> nodeJoint;
		int skeletonNode = -1;

		// Accessor
		int inverseBindMatrices = -1;

		std::string name;
	};

	enum NodeType
	{
		NODE_EMPTY,
		NODE_MESH,
		NODE_SKINNED_MESH,
		NODE_JOINT
	};

	struct Node
	{
		NodeType type;
		int parent = -1;
		std::vector<int> childIndices;

		int meshIndex = -1;
		std::string name = "";
		
		float px = 0.0f;
		float py = 0.0f;
		float pz = 0.0f;

		float sx = 1.0f;
		float sy = 1.0f;
		float sz = 1.0f;

		float rx = 0.0f;
		float ry = 0.0f;
		float rz = 0.0f;
		float rw = 1.0f;

		float matrix[16];
		bool usesMatrix = false;
	};

	struct Mesh
	{
		std::vector<int> primitives;
	};

	simple_glTF();
	~simple_glTF();

	//Reads gltf json information and loads the raw binary data
	bool readModelInformation(std::string path, std::string fileName);

	// TODO: Add vertex layout for joints

	//Sets the vertex layout. Each parameter should be set to the needed offset within each vertex
	//An offset of -1 means the parameter is unused
	void setVertexLayout( int positionOffset, int normalOffset, 
			int tangentOffset, int textureOffset);

	//Fills vertex and index arrays with the geometry of a single primative
	void setMeshIndices(Primitive* primitive, std::vector<uint32_t>* indices);

	// Assumes column major ordering
	void setInverseBindMatrices(const Skin* skin, float* dataDst) {
		int indexCount = skin->nodeJoint.size() * 16;

		if (skin->inverseBindMatrices != - 1) {
			Accessor& invBindAccessor = accessors[skin->inverseBindMatrices];
			BufferView& invBindBuffer = bufferViews[invBindAccessor.bufferView];
			memcpy(dataDst, buffers[invBindBuffer.buffer].data(), indexCount * sizeof(float));
		} else {
			// Default to identity
			int offset = 0;
			for (int i = 0; i < indexCount; i++) {
				if (i % 16 == 0) {
					offset = 0;
				}

				if ((i % 4) - offset == 0) {
					*dataDst = 1.0f;
				} else {
					*dataDst = 0.0f;
				}
				
				if (i % 4 == 3) {
					offset++;
				}

				dataDst++;
			}
		}
	}

	template<typename T>
	bool setVertexJoints(Primitive* primitive, std::vector<T>* vertices)
	{
		// Data missing for this primitive
		// Can either be unsigned byte or unsigned short
		if (primitive->joints == -1 || primitive->weights == -1 || 
				jointOffsets.weights == -1 || jointOffsets.jointIndices == -1) {
			return false;
		}

		Accessor& jointAcessor = accessors[primitive->joints];
		BufferView& jointBuffer = bufferViews[jointAcessor.bufferView];
		vertices->resize(jointAcessor.count);

		uint8_t* jointPtr = buffers[jointBuffer.buffer].data() + 
			jointBuffer.byteOffset + jointAcessor.byteOffset;
		uint8_t* dstPtr = reinterpret_cast<uint8_t*>(vertices->data()) + jointOffsets.jointIndices;
		// TODO: Account for other data types
		if (jointAcessor.componentType == UNSIGNED_BYTE) {
			// Conversion from byte needed
			for (int i = 0; i < jointAcessor.count; ++i) {
				for (int j = 0; j < 4; ++j) {
					uint16_t joint16 = static_cast<uint16_t>(*jointPtr);
					*reinterpret_cast<uint16_t*>(dstPtr) = joint16;

					dstPtr += 2;
					jointPtr += 1;
				}
				dstPtr += sizeof(T) - 8;
			}
		} else if (jointAcessor.componentType == UNSIGNED_SHORT) {
			for (int i = 0; i < jointAcessor.count; ++i) {
				memcpy(dstPtr, jointPtr, 8);
				dstPtr += sizeof(T);
				jointPtr += 8;
			}
		} else {
			std::cout << "Unsupported joint data type" << std::endl;
			return false;
		}

		Accessor& weightAccessor = accessors[primitive->weights];
		BufferView weightBuffer = bufferViews[weightAccessor.bufferView];
		uint8_t* weightPtr = buffers[weightBuffer.buffer].data() + 
			weightBuffer.byteOffset + weightAccessor.byteOffset;
		dstPtr = reinterpret_cast<uint8_t*>(vertices->data()) + jointOffsets.weights;
		for (int i = 0; i < weightAccessor.count; ++i) {
			memcpy(dstPtr, weightPtr, 16);
			dstPtr += sizeof(T);
			weightPtr += 16;
		}

		return true;
	}

	template<typename T>
	void setMeshVertices(Primitive* primitive, std::vector<T>* vertices)
	{
		int vertCount = accessors[primitive->position].count;
		vertices->resize(vertCount);

		if (vertexOffsets.pos != -1 && primitive->position != -1) {
			Accessor& posAccessor = accessors[primitive->position];
			BufferView& posBuffer = bufferViews[posAccessor.bufferView];
			uint8_t*  posPtr = buffers[posBuffer.buffer].data() + 
				posBuffer.byteOffset + posAccessor.byteOffset;
			uint8_t* dstPtr = reinterpret_cast<uint8_t*>(vertices->data()) + vertexOffsets.pos;

			for (int i = 0; i < vertCount; ++i) {
				memcpy(dstPtr, posPtr, 12);
				dstPtr += sizeof(T);
				posPtr += 12;
			}
		}

		if (vertexOffsets.norm != -1 && primitive->normal!= -1) {
			Accessor& normAccessor = accessors[primitive->normal];
			BufferView& normBuffer = bufferViews[normAccessor.bufferView];
			uint8_t* normPtr = buffers[normBuffer.buffer].data() + 
				normBuffer.byteOffset + normAccessor.byteOffset;
			uint8_t* dstPtr = reinterpret_cast<uint8_t*>(vertices->data()) + vertexOffsets.norm;

			for (int i = 0; i < vertCount; ++i) {
				memcpy(dstPtr, normPtr, 12);
				dstPtr += sizeof(T);
				normPtr += 12;
			}
		}

		if (vertexOffsets.tex != -1 && primitive->texCoord != -1) {
			Accessor& texAccessor = accessors[primitive->texCoord];
			BufferView& texBuffer = bufferViews[texAccessor.bufferView];
			uint8_t* texPtr = buffers[texBuffer.buffer].data() + 
				texBuffer.byteOffset + texAccessor.byteOffset;
			uint8_t* dstPtr = reinterpret_cast<uint8_t*>(vertices->data()) + vertexOffsets.tex;

			for (int i = 0; i < vertCount; ++i) {
				memcpy(dstPtr, texPtr, 8);
				dstPtr += sizeof(T);
				texPtr += 8;
			}
			
		}

		if (vertexOffsets.tan != -1 && primitive->tangent != -1) {
			Accessor& tanAccessor = accessors[primitive->tangent];
			BufferView& tanBuffer = bufferViews[tanAccessor.bufferView];
			uint8_t* tanPtr = buffers[tanBuffer.buffer].data() + 
				tanBuffer.byteOffset + tanAccessor.byteOffset;
			uint8_t* dstPtr = reinterpret_cast<uint8_t*>(vertices->data()) + vertexOffsets.tan;

			for (int i = 0; i < vertCount; ++i) {
				memcpy(dstPtr, tanPtr, 16);
				dstPtr += sizeof(T);
				tanPtr += 16;
			}
		}
	}

	void setMeshBounds(Primitive* primitive, float min[3], float max[3])
	{
		Accessor& posAccessor = accessors[primitive->position];

		for (int i = 0; i < 3; ++i) {
			min[i] = posAccessor.min[i];
			max[i] = posAccessor.max[i];
		}
	}

	std::vector<std::string> textureNames;
	std::vector<std::vector<uint8_t>> buffers;
	std::vector<Primitive> primitives;
	std::vector<Material> materials;
	std::vector<Animation> animations;
	std::vector<Skin> skins;

	std::vector<std::vector<int>> meshPrimitives;

	std::vector<Node> sceneNodes;

private:
	void parseAccessors(nlohmann::json& json);
	void parseMaterials(nlohmann::json& json);
	void parseMeshes(nlohmann::json& json);
	void parseAnimations(nlohmann::json& json);
	void parseScene(nlohmann::json& json);
	void parseSkins(nlohmann::json& json);

	struct Accessor
	{
		uint32_t bufferView;
		uint32_t componentType;
		uint32_t count;
		uint32_t byteOffset;
		float min[4];
		float max[4];
		std::string type;
	};
	std::vector<Accessor> accessors;

	struct BufferView
	{
		uint32_t buffer;
		uint32_t byteLen;
		uint32_t byteOffset;
		uint32_t byteStride;
	};
	std::vector<BufferView> bufferViews;

	enum ComponentType {
		BYTE = 5120, UNSIGNED_BYTE = 5121, SHORT = 5122,
		UNSIGNED_SHORT = 5123, UNSIGNED_INT = 5125, FLOAT = 5126
	};


	struct vertexOffsets {
		int pos = 0;
		int norm = 12;
		int tan = 24;
		int tex = 40;
	} vertexOffsets;

	struct JointOffsets {
		int jointIndices = 0;
		int weights = 8;
	} jointOffsets;
};

#endif //SIMPLE_GLTF_H

