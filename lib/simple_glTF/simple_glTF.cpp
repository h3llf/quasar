#include "simple_glTF.hpp"
#include <filesystem>
#include <fstream>
#include <iostream>

simple_glTF::simple_glTF() {}

simple_glTF::~simple_glTF() {}

bool simple_glTF::readModelInformation(std::string path, std::string fileName)
{
	if (!std::filesystem::exists(path + fileName))
		return false;

	std::ifstream jsonFile(path + fileName, std::ios::in | std::ios::binary);
	nlohmann::json json;
	jsonFile >> json;
	jsonFile.close();

	//Reading buffer and buffer access information
	buffers.resize(json["buffers"].size());
	
	for (int i = 0; i < json["buffers"].size(); ++i) {
		buffers[i].resize(json["buffers"][i]["byteLength"]);

		std::string bufferPath = path + std::string(json["buffers"][i]["uri"]);

		std::ifstream input(bufferPath.c_str(),	std::ios::in | std::ios::binary);

		input.read((char*)buffers[i].data(), buffers[i].size());
		input.close();
	}

	bufferViews.reserve(json["bufferViews"].size());
	for (auto& bView : json["bufferViews"]) {

		uint32_t vBuffer = (bView.contains("buffer")) ? (uint32_t)bView["buffer"] : 0;
		uint32_t vByteLen = (uint32_t)bView["byteLength"]; // TODO: Check that this is required
		uint32_t vByteOffset = (bView.contains("byteOffset")) ? 
			(uint32_t)bView["byteOffset"] : 0;
		uint32_t vByteStride = (bView.contains("byteStride")) ? 
			(uint32_t)bView["byteStride"] : 0;

		bufferViews.push_back({vBuffer, vByteLen, vByteOffset, vByteStride});
	}

	parseAccessors(json);

	parseMaterials(json);

	parseMeshes(json);

	parseAnimations(json);

	parseScene(json);

	parseSkins(json);

	return true;
}

void simple_glTF::parseAccessors(nlohmann::json& json)
{
	accessors.reserve(json["accessors"].size());
	for (auto& access : json["accessors"]) {

		uint32_t aBufferView = access["bufferView"];
		uint32_t aComponentType = access["componentType"];
		uint32_t aCount = access["count"];
		uint32_t aByteOffset = (access.contains("byteOffset")) ? 
			(uint32_t)access["byteOffset"] : 0;
		std::string aType = access["type"];

		float min[4] = {};
		float max[4] = {};
		int valNum = 0;

		if (access.contains("max")) {
			if (aType == "SCALAR") {
				valNum = 1;
			} else if (aType == "VEC2") {
				valNum = 2;
			} else if (aType == "VEC3"){
				valNum = 3;
			} else {
				valNum = 4;
			}
	 
			for (int i = 0; i < valNum; ++i) {
				min[i] = access["min"][i];
				max[i] = access["max"][i];
			}
		}

		accessors.push_back({aBufferView, aComponentType, aCount, aByteOffset,
				{min[0], min[1], min[2], min[3]}, {max[0], max[1], max[2], max[3]},
				aType});
	}
}

void simple_glTF::parseMaterials(nlohmann::json& json)
{
	// Texture images (samplers currently ignored)
	textureNames.reserve(json["textures"].size());
	for (auto& texture : json["textures"]) {
		int i = texture["source"];
		textureNames.push_back(json["images"][i]["uri"]);
	}

	//Material data
	materials.reserve(json["materials"].size());
	for (auto& mat : json["materials"]) {
		Material material {};

		if (mat.contains("pbrMetallicRoughness")) {
			auto& tmpMat = mat["pbrMetallicRoughness"];

			if (tmpMat.contains("baseColorTexture")) {
				material.albedoIndex = tmpMat["baseColorTexture"]["index"];
			}

			if (tmpMat.contains("baseColorFactor")) {
				material.baseColourFactor[0] = tmpMat["baseColorFactor"][0];
				material.baseColourFactor[1] = tmpMat["baseColorFactor"][1];
				material.baseColourFactor[2] = tmpMat["baseColorFactor"][2];
				material.baseColourFactor[3] = tmpMat["baseColorFactor"][3];
			}
			
			if (tmpMat.contains("metallicRoughnessTexture")) {
				material.PBRIndex = tmpMat["metallicRoughnessTexture"]["index"];
			}


			if (tmpMat.contains("metallicFactor")) {
				material.metallicFactor = tmpMat["metallicFactor"];
			}

			if (tmpMat.contains("roughnessFactor")) {
				material.metallicFactor = tmpMat["roughnessFactor"];
			}
		}

		if (mat.contains("normalTexture")) {
			material.normalIndex = mat["normalTexture"]["index"];

			if (mat["normalTexture"].contains("scale")) {
				material.normalScale = mat["normalTexture"]["scale"];
			}
		}

		if (mat.contains("emissiveTexture")) {
			material.emissiveIndex = mat["emissiveTexture"]["index"];
		}


		if (mat.contains("emissiveFactor")) {
			material.emissiveFactor[0] = mat["emissiveFactor"][0];
			material.emissiveFactor[1] = mat["emissiveFactor"][1];
			material.emissiveFactor[2] = mat["emissiveFactor"][2];
		}

		if (mat.contains("doubleSided")) {
			material.doubleSided = mat["doubleSided"];
		}

		if (mat.contains("extensions")) {
			auto& ext = mat["extensions"];

			if (ext.contains("KHR_materials_emissive_strength")) {
				if (ext["KHR_materials_emissive_strength"].contains("emissiveStrength")) {
					float s = ext["KHR_materials_emissive_strength"]["emissiveStrength"];
					material.emissiveFactor[0] *= s;
					material.emissiveFactor[1] *= s;
					material.emissiveFactor[2] *= s;
				}
			}
		}
		materials.push_back(material);
	}
}

void simple_glTF::parseMeshes(nlohmann::json& json)
{
	//Meshes
	meshPrimitives.reserve(json["meshes"].size());
	for (int i = 0; i < json["meshes"].size(); ++i) {
		meshPrimitives.push_back(std::vector<int> {});

		primitives.reserve(json["meshes"][i]["primitives"].size());
		for (auto& prim : json["meshes"][i]["primitives"]) {
			Primitive primitive;
			primitive.indices = prim["indices"];
			primitive.materialIndex = prim["material"];

			auto& attrib = prim["attributes"];

			primitive.position = attrib["POSITION"];


			if (attrib.contains("NORMAL")) {
				primitive.normal = attrib["NORMAL"];
			}

			if (attrib.contains("TEXCOORD_0")) {
				primitive.texCoord = attrib["TEXCOORD_0"];
			}

			if (attrib.contains("TANGENT")) {
				primitive.tangent = attrib["TANGENT"];
			}

			if (attrib.contains("JOINTS_0")) {
				primitive.joints = attrib["JOINTS_0"];
			}

			if (attrib.contains("WEIGHTS_0")) {
				primitive.weights = attrib["WEIGHTS_0"];
			}

			meshPrimitives.back().push_back(primitives.size());
			primitives.push_back(primitive);
		}
	}


}

void simple_glTF::parseAnimations(nlohmann::json& json)
{
	if (!json.contains("animations")) {
		return;
	}

	for (const auto& a : json["animations"]) {
		Animation animation {};
		if (a.contains("name")) {
			animation.name = a["name"];
		}

		for (const auto& ch : a["channels"]) {
			Animation::Channel channel;

			channel.sampler = ch["sampler"];

			const auto& t = ch["target"];
			if (!t.contains("node")) {
				std::cout << "Warning: target node not found in channel" << std::endl;
				continue;
			}
			channel.targetNode = t["node"];

			const std::string path = t["path"];
			if (path == "translation") {
				channel.path = PATH_TRANSLATION;
			} else if (path == "rotation") {
				channel.path = PATH_ROTATION;
			} else if (path == "sclae") {
				channel.path = PATH_SCALE;
			} else if (path == "weights") {
				channel.path = PATH_WEIGHTS;
			}

			animation.channels.push_back(channel);
		}

		for (const auto& s : a["samplers"]) {
			Animation::Sampler sampler;
			
			sampler.input = s["input"];
			sampler.output = s["output"];

			if (s.contains("interpolation")) {
				const std::string inter = s["interpolation"];

				if (inter == "LINEAR") {
					sampler.interpolation = LINEAR;
				} else if (inter == "STEP") {
					sampler.interpolation = STEP;
				} else if (inter == "CUBICSPLINE") {
					sampler.interpolation = CUBICSPLINE;
				}
			}
				
			animation.samplers.push_back(sampler);
		}

		animations.push_back(animation);
	}
}

void simple_glTF::parseScene(nlohmann::json& json)
{
	// Scene
	sceneNodes.resize(json["nodes"].size());
	for (int i = 0; i < json["nodes"].size(); ++i) {
		auto node = json["nodes"][i];
		Node& instance = sceneNodes[i];

		if (node.contains("mesh")) {
			instance.meshIndex = node["mesh"];
			if (node.contains("skin")) {
				instance.type = NODE_SKINNED_MESH;
			} else {
				instance.type = NODE_MESH;
			}
		}

		if (node.contains("name")) {
			instance.name = node["name"];
		}

		if (node.contains("children")) {
			for (int child : node["children"]) {
				instance.childIndices.push_back(child);
				sceneNodes[child].parent = i;
			}
		}
			
		if (!node.contains("matrix")) {
			if (node.contains("translation")) {
				instance.px = node["translation"][0];
				instance.py = node["translation"][1];
				instance.pz = node["translation"][2];
			} else {
				instance.px = 0;
				instance.py = 0;
				instance.pz = 0;
			}

			if (node.contains("rotation")) {
				instance.rx = node["rotation"][0];
				instance.ry = node["rotation"][1];
				instance.rz = node["rotation"][2];
				instance.rw = node["rotation"][3];
			} else {
				instance.rx = 0;
				instance.ry = 0;
				instance.rz = 0;
				instance.rw = 1.0;
			}

			if (node.contains("scale")) {
				instance.sx = node["scale"][0];
				instance.sy = node["scale"][1];
				instance.sz = node["scale"][2];
			} else {
				instance.sx = 1;
				instance.sy = 1;
				instance.sz = 1;
			}

			instance.usesMatrix = false;
		} else {
			for (int i = 0; i < 16; ++i) {
				instance.matrix[i] = node["matrix"][i];
			}

			instance.usesMatrix = true;
		}
	}
}

void simple_glTF::parseSkins(nlohmann::json& json)
{
	if (!json.contains("skins")) {
		return;
	}

	for (const auto& s : json["skins"]) {
		Skin skin {};

		if (s.contains("name")) {
			skin.name = s["name"];
		}
		
		if (s.contains("inverseBindMatrices")) {
			skin.inverseBindMatrices = s["inverseBindMatrices"];
		}

		if (s.contains("skeleton")) {
			skin.skeletonNode = s["skeleton"];
		}

		for (int joint : s["joints"]) {
			skin.nodeJoint.push_back(joint);
		}

		skins.push_back(skin);
	}
}

void simple_glTF::setVertexLayout( int positionOffset, int normalOffset, 
		int tangentOffset, int textureOffset)
{
	vertexOffsets.pos = positionOffset;
	vertexOffsets.norm = normalOffset;
	vertexOffsets.tan = tangentOffset;
	vertexOffsets.tex = textureOffset;
}

//Fills vertex and index arrays with the geometry of a single primative
void simple_glTF::setMeshIndices(Primitive* primitive, std::vector<uint32_t>* indices)
{
	Accessor& indicesAccessor = accessors[primitive->indices];
	BufferView& indicesBuffer = bufferViews[indicesAccessor.bufferView];

	void* srcPtr = buffers[indicesBuffer.buffer].data() + 
		indicesAccessor.byteOffset + indicesBuffer.byteOffset;
	int indexCount = indicesAccessor.count;
	indices->resize(indexCount);

	//Indices convert to uint32 if needed
	if (indicesAccessor.componentType == ComponentType::UNSIGNED_SHORT) {
		uint16_t* tmpPtr = reinterpret_cast<uint16_t*>(srcPtr);

		for (int i = 0; i < indexCount; ++i) {
			indices->at(i) = tmpPtr[i];
		}
	} else if (indicesAccessor.componentType == ComponentType::UNSIGNED_BYTE) {
		uint8_t* tmpPtr = reinterpret_cast<uint8_t*>(srcPtr);

		for (int i = 0; i < indexCount; ++i) {
			indices->at(i) = tmpPtr[i];
		}
			
	} else if (indicesAccessor.componentType == ComponentType::UNSIGNED_INT) {
		memcpy(indices->data(), srcPtr, indicesAccessor.count * sizeof(uint32_t));
	} else {
		//Shouldn't happen anyway
		std::cout << "Unsuppoerted index type " << indicesAccessor.componentType << std::endl;
		return;
	}
}
