# github.com/M-Fatah/hot_reloading_with_cmake

macro(RANDOMIZE_PDB_NAME target)
    string(RANDOM random_string)
    set(pdbname "${target}_")
    string(APPEND pdbname ${random_string})
    set_target_properties(${target}
        PROPERTIES
        COMPILE_PDB_NAME ${pdbname}
        PDB_NAME ${pdbname}
    )
endmacro()
#
#RANDOMIZE_PDB_NAME(${TARGET_NAME})