set(CMAKE_CXX_STANDARD 20)

# TODO: Add separate bin directories for debug and release mode
if (${CMAKE_SYSTEM_NAME} MATCHES "Linux" )
	set(CMAKE_BUILD_TYPE Debug)
	set(CMAKE_CXX_FLAGS_RELEASE "-Ofast -mf16c -mavx -DNDEBUG")
	set(CMAKE_CXX_FLAGS_DEBUG "-O0 -ggdb -mf16c -D_DEBUG -Wall -Wno-pragmas -Wno-unused-function -Wno-unused-variable -Wno-switch -Wno-sign-compare -fno-gnu-unique")
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
	# Keep output directory consistant with linux
	set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build")
	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build")
	set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/build")
	set(CMAKE_BINARY_DIR "${CMAKE_SOURCE_DIR}/build")
	set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)
	message("VULKAN_SDK = $ENV{VULKAN_SDK}")
else()
	message("Unsupported platform: " ${CMAKE_SYSTEM_NAME})
endif()

#code completion
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

