
include_directories(${QUASAR_RELATIVE_DIR}Engine)

#VMA
include_directories(${QUASAR_RELATIVE_DIR}lib/VulkanMemoryAllocator/include)

#Various libs
include_directories(${QUASAR_RELATIVE_DIR}lib)

#PhysX
include_directories(${QUASAR_RELATIVE_DIR}lib/physx/include)
include_directories(${QUASAR_RELATIVE_DIR}lib/physx/source/physxextensions/src)
include_directories(${QUASAR_RELATIVE_DIR}lib/physx/source/physxgpuextensions/src)

include_directories(${QUASAR_RELATIVE_DIR}Engine/Platform)

if (${CMAKE_SYSTEM_NAME} MATCHES "Linux" )
	include_directories(${QUASAR_RELATIVE_DIR}Engine/Platform/Linux)
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Windows")

	#Vulkan windows
	include_directories($ENV{VULKAN_SDK}/Include)
	#Windows specific platform
	include_directories(${QUASAR_RELATIVE_DIR}Engine/Platform/Windows)
endif()

#GLFW
include_directories(${QUASAR_RELATIVE_DIR}lib/glfw/include)

#ImGui, ImGuizmo & implot
include_directories(${QUASAR_RELATIVE_DIR}lib/imgui)
include_directories(${QUASAR_RELATIVE_DIR}lib/ImGuizmo)
include_directories(${QUASAR_RELATIVE_DIR}lib/implot)
