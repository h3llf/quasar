#include "Launcher.hpp"
#include "Core/Application.hpp"

using namespace quasar;
QuasarApp application;

typedef void (*GameInitFn)(ECS* clientECS, ECS* serverECS);

int main()
{
	application.init();
	application.startServerThread();
	application.startGameLoop();
	application.cleanup();

	return 0;
}
